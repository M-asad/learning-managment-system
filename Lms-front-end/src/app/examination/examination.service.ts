import { Injectable } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Injectable()
export class ExaminationService {
  constructor (private fb: FormBuilder) {
  }

  examDetailForm() {
    return this.fb.group({
      examType: this.fb.group({
        autoComplete: [null, Validators.required]
      }),
      classes: [null, Validators.required],
      range: [null, Validators.required]
    });
  }
}
