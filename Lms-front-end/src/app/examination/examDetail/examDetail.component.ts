import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../../common/data.service';
import { ExaminationService } from '../examination.service'
import { Observable } from 'rxjs/Rx';
import { Subscription } from 'rxjs';


@Component({
  selector: 'exam-detail',
  templateUrl: './examDetail.component.html'
})

export class ExamDetailComponent implements OnInit {
  public examDetailForm: FormGroup;
  private examId: string;
  public examDetail: any;
  public isEdit = false;
  public busy: Subscription;
  public examDetailEdit: any = {
    description: '',
    classes: [],
    examType: {},
    rangeDates: []
  };
  private classes: any[] = [];
  private examType;
  private classOptions;
  private isTimeTableEdit = false;
  private timeTableEdit: any = {};
  private courseOptions: any;
  private updateTimeTableIndexs: number[] = [];
  public updatedTimeTable: any = {};
  public showCoursesDropDown = false;
  constructor(
    private dataService: DataService,
    private route: ActivatedRoute,
    private router: Router,
    private examService: ExaminationService
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.examId = params['id'];
    });
    this.examDetailForm = this.examService.examDetailForm();
    if (this.examId) {
      this.busy = this.dataService.getExamDetail(this.examId)
        .subscribe(res => {
          this.examDetail = res;
          this.examDetailEdit = {
            description: res.description,
            classes: this.formatClassdropDown(res.classes, 'text'),
            examType: {
              type: res.examType,
              examTypeID: res.examTypeID
            },
            rangeDates: [new Date(res.startDate), new Date(res.endDate)]
          };

          this.timeTableEdit = res.timeTable;
        });
    }
    else {
      this.editExam();
      this.isEdit = true;
    }
  }

  convertStringToDate(date) {
    return new Date(date);
  }

  editExam() {
    const dropDowns = [
      this.dataService.getExamTypes(),
      this.dataService.getExamClasses()
    ];
    this.busy = Observable.forkJoin(dropDowns)
      .subscribe(res => {
        this.examType = res[0];
        this.classOptions = this.formatClassdropDown(res[1], 'text');
        this.isEdit = true;
      });
  }

  formatClassdropDown(classes, title) {
    const classOptions = [];
    classes.forEach(item => {
      classOptions.push({
        name: item[title],
        code: item.id
      });
    });
    return classOptions;
  }

  updateExamType(examType) {
    this.examDetailEdit.examType = examType;
  }

  cancelEdit() {
    this.examDetailEdit = {
      description: this.examDetail.description,
      classes: this.examDetail.classNames,
      examType: {
        type: this.examDetail.examType
      },
      rangeDates: [new Date(this.examDetail.startDate), new Date(this.examDetail.endDate)]
    };
    this.isEdit = false;
  }

  updateExam() {
    const examData = {
      examTypeId: this.examDetailEdit.examType.id,
      examID: parseInt(this.examId) || 0,
      description: this.examDetailEdit.description,
      startDate: this.examDetailForm.controls.range.value[0],
      endDate: this.examDetailForm.controls.range.value[1],
      classIDs: []
    };
    this.examDetailEdit.classes.forEach(classes => {
      examData.classIDs.push(classes.code);
    });
    this.busy = this.dataService.createExam(examData)
      .subscribe(res => {
        this.examId = res.examID;
       this.examDetail = res;
        this.timeTableEdit = res.timeTable;
        this.isEdit = false;
      });
  }

  editTable() {
    this.busy = this.dataService.getCourses()
      .subscribe(res => {
        this.courseOptions = res;
        this.isTimeTableEdit = true;
      });
  }

  setTableIndex(timeTableIndex, classIndex) {
    this.updateTimeTableIndexs[0] = timeTableIndex;
    this.updateTimeTableIndexs[1] = classIndex;
    this.updatedTimeTable = JSON.parse(JSON.stringify(this.timeTableEdit[timeTableIndex].details[classIndex]));
    this.updatedTimeTable.date = this.timeTableEdit[timeTableIndex].date;
    this.updatedTimeTable.title = this.updatedTimeTable.course;
    this.updatedTimeTable.examID = parseInt(this.examId);
    setTimeout(() => {
      this.showCoursesDropDown = true;
    }, 100);
  }

  updateCourse(course) {
    this.updatedTimeTable.course = course.title;
    this.updatedTimeTable.courseId = course.id;
  }

  updateTimeTable() {
    this.updatedTimeTable.examID = this.examId;
    this.busy = this.dataService.saveExamDetail(this.updatedTimeTable)
    .subscribe(res => {
      this.updatedTimeTable.examDetailID = res;
      this.timeTableEdit[this.updateTimeTableIndexs[0]].details[this.updateTimeTableIndexs[1]] =
      JSON.parse(JSON.stringify(this.updatedTimeTable));
      this.showCoursesDropDown = false;
    });
  }

  closeModal() {
    this.updatedTimeTable = {};
    this.showCoursesDropDown = false;
  }
}
