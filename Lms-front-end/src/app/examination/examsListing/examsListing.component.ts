import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../../common/data.service';
import { Subscription } from 'rxjs';
import { LocalStorageService } from '../../common/localstorage.service';

@Component({
    selector: 'exams-listing',
    templateUrl: './examsListing.component.html'
})

export class ExamsListingComponent implements OnInit {
    public gridData: any;
    private pageNo = 1;
    public query = '';
    public showGrid = false;
    public busy: Subscription;
    private navigatedFrom: string;
    private role: string;
    public gridAction = [{
      url: this.checkFromNavigation() ? '/exam' : '/result',
      icon: 'icon-edit'
    }];

    constructor(
        private dataService: DataService,
        private localStorage: LocalStorageService,
        private router: Router) {
    }

    ngOnInit() {
        this.navigatedFrom = this.router.url.split('/')[1];
        this.role = this.localStorage.map('accessToken').role_name;
        this.busy = this.dataService.getExamsListing(this.query, this.pageNo)
            .subscribe(res => {
                this.gridData = {
                    content: res.data,
                    viewUrl: this.checkFromNavigation() ? '/exam' : this.checkPermission() ? '/result/class' : '/result',
                    pageCount: res.pageCount,
                    listingMethod: 'getExamsListing'
                };
                this.showGrid = true;
            });
    }

    checkPermission() {
        return this.role !== 'parent';
    }

    searchExams() {
        this.pageNo = 1;
        this.showGrid = false;
        this.busy = this.dataService.getExamsListing(this.query, this.pageNo)
            .subscribe(res => {
                this.gridData.content = res.data;
                this.gridData.pageCount = res.pageCount;
                this.showGrid = true;
            });
    }

    examDetail() {

        const route = this.checkFromNavigation() ? '/exam/add' : '/result/create';
        this.router.navigate([route]);
    }

    checkFromNavigation() {
        return this.navigatedFrom === 'exam';
    }

}
