import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {ExamDetailComponent} from './examDetail/examDetail.component';
import {ExamsListingComponent} from './examsListing/examsListing.component';


const routes: Routes = [
  {
    path: '',
    component: ExamsListingComponent
  },
  {
    path: 'add',
    component: ExamDetailComponent
  },
  {
    path: ':id',
    component: ExamDetailComponent
  },
];

export const examRouting: ModuleWithProviders = RouterModule.forChild(routes);
