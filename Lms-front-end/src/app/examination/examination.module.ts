import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WidgetsModule } from '../widgets/widgets.module';
import { BusyModule } from 'angular2-busy';
import { CalendarModule } from 'primeng/primeng';
import { MultiSelectModule } from 'primeng/primeng';

import { ExamDetailComponent } from './examDetail/examDetail.component';
import { ExaminationService } from './examination.service';
import { examRouting } from './exam.routing';
import { AppCommonModule } from '../common/common.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    WidgetsModule,
    BusyModule,
    CalendarModule,
    MultiSelectModule,
    AppCommonModule,
    examRouting
  ],
  declarations: [
    ExamDetailComponent
  ],
  providers: [
    ExaminationService
  ]
})
export class ExaminationModule { }
