import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {MessagingComponent} from './messaging.component';


const routes: Routes = [
  {
    path: '',
    component: MessagingComponent
  }
];

export const messageRouting: ModuleWithProviders = RouterModule.forChild(routes);
