import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { WidgetsModule} from '../widgets/widgets.module';

import { MessagingComponent } from './messaging.component';
import {messageRouting} from './messaging.routing';


@NgModule({
  imports: [
    CommonModule,
    WidgetsModule,
    FormsModule,
    RouterModule,
    messageRouting
  ],
  declarations: [
    MessagingComponent
  ]
})
export class MessagingModule { }
