import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './auth/login/login.component';
import { ForgotPasswordComponent } from './auth/forgotPassword/forgotPassword.component';
import { ResetPasswordComponent } from './auth/resetPassword/resetPassword.component';
import { ShellComponent } from './shell/shell.component';
import {TeacherModule} from './teachers/teachers.module';

const appRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'forgotPassword',
    component: ForgotPasswordComponent
  },
  {
    path: 'resetPassword',
    component: ResetPasswordComponent
  },
   {
    path: '',
    component: ShellComponent,
    children: [
      {
        path: '',
        redirectTo: '/class',
        pathMatch: 'full'
      },
      {
        path: 'class',
        loadChildren: 'app/classes/class.module#ClassModule',
      },
      {
        path: 'teacher',
        loadChildren: 'app/teachers/teachers.module#TeacherModule'
      },
      {
        path: 'student',
        loadChildren: 'app/students/students.module#StudentModule'
      },
      {
        path: 'parent',
        loadChildren: 'app/parents/parents.module#ParentModule'
      },
      {
        path: 'attendance',
        loadChildren: 'app/attendance/attendance.module#AttendanceModule'
      },
      {
        path: 'timeTable',
        loadChildren: 'app/timeTable/timeTable.module#TimeTableModule'
      },
      {
        path: 'exam',
        loadChildren: 'app/examination/examination.module#ExaminationModule'
      },
      {
        path: 'result',
        loadChildren: 'app/results/results.module#ResultModule'
      },
      {
        path: 'assignment',
        loadChildren: 'app/assignment/assignment.module#AssignmentModule'
      },
      {
        path: 'messaging',
        loadChildren: 'app/assignment/assignment.module#AssignmentModule'
      },
      {
        path: 'diary',
        loadChildren: 'app/diary/diary.module#DiaryModule'
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'login'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRouting {}
