import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {AllParentsComponent} from './allParents/allParents.component';
import {ParentDetailComponent} from './parentDetail/parentDetail.component';


const routes: Routes = [
  { path: '',
    component: AllParentsComponent,
  },
  { path: 'add',
    component: ParentDetailComponent
  },
  {
    path: ':id',
    component: ParentDetailComponent
  }
];

export const parentRouting: ModuleWithProviders = RouterModule.forChild(routes);
