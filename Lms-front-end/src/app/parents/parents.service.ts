import { Injectable } from '@angular/core';
import { DataService } from '../common/data.service';


@Injectable()
export class ParentsService {
  constructor (private dataService: DataService) {
  }

  getParentList(pageNo, searchKey) {
    return this.dataService.getParentsList(pageNo, searchKey);
  }

  getParent(id) {
    return this.dataService.getParent(id);
  }

  editParent(id:string,parentData: any) {
    return this.dataService.editParent(id, parentData);
  }

  createParent(parentData: any) {
    return this.dataService.createParent(parentData);
  }

  getEmptyParent() {
    return {
     fullName: null,
     address: null,
     phone: null,
     cell: null,
     email: null,
     cnic: null,
     gender: null
    };
  }
}
