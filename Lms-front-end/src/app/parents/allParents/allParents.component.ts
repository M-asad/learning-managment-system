'use strict';

import { Component, OnInit } from '@angular/core';
import { ParentsService } from '../parents.service';
import { Grid } from '../../widgets/grid/grid.interface';
import { Router, ActivatedRoute } from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'all-parent',
  templateUrl: './allParents.component.html'
})
export class AllParentsComponent implements OnInit {
  public gridData: Grid;
  public showGrid: boolean = false;
  private pageNo: string = '1';
  public searchKey: string = '';
  public busy: Subscription;

  constructor(private parentService: ParentsService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
   this.busy = this.parentService.getParentList(this.pageNo, this.searchKey)
     .subscribe(res => {
      if (res) {
        let mapData = {
          content : res.data,
          viewUrl: '/parent',
          pageCount: res.pageCount,
          listingMethod: 'getParentsList'
        };
        this.gridData = mapData;
        this.showGrid = true;
      }
    });
  }

  addParent() {
    this.router.navigate(['add'], {relativeTo: this.route});
  }

  searchParent() {
    if (this.searchKey) {
      this.pageNo = '1';
      this.showGrid = false;
      this.parentService.getParentList(this.pageNo, this.searchKey)
        .subscribe(res => {
          this.gridData.content = res.data;
          this.gridData.pageCount = res.pageCount;
          this.showGrid = true;
        })
    }
  }
}
