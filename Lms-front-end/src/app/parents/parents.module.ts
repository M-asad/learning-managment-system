import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { BusyModule } from 'angular2-busy';


import { AllParentsComponent } from './allParents/allParents.component';
import { ParentDetailComponent } from './parentDetail/parentDetail.component';

import { WidgetsModule} from '../widgets/widgets.module';

import { ParentsService } from './parents.service';
import {parentRouting} from './parent.routing';



@NgModule({
  imports: [
    CommonModule,
    WidgetsModule,
    FormsModule,
    BusyModule,
    TextMaskModule,
    parentRouting
  ],
  declarations: [
    AllParentsComponent,
    ParentDetailComponent
  ],
  providers: [
    ParentsService
  ]
})
export class ParentModule { }
