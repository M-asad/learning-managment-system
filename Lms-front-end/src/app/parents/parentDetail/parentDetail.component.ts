'use strict';

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ParentsService } from '../parents.service';
import { Subscription } from 'rxjs/Rx';

@Component({
  selector: 'parent-detail',
  templateUrl: './parentDetail.component.html',
  styleUrls: ['./parentDetail.scss'],
})
export class ParentDetailComponent implements OnInit {
  private parentId: string;
  private parentData: any;
  private editParentData: any;
  public isEdit = false;
  public showData = false;
  public studentGrid: any;
  private submitType = 'Save';
  private date = new Date();
  private mask = [/\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/];
  public busy: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private parentService: ParentsService
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.parentId = params['id'];
    });

    if (this.parentId) {
     this.busy = this.parentService.getParent(this.parentId).subscribe(res => {
        this.parentData = res;
        this.editParentData = Object.assign({}, res);
        this.studentGrid =  {
          content : res.students,
          viewUrl: '/student',
          pageCount: 0
        };
        this.showData = true;
      });
    }else {
      this.editParentData = this.parentService.getEmptyParent();
      this.isEdit = true;
      this.submitType = 'Create';
      this.showData = true;
    }
  }

  editParent(isValid) {
    if (isValid) {
      if (!this.parentId) {
       this.busy = this.parentService.createParent(this.editParentData).subscribe(res => {
          this.router.navigate(['/parent']);
        });
      } else {
       this.busy = this.parentService.editParent(this.editParentData.id, this.editParentData).subscribe(res => {
          this.router.navigate(['/parent']);
        });
      }
    }
  }
}
