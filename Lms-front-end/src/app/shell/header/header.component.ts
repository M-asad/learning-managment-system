import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { LocalStorageService } from '../../common/localstorage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
  constructor(
              private localStorage: LocalStorageService,
              private _router: Router) { }

  ngOnInit() {
  }

  logout() {
    this.localStorage.destroy('accessToken');
    this._router.navigate(['login']);
  }
}
