import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ShellComponent } from './shell.component';
import { HeaderComponent } from './header/header.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { CommonService } from '../common/common.service';
import { SelectModule } from 'ng2-select';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SelectModule
  ],
  declarations: [
    ShellComponent,
    HeaderComponent,
    SideMenuComponent
  ],
  providers: [
    CommonService
  ]
})
export class ShellModule { }
