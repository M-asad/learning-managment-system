import { Component, OnInit, HostListener } from '@angular/core';
import { Request, Headers } from '@angular/http';
import { Router, NavigationEnd, Event as NavigationEvent } from "@angular/router";

import { CommonService } from '../common/common.service';

import { LocalStorageService } from '../common/localstorage.service';


@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html'
})
export class ShellComponent implements OnInit {
  public isWindowLarge: boolean = window.innerWidth > 1024;
  constructor(
    private router: Router,
    private localStorage: LocalStorageService,
    private commonService: CommonService) {
  }

  ngOnInit() {
    this.router.events
      .subscribe((event: NavigationEvent) => {
       if (event instanceof NavigationEnd) {
        if (!this.localStorage.map('accessToken') && event.url !== '/login') {
          this.router.navigate(['/login']);
          return false;
        }
        else if (!this.commonService.checkRolePermission(event.url)) {
          this.router.navigate(['/login']);
          return false;
        }
       }
      });
  }


  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (event.target.innerWidth <= 1024) {
      this.isWindowLarge = false;
    }
    else {
      this.isWindowLarge = true;
    }
  }

}
