import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import * as _ from 'lodash';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html'
})
export class SideMenuComponent implements OnInit {

  constructor(private router: Router) { }
  private currentSubMenu: any = {
    title: ''
  };
  public sideMenus : any = [{
    title: 'Classes',
    isActive: false,
    icon: "icon-class",
    url: "/class"
  },{
    title: 'Student',
    isActive: false,
    icon: "icon-student",
    url: '/student'
  },{
    title: 'Teachers',
    isActive: false,
    icon: "icon-teacher",
    url: '/teacher'
  },{
    title: 'Parents',
    isActive: false,
    icon: "icon-parent",
    url: '/parent'
  },{
    title: 'Schedule',
    isActive: false,
    icon: "icon-schedule",
    url: '/timeTable'
  },{
    title: 'Time Table',
    isActive: false,
    icon: "icon-timetale",
    url: '/timeTable'
  },{
    title: 'Examination',
    isActive: false,
    icon: "icon-exam",
    url: '/exam'
  },{
    title: 'attendance',
    isActive: false,
    icon: "icon-attendance",
    url: '/attendance',
    subMenu: [{
      title: 'Student Attendance',
      icon: "",
      url: '/attendance/student',
      isChild: true
    },{
      title: 'Mark Attendance',
      icon: "",
      url: '/attendance/mark',
      isChild: true
    },{
      title: 'Class Attendance',
      icon: "",
      url: '/attendance/class',
      isChild: true
    }]
  },{
    title: 'Results',
    isActive: false,
    icon: "icon-result",
    url: '/result'
  },{
    title: 'Assignments',
    isActive: false,
    icon: "icon-assignment",
    url: '/assignment'
  },{
    title: 'Messaging',
    isActive: false,
    icon: "icon-message",
    url: '/messaging'
  },{
    title: 'Daily Diary',
    isActive: false,
    icon: "icon-diary",
    url: '/diary'
  }];

  ngOnInit() {
    this.sideMenus.forEach((item) => {
      if(item.url === this.router.url) {
        item.isActive = true;
      }
      if(item.subMenu) {
        item.subMenu.forEach(subItem => {
          if(subItem.url == this.router.url) {
            this.currentSubMenu = subItem;
            item.isActive = true;
          }
        })
      }
    })
  }

  changeActive(menu, parentIndex, index) {
   if(!menu.isChild) {
     this.sideMenus.forEach((menuItem : any) => {
       if (menuItem.title != menu.title) {
         menuItem.isActive = false
       }
       else{
         menuItem.isActive = true;
       }
     });
   }
    else {
     this.currentSubMenu = menu;
     this.sideMenus[parentIndex].subMenu[index].isActive = true;
   }
    if(!menu.subMenu) {
      this.router.navigate([menu.url]);
    }
  }
}
