import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TimeTableComponent } from './timeTable.component/timeTable.component';
import {timeTableRouting} from './timeTable.routing';


@NgModule({
  imports: [
    CommonModule,
    timeTableRouting
  ],
  declarations: [
    TimeTableComponent
  ]
})
export class TimeTableModule { }
