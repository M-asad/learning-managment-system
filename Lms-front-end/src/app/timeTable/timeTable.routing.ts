import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TimeTableComponent} from './timeTable.component/timeTable.component';




const routes: Routes = [
  { path: '',
    component: TimeTableComponent,
  }
];

export const timeTableRouting: ModuleWithProviders = RouterModule.forChild(routes);
