import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import {StudentDetailComponent} from './studentDetail/studentDetial.component';
import {AllStudentsComponent} from './allStudents/allStudent.component';


const routes: Routes = [
  { path: '',
    component: AllStudentsComponent,
  },
  { path: 'add',
    component: StudentDetailComponent
  },
  {
    path: ':id',
    component: StudentDetailComponent
  }
];

export const studentRouting: ModuleWithProviders = RouterModule.forChild(routes);
