import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AttendanceModule } from '../attendance/attendance.module';

import { StudentsService } from './students.service';
import { AllStudentsComponent } from './allStudents/allStudent.component';
import { WidgetsModule} from '../widgets/widgets.module';
import { BusyModule } from 'angular2-busy';
import { NKDatetimeModule } from 'ng2-datetime/ng2-datetime';
import { SelectModule } from 'ng2-select';

import { StudentDetailComponent} from './studentDetail/studentDetial.component';
import {studentRouting} from './student.routing';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    WidgetsModule,
    NKDatetimeModule,
    SelectModule,
    BusyModule,
    AttendanceModule,
    studentRouting
  ],
  declarations: [
    AllStudentsComponent,
    StudentDetailComponent
  ],
  providers: [
    StudentsService
  ]
})
export class StudentModule { }
