'use strict';

import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Grid } from '../../widgets/grid/grid.interface';
import { StudentsService } from '../students.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'all-students',
  templateUrl: './allStudent.component.html'
})
export class AllStudentsComponent implements OnInit {
  public gridData: Grid;
  public showGrid: boolean = false;
  private pageNo: string = '1';
  public searchKey: string = '';
  public busy: Subscription;
  constructor(private studentService: StudentsService,
              private router: Router) {
  }

  ngOnInit() {
    this.busy = this.studentService.getStudentsList(this.pageNo, this.searchKey).subscribe(res => {
      if (res) {
        this.gridData =  {
          content : res.data,
          viewUrl: '/student',
          pageCount: res.pageCount,
          listingMethod: 'getStudentsList'
        };
        this.showGrid = true;
      }
    });
  }

  addStudent() {
    this.router.navigate(['/student/add']);
  }

  searchStudent() {

    this.pageNo ='1';
    this.showGrid = false;
    this.studentService.getStudentsList(this.pageNo, this.searchKey)
      .subscribe(res => {
        this.gridData.content = res.data;
        this.gridData.pageCount = res.pageCount;
        this.showGrid = true;
      });
  }
}
