import { Injectable } from '@angular/core';
import { DataService } from '../common/data.service';


@Injectable()
export class StudentsService {
  constructor (private dataService: DataService) {
  }

  getStudentsList(pageNo, searchKey) {
    return this.dataService.getStudentsList(pageNo, searchKey);
  }

  getStudent(id) {
    return this.dataService.getStudent(id);
  }

  editStudent(id: string, studentData: any) {
    return this.dataService.editStudent(id, studentData);
  }

  createStudent(studentData: any) {
    return this.dataService.createStudent(studentData);
  }

  getEmptyStudent() {
    return {
      "grNo": null,
      "firstName": null,
      "fastName": null,
      "joinDate": new Date(),
      "birthDate": null,
      "birthPlace": null,
      "gender": null,
      "bloodGroup": null,
      "nationality": null,
      "religion": null,
      "leavingDate": null,
      "parentID": 0,
      "address": null,
      "phone": null,
      "picture": null
    }
  }
}
