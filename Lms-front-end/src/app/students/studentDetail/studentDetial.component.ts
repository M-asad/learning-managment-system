'use strict';

import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from '../../common/common.service';
import { StudentsService } from '../students.service';
import { AppConstants } from '../../common/app.constants';
import { Subscription } from 'rxjs';
import { DataService } from '../../common/data.service';

@Component({
  selector: 'students-detail',
  templateUrl: './studentDetail.component.html'
})
export class StudentDetailComponent implements OnInit {
  private studentId: string;
  private studentData: any;
  private bloodGroups: any[] = AppConstants.misc.bloodGroup;
  private editStudentData: any;
  public showData = false;
  private submitType = 'Save';
  private birthDatePicker: any;
  private selectedBloodGroup: any;
  private joinDatePicker: any;
  public isEdit = false;
  private profileImg: string = AppConstants.misc.defaultProfileImg;
  public busy: Subscription;
  private classes: Object[];
  private selectedClass;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private studentService: StudentsService,
    private commonService: CommonService,
    private dataService: DataService
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.studentId = params['id'];
    });

    this.busy = this.dataService.getClassroomOptions()
      .subscribe(res => {
        this.classes = res;
      });

    if (this.studentId) {
      this.busy = this.studentService.getStudent(this.studentId).subscribe(res => {
        this.studentData = res;
        this.editStudentData = Object.assign({}, res);
        this.editStudentData.birthDate = new Date(this.editStudentData.birthDate);
        this.editStudentData.joinDate = new Date(this.editStudentData.joinDate);
        this.selectedBloodGroup = { type: this.editStudentData.bloodGroup };
        this.profileImg = this.editStudentData.picture ? this.editStudentData.picture : AppConstants.misc.defaultProfileImg;
        if (res.parent) {
          this.editStudentData.parentID = res.parent.id;
          this.editStudentData.parent = {
            text: res.parent.fullName,
            id: res.parent.id
          };
        }
        this.showData = true;
      });
    } else {
      this.editStudentData = this.studentService.getEmptyStudent();
      this.submitType = 'Create';
      this.isEdit = true;
      this.showData = true;
    }
    this.birthDatePicker = this.commonService.getDatePickerOptions();
    this.joinDatePicker = this.commonService.getDatePickerOptions();
  }

  editStudent(isValid) {
    if (this.profileImg !== AppConstants.misc.defaultProfileImg) {
      this.editStudentData.picture = this.profileImg;
    }
    if (isValid) {
      if (!this.studentId) {
        this.busy = this.studentService.createStudent(this.editStudentData).subscribe(res => {
          this.router.navigate(['/student']);
        });
      } else {
        this.busy = this.studentService.editStudent(this.editStudentData.id, this.editStudentData).subscribe(res => {
          this.router.navigate(['/student']);
        });
      }
    }
  }

  updateClass(event) {
    if (event) {
      this.editStudentData.classroomID = event.classroomId;
    }
  }

  validateStart(value: Date) {
    this.joinDatePicker = this.commonService.getDatePickerOptions(value);
  }

  validateEnd(value: Date) {
    this.birthDatePicker = this.commonService.getDatePickerOptions(null, value);
  }

  updateParent(parent) {
    this.editStudentData.parentID = parent.id;
    this.editStudentData.parentName = parent.text;
  }

  updateBloodGroup(val) {
    this.editStudentData.bloodGroup = val.type;
  }

  updateImage(url) {
    this.profileImg = url;
  }

  cancel() {
    this.studentId ? this.isEdit = false : this.router.navigate(['student']);
  }
}
