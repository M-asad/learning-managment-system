import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {MarkAttendanceComponent} from './markAttendance/markAttendance.component';
import {StudentAttendanceComponent} from './studentAttendance/studentAttendance.component';
import {ClassAttendanceComponent} from './classAttendance/classAttendance.component';


const routes: Routes = [
  {
    path: 'class',
    component: ClassAttendanceComponent
  },
  {
    path: 'mark',
    component: MarkAttendanceComponent
  },
  {
    path: 'mark/:id',
    component: MarkAttendanceComponent
  },
  {
    path: 'student',
    component: StudentAttendanceComponent
  },
  {
    path: 'student/:id',
    component: StudentAttendanceComponent
  }
];

export const attendanceRouting: ModuleWithProviders = RouterModule.forChild(routes);
