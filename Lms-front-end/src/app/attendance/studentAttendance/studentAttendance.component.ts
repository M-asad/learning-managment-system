import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../../common/data.service';
import { CommonService } from '../../common/common.service';
import { Subscription } from 'rxjs';
import * as swal from 'sweetalert2';

@Component({
  selector: 'student-attendance',
  templateUrl: './studentAttendance.component.html'
})

export class StudentAttendanceComponent implements OnInit {
  @Input() isChild;
  private studentId: string;
  public showAttendance: boolean;
  public allowStudentSearch = true;
  public date = new Date();
  public AttendanceData: any;
  private studentList;
  public selectedStudent;
  public busy: Subscription;
  private dayOffset = [];
  private updatedAttendance: any;
  public isEdit = false;
  private monthFullName: string[] = ['January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dataService: DataService,
    private commonService: CommonService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params['id']) {
        this.studentId = params['id'];
        this.getAttendance();
        this.busy = this.dataService.getStudent(this.studentId)
          .subscribe(res => {
            this.selectedStudent = {
              studentID: res.studentID,
              grNo: res.grNo,
              name: `${res.firstName} ${res.lastName}`
            };

            this.updatedAttendance = {
              studentID: res.studentID,
              grNo: res.grNo,
              attendance: []
            };
            this.allowStudentSearch = false;
          });
      }
    });
  }

  getAttendance() {
    if (this.studentId && this.date) {
      this.busy = this.dataService.getStudentAttendance(this.studentId, this.date.getFullYear(), this.date.getMonth() + 1)
        .subscribe(res => {
          this.AttendanceData = res;
          this.dayOffset.length = new Date(res.attendance[0].date).getDay();
          this.showAttendance = true;
        });
    }
  }

  updateStudent(event) {
    if (event.text) {
      this.studentId = event.id;
      this.selectedStudent = {
        name: event.text,
        grNo: event.grNo,
        studentID: event.id
      };
      this.updatedAttendance = {
        studentID: event.id,
        grNo: event.grNo,
        attendance: []
      };
      this.getAttendance();
    }
  }

  updateMonth(event) {
    this.date = new Date(event);
    this.getAttendance();
  }

  getDateDay(date) {
    return new Date(date).getDate();
  }

  getDay(date) {
    return new Date(date).getDay();
  }

  changeAttendance(index, attendance) {
    if (this.isEdit) {
      const attendanceDay = new Date(attendance.date).getDay();
      if (attendanceDay !== 6 && attendanceDay !== 0 && attendance.date && this.compareCurrentDates(attendance.date)) {
        if (attendance.attendanceType === 'P') {
          this.AttendanceData.attendance[index].attendanceType = 'A';
        }
        else if (attendance.attendanceType === 'NA') {
          this.AttendanceData.attendance[index].attendanceType = 'P';
        }

        else if (attendance.attendanceType === 'A') {
          this.AttendanceData.attendance[index].attendanceType = 'NA';
        }
      }
      this.updatedAttendance.attendance.push({
        attendanceID: attendance.attendanceID,
        date: attendance.date,
        attendanceType: this.AttendanceData.attendance[index].attendanceType
      });
    }
  }

  saveAttendance() {
    this.busy = this.dataService.saveAttendance(this.updatedAttendance)
      .subscribe(res => {
        swal({
          title: 'Success',
          text: 'Attendance Update Sucessfully!',
          type: 'success',
          confirmButtonText: 'Ok'
        });
        this.isEdit = false;
      });
  }

  compareCurrentDates(attendanceDate) {
    attendanceDate = new Date(attendanceDate);
    const currentDate = new Date();
    if (attendanceDate <= currentDate) {
      return true;
    }
    return false;
  }
}
