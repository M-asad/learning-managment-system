import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { DataService } from '../../common/data.service';
import { CommonService } from '../../common/common.service';
import * as swal from 'sweetalert2';


@Component({
  selector: 'mark-attendance',
  templateUrl: './markAttendance.component.html'
})

export class MarkAttendanceComponent implements OnInit {

  private today: Date = new Date();
  private date: any = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate());
  private classId: string;
  public classList: any;
  public busy: Subscription;
  private classAttendance: any;
  public selectedClass: any;
  private datePickerOpts: any = this.commonService.getDatePickerOptions(null, this.date);
  public showAttendanceMarker = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService,
    private commonService: CommonService
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params['id']) {
        this.classId = params['id'];
        this.getClassInfo();
      }
    });

    this.busy = this.dataService.getClassesList()
    .subscribe(res => {
        this.classList = res;
        if (this.classId) {
          this.selectedClass = this.classList.find(item => (item.classroomID == this.classId));
        }
      });
  }

  getClassInfo() {
    if (this.classId && this.date) {
     this.busy = this.dataService.getMarkAttendance(this.classId, this.date.toISOString().split('T')[0])
        .subscribe(res => {
         this.classAttendance = res;
         this.showAttendanceMarker = true;
       });
    }
  }

  updateClass(event) {
    this.classId = event.classId;
    this.getClassInfo();
  }

  markAttendance(val, index) {
    this.classAttendance.studentAttendance[index].attendanceType = val !== 'P' ? 'P' : 'A';
  }

  saveAttendance() {
    this.dataService.markClassAttendance(this.classAttendance)
    .subscribe(res => {
        swal({
          title: 'Success',
          text: 'Attendance Update Successfully!',
          type: 'success',
          confirmButtonText: 'Ok'
        });
      });
  }
}
