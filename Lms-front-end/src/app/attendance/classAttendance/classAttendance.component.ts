import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../../common/data.service';
import { CommonService } from '../../common/common.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'class-attendance',
  templateUrl: './classAttendance.component.html'
})

export class ClassAttendanceComponent implements OnInit {
  private classroomId: string;
  public date: any;
  public classes: Object[];
  private selectedClass: string;
  public attendance: any;
  public datePicker: any = this.commonService.getDatePickerOptions(null, new Date().toISOString());
  public busy: Subscription;
  public selectedClassName: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService,
    private commonService: CommonService
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.classroomId = params['class'];
      this.date = params['date'];
    });

    this.busy = this.dataService.getClassesList()
      .subscribe(res => {
        this.classes = res;
      });

     this.updateAttendanceGrid();
  }

  updateClass(event) {
    if (event) {
      this.selectedClass = event.classId;
      this.selectedClassName = event.class;
      this.updateAttendanceGrid();
    }
    else {
      this.selectedClass = null;
      this.attendance = null;
    }
  }

  dayName(date) {
   const weekDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
   return weekDays[new Date(date).getDay()];
  }

  getDay(date) {
    return (new Date(date).getDate());
  }

  updateAttendanceGrid() {
   if (this.selectedClass && this.date) {
     this.busy = this.dataService.getClassAttendance(this.selectedClass, (new Date(this.date).toISOString()).split('T')[0])
       .subscribe(res => {
         this.attendance = res;
       });
   }
  }
}
