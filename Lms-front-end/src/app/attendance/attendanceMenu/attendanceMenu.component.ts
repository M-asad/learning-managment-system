import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'attendance-menu',
  templateUrl: './attendanceMenu.component.html'
})

export class AttendanceMenuComponent implements OnInit {

  public attendanceOptions = [{
    type: 'Class Attendance',
    url: '/class'
  },{
    type: 'Student Attendance',
    url: '/student'
  }];

  constructor(
    private router: Router
  ) {
  }

  ngOnInit() {}

  showAttendance(event) {
    if(event) {
      this.router.navigate([`/attendance/${event.url}`]);
    }
    else {
      this.router.navigate([`/attendance`]);
    }
  }

}
