import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { WidgetsModule} from '../widgets/widgets.module';
import { NKDatetimeModule } from 'ng2-datetime/ng2-datetime';
import { BusyModule } from 'angular2-busy';

import { ClassAttendanceComponent } from './classAttendance/classAttendance.component';
import { StudentAttendanceComponent} from './studentAttendance/studentAttendance.component';
import { AttendanceMenuComponent} from './attendanceMenu/attendanceMenu.component';
import { MarkAttendanceComponent } from './markAttendance/markAttendance.component';
import { attendanceRouting } from './attendance.routing';


@NgModule({
  imports: [
    CommonModule,
    WidgetsModule,
    FormsModule,
    RouterModule,
    NKDatetimeModule,
    BusyModule,
    attendanceRouting
  ],
  declarations: [
    ClassAttendanceComponent,
    AttendanceMenuComponent,
    StudentAttendanceComponent,
    MarkAttendanceComponent
  ],
  exports: [
    StudentAttendanceComponent
  ]
})
export class AttendanceModule { }
