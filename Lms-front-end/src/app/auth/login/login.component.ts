import { Component, OnInit } from '@angular/core';

import { Router } from "@angular/router";

import { AuthService } from '../auth.service';
import { LocalStorageService } from '../../common/localstorage.service'


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent {

  constructor(
    private authService: AuthService,
    private localStorage:LocalStorageService,
    private _router: Router
  ) {

  }
  public user: {
    username: string;
    password: string;
    grant_type: string} = {
    username : '',
    password : '',
    grant_type: 'password'
  };

  login(isValid: Boolean){
    if (isValid) {
      this.authService.login(this.user).subscribe(res => {
        this.localStorage.map('accessToken', res);
        this._router.navigate(['/class']);
      });
    }
  }

  forgotPassword() {
    this._router.navigate(['/forgotPassword']);
  }

}
