import { Injectable } from '@angular/core';
import { Observable }     from 'rxjs/Observable';


import { HttpService } from '../common/http.service';
import { AppConstants } from '../common/app.constants';

@Injectable()
export class AuthService {

    private _constants: any;
    public tenants = {data: <any>[]};

    constructor (
        private _httpService: HttpService
    ) {
        this._constants = AppConstants;
    }

    login (data: any): Observable<any> {
        return this._httpService.post(this._constants.appUrls.auth.login, data, false);
    }
}
