import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { DataService } from '../../common/data.service';
import { Subscription } from 'rxjs/Rx';
import { Grid } from '../../widgets/grid/grid.interface';
import { Router } from '@angular/router';

@Component ({
  selector: 'all-assignment',
  templateUrl: './allAssignment.component.html',
})

export class AllAssignmentComponent implements OnInit {
  @ViewChild('showModal') modal: ElementRef;
  selectedAssignment;
  public gridData: Grid;
  public showGrid: boolean = false;
  private pageNo: string = '1';
  public searchKey: string = '';
  public busy: Subscription;
  public gridAction = [{
    url: 'assignment',
    icon: 'icon-eye'
  },
   {
    event: 'edit',
    icon: 'icon-edit'
  },
    {
    event: 'remove',
    icon: 'icon-delete'
  }];

  constructor (private dataService: DataService,
              private router: Router) {
  }

  ngOnInit() {
    this.busy = this.dataService.getAssignments(this.pageNo, this.searchKey)
      .subscribe(res => {
        if (res) {
          this.gridData =  {
            content : res.data,
            viewUrl: '/assignment',
            pageCount: res.pageCount,
            listingMethod: 'getAssignments'
          };
          this.showGrid = true;
        }
        console.log(res);
      });
  }

  eventHandler(val) {
    if(val.event === 'remove') {
      this.removeAssigment(val);
    }
    else {
      this.editAssignment(val);
    }
  }

  removeAssigment(item) {
    return this.dataService.removeAssignment(item.id)
      .subscribe(res => {
        this.gridData = res;
      });
  }

  editAssignment(item) {
    this.router.navigate(['assignment', item.id, {isEdit: true}]);
  }

  updateAssignmentList() {
    this.busy = this.dataService.getAssignments(this.pageNo, this.searchKey)
      .subscribe(res => {
        if (res) {
          this.gridData.content = res.data;
        }
      });
  }
}
