import {Component, Input, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {DataService} from '../../common/data.service';
import { Subscription } from 'rxjs';

@Component ({
  selector: 'assignment-Detail',
  templateUrl: './assignmentDetail.component.html',
})

export class AssignmentDetailComponent implements OnInit {
  assignmentId: string;
  assignmentDetail: any;
  busy: Subscription;
  constructor (private dataService: DataService,
              private router: Router,
              private route: ActivatedRoute){
  }
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.assignmentId = params['id'];
    });

    this.busy = this.dataService.getAssignment(this.assignmentId)
      .subscribe(res => {
        this.assignmentDetail = res;
      });
    }

  updateAssignment(assignment) {
    this.assignmentDetail = assignment;
  }
}
