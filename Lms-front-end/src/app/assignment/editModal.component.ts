import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import { DataService } from '../common/data.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import {CommonService} from '../common/common.service';
import * as swal from 'sweetalert2';

@Component ({
  selector: 'edit-modal',
  templateUrl: './editModal.component.html',
})

export class EditModalComponent implements OnInit {
  @Output('change') bindChange:EventEmitter<string> = new EventEmitter<string>();
  @Input() assignment;
  @ViewChild('close') el: ElementRef;
  assignmentForm;
  classList;
  courseList;
  datePicker = this.commonService.getDatePickerOptions();
  constructor (private dataService: DataService,
               private formBuilder: FormBuilder,
               private commonService: CommonService) {
  }
  ngOnInit() {
    const dropDowns = [
      this.dataService.getCourses(),
      this.dataService.getClassesList()
    ];

    Observable.forkJoin(dropDowns)
      .subscribe(res => {
        this.courseList = res[0];
        this.classList = res[1];
      });

    this.assignmentForm = this.formBuilder.group({
      className:  this.formBuilder.group({
        autoComplete: ['', Validators.required]
      }),
      classroomID: ['', Validators.required],
      courseID: ['', Validators.required],
      courseName: this.formBuilder.group({
        autoComplete: [null, Validators.required]
      }),
      title: ['', Validators.required],
      assignedOn: ['', Validators.required],
      description: [''],
      endDate: ['', Validators.required],
      totalMarks: ['', Validators.required]
    });

    if (this.assignment) {
      this.assignment.assignedOn = new Date(this.assignment.assignedOn);
      this.assignment.endDate = new Date(this.assignment.endDate);
      this.assignmentForm.patchValue(this.assignment);
    }
  }

  updateClass(event) {
    this.assignmentForm.controls.classroomID.setValue(event ? event.classroomID : null);
    this.assignmentForm.get('className').controls.autoComplete.setValue(event? event.className: null);
    this.assignmentForm.get('className').controls.autoComplete.markAsTouched();

  }

  updateCourse(event) {
    this.assignmentForm.controls.courseID.setValue(event ? event.courseID : null);
    this.assignmentForm.get('courseName').controls.autoComplete.setValue(event ? event.title: null);
    this.assignmentForm.get('courseName').controls.autoComplete.markAsTouched();
  }

  submit() {
    this.commonService.markFormGroupTouched(this.assignmentForm);
    if (this.assignmentForm.valid) {
      const assignment = this.assignmentForm.getRawValue();
      assignment.className = assignment.className.autoComplete;
      assignment.courseName = assignment.courseName.autoComplete;
      const observable = assignment.id ? this.dataService.editAssignment(assignment) : this.dataService.createAssignment(assignment);
      observable
        .subscribe(res => {
          this.el.nativeElement.click();
          swal({
            title: 'Success',
            text: 'Assignment Updated Successfully!',
            type: 'success',
            confirmButtonText: 'Ok'
          });
          this.bindChange.emit(res);
        });
    }
  }

  PreSelectedClass() {
    if (this.assignment) {
      return { className: this.assignment.classroomTitle};
    }
  }

  PreSelectedCourse() {
    if (this.assignment) {
      return { title: this.assignment.courseName};
    }
  }
}
