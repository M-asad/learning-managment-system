import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AllAssignmentComponent } from './allAssignments/allAssignment.component';
import {AssignmentComponent} from './assignment.component';
import { AssignmentDetailComponent } from './assignmentDetail/assignmentDetail.component';


const routes: Routes = [
  {
    path: '',
    component: AllAssignmentComponent
  },
  {
    path: 'test',
    component: AssignmentComponent
  },
  {
    path: ':id',
    component: AssignmentDetailComponent
  }
];

export const assignmentRouting: ModuleWithProviders = RouterModule.forChild(routes);
