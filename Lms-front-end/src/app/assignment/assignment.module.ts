import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { WidgetsModule} from '../widgets/widgets.module';
import { BusyModule } from 'angular2-busy';


import { AllAssignmentComponent } from './allAssignments/allAssignment.component';
import { AssignmentComponent } from './assignment.component';
import { assignmentRouting } from './assignment.routing';
import { AssignmentDetailComponent } from './assignmentDetail/assignmentDetail.component';
import { EditModalComponent } from './editModal.component';
import { NKDatetimeModule } from 'ng2-datetime/ng2-datetime';

@NgModule({
  imports: [
    CommonModule,
    WidgetsModule,
    FormsModule,
    ReactiveFormsModule,
    NKDatetimeModule,
    RouterModule,
    assignmentRouting,
    BusyModule
  ],
  declarations: [
    AssignmentComponent,
    AllAssignmentComponent,
    AssignmentDetailComponent,
    EditModalComponent
  ]
})
export class AssignmentModule { }
