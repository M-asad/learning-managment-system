import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {DiaryComponent} from './diary.component';


const routes: Routes = [
  {
    path: '',
    component: DiaryComponent
  }
];

export const diaryRouting: ModuleWithProviders = RouterModule.forChild(routes);
