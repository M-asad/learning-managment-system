import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { WidgetsModule} from '../widgets/widgets.module';

import { DiaryComponent } from './diary.component';
import {diaryRouting} from './diary.routing';


@NgModule({
  imports: [
    CommonModule,
    WidgetsModule,
    FormsModule,
    RouterModule,
    diaryRouting
  ],
  declarations: [
    DiaryComponent
  ]
})
export class DiaryModule { }
