import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClassDetailComponent } from './classDetail/classDetail.component';
import { ClassListingComponent } from './classListing/classListing.component';
import {RoleGuardService} from '../common/roleGuard.service';


const routes: Routes = [
  { path: '',
    component: ClassListingComponent,
    canActivate: [RoleGuardService],
    data: {role: ['Admin', 'Teacher', 'SuperAdmin']}
  },
  { path: 'add',
    component: ClassDetailComponent
  },
  {
    path: ':id',
    component: ClassDetailComponent
  }
];

export const classRouting: ModuleWithProviders = RouterModule.forChild(routes);
