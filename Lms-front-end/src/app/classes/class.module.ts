import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { WidgetsModule} from '../widgets/widgets.module';

import { ClassListingComponent } from './classListing/classListing.component';
import { ClassDetailComponent } from './classDetail/classDetail.component';
import { BusyModule } from 'angular2-busy';
import { classRouting } from './class.routing';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    WidgetsModule,
    BusyModule,
    classRouting
  ],
  declarations: [
    ClassListingComponent,
    ClassDetailComponent
  ]
})
export class ClassModule { }
