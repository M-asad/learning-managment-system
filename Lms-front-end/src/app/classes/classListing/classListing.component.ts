import { Component,OnInit   } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../common/data.service';
import {Subscription} from 'rxjs';


@Component({
  selector: 'class-listing',
  templateUrl: './classListing.component.html'
})
export class ClassListingComponent implements OnInit {
  public allClasses: any;
  public busy: Subscription;

  constructor(
    private dataService: DataService,
    private router: Router
  ) {

  }

  ngOnInit(){
    this.busy = this.dataService.getClasses()
      .subscribe(res => {
        this.allClasses = res;
        console.log(res);
      })
  }

  classDetail(classroomId: string) {
    this.router.navigate([`class/${classroomId}`]);
  }

  addClass() {
    this.router.navigate([`class/add`]);
  }
}
