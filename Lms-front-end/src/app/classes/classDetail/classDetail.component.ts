import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../../common/data.service';
import { Observable } from 'rxjs/Rx';
import { Subscription } from 'rxjs';
import * as swal from 'sweetalert2';


@Component({
  selector: 'class-detail',
  templateUrl: './classDetail.component.html'
})

export class ClassDetailComponent implements OnInit {
  private classroomID: string;
  public classroomData: any;
  public editClassroomData: any = {
    courseTeachers: [],
    classroomStudents: []
  };
  public isEdit: boolean = false;
  private courseList: Object[];
  private teachers: Object[];
  private selectedTeacherID: any;
  private selectedCourseID: any;
  private sectionId;
  private classId;
  private selectedStudent: any;
  private filteredStudents = [];
  private studentQuery: string = '';
  public busy: Subscription;
  private classList: object[];
  private sectionList: object[];
  private selectedClassTeacher: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.classroomID = params['id'];
    });

    const dropDowns = [
      this.dataService.getCourses(),
      this.dataService.teacherDropdown(),
      this.dataService.getSections(),
      this.dataService.getClassOptions()
    ];

    if (this.classroomID) {
      dropDowns.push(this.dataService.getClassDetail(this.classroomID));
    }

    this.busy = Observable.forkJoin(dropDowns)
      .subscribe(res => {
        if (this.classroomID) {
          this.classroomData = JSON.parse(JSON.stringify(res[4]));
          this.editClassroomData = JSON.parse(JSON.stringify(res[4]));
          this.selectedClassTeacher = {
            teacherID: this.editClassroomData.classTeacherID,
            name: this.editClassroomData.classTeacher
          };
          this.filteredStudents = this.editClassroomData.classroomStudents;
        }
        this.courseList = res[0];
        this.teachers = res[1];
        this.sectionList = res[2];
        this.classList = res[3];
      });
    this.isEdit = this.classroomID ? this.isEdit : true;
  }

  updateCourse(event) {
    if (event) {
      this.selectedCourseID = {
        courseName: event.title,
        courseID: event.id
      };
    }
    else {
      this.selectedCourseID = null;
    }
  }

  updateClass(event) {
    this.classId = event.id;
  }

  updateSection(event) {
    this.sectionId = event.id;
  }

  updateTeacher(event) {
    if (event) {
      this.selectedTeacherID = {
        teacherName: event.text,
        teacherID: event.id
      };
    }
    else {
      this.selectedTeacherID = null;
    }

  }

  updateSelectedStudent(event) {
    if (event) {
      this.selectedStudent = {
        grNo: event.grNo,
        studentID: event.id,
        studentName: event.fullName,
      };
    } else {
      this.selectedStudent = null;
    }

  }

  addStudent() {
    let isExist = false;
    this.editClassroomData.classroomStudents.forEach(item => {
      if (item.studentID === this.selectedStudent.studentID) {
        swal({
          title: 'Warning',
          text: `${this.selectedStudent.studentName} already added!`,
          type: 'warning',
          confirmButtonText: 'Ok'
        });
        isExist = true;
      }
    });
    if (!isExist) {
      this.editClassroomData.classroomStudents.push(this.selectedStudent);
      this.filteredStudents = this.editClassroomData.classroomStudents;
      this.studentQuery = '';
    }
  }

  updateClassTeacher(event) {
    this.selectedClassTeacher = event.id;
  }

  addCourseTeacher() {
    let isCourseAdded = false;
    this.editClassroomData.classroomCourses.forEach(item => {
      if (item.courseID === this.selectedCourseID) {
        swal({
          title: 'Warning',
          type: 'warning',
          text: `${item.courseName} already assigned to teacher!`,
          confirmButtonText: 'Ok'
        });
        isCourseAdded = true;
      }
    });
    if (!isCourseAdded) {
      this.editClassroomData.classroomCourses.push(Object.assign({}, this.selectedCourseID, this.selectedTeacherID));
    }
  }

  removeCourse(index) {
    this.editClassroomData.courseTeachers.splice(index, 1);
  }

  cancelEdit() {
    this.isEdit = false;
    this.editClassroomData = JSON.parse(JSON.stringify(this.classroomData));
    this.filteredStudents = this.editClassroomData.classroomStudents;
  }

  udpateClassroom() {
    let method;
    this.editClassroomData.classTeacherId = this.selectedClassTeacher;
    if (this.classroomID) {
      method = this.dataService.updateClassroom(this.editClassroomData);
    } else {
      this.editClassroomData.classID = this.classId;
      this.editClassroomData.sectionID = this.sectionId;
      method = this.dataService.createClassroom(this.editClassroomData);
    }
    this.busy = method.subscribe(res => {
      this.classroomData = res;
      this.isEdit = false;
      this.router.navigate([`class/${res.id}`]);
    });
  }

  filterStudent() {
    if (this.studentQuery) {
      this.filteredStudents = this.editClassroomData.classroomStudents.filter(student => {
        if (student.grNo.indexOf(this.studentQuery) !== -1
          || student.studentName.indexOf(this.studentQuery) !== -1 ||
          student.parentName.indexOf(this.studentQuery) !== -1) {
          return student;
        }
      });
    }
    else {
      this.filteredStudents = this.editClassroomData.classroomStudents;
    }
  }

  removeStudent(index) {
    this.filteredStudents.splice(index, 1);
  }

  profile(id, type) {
    if(type === 'student') {
      this.router.navigate(['student', id]);
    }
    else if(type === 'parent') {
      this.router.navigate(['parent', id]);
    }
    else {
      this.router.navigate(['teacher', id]);
    }
  }
}
