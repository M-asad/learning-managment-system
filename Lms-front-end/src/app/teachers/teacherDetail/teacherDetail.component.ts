'use strict';

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from '../../common/common.service';
import { TeachersService } from '../teachers.service';
import { AppConstants } from '../../common/app.constants';
import {Subscription} from 'rxjs';
import * as swal from 'sweetalert2';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';


@Component({
  selector: 'teacher-detail',
  templateUrl: './teacherDetail.component.html',
})
export class TeacherDetailComponent implements OnInit {
  private teacherId: string;
  private teacherData: any;
  private bloodGroups: any[] = AppConstants.misc.bloodGroup;
  public editTeacherData: any;
  public isEdit: boolean = false;
  public showData: boolean = false;
  private submitType: string = 'Save';
  private mask = [/\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/];
  private date = new Date();
  private birthDatePickerOpts: any;
  private joinDatePickerOpts : any;
  public profileImg: string;
  private selectedBloodGroup: any;
  public busy: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private teacherService: TeachersService,
    private commonService: CommonService,
    private toastr: ToastsManager

  ) {
  }

  ngOnInit(){
    this.route.params.subscribe(params => {
      this.teacherId = params['id'];
    });
    if(this.teacherId){
      this.busy  = this.teacherService.getTeacher(this.teacherId).subscribe(res => {
        this.teacherData = res;
        this.editTeacherData = Object.assign({}, res);
        this.editTeacherData.birthDate = new Date(this.editTeacherData.birthDate);
        this.editTeacherData.joinDate = new Date(this.editTeacherData.joinDate);
        this.selectedBloodGroup = {type: this.editTeacherData.bloodGroup};
        this.profileImg = this.editTeacherData.picture ? this.editTeacherData.picture : AppConstants.misc.defaultProfileImg;
        this.showData = true;
      });
    }else {
      this.editTeacherData = this.teacherService.getEmptyTeacher();
      this.isEdit = true;
      this.profileImg = AppConstants.misc.defaultProfileImg;
      this.submitType = 'Create';
      this.showData = true;
    }
    this.birthDatePickerOpts = this.commonService.getDatePickerOptions(null, new Date());
    this.joinDatePickerOpts  = this.commonService.getDatePickerOptions();
  }

  editTeacher(isValid) {
    if (isValid) {
      if (this.profileImg !== AppConstants.misc.defaultProfileImg) {
        this.editTeacherData.picture = this.profileImg;
      }
      if (!this.teacherId) {
       this.busy = this.teacherService.createTeacher(this.editTeacherData).subscribe(res => {
          this.router.navigate(['/teacher']);
        });
      }else {
        this.busy = this.teacherService.editTeacher(this.editTeacherData.id, this.editTeacherData).subscribe(res => {
          this.router.navigate(['/teacher']);
        });
      }
    }
  }

  validateStart( value: Date) {
    this.joinDatePickerOpts = this.commonService.getDatePickerOptions(value);
  }

  validateEnd(value: Date) {
    this.birthDatePickerOpts = this.commonService.getDatePickerOptions(null, value);
  }

  updateImage(event) {
    this.profileImg = event;
  }

  deActivate() {
   this.teacherService.haveDependencies(this.teacherId)
     .subscribe(res => {
       if (!res.haveDependency) {
         swal({
           title: 'Do you really want to deactivate this account?',
           type: 'warning',
           showCancelButton: true,
           confirmButtonColor: '#3085d6',
           cancelButtonColor: '#d33',
           confirmButtonText: 'Publish!'
         }).then(() => {
          this.teacherService.deleteTeacher(this.teacherId)
            .subscribe(res => {
              this.router.navigate(['teacher']);
            });
         });
       }
       else {
         this.toastr.error('System is dependant on this account!', 'Account deactivation unsuccessful!!');
       }
     });
  }

  updateBloodGroup(val) {
    this.editTeacherData.bloodGroup = val.type;
  }
}
