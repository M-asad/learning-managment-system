import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import {AllTeachersComponent} from './allTeachers/allTeachers.component';
import {TeacherDetailComponent} from './teacherDetail/teacherDetail.component';


const routes: Routes = [
  { path: '',
    component: AllTeachersComponent,
  },
  { path: 'add',
    component: TeacherDetailComponent
  },
  {
    path: ':id',
    component: TeacherDetailComponent
  }
];

export const teacherRouting: ModuleWithProviders = RouterModule.forChild(routes);
