import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { NKDatetimeModule } from 'ng2-datetime/ng2-datetime';
import { AllTeachersComponent } from './allTeachers/allTeachers.component';
import { TeacherDetailComponent } from './teacherDetail/teacherDetail.component';
import { TextMaskModule } from 'angular2-text-mask';
import { BusyModule } from 'angular2-busy';


import { WidgetsModule} from '../widgets/widgets.module';

import { TeachersService } from './teachers.service';
import { teacherRouting } from './teacher.routing';



@NgModule({
  imports: [
    CommonModule,
    WidgetsModule,
    FormsModule,
    teacherRouting,
    NKDatetimeModule,
    TextMaskModule,
    BusyModule
  ],
  declarations: [
    AllTeachersComponent,
    TeacherDetailComponent
  ],
  providers: [
    TeachersService
  ]
})
export class TeacherModule { }
