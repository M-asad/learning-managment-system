'use strict';

import { Component, OnInit } from '@angular/core';
import { TeachersService } from '../teachers.service';
import { Grid } from '../../widgets/grid/grid.interface';
import { Router, ActivatedRoute } from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'all-teacher',
  templateUrl: './allTeachers.component.html'
})
export class AllTeachersComponent implements OnInit {
  public gridData: Grid;
  public showGrid: boolean = false;
  private pageNo: string = '1';
  public searchKey: string= '';
  public busy: Subscription;

  constructor(private teacherService: TeachersService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.busy = this.teacherService.getTeacherList(this.pageNo, this.searchKey).subscribe(res => {
      if (res) {
        this.gridData  = {
          content : res.data,
          viewUrl: '/teacher',
          pageCount: res.pageCount,
          listingMethod: 'getTeachersList'
        };
        this.showGrid = true;
      }
    });
  }

  addTeacher() {
    this.router.navigate(['add'], { relativeTo: this.route });
  }

  searchTeacher() {
    if (this.searchKey) {
      this.pageNo = '1';
      this.showGrid = false;
      this.teacherService.getTeacherList(this.pageNo, this.searchKey)
        .subscribe(res => {
          this.gridData.content = res.data;
          this.gridData.pageCount = res.pageCount;
          this.showGrid = true;
        });
    }
  }
}
