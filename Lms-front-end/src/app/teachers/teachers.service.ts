import { Injectable } from '@angular/core';
import { DataService } from '../common/data.service';


@Injectable()
export class TeachersService {
  constructor (private dataService: DataService) {
  }

  getTeacherList(pageNo, searchKey) {
    return this.dataService.getTeachersList(pageNo, searchKey);
  }

  getTeacher(id) {
    return this.dataService.getTeacher(id);
  }

  editTeacher(id:string,teacherData: any) {
    return this.dataService.editTeacher(id, teacherData);
  }

  createTeacher(teacherData: any) {
    return this.dataService.createTeacher(teacherData);
  }

  getEmptyTeacher() {
    return {
      "fullName": null,
      "joinDate": new Date,
      "resignationDate": null,
      "birthDate": null,
      "qualification": null,
      "gender": null,
      "bloodGroup": null,
      "nationality": null,
      "religion": null,
      "picture": null,
      "cnic": null,
      "address": null,
      "phone": null,
      "email": null,
      "cell": null,
      "username": null
    }
  }

  haveDependencies(id) {
    return this.dataService.teacherhaveDependencies(id);
  }

  deleteTeacher(id) {
    return this.dataService.deleteTeacher(id);
  }
}
