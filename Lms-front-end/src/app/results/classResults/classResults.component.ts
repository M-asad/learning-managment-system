import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../../common/data.service';
import { Subscription } from 'rxjs';
import * as swal from 'sweetalert2';

@Component({
  selector: 'class-results',
  templateUrl: './classResults.component.html'
})

export class ClassResultComponent implements OnInit {
  private examId;
  public busy: Subscription;
  public classResults;
  constructor(private router: Router,
              private route: ActivatedRoute,
              private dataService: DataService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.examId = params['id'];
    });

    this.busy = this.dataService.getExamClassrooms(this.examId)
      .subscribe(res => {
        this.classResults = res;
      });
  }

  createResult(classId) {
    this.router.navigate(['/result/create', this.examId, classId]);
  }

  classResultRemarks(classId) {
    this.router.navigate(['/result/publish', this.examId, classId]);
  }

  publishExamResult() {
    swal({
      title: 'Do you really want to publish Exam result?',
      text: 'Please confirm all courses result is submitted!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Publish!'
    }).then((result) => {
      swal({
        title: 'Success',
        text: 'Result Published Successfully',
        type: 'success',
        confirmButtonText: 'Ok'
      });
    });
  }
}
