'use strict';

import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../../common/data.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'view-result',
    templateUrl: './viewResult.component.html'
})
export class ViewResultComponent implements OnInit {
    private examId;
    public busy: Subscription;
    public classList;
    private classroomId;
    private studentId;
    public result;
    private studentList;
    public showStudentDropDown = true;

    constructor(private router: Router,
        private route: ActivatedRoute,
        private dataService: DataService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.examId = params['id'];
        });
        this.busy = this.dataService.getResultClassroom(this.examId)
        .subscribe(res => {
            this.classList = res;
        });
    }

    updateClassroom(event) {
        this.classroomId = event.id;
        this.getStudentList();
    }

    updateStudent(event) {
        this.studentId = event.id;
        this.updateResult();
    }

    updateResult() {
        if (this.studentId) {
            this.busy = this.dataService.getStudentResult(this.examId, this.studentId)
                .subscribe(res => {
                    this.result = res;
                });
        }
    }

    getStudentList() {
        this.showStudentDropDown = false;
        this.busy = this.dataService.getResultClassStudent(this.examId, this.classroomId)
        .subscribe(res => {
            this.studentList = res;
            this.studentId = null;
            this.showStudentDropDown = true;
        });
    }
}
