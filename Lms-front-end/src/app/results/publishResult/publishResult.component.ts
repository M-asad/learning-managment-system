import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../../common/data.service';
import { Subscription } from 'rxjs';
import {Observable} from 'rxjs/Rx';
import * as swal from 'sweetalert2';


@Component({
  selector: 'publish-results',
  templateUrl: './publishResult.component.html'
})

export class PublishResultComponent implements OnInit {
  private examId;
  private classroomId;
  public busy: Subscription;
  public classResult;
  private classMasterDetail;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private dataService: DataService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.examId = params['examId'];
      this.classroomId = params['classId'];
    });

    const dropDowns = [this.dataService.getClassroomMaster(this.classroomId),
      this.dataService.getCommulativeResult(this.examId, this.classroomId)];
    this.busy = Observable.forkJoin(dropDowns)
      .subscribe(res => {
        this.classResult = res[1];
        this.classMasterDetail = res[0];
      });
  }

  save() {
    const saveClassResult = {
      examID: this.examId,
      classroomID: this.classroomId,
      results: this.classResult
    };
    this.busy = this.dataService.saveCommulativeResult(saveClassResult)
      .subscribe(res => {
        swal({
          title: 'Success',
          text: 'Student Result updated successfully!',
          type: 'success',
          confirmButtonText: 'Ok'
        });
      });
  }

  publish() {
    swal({
      title: 'Do you really want to publish result?',
      text: 'Please confirm all class results are published!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Publish!'
    }).then((result) => {
      this.dataService.publishClassResult({
        examID: this.examId,
        classroomID: this.classroomId
      }).subscribe(res => {
        swal({
          title: 'Success',
          text: 'Result Published Successfully',
          type: 'success',
          confirmButtonText: 'Ok'
        });
      });
    });
  }

}
