import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BusyModule } from 'angular2-busy';
import { TextMaskModule } from 'angular2-text-mask';

import { WidgetsModule } from '../widgets/widgets.module';
import { CreateResultComponent } from './createResult/createResult.component';
import { ViewResultComponent } from './viewResult/viewResult.component';
import { ClassResultComponent } from './classResults/classResults.component';
import { PublishResultComponent } from './publishResult/publishResult.component';
import { resultRouting } from './result.routing';
import { AppCommonModule } from '../common/common.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    WidgetsModule,
    BusyModule,
    TextMaskModule,
    AppCommonModule,
    resultRouting
  ],
  declarations: [
    CreateResultComponent,
    ViewResultComponent,
    ClassResultComponent,
    PublishResultComponent
  ]
})
export class ResultModule { }
