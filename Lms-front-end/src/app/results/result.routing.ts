import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {PublishResultComponent} from './publishResult/publishResult.component';
import {ClassResultComponent} from './classResults/classResults.component';
import {ViewResultComponent} from './viewResult/viewResult.component';
import {CreateResultComponent} from './createResult/createResult.component';
import {ExamsListingComponent} from '../examination/examsListing/examsListing.component';


const routes: Routes = [
  {
    path: '',
    component: ExamsListingComponent
  },
  {
    path: ':id',
    component: ViewResultComponent
  },
  {
    path: 'create/:examId/:classId',
    component: CreateResultComponent
  },
  {
    path: 'publish/:examId/:classId',
    component: PublishResultComponent
  },
  {
    path: 'class/:id',
    component: ClassResultComponent
  },
];

export const resultRouting: ModuleWithProviders = RouterModule.forChild(routes);
