'use strict';

import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DataService } from '../../common/data.service';
import { Grid } from '../../widgets/grid/grid.interface';
import { Subscription } from 'rxjs';

@Component({
    selector: 'create-result',
    templateUrl: './createResult.component.html'
})
export class CreateResultComponent implements OnInit {
    public busy: Subscription;
    private examId;
    public examCourses: any;
    public examClasses: any;
    private classId;
    public classMasterDetail;
    private courseId;
    public studentList;
    private mask = [/\d/];
    constructor(private router: Router,
        private route: ActivatedRoute,
        private dataService: DataService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.examId = params['examId'];
            this.classId = params['classId'];
        });
        const dropDowns = [this.dataService.getClassroomMaster(this.classId),
        this.dataService.getExamCourses(this.examId)];
        this.busy = Observable.forkJoin(dropDowns)
            .subscribe(res => {
                this.examCourses = res[1];
                this.classMasterDetail = res[0];
            });
    }

    updateCourse(course) {
        this.courseId = course.id;
        this.updateStudentList();
    }

    updateStudentList() {
        if (this.classId && this.courseId) {
            this.busy = this.dataService.GetStudentResultList(this.examId, this.classId, this.courseId)
                .subscribe(res => {
                    this.studentList = res;
                });
        }
    }

    submitResult() {
        this.busy = this.dataService.saveResult(this.mapsaveResult())
            .subscribe(res => {
                this.router.navigate(['/result/create']);
            });
    }

    setMasking() {
        return [/\d/, /\d/, /\d/];
    }

    mapsaveResult() {
        return {
            examID: this.examId,
            classroomID: this.classId,
            courseID: this.courseId,
            studentResults: this.studentList.resultList
        };
    }
}
