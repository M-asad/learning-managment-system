import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { GridService } from './grid/grid.service';
import { GridComponent } from './grid/grid.component';
import { imageUploadComponent } from './imageUpload/imageUpload.component';
import { AutocompleteComponent } from './autoComplete/autoComplete.component';
import { FileDropModule } from 'angular2-file-drop';
import { MonthYearPickerComponent } from './monthYearPicker/monthYearPicker.component';
import {ImageCropperModule} from 'ng2-img-cropper/src/imageCropperModule';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FileDropModule,
    ReactiveFormsModule,
    ImageCropperModule
  ],
  declarations: [
    GridComponent,
    imageUploadComponent,
    AutocompleteComponent,
    MonthYearPickerComponent
  ],
  exports: [
    GridComponent,
    imageUploadComponent,
    AutocompleteComponent,
    MonthYearPickerComponent
  ],
  providers: [
    GridService
  ]
})
export class WidgetsModule { }
