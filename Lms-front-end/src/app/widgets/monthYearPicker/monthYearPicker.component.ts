'use strict';

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'month-year-picker',
  templateUrl: './monthYearPicker.component.html'
})
export class MonthYearPickerComponent implements OnInit {
  @Output('change') bindChange = new EventEmitter();

  public showCalendar: boolean;
  private years: number[] = [];
  private months: string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  private selectedMonth: string = this.months[new Date().getMonth()];
  private selectedYear: number = new Date().getFullYear();
  public monthYear = `${ this.selectedMonth} - ${this.selectedYear}`;

  constructor() {
  }

  ngOnInit() {
    for (let i = 2010; i <= this.selectedYear; i++) {
      this.years.push(i);
    }
  }

  incrementYear() {
    if (this.selectedYear < this.years[this.years.length - 1]) {
      this.selectedYear++;
    }
  }

  decrementYear() {
    if (this.selectedYear > this.years[0]) {
      this.selectedYear--;
    }
  }

  changeMonth(month) {
    this.selectedMonth = month;
    this.monthYear = `${ this.selectedMonth} - ${this.selectedYear}`;
    this.bindChange.emit(this.monthYear);
    this.showCalendar = false;
  }
}
