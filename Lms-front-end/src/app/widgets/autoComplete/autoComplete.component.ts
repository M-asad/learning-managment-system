import { Component, OnInit, Input, Output, ElementRef, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DataService } from '../../common/data.service';
import * as _ from 'lodash';

@Component({
  selector: 'autocomplete',
  templateUrl: './autoComplete.component.html',
  host: {
    '(document:click)': 'handleClick($event)'
  }
})

export class AutocompleteComponent implements OnInit {
  @Output('change') bindChange: EventEmitter<string> = new EventEmitter<string>();
  @Input() url;
  @Input() key;
  @Input() dataList;
  @Input() label;
  @Input() bind;
  @Input() allowSearch;
  @Input() allowClear;
  @Input() isDisabled;
  @Input() formValidator: FormGroup;

  public query = {
    name: ''
  };
  public filteredList = this.dataList ? this.dataList : [];
  public elementRef;
  public isLoader;
  public showAutocomplete = false;
  public selectedItem;
  public isSearch: boolean;

  constructor(myElement: ElementRef,
    private dataService: DataService) {
    this.elementRef = myElement;
  }

  ngOnInit() {
    this.isSearch = true;
    if (this.bind) {
      this.selectedItem = this.bind[this.key];
      if (this.formValidator) {
        this.formValidator.patchValue({
          autoComplete: this.bind[this.key]
        });
      }
    }
    if (!this.dataList && this.url) {
      this.dataService.getAutoComplete(this.query.name, this.url)
        .subscribe(res => {
          this.filteredList = res.data || res;
        });
    }
  }

  filter() {
    if (this.query.name && !this.dataList) {
      this.isLoader = true;
      this.dataService.getAutoComplete(this.query.name, this.url)
        .subscribe(res => {
          this.filteredList = res;
          this.isLoader = false;
        });
    } else if (this.query.name && this.dataList) {
      this.filteredList = this.dataList.filter(data => {
        if (data[this.key].indexOf(this.query.name) !== -1) {
          return data;
        }
      });
    } else {
      this.filteredList = this.dataList ? this.dataList : this.filteredList;
    }
  }

  select(item) {
    this.query.name = '';
    this.selectedItem = item[this.key];
    this.bind = item;
    this.filteredList = this.dataList ? this.dataList : this.filteredList;
    if (this.formValidator) {
      this.formValidator.patchValue({
        autoComplete: item[this.key]
      });
    }
    this.showAutocomplete = false;
    this.bindChange.emit(this.bind);
  }

  handleClick(event) {
    let clickedComponent = event.target;
    this.filteredList = this.dataList ? this.dataList : this.filteredList;
    let inside = false;
    do {
      if (clickedComponent === this.elementRef.nativeElement) {
        inside = true;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent);
    if (!inside) {
      this.showAutocomplete = false;
    }
  }

  clearItem(event) {
    this.selectedItem = '';
    this.bindChange.emit();
    event.stopPropagation();
  }

  show() {
    if (!this.isDisabled) {
      this.showAutocomplete = true;
    }
  }
}
