'use strict';

import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {ImageCropperComponent, CropperSettings} from 'ng2-img-cropper';
import { DataService } from '../../common/data.service';
import {AppConstants } from '../../common/app.constants';

@Component({
  selector: 'app-image-upload',
  templateUrl: './imageUpload.component.html'
})
export class imageUploadComponent implements OnInit {
  @ViewChild('cropper', undefined)
    cropper:ImageCropperComponent;

  @Input() profileImage;
  @Output('change') profileImageChange:EventEmitter<string> = new EventEmitter<string>();
  @Input() isEdit;

  private defaultImage: string = AppConstants.misc.defaultProfileImg;

  data: any = {};
  cropperSettings: CropperSettings;


  uploadFile: any;
  isLoader: boolean = false;
  uploadFileName: any;
  hasBaseDropZoneOver: boolean = false;
  options: Object = {
    url: '/api/image'
  };


  constructor(
    private router: Router,
    private dataService: DataService) {

    this.cropperSettings = new CropperSettings();
    this.cropperSettings.noFileInput = true;
    this.cropperSettings.width = 50;
    this.cropperSettings.height = 50;
    this.cropperSettings.rounded = true;
    this.cropperSettings.cropperDrawSettings.strokeColor = '#000';
    this.cropperSettings.minWithRelativeToResolution = true;
    this.cropperSettings.croppedWidth =100;
    this.cropperSettings.croppedHeight = 100;
    this.cropperSettings.canvasWidth = 400;
    this.cropperSettings.canvasHeight = 300;
  }

  ngOnInit() {
    if (this.profileImage) {
      this.uploadFileName = this.profileImage.split('/')[2];
      this.uploadFile = this.profileImage;
    }
  }

  fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  uploadProfileImage() {
    this.isLoader = true;
    this.dataService.uploadImage(this.data.image)
    .subscribe(res => {
        this.profileImage = `http://${res.message}`;
        this.profileImageChange.emit(this.profileImage);
        this.isLoader = false;
      });
  }

  fileChangeListener($event) {
    const image: any = new Image();
    const file: File = $event.target ? $event.target.files[0] : $event;
    const myReader: FileReader = new FileReader();
    const that = this;
    myReader.onloadend = function (loadEvent: any) {
      image.src = loadEvent.target.result;
      that.cropper.setImage(image);
    };

    myReader.readAsDataURL(file);
  }

  removeUploadedImage() {
    this.profileImage = this.defaultImage;
    this.profileImageChange.emit(this.profileImage);
  }
}
