'use strict';

export interface Grid {
  content: any[],
  viewUrl: string,
  pageCount: number,
  listingMethod :string
}
