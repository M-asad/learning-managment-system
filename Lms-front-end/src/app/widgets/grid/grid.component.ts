'use strict';

import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Router } from '@angular/router';
import { GridService } from './grid.service';
import { DataService } from '../../common/data.service';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html'
})
export class GridComponent implements OnInit {
  @Input() gridData;
  @Input() gridFor;
  @Input() onAction;
  @Output()
  eventEmit = new EventEmitter<any>();

  public gridHeadings;
  public pageCount = [];
  public currentPage: number = 1;

  constructor(
    private gridService: GridService,
    private router: Router,
    private dataService: DataService) { }

  ngOnInit() {
    this.gridHeadings = this.gridService.getGridHeadings(this.gridFor);
    this.pageCount.length = this.gridData.pageCount;
  }

  viewGridItem(item: any) {
    this.router.navigate([this.gridData.viewUrl, item['id']]);
  }

  contentKeys(obj) {
    return Object.keys(obj);
  }

  changePage(pageNo) {
    if ( pageNo <= this.gridData.pageCount && pageNo > 0) {
      this.currentPage = pageNo;
      this.dataService[this.gridData.listingMethod](pageNo, '').subscribe(res => {
        this.gridData.content = res.data;
      });
    }
  }

  action(event, content, item) {
    event.stopPropagation();
    if(item.url) {
      this.router.navigate([item.url, content.id ]);
    }
    else {
      this.eventEmit.emit({
        event: item.event,
        id: content.id
      });
    }
  }
}
