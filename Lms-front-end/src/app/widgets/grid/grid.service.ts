import { Injectable } from '@angular/core';

@Injectable()
export class GridService {
 private gridHeading: any = {
   teachers: [{
       heading: 'Name',
       key: 'fullName'
   },{
       heading: 'Email',
       key: 'email'
   },{
       heading: 'Cell No.',
       key: 'cell'
   }],
   students:  [{
     heading: 'Gr No.',
     key: 'grNo'
   }, {
     heading: 'Name',
     key: 'fullName'
   }, {
    heading: 'Class',
    key: 'classroomName'
  },{
    heading: 'Roll No.',
    key: 'rollNo'
  }],
   parents: [
     {
       heading: 'Name',
       key: 'fullName'
     },
     {
      heading: 'Address',
      key: 'address'
     },
     {
       heading: 'Contact',
       key: 'phone'
     },
     {
       heading: 'Email',
       key: 'email'
     }
   ],
   exams: [
     {
       heading: 'Exam Type',
       key: 'examType'
     },
     {
      heading: 'Description',
      key: 'description'
    },
    {
      heading: 'Start Date',
      key: 'startDate'
    },
    {
      heading: 'EndDate',
      key: 'endDate'
    },{
      heading: 'Academic Year',
      key: 'academicYear'
    }
   ],
   assignments: [
     {
       heading: 'Title',
       key: 'title'
     },
     {
       heading: 'Description',
       key: 'title'
     },
     {
       heading: 'Classroom',
       key: 'classroom'
     },
     {
       heading: 'Submission Date',
       key: 'endDate',
       isDate: true
     }
   ]
 };
 constructor () { }

 getGridHeadings(gridRole) {
   return this.gridHeading[gridRole];
 }
}
