import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BusyModule} from 'angular2-busy';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRouting } from './app.routing';
import { AuthModule } from './auth/auth.module';
import { ShellModule } from './shell/shell.module';
import { WidgetsModule } from './widgets/widgets.module';
import { CourseModule } from './courses/courses.module';
import { AdminModule } from './admin/admin.module';
import { NKDatetimeModule } from 'ng2-datetime/ng2-datetime';
import { AppComponent } from './app.component';
import { HttpService } from './common/http.service';
import { LocalStorageService } from './common/localstorage.service';
import { DataService } from './common/data.service';
import { RoleGuardService } from './common/roleGuard.service';
import { ToastModule } from 'ng2-toastr/ng2-toastr';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRouting,
    AuthModule,
    ShellModule,
    WidgetsModule,
    CourseModule,
    AdminModule,
    NKDatetimeModule,
    BrowserAnimationsModule,
    BusyModule,
    ToastModule.forRoot()
  ],
  providers: [
    HttpService,
    LocalStorageService,
    DataService,
    RoleGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }



