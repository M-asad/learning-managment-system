import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';

import { LocalStorageService } from '../common/localstorage.service';


@Injectable()
export class HttpService  {

  private headers: Headers;
  private options: RequestOptions;
  private headersWithoutToken: Headers;
  private optionsWithoutToken: RequestOptions;
  private centralizeUrl: string = 'api';


  constructor(
    private _http: Http,
    private _localStorageService: LocalStorageService,
    private toastr: ToastsManager,
    private router: Router
  ) {
    let token: string = null;
    if(this._localStorageService.has('accessToken')){
      token = this._localStorageService.map('accessToken').access_token;
    }
    this.setHeaders();
    this.setTokenHeaders(token);
  }

  get(url: string, tokenHeaders:boolean = true): Observable<any> {
    const options = tokenHeaders ? this.setTokenHeaders(this._localStorageService.map('accessToken').access_token) : this.optionsWithoutToken;
    return this._http.get(this._processURL(url), options)
      .map(this._handleResponse)
      .catch(this._handleError.bind(this));
  }

  post(url: string, body?: any, tokenHeaders:boolean = true): Observable<any> {
    const options = tokenHeaders ?
     this.setTokenHeaders(this._localStorageService.map('accessToken').access_token) : this.optionsWithoutToken;
    const _body = JSON.stringify(body);
    return this._http.post(this._processURL(url), _body, options)
      .map(this._handleResponse.bind(this))
      .catch(this._handleError.bind(this));
  }

  put(url: string, body: any, tokenHeaders: boolean = true): Observable<any> {
    const options = tokenHeaders ?
    this.setTokenHeaders(this._localStorageService.map('accessToken').access_token) : this.optionsWithoutToken;
    const _body = JSON.stringify(body);
    return this._http.put(this._processURL(url), _body, options)
      .map(this._handleResponse.bind(this))
      .catch(this._handleError.bind(this));
  }

  delete(url: string): Observable<any> {
    return this._http.delete(this._processURL(url), this.options)
      .map(this._handleResponse)
      .catch(this._handleError.bind(this));
  }

  setTokenHeaders(token: string){
    if(token){
      this.headers = new Headers({
        'Content-Type': 'application/json',
        'Authorization': 'bearer ' + token
      });
    } else {
      this.headers = new Headers({ 'Content-Type': 'application/json' });
    }
    return new RequestOptions({ headers: this.headers });
  }

  setHeaders(){
    this.headersWithoutToken = new Headers({ 'Content-Type': 'application/json' });
    this.optionsWithoutToken = new RequestOptions({headers: this.headersWithoutToken});
  }

  private _processURL(url: string){
    return this.centralizeUrl + '/' + url;
  }

  private _handleResponse(response: any, value: any): any {
    try {
      let body = response.json();
      return body || { };


    }
    catch (e) {
      return { };
    }
  }

  private _handleError (error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';

    if (error.status === 500 || error.status === 404){
      this.toastr.error('something bad happens!', 'Oops!');
    }else if ( error.status === 401) {
      this.router.navigate(['/login']);
    }

    // log to console instead
    // console.error(errMsg);
    return Observable.throw(errMsg);
  }
}



