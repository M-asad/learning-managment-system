import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { AppConstants } from './app.constants';



@Injectable()
export class DataService {
  private constants: any;
  constructor (private httpService: HttpService) {
    this.constants = AppConstants;
  }

  /*******************************Teachers****************************/
  getTeachersList(pageNo, searchKey) {
    return this.httpService.get(`${this.constants.appUrls.teachers.allTeachers}?prefix=${searchKey}&page=${pageNo}`);
  }

  getTeacher(id: string) {
    return this.httpService.get(`${this.constants.appUrls.teachers.getTeacher}${id}`);
  }

  editTeacher(id: string, teacherData: any) {
    return this.httpService.put(this.constants.appUrls.teachers.editTeacher, teacherData);
  }

  createTeacher(teacherData: any) {
    return this.httpService.post(this.constants.appUrls.teachers.createTeacher, teacherData);
  }

  teacherhaveDependencies(id) {
    return this.httpService.get(`${this.constants.appUrls.teachers.haveDependencies}?id=${id}`);
  }

  deleteTeacher(id) {
    return this.httpService.post(`${this.constants.appUrls.teachers.deleteTeacher}?id=${id}`);
  }
  /*******************************Students****************************/

  getStudentsList(pageNo, searchKey) {
    return this.httpService.get(`${this.constants.appUrls.students.allStudents}?prefix=${searchKey}&page=${pageNo}`);
  }

  editStudent(id: string, studentData: any) {
    return this.httpService.put(this.constants.appUrls.students.editStudent, studentData);
  }

  createStudent(studentData: any) {
    return this.httpService.post(this.constants.appUrls.students.createStudent, studentData);
  }

  getStudent(id: string) {
    return this.httpService.get(`${this.constants.appUrls.students.getStudent}${id}`);
  }

  /*******************************Parents****************************/

  getParentsList(pageNo, searchKey) {
    return this.httpService.get(`${this.constants.appUrls.parents.allParents}?prefix=${searchKey}&page=${pageNo}`);
  }

  editParent(id: string, parentData: any) {
    return this.httpService.put(this.constants.appUrls.parents.editParent, parentData);
  }

  createParent(parentData: any) {
    return this.httpService.post(this.constants.appUrls.parents.createParent, parentData);
  }

  getParent(id: string) {
    return this.httpService.get(`${this.constants.appUrls.parents.getParent}${id}`);
  }

  /**************************Widgets**************************************/

  getAutoComplete(query: string, url: string) {
    return this.httpService.get(`${url}?prefix=${query}`);
  }

  uploadImage(image: File) {
    return this.httpService.post(this.constants.appUrls.widgets.imageUpload, image);
  }

  /***************************Classes*****************************************/

  getClasses() {
    return this.httpService.get(this.constants.appUrls.classes.getClasses);
  }

  getClassDetail(classId: string) {
    return this.httpService.get(`${this.constants.appUrls.classes.classDetail}${classId}`);
  }

  getCourses() {
    return this.httpService.get(this.constants.appUrls.classes.getCourses);
  }

  teacherDropdown() {
    return this.httpService.get(this.constants.appUrls.teachers.teacherList);
  }

  updateClassroom(classroom) {
    return this.httpService.put(this.constants.appUrls.classes.updateClass, classroom);
  }

  createClassroom(classroom) {
    return this.httpService.post(this.constants.appUrls.classes.createClassroom, classroom);
  }

  getClassesList() {
    return this.httpService.get(this.constants.appUrls.classes.getClassList);
  }

  getClassOptions() {
    return this.httpService.get(this.constants.appUrls.classes.classOptions);
  }

  getSections() {
    return this.httpService.get(this.constants.appUrls.classes.section);
  }

  getClassroomMaster(classroomID) {
    return this.httpService.get(`${this.constants.appUrls.classes.getClassroomMaster}?id=${classroomID}`);
  }

  getClassroomOptions() {
    return this.httpService.get(`${this.constants.appUrls.classes.getClassroomOptions}`);
  }


  /*******************************Attendance*********************************/

  getClassAttendance(classID, date) {
    return this.httpService.get(`${this.constants.appUrls.attendance.getClassAttendance}?classroomID=${classID}&Date=${date}`)
  }

  getMarkAttendance(classID, date) {
    return this.httpService.get(`${this.constants.appUrls.attendance.getMarkAttendance}?classroomID=${classID}&Date=${date}`)
  }

  getStudentAttendance(studentID, year, month) {
    return this.httpService.get(
      `${this.constants.appUrls.attendance.getStudentAttendance}?studentID=${studentID}&year=${year}&month=${month}`
    );
  }

  saveAttendance(attendance) {
    return this.httpService.post(`${this.constants.appUrls.attendance.saveAttendance}`, attendance);
  }

  markClassAttendance(attendance) {
    return this.httpService.post(`${this.constants.appUrls.attendance.markClassAttendance}`, attendance);
  }

  updateAttendance(attendance) {
    return this.httpService.post(`${this.constants.appUrls.attendance.updateAttendance}`, attendance);
  }

    /*******************************Examination*********************************/

  getExamClasses() {
    return this.httpService.get(this.constants.appUrls.exams.getExamClasses);
  }

  getExamsListing(query, pageNo) {
    return this.httpService.get(`${this.constants.appUrls.exams.allExams}?prefix=${query}&page=${pageNo}`);
  }

  getExamDetail (id) {
    return this.httpService.get(`${this.constants.appUrls.exams.examDetail}/${id}`);
  }

  getExamTypes() {
    return this.httpService.get(`${this.constants.appUrls.exams.examType}`);
  }

  createExam(examDetail) {
    return this.httpService.post(`${this.constants.appUrls.exams.createExam}`, examDetail);
  }

  saveExamDetail(examDetail) {
    return this.httpService.post(`${this.constants.appUrls.exams.updateExamDetail}`, examDetail);
  }


  /*******************************Results*********************************/

  getResultClassroom(examId) {
    return this.httpService.get(`${this.constants.appUrls.result.getResultClassroom}?examid=${examId}`);
  }

  getExamCourses(examId) {
    return this.httpService.get(`${this.constants.appUrls.result.getExamCourses}?examid=${examId}`);
  }

  GetStudentResultList(examId, classId, courseId) {
    return this.httpService.get(
      `${this.constants.appUrls.result.GetStudentResultList}?examID=${examId}&classroomId=${classId}&courseId=${courseId}`
    );
  }

  saveResult(result) {
    return this.httpService.post(this.constants.appUrls.result.saveResult, result);
  }

  getStudentResult(examId, studentId) {
    return this.httpService.get(`${this.constants.appUrls.result.getResult}?examid=${examId}&studentid=${studentId}`);
  }

  getResultClassStudent(examId, classId) {
    return this.httpService.get(`${this.constants.appUrls.result.getResultStudentList}?classroomID=${classId}&examId=${examId}`);
  }

  getExamClassrooms(examId) {
    return this.httpService.get(`${this.constants.appUrls.result.getClassResult}?examid=${examId}`);
  }

  getCommulativeResult(examId, classroomId) {
    return this.httpService.get(`${this.constants.appUrls.result.getCommulativeResult}?examid=${examId}&classroomId=${classroomId}`);
  }

  saveCommulativeResult(classResult) {
    return this.httpService.post(this.constants.appUrls.result.saveCommulativeResult, classResult);
  }

  publishClassResult(data) {
    return this.httpService.post(this.constants.appUrls.result.publishClassroomResult, data);
  }

  /*****************************Assignements**************************************/

  getAssignments(page, prefix) {
    return this.httpService.get(`${this.constants.appUrls.assignment.getAssignments}?prefix=${prefix}&page=${page}`);
  }

  getAssignment(assignmentId) {
    return this.httpService.get(`${this.constants.appUrls.assignment.get}/${assignmentId}`);
  }

  createAssignment(assignment) {
    return  this.httpService.post(this.constants.appUrls.assignment.create, assignment);
  }

  editAssignment(assignment) {
    return  this.httpService.put(this.constants.appUrls.assignment.create, assignment);
  }

  removeAssignment(id) {
    return this.httpService.delete(`${this.constants.appUrls.assignment.create}/${id}`);
  }
}
