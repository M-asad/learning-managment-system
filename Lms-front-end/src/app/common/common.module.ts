import { NgModule } from '@angular/core';
import {ExamsListingComponent} from '../examination/examsListing/examsListing.component';
import {WidgetsModule} from '../widgets/widgets.module';
import {BusyModule} from 'angular2-busy';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    WidgetsModule,
    BusyModule,
  ],
  declarations: [
    ExamsListingComponent
  ],
  exports: [ExamsListingComponent]
})
export class AppCommonModule { }
