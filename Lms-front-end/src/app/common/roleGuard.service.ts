import { Injectable } from '@angular/core';
import {LocalStorageService} from './localstorage.service';
import { Router, ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class RoleGuardService implements CanActivate {

  constructor(private localStorage: LocalStorageService,
              private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean{
    const token = this.localStorage.map('accessToken');
    if (!token) {
      this.router.navigate(['/login']);
    }
    const authorizedRole = route.data['role'];
    const currentUserRole = this.localStorage.map('accessToken').role_name;
    return authorizedRole.indexOf(currentUserRole) !== -1;
  }
}
