import { Injectable } from '@angular/core';

import { LocalStorageService } from '../common/localstorage.service';
import { RoleConstant } from '../common/roleAccess.constants';
import {FormControl, FormGroup} from '@angular/forms';


@Injectable()
export class CommonService {
  private roleConstant: any;


  constructor (
    private localStorage: LocalStorageService
  ) {
    this.roleConstant = RoleConstant
  }

  checkRolePermission(url: string) {
    const currentUserRole = this.localStorage.map('accessToken');
    if ( !currentUserRole || this.roleConstant[currentUserRole.role_name].indexOf(url) !== -1){
      return false;
    }
    return true;
  }

  getDatePickerOptions(start?, end?) {
    return {
      startDate: start ? start : null,
      endDate: end ? end : null,
      autoclose: true,
      todayBtn: 'linked',
      todayHighlight: true,
      assumeNearbyYear: true,
      format: 'dd-MM-yyyy'
    };
  }

  markFormGroupTouched(formGroup: FormGroup) {

    if (formGroup.controls) {
      const keys = Object.keys(formGroup.controls);
      for (let i = 0; i < keys.length; i++) {
        const control = formGroup.controls[keys[i]];

        if (control instanceof FormControl) {
          control.markAsTouched();
        } else if (control instanceof FormGroup) {
          this.markFormGroupTouched(control);
        }
      }
    }
  }
}

