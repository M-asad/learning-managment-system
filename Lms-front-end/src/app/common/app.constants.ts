'use strict';

export const AppConstants = {
  appUrls: {
    auth: {
      login: 'account/login'
    },
    teachers: {
      allTeachers: 'teacher',
      getTeacher: 'teacher/',
      editTeacher: 'teacher',
      createTeacher: 'teacher/create',
      teacherList: 'Teacher/GetSelectList',
      haveDependencies: 'teacher/HaveDependencies',
      deleteTeacher: 'teacher/delete'
    },
    students: {
      allStudents: 'student',
      getStudent: 'student/',
      editStudent: 'student/update',
      createStudent: 'student'
   },
    parents: {
      allParents: 'Parent',
      getParent: 'parent/',
      editParent: 'parent',
      createParent: 'parent'
    },
    widgets: {
      getAutoComplete: 'parent/GetParentsByPrefix',
      imageUpload: 'upload/image'
    },
    classes: {
      getClasses: 'Classroom/GetCurrentClasses',
      classDetail: 'classroom/',
      getCourses: 'general/courses',
      updateClass: 'classroom',
      getClassList: 'classroom/getCurrentClasses',
      classOptions: 'general/classes',
      section: 'general/sections',
      createClassroom: 'classroom',
      getClassroomMaster: 'classroom/GetClassroomMaster',
      getClassroomOptions: 'classroom/getCurrentClassesddl',
    },
    attendance: {
      getClassAttendance: 'attendance/getCalendar',
      getMarkAttendance: 'attendance',
      getStudentAttendance: 'attendance/getStudentCalendar',
      updateAttendance: 'attendance/update',
      saveAttendance: 'attendance/SaveStudentCalendar',
      markClassAttendance: 'attendance'
    },
    exams: {
      allExams: 'exam',
      examDetail: 'exam',
      examType: 'examType/getSelectList',
      createExam: 'exam',
      updateExamDetail: 'exam/SaveDetail',
      getExamClasses: 'exam/GetCurrentYearClasses'
    },
    result: {
      getResultClassroom: 'Result/getClassrooms',
      getExamCourses: 'Exam/getExamCourses',
      GetStudentResultList: 'Result/GetStudentResultList',
      saveResult: 'result',
      getResult: 'Result/GetStudentResult',
      getResultStudentList: 'result/GetExamStudents',
      getClassResult: 'result/GetExamClassrooms',
      getCommulativeResult: 'result/GetCommulativeResult',
      saveCommulativeResult: 'result/SaveClassroomResult',
      publishClassroomResult: 'result/publishClassroomResult'
    },
    assignment: {
      getAssignments: 'assignment',
      get: 'assignment',
      create: 'assignment'
    }
  },
  misc: {
    defaultProfileImg: './images/profileImage.png',
    bloodGroup: [{
      type: 'A+'
    },{
      type: 'A-'
    },{
      type: 'B+'
    },{
      type: 'B-'
    },{
      type: 'AB+'
    },{
      type: 'AB-'
    },{
      type: 'O+'
    },{
      type: 'O-'
    },]
  }
};
