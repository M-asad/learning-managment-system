﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;

namespace LMS.API.Infrastructure
{
    public class GlobalErrorHandling : ExceptionHandler
    {
        public override void Handle(ExceptionHandlerContext context)
        {
            string errorMessage = context.Exception.Message;
            var innerException = context.Exception.InnerException;
            var response = context.Request.CreateResponse(
                HttpStatusCode.InternalServerError,
                new
                {
                    Message = errorMessage,
                    InnerException = innerException
                });

            response.ReasonPhrase = errorMessage.Replace(Environment.NewLine, " ");
            context.Result = new ResponseMessageResult(response);
        }
    }
}