﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LMS.API.Models
{
    public class ClassroomModel
    {
        [Required]
        public int ClassId { get; set; }

        [Required]
        public int SectionId { get; set; }

        [Required]
        public int ClassTeacherId { get; set; }

        public List<ClassroomCourseModel> CourseTeachers { get; set; }

        public List<ClassroomStudentModel> ClassroomStudents { get; set; }        
    }

    public class ClassroomCourseModel
    {
        public int ClassCourseId { get; set; }

        [Required]
        public int CourseId { get; set; }

        [Required]
        public int TeacherId { get; set; }
    }

    public class ClassroomUpdateModel : ClassroomModel
    {
        public int ClassroomId { get; set; }
    }


    public class ClassroomStudentModel
    {
        public int StudentId { get; set; }
    }
}