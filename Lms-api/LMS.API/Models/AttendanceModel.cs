﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.API.Models
{
    public class AttendanceModel
    {
        public int ClassroomId { get; set; }
        public DateTime StartDate { get; set; }
    }

    public class AttendanceCreateModel
    {
        public int ClassroomId { get; set; }
        public DateTime AttendanceDate { get; set; }

    }

    public class StudentAttendanceModel
    {

    }
}