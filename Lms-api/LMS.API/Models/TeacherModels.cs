﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LMS.API.Models
{
    public class TeacherModel
    {
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        public DateTime? JoinDate { get; set; }

        public DateTime? ResignationDate { get; set; }

        public DateTime? BirthDate { get; set; }

        [StringLength(100)]
        public string Qualification { get; set; }

        [Required]
        [StringLength(1)]
        public string Gender { get; set; }

        [StringLength(3)]
        public string BloodGroup { get; set; }

        [StringLength(15)]
        public string Nationality { get; set; }

        [StringLength(20)]
        public string Religion { get; set; }

        [StringLength(70)]
        public string Picture { get; set; }

        [StringLength(15)]
        public string CNIC { get; set; }

        [StringLength(200)]
        public string Address { get; set; }

        [StringLength(15)]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(15)]
        public string Cell { get; set; }

    }

    public class TeacherUpdateModel : TeacherModel
    {
        [Required]
        public int TeacherId { get; set; }
    }
}
