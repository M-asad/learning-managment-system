﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.API.Models
{
    public class AssignmentSearchModel : SearchModel
    {
        public int StudentId { get; set; }
    }
}