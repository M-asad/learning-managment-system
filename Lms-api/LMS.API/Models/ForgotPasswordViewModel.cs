﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.API.Models
{
    public class ForgotPasswordViewModel
    {
        public string Code { get; set; }
        public string Password { get; set; }
        public int UserId { get; set; }
    }
}