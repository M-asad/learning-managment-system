﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.API.Models
{
    public class SearchModel
    {
        public string Prefix { get; set; }
        public int Page { get; set; }
    }

    public class PrefixModel
    {
        public string prefix { get; set; }
    }
}