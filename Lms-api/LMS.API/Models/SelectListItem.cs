﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.API.Models
{
    public class SelectListItem
    {
        public int Id { get; set; }
        public string Text { get; set; }
    }
}