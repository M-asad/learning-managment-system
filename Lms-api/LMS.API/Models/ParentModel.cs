﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LMS.API.Models
{
    public class ParentModel
    {
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(200)]
        public string Address { get; set; }

        [StringLength(15)]
        public string Phone { get; set; }

        [StringLength(15)]
        public string Cell { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(20)]
        public string CNIC { get; set; }

    }

    public class ParentUpdateModel : ParentModel
    {
        [Required]
        public int ID { get; set; }
    }
}
