﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LMS.API.Models
{
    public class StudentModel
    {
        [Required]
        [StringLength(10)]
        public string GRNo { get; set; }

        [Required]
        public int RollNo { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        public DateTime JoinDate { get; set; }

        public DateTime BirthDate { get; set; }

        [StringLength(50)]
        public string BirthPlace { get; set; }

        [StringLength(1)]
        public string Gender { get; set; }

        [StringLength(3)]
        public string BloodGroup { get; set; }

        [StringLength(50)]
        public string Nationality { get; set; }

        [StringLength(50)]
        public string Religion { get; set; }

        public DateTime? LeavingDate { get; set; }

        [Required]
        public int ID { get; set; }

        [StringLength(200)]
        public string Address { get; set; }

        [StringLength(15)]
        public string Phone { get; set; }

        [StringLength(70)]
        public string Picture { get; set; }

        public int? ClassroomId { get; set; }
    }

    public class StudentUpdateModel : StudentModel
    {
        [Required]
        public int StudentId { get; set; }
    }
}
