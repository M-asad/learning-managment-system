﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace LMS.API.Controllers
{
    [RoutePrefix("api/Upload")]
    public class UploadController : BaseController
    {
        [HttpPost]
        [Route("Image")]
        public HttpResponseMessage Image([FromBody] string ImgStr)
        {
            try
            {
                var httpRequest = HttpContext.Current.Request;
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);

                string contentString = ImgStr.Split(new string[] { "base64," }, StringSplitOptions.None)[1];
                byte[] imageBytes = Convert.FromBase64String(contentString);

                string filename = Helper.GetTimestamp(DateTime.Now) + ".jpg";

                var filePath = HttpContext.Current.Server.MapPath("~/Picture/" + filename);

                File.WriteAllBytes(filePath, imageBytes);

                var fileURL = HttpContext.Current.Request.Url.Host + "/Picture/" + filename;
                return Request.CreateErrorResponse(HttpStatusCode.Created, fileURL);
            }
            catch (Exception ex)
            {
                var res = string.Format(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, res);
            }
        }

        [HttpPost]
        [Route("Document")]
        public async Task<HttpResponseMessage> Document()
        {
            try
            {
                string root = HttpContext.Current.Server.MapPath("~/Document");
                if (Request.Content.IsMimeMultipartContent())
                {
                    string path = Path.Combine(root, "temp");
                    var streamProvider = new MultipartFormDataStreamProvider(path);
                    await Request.Content.ReadAsMultipartAsync(streamProvider);
                    foreach (MultipartFileData fileData in streamProvider.FileData)
                    {
                        if (string.IsNullOrEmpty(fileData.Headers.ContentDisposition.FileName))
                        {
                            return Request.CreateResponse(HttpStatusCode.NotAcceptable, "This request is not properly formatted");
                        }
                        //string fileName = fileData.Headers.ContentDisposition.FileName;
                        string fileName = fileData.LocalFileName;
                        if (fileName.StartsWith("\"") && fileName.EndsWith("\""))
                        {
                            fileName = fileName.Trim('"');
                        }
                        if (fileName.Contains(@"/") || fileName.Contains(@"\"))
                        {
                            fileName = Path.GetFileName(fileName);
                        }
                        var extension = "." + fileData.Headers.ContentDisposition.FileName.Split('.')[1].TrimEnd('"');
                        File.Move(fileData.LocalFileName, Path.Combine(root, fileName.Replace("BodyPart_", "")) + extension);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotAcceptable, "This request is not properly formatted");
                }
            }
            catch(Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
            //// Check if the request contains multipart/form-data.
            //if (!Request.Content.IsMimeMultipartContent())
            //{
            //    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            //}

            //string root = HttpContext.Current.Server.MapPath("~/Document");
            //var provider = new MultipartFormDataStreamProvider(root);

            //try
            //{
            //    // Read the form Entity.
            //    await Request.Content.ReadAsMultipartAsync(provider);

            //    // This illustrates how to get the file names.
            //    List<string> filenames = new List<string>();
            //    foreach (MultipartFileData file in provider.FileData)
            //    {
            //        // filenames.Add(file.Headers.ContentDisposition.FileName);
            //        filenames.Add((file.LocalFileName.Replace(root + "\\", "") + "." + file.Headers.ContentDisposition.FileName.Split('.')[1]).TrimEnd('"'));
            //    }
            //    return Request.CreateResponse(HttpStatusCode.OK, filenames);
            //}
            //catch (System.Exception e)
            //{
            //    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            //}
        }
        //[HttpPost]
        //[Route("Document")]
        //public HttpResponseMessage Document([FromBody] string ImgStr)
        //{
        //    HttpResponseMessage result = null;

        //    var httpRequest = HttpContext.Current.Request;
        //    if (httpRequest.Files.Count > 0)
        //    {
        //        var docfiles = new List<string>();
        //        foreach (string file in httpRequest.Files)
        //        {
        //            var postedFile = httpRequest.Files[file];
        //            var filePath = HttpContext.Current.Server.MapPath("~/" + postedFile.FileName);
        //            postedFile.SaveAs(filePath);
        //            docfiles.Add(filePath);
        //        }
        //        result = Request.CreateResponse(HttpStatusCode.Created, docfiles);
        //    }
        //    else
        //    {
        //        result = Request.CreateResponse(HttpStatusCode.BadRequest);
        //    }
        //    return result;
        //}

        //[HttpPost]
        //public HttpResponseMessage Index()
        //{
        //    try
        //    {
        //        var httpRequest = HttpContext.Current.Request;
        //        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);

        //        var postedFile = httpRequest.Files[0];
        //        if (postedFile != null && postedFile.ContentLength > 0)
        //        {
        //            int MaxContentLength = 1024 * 1024 * 1; //Size = 1 MB

        //            IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
        //            var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
        //            var extension = ext.ToLower();
        //            if (!AllowedFileExtensions.Contains(extension))
        //            {
        //                var message = string.Format("Please Upload image of type .jpg,.gif,.png.");
        //                return Request.CreateResponse(HttpStatusCode.BadRequest, message);
        //            }
        //            else if (postedFile.ContentLength > MaxContentLength)
        //            {
        //                var message = string.Format("Please Upload a file upto 1 mb.");
        //                return Request.CreateResponse(HttpStatusCode.BadRequest, message);
        //            }
        //            else
        //            {
        //                string filename = Helper.GetTimestamp(DateTime.Now) + extension;

        //                var filePath = HttpContext.Current.Server.MapPath("~/Picture/" + filename);
        //                postedFile.SaveAs(filePath);
        //                var fileURL = HttpContext.Current.Request.Url.Host + "/Picture/" + filename;
        //                // var message = string.Format("Image Uploaded Successfully.");
        //                return Request.CreateErrorResponse(HttpStatusCode.Created, fileURL);
        //            }
        //        }
        //        var res = string.Format("Please Upload a image.");
        //        return Request.CreateResponse(HttpStatusCode.NotFound, res);
        //    }
        //    catch (Exception ex)
        //    {
        //        var res = string.Format(ex.Message);
        //        return Request.CreateResponse(HttpStatusCode.NotFound, res);
        //    }
        //}
    }
}