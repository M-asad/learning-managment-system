﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LMS.Core.Entity;
using LMS.API.Models;
using LMS.Core.DTO;
using LMS.API.Infrastructure;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using System.Web;
using LMS.Core.Attribute;
using LMS.Core.Enums;
using LMS.Core.IService;
using LMS.Core.Infrastructure;
using LMS.Common.Helper;
using LMS.Core.DTO.CustomDTOs;

namespace LMS.API.Controllers
{
    [RoutePrefix("api/Student")]
    [Authorize]
    public class StudentController : BaseController<IStudentService, StudentDTO, Student, StudentListDTO>
    {
        public StudentController(IStudentService service, IRequestInfo requestInfo)
            : base(service)
        {
        }

        /// <summary>
        /// Give all students for dropdown
        /// </summary>
        /// <returns>SelectListItem</returns>
        [HttpGet]
        [Route("GetSelectList")]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public async Task<IList<SelectListItem>> GetSelectList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var row in await this.Service.GetAllAsync())
            {
                list.Add(new SelectListItem()
                {
                    Id = row.Id,
                    Text = row.FullName
                });
            }
            return list;
        }

        /// <summary>
        /// Get all students containing prefix for autocomplete
        /// </summary>
        /// <param name="prefix"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentList")]
        public async Task<IList<SelectListItem>> GetStudentList([FromUri]PrefixModel model)
        {
            var list = await this.Service.GetAllAsync(this.GetJsonApiRequest(new SearchModel() {
                Page = 1,
                Prefix = model.prefix ?? ""
            }));
            List<SelectListItem> result = new List<SelectListItem>();
            foreach (var row in list.Data)
            {
                result.Add(new SelectListItem()
                {
                    Id = row.Id,
                    Text = row.FullName
                });
            }
            return result;
        }

        /// <summary>
        /// Get all unenrolled students of current year
        /// </summary>
        [HttpGet]
        [Route("GetUnenrolledStudents")]
        [AuthorizeRoles(UserRoles.Admin)]
        public IList<StudentSimpleDTO> GetUnEnrolledStudents([FromUri]PrefixModel model)
        {
            return this.Service.GetUnEnrolledStudents(model.prefix, ConfigManager.RecordCount);
        }
        /// <summary>
        /// Get Students of parent
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentsByParentId")]
        [AuthorizeRoles(UserRoles.Admin)]
        public IHttpActionResult GetStudentsByParentId(int parentId)
        {
            var list = this.Service.GetStudentsByParentId(parentId);
            return Ok(list);
        }

        /// <summary>
        /// Get all enrolled students of current year
        /// </summary>
        [HttpGet]
        [Route("GetEnrolledStudents")]
        [AuthorizeRoles(UserRoles.Admin)]
        public IList<StudentSimpleDTO> GetEnrolledStudents([FromUri]PrefixModel model)
        {
            return this.Service.GetEnrolledStudents(model.prefix, ConfigManager.RecordCount);
        }
    }
}
