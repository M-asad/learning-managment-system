﻿using LMS.API.Models;
using LMS.Core.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using LMS.Core.Attribute;
using LMS.Core.Enums;
using LMS.Core.IService;
using LMS.Core.Infrastructure;

namespace LMS.API.Controllers
{
    [RoutePrefix("Api/Result")]
    [Authorize]
    public class ResultController : BaseController
    {
        IRequestInfo requestInfo;
        IResultService service;
        IClassroomService classroomService;
        IParentService parentService;

        public ResultController(IRequestInfo requestInfo, IResultService service, IClassroomService classroomService, IParentService parentService)
        {
            this.requestInfo = requestInfo;
            this.service = service;
            this.classroomService = classroomService;
            this.parentService = parentService;
        }

        /// <summary>
        /// Exams with atleast one paper's date is arrived
        /// </summary>
        [HttpGet]
        [Authorize]
        public IHttpActionResult Get(string prefix, int page)
        {
            int pageCount = 0;
            List<ExamListDTO> list = this.service.GetResultList(prefix, ConfigManager.RecordCount, page, out pageCount);
            ResultSet<ExamListDTO> result = new ResultSet<ExamListDTO>()
            {
                Data = list,
                PageCount = pageCount
            };
            return Ok(result);
        }

        [HttpGet]
        [Route("GetClassrooms")] // for dropdown 
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public IHttpActionResult GetClassrooms(int examId)
        {
            try
            {
                List<KeyValueDTO> list = new List<KeyValueDTO>();
                list = this.service.GetExamClassroomList(examId);
                //var user = base.GetCurrentUser();
                //var userRoleIds = user.Roles.Select(x => x.RoleId).ToList();
                //var userRoles = base.AppRoleManager.Roles.Where(x => userRoleIds.Contains(x.Id)).Select(x => x.Name);

                //if (userRoles.Contains(UserRoles.Admin) || userRoles.Contains(UserRoles.SuperAdmin))
                //    list = this.service.GetExamClassrooms(Id);
                //else
                //    list = this.service.GetClassroomForTeacher(Id, user.Id);

                //if (list == null || list.Count == 0)
                //    return BadRequest("No data found.");
                return Ok(list);
            }
            catch(Exception ex) {
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Get result detail
        /// </summary>
        [HttpGet]
        [Route("GetEditResult")]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public IHttpActionResult GetEditResult(int examId)
        {
            ResultSaveDTO data = this.service.GetResultDetail(examId);
            if (data == null)
                return BadRequest("No data found.");
            return Ok(data);
        }

        [HttpPost]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public IHttpActionResult Save(ResultSaveDTO model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                if (this.requestInfo.Role.Equals(UserRoles.Teacher))
                {
                    if (!this.classroomService.IsUserClassTeacher(this.requestInfo.UserId, model.ClassroomId))
                    {
                        return BadRequest("Only class teachers are allowed to save results of their classroom");
                    }
                }
                this.service.SaveResult(model);
                return Ok(model.ExamId);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        /// <summary>
        ///  Give list of all student with or without result to save/edit marks
        /// </summary>
        [HttpGet]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        [Route("GetStudentResultList")]
        public IHttpActionResult GetStudentResultList(int examId, int classroomId, int courseID)
        {
            StudentResultListDTO data = this.service.GetStudentResultList(examId, classroomId, courseID);
            if (data == null)
                return BadRequest("No data found.");
            return Ok(data);
        }

        /// <summary>
        /// View of single student. Student will be selected from dropdown
        /// </summary>        
        [HttpGet]
        [Route("GetStudentResult")]
        // [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher, UserRoles.Parent)] // TODO: Confirmed?
        public IHttpActionResult GetStudentResult(int examId, int studentId)
        {
            if (this.requestInfo.Role.Equals(UserRoles.Parent.ToString()))
            {
                if (!this.parentService.HaveParentChildRelation(this.requestInfo.UserId, studentId))
                    return Unauthorized(); //"Parent can only check his/her child records"
            }
            StudentResultMasterDTO data = this.service.GetStudentResult(examId, studentId);
            if (data == null)
                return BadRequest("No data found.");
            return Ok(data);
        }

        /// <summary>
        /// This will give ddl values of all students of a classroom
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetExamStudents")]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public IHttpActionResult GetStudentList(int examId, int classroomId)
        {
            List<KeyValueDTO> list = this.service.GetExamStudents(examId, classroomId);
            if (list == null)
                return BadRequest("No data found.");
            return Ok(list);
        }

        /// <summary>
        /// This will give commulative result list of all classroom
        /// Access only to teachers and admin
        /// </summary>
        [HttpGet]
        [Route("GetExamClassrooms")]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public IHttpActionResult GetExamClassrooms(int examId)
        {
            ResultClassroomsDTO data = this.service.GetExamClassrooms(examId, this.requestInfo.UserId);
            if (data == null)
                return BadRequest("No data found.");

            return Ok(data);
        }

        /// <summary>
        /// This will give commulative result list of all students of classroom
        /// </summary>
        [HttpGet]
        [Route("GetCommulativeResult")]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public IHttpActionResult GetCommulativeResult(int examId, int classroomId)
        {
            List<StudentResultCommulativeDTO> list = this.service.GetStudentCommulativeResultList(examId, classroomId);
            return Ok(list);
        }

        /// <summary>
        /// This will save remarks for all students of a classroom 
        /// </summary>
        [HttpPost]
        [Route("SaveClassroomResult")]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public IHttpActionResult SaveClassroomResult(SaveClassroomResult model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                if (this.requestInfo.Role.Equals(UserRoles.Teacher.ToString()))
                {
                    if (!this.classroomService.IsUserClassTeacher(this.requestInfo.UserId, model.ClassroomId))
                    {
                        return BadRequest("Only class teachers are allowed to save results of their classroom");
                    }
                }
                this.service.SaveClassroomResult(model.examId, model.ClassroomId, model.results);
                return Ok(this.service.GetStudentCommulativeResultList(model.examId, model.ClassroomId));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Save individual classroom's result. Access only to classteachers
        /// </summary>
        [HttpPost]
        [Route("PublishClassroomResult")]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public IHttpActionResult PublishClassroomResult(PublishClassroomResults data)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                if (this.requestInfo.Role.Equals(UserRoles.Teacher.ToString()))
                {
                    if (!this.classroomService.IsUserClassTeacher(this.requestInfo.UserId, data.ClassroomId))
                    {
                        return BadRequest("Only class teachers are allowed to save results of their classroom");
                    }
                }
                this.service.PublishClassroomResult(data.examId, data.ClassroomId);
                return Ok(this.service.GetStudentCommulativeResultList(data.examId, data.ClassroomId));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Publish result to all 
        /// </summary>
        [HttpPost]
        [Route("PublishResult")]
        [AuthorizeRoles(UserRoles.Admin)]
        public IHttpActionResult PublishResult(int examId)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                if (!this.service.IsExamPublishable(examId))
                {
                    return BadRequest("There are some unpublished classrooms");
                }
                this.service.PublishResult(examId);
                return Ok(this.service.GetExamClassrooms(examId, this.requestInfo.UserId));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// View of single student. Student will be selected from dropdown
        /// </summary>
        [HttpGet]
        [Route("GetStudentPastResult")]
        [Authorize]
        public IHttpActionResult GetStudentPastResult(int studentId)
        {
            if (this.requestInfo.Role.Equals(UserRoles.Parent.ToString()))
            {
                if (!this.parentService.HaveParentChildRelation(this.requestInfo.UserId, studentId))
                    return BadRequest("Parent can only check his/her child records");
            }
            List<StudentResultMasterDTO> list = this.service.GetStudentPastResult(studentId);
            return Ok(list);
        }
    }
}
