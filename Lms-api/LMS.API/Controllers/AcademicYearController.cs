﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LMS.Core.Entity;
using LMS.Core.DTO;
using LMS.Core.Attribute;
using LMS.Core.Enums;
using LMS.Core.IService;
using System.Threading.Tasks;
using LMS.Common.Helper;

namespace LMS.API.Controllers
{
    [RoutePrefix("api/AcademicYear")]
    [AuthorizeRoles(UserRoles.Admin)]
    public class AcademicYearController : BaseController<IAcademicYearService, AcademicYearDTO, AcademicYear>
    {
        public AcademicYearController(IAcademicYearService service) 
            : base(service)
        {

        }

        /// <summary>
        /// Get Current academic year 
        /// </summary>
        /// <returns>Academic year</returns>
        [HttpGet]
        [Route("GetCurrentYear")]
        public AcademicYearDTO GetCurrentYear()
        {
            return this.Service.GetCurrentYear();
        }
    }
}
