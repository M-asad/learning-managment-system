﻿using System.Web.Http;
using System.Threading.Tasks;
using LMS.API.Models;
using Microsoft.AspNet.Identity;
using LMS.API.Infrastructure;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System;
using LMS.Core.Helpers;
using LMS.Core.DTO;
using System.Net;
using LMS.Core.IService;
using LMS.Core.Infrastructure;
using LMS.Core.Entity;
using LMS.Core.Enums;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;

namespace LMS.API.Controllers
{
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        //private AuthRepository _repo = null;
        private IUserService userService;
        private IExceptionHelper exceptionHelper;
        private IRequestInfo requestInfo;

        public AccountController(IUserService userService, IExceptionHelper exceptionHelper, IRequestInfo requestInfo)
        {
            this.userService = userService;
            this.exceptionHelper = exceptionHelper;
            this.requestInfo = requestInfo;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("Login")]
        public async Task<IHttpActionResult> Login(UserLoginModel loginModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                string BaseURL = Request.RequestUri.GetLeftPart(System.UriPartial.Authority);
                // Call post rquest here
                var oAuthData = new List<KeyValuePair<string, string>>
                { 
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", loginModel.Username),
                    new KeyValuePair<string, string>("password", loginModel.Password)
                };

                TokenResponse response = await WebHelper.PostFormUrlEncoded<TokenResponse>(BaseURL + "/api/token", oAuthData);
                if (response.Error != null)
                {
                    this.exceptionHelper.ThrowAPIException(HttpStatusCode.ExpectationFailed, response.Error);
                    return null;
                }
                return Ok(response);
            }
            catch
            {
                return Unauthorized();
            }
        }

        [Route("RefreshToken")]
        public async Task<IHttpActionResult> RefreshUserToken(string refreshToken)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                string BaseURL = Request.RequestUri.GetLeftPart(System.UriPartial.Authority);
                // Call post rquest here
                var oAuthData = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("grant_type", "refresh_token"),
                    new KeyValuePair<string, string>("refresh_token", refreshToken)
                };

                TokenResponse response = await WebHelper.PostFormUrlEncoded<TokenResponse>(BaseURL + "/api/token", oAuthData);
                if (response.Error != null)
                {
                    this.exceptionHelper.ThrowAPIException(HttpStatusCode.ExpectationFailed, response.Error);
                    return null;
                }
                return Ok(response);
            }
            catch
            {
                return Unauthorized();
            }
        }

        [HttpGet]
        public HttpResponseMessage Angular()
        {
            try
            {
                var response = new HttpResponseMessage();
                response.Content = new StringContent(System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/index.html")));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                return response;
            }
            catch
            {
                return null;
            }
        }

        // POST: /Account/ForgotPassword
        [HttpPost]
        [Route("ForgotPassword")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ForgotPassword(string email)
        {
            if (ModelState.IsValid)
            {
                ApplicationUserManager manager = new ApplicationUserManager(new ApplicationUserStore(this.requestInfo.Context), new EmailService());
                var user = await manager.FindByEmailAsync(email);
                //if (user == null || !(await AppUserManager.IsEmailConfirmedAsync(user.Id)))
                if (user == null)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return BadRequest("Either user does not exist or you have not confirmed your email.");
                }

                try
                {
                    // Send an email with this link
                    string code = await manager.GeneratePasswordResetTokenAsync(user.Id);
                    // var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);
                    var callbackUrl = ConfigManager.ResetPasswordURL + "?userid=" + user.Id + "&code=" + code;
                    await manager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                    return Ok();
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
            return BadRequest();
        }

        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [Route("ResetPassword")]
        public async Task<IHttpActionResult> ResetPassword(ForgotPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            ApplicationUserManager manager = new ApplicationUserManager(new ApplicationUserStore(this.requestInfo.Context), new EmailService());
            var user = await manager.FindByIdAsync(model.UserId);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return BadRequest();
            }
            var result = await manager.ResetPasswordAsync(model.UserId, model.Code, model.Password);
            if (result.Succeeded)
            {
                return Ok();
            }
            return BadRequest(result.Errors.FirstOrDefault());
        }

        // POST: /Account/ChangePassword
        [HttpPost]
        [Authorize]
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordModel model)
        {
            ApplicationUserManager manager = new ApplicationUserManager(new ApplicationUserStore(this.requestInfo.Context));            
            var result = await manager.ChangePasswordAsync(model.UserId, model.CurrentPassword, model.NewPassword);
            if (result.Succeeded)
                return Ok();
            return BadRequest(result.Errors.FirstOrDefault());
        }

        [Authorize]
        [Route("GetProfile")]
        public IHttpActionResult GetUserProfile()
        {
            return Ok(new ProfileDTO()
            {
                UserId = "123",
                FullName = "Syed Mohsin",
                Picture = "Picture/default.png",
                Role = "SuperAdmin"
            });
        }
    }
}