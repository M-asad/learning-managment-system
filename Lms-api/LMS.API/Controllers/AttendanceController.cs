﻿using LMS.Core.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using LMS.Core.Helpers;
using LMS.Core.Attribute;
using LMS.Core.Enums;
using LMS.Core.IService;
using System.Threading.Tasks;
using LMS.Core.Infrastructure;

namespace LMS.API.Controllers
{
    [Authorize]
    [RoutePrefix("api/Attendance")]
    public class AttendanceController : BaseController
    {
        IAttendanceService service;
        IClassroomService classroomService;
        IStudentService studentService;
        IRequestInfo requestInfo;

        public AttendanceController(IAttendanceService service, IRequestInfo requestInfo, IClassroomService classroomService, IStudentService studentService)
        {
            this.service = service;
            this.classroomService = classroomService;
            this.studentService = studentService;
            this.requestInfo = requestInfo;
        }

        /// <summary>
        /// Give weeks attendance of all students of a classroom
        /// </summary>
        [HttpGet]
        [Route("GetCalendar")]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public async Task<IHttpActionResult> GetCalendar(int ClassroomId, DateTime Date)
        {
            // Get week from start date
            DateTime FromDate, ToDate;
            int dayOfWeek = (int)(Date.DayOfWeek + 6) % 7;
            FromDate = Date.AddDays(-1 * dayOfWeek);
            ToDate = Date.AddDays(6 - dayOfWeek);

            var list = await this.service.GetAttendanceCalendar(ClassroomId, FromDate, ToDate);
            return Ok(list);
        }

        /// <summary>
        /// Give months attendance of a student
        /// </summary>
        [HttpGet]
        [Authorize]
        [Route("GetStudentCalendar")]
        public async Task<IHttpActionResult> GetStudentCalendar(int studentID, int Year, int Month)
        {
            var student = await this.studentService.GetAsync(studentID);
            if (student == null)
                return BadRequest("Student not found");
            var list = this.service.GetStudentCalendar(student, Year, Month);
            return Ok(list);
        }

        /// <summary>
        /// Shows all attendance of all enrolled student of a classroom for update page
        /// </summary>
        [HttpGet]
        [Route("")]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public async Task<IHttpActionResult> Get(int ClassroomId, DateTime Date)
        {
            var classroom = await this.classroomService.GetAsync(ClassroomId);
            if (classroom == null)
            {
                return BadRequest("Classroom not found");
            }
            var data = this.service.GetAttendance(ClassroomId, Date, true);
            return Ok(data);
        }

        /// <summary>
        /// Saves/Updates attendance of a classroom
        /// </summary>
        /// <param name="attendance"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public IHttpActionResult Create(SaveClassAttendanceDTO attendance)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                if (this.requestInfo.Role.Equals(UserRoles.Teacher.ToString()))
                {
                    if (!this.classroomService.IsUserClassTeacher(this.requestInfo.UserId, attendance.ClassroomId))
                    {
                        return BadRequest("Only class teachers are allowed to save attendance of their classroom");
                    }
                }
                if (attendance.AttendanceDate > DateTime.Now)
                {
                    ModelState.AddModelError("", "Addendance on future date is not allowed");
                    return BadRequest(ModelState);
                }
                this.service.SaveAttendance(attendance);
                return Ok(this.service.GetAttendance(attendance.ClassroomId, attendance.AttendanceDate, true));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        /// <summary>
        /// Saves/Updates Months attendance of a student 
        /// </summary>
        /// <param name="attendance"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveStudentCalendar")]
        [AuthorizeRoles(UserRoles.Admin)]
        public async Task<IHttpActionResult> SaveStudentCalendar(AttendanceCalendarDTO attendance)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var student = await this.studentService.GetAsync(attendance.StudentId);
                if (student == null)
                {
                    ModelState.AddModelError("", "No Student found.");
                    return BadRequest(ModelState);
                }
                var changedAttendance = attendance.Attendance.Where(x => x.AttendanceID != 0 || x.AttendanceType != "NA").ToList();
                if (changedAttendance != null && changedAttendance.Count > 0 && changedAttendance.Max(x => x.Date) > DateTime.Now)
                {
                    ModelState.AddModelError("", "Addendance on future date is not allowed");
                    return BadRequest(ModelState);
                }
                this.service.SaveStudentCalendar(attendance);
                return Ok(this.service.GetStudentCalendar(student, attendance.Attendance[0].Date.Year, attendance.Attendance[0].Date.Month));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [Route("Update")]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public IHttpActionResult Update(AttendanceUpdateDTO attendance)
        {
            try
            {
                // * #RoleUpdates
                // * where is it used
                 
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                this.service.UpdateAttendance(attendance);
                return Ok(attendance);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}