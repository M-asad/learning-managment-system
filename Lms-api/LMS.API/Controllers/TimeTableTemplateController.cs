﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LMS.Core.DTO;
using LMS.Core.Entity;
using System.Net.Http;
using System.Web.Http;
using System.Net;
using LMS.Core.IService;
using System.Threading.Tasks;

namespace LMS.API.Controllers
{
    [RoutePrefix("api/timeTableTemplate")]
    public class TimeTableTemplateController : BaseController<ITimeTableTemplateService, TimeTableTemplateDTO, TimeTableTemplate>
    {
        public TimeTableTemplateController(ITimeTableTemplateService service)
                    : base(service)
        {
        }
        /*
        [HttpGet]
        [Route("GetTimeTableTemplates")]
        public HttpResponseMessage GetTimeTableTemplates(int Id)
        {
            var data = this.Service.Get(x => x.TemplateID == Id && x.IsActive);
            if (data == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No data found.");
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [HttpPost]
        public IHttpActionResult Create(TimeTableTemplateDTO model)
        {
            try
            {
                model.UserID = GetCurrentUserID();
                repo.Add(model.ConvertToEntity());
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPut]
        public IHttpActionResult Update([FromBody]int id, string title)
        {
            var data = repo.Get(x => x.TemplateID == id && x.IsActive);
            if (data == null)
                return NotFound();
            data.Title = title;
            repo.Update(data);

            return Ok();
        }
        */

        [HttpPut]
        [Route("UpdateSchedule")]
        public async Task<TimeTableTemplateDetailDTO> UpdateSchedule(TimeTableTemplateDetailDTO dtoObject)
        {
            return await this.Service.UpdateDetailAsync(dtoObject);
        }
    }
}