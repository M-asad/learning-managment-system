﻿using LMS.Core.IService;
using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.Infrastructure;
using System.Web.Http;
using LMS.Core.Attribute;

namespace LMS.API.Controllers
{
    [RoutePrefix("api/Diary")]
    [AuthorizeRoles]
    public class DiaryController : BaseController<IDiaryService, DiaryDTO, Diary>
    {
        public DiaryController(IDiaryService service, IRequestInfo requestInfo)
            : base(service)
        {
        }
    }
}
