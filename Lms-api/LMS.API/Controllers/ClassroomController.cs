﻿using LMS.Core.DTO;
using System.Collections.Generic;
using System.Web.Http;
using LMS.Core.Entity;
using LMS.Core.Attribute;
using LMS.Core.Enums;
using LMS.Core.Infrastructure;
using LMS.Core.IService;
using System.Threading.Tasks;

namespace LMS.API.Controllers
{
    [RoutePrefix("api/Classroom")]
    [Authorize]
    public class ClassroomController : BaseController<IClassroomService, ClassroomDTO, Classroom>
    {
        IClassroomStudentService ClassroomStudentService;
        IClassroomCourseService ClassroomCourseService;
        public ClassroomController(IClassroomService service, IRequestInfo requestInfo, IClassroomStudentService classroomStudentService, IClassroomCourseService classroomCourseService) 
            : base(service)
        {
            this.ClassroomStudentService = classroomStudentService;
            this.ClassroomCourseService = classroomCourseService;
        }

        /// <summary>
        /// Enrolled students of a classroom
        /// </summary>
        /// <param name="classroomId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudents/{classroomId}")]
        [AuthorizeRoles(UserRoles.Admin)]
        public IHttpActionResult GetStudents(int classroomId)
        {
            // TODO: student or selectItem or Student simplified dto 
            var list = this.Service.GetStudents(classroomId);
            return Ok(list);
        }

        /// <summary>
        /// Give classroom detail with enrolled students, courses and teachers
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public IHttpActionResult Get(int id)
        {
            var data = this.Service.GetClassroomDetail(id);
            return Ok(data);
        }

        [HttpGet]
        [Route("GetClassroomMaster/{id}")]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public IHttpActionResult GetClassroomMaster(int id)
        {
            var data = this.Service.GetClassroomMaster(id);
            return Ok(data);
        }

        /// <summary>
        /// Get classrooms of current year
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetCurrentClasses")]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public IHttpActionResult GetCurrentClasses()
        {
            List<ClassroomListDTO> list = this.Service.GetCurrentYearClasses();
            return Ok(list);
        }

        /// <summary>
        /// Get Classes by Academic year 
        /// </summary>
        /// <param name="academicYearId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetClasses/{academicYearId}")]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public IHttpActionResult GetClasses(int academicYearId)
        {
            List<ClassroomListDTO> list = this.Service.GetClasses(academicYearId);
            return Ok(list);
        }

        /// <summary>
        /// Get Current classes for dropdown list 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetCurrentClassesddl")]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public IHttpActionResult GetCurrentClassesddl()
        {
            var list = this.Service.GetCurrentClassesddl();
            return Ok(list);
        }

        [HttpGet]
        [Route("GetClassroomCourses/{id}")]
        public IHttpActionResult GetClassroomCourses(int id)
        {
            List<string> list = this.Service.GetClassroomCourses(id);
            return Ok(list);
        }

        [HttpPost]
        [Route("EnrollStudent")]
        public async Task<ClassroomStudentDTO> EnrollStudent(EnrollStudentDTO dto)
        {
            return await this.ClassroomStudentService.EnrollStudent(dto);
        }

        [HttpPost]
        [Route("RemoveStudent")]
        public async Task<IHttpActionResult> RemoveStudent(int Id)
        {
            await this.ClassroomStudentService.DeleteAsync(Id);
            return Ok();
        }

        [HttpPost]
        [Route("AssignCourse")]
        public async Task<ClassroomCourseDTO> AssignCourse(ClassroomCourseDTO dto)
        {
            return await this.ClassroomCourseService.CreateAsync(dto);
        }

        [HttpPost]
        [Route("RemoveCourse")]
        public async Task<IHttpActionResult> RemoveCourse(int Id)
        {
            await this.ClassroomCourseService.DeleteAsync(Id);
            return Ok();
        }

        /// <summary>
        /// Give section to show on classroom add page 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSection/{classroomId}")]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public IHttpActionResult GetSection(int classroomId)
        {
            var data = this.Service.GetSection(classroomId);
            return Ok(data);
        }
    }
}
