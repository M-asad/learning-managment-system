﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LMS.Core.DTO;
using System.Net.Http;
using LMS.Core.Entity;
using LMS.Core.IService;

namespace LMS.API.Controllers
{
    [Authorize]
    [RoutePrefix("api/timetable")]
    public class TimetableController : BaseController<ITimeTableService, TimeTableDTO, TimeTable>
    {
        public TimetableController(ITimeTableService service)
                    : base(service)
        {
        }

        //[HttpGet]
        //[Route("GetTimeTableTemplates")]
        //public HttpResponseMessage GetTimeTableTemplates(int Id)
        //{
        //    return repo.Get(x=>x.TimeTableID == Id);
        //}
    }
}