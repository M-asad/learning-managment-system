﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LMS.API.Models;
using LMS.Core.IService;
using LMS.Core.Infrastructure;
using System.Threading.Tasks;

namespace LMS.API.Controllers
{
    [RoutePrefix("Api/General")]
    [Authorize]
    public class GeneralController : BaseController
    {
        IClassService classService;
        ISectionService sectionService;
        IExamTypeService examTypeService;
        ICourseService courseService;
        IGradeService gradeService;
        IRequestInfo requestInfo;
        public GeneralController(IClassService classService, 
            ISectionService sectionService, 
            IExamTypeService examTypeService, 
            ICourseService courseService, 
            IGradeService gradeService,
            IRequestInfo requestInfo)
        {
            this.classService = classService;
            this.sectionService = sectionService;
            this.examTypeService = examTypeService;
            this.courseService = courseService;
            this.gradeService = gradeService;
            this.requestInfo = requestInfo;
        }

        [HttpGet]
        [Route("Classes")]
        public async Task<IHttpActionResult> Classes()
        {
            return Ok(await classService.GetAllAsync());
        }

        [HttpGet]
        [Route("Sections")]
        public async Task<IHttpActionResult> Sections()
        {
            return Ok(await sectionService.GetAllAsync());
        }

        [HttpGet]
        [Route("Courses")]
        public async Task<IHttpActionResult> Courses()
        {
            return Ok(await courseService.GetAllAsync());
        }

        [HttpGet]
        [Route("Grades")]
        public async Task<IHttpActionResult> Grades()
        {
            return Ok(await gradeService.GetAllAsync());
        }
    }
}
