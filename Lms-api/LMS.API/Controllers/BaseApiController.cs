﻿using LMS.API.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using LMS.API.Models;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Web;

namespace LMS.API.Controllers
{
/*    public class BaseController : ApiController
    {
        private int _userId;
        public int UserId
        {
            get
            {
                if(_userId == 0)
                    _userId = Convert.ToInt32(this.GetValueFromClaims(ClaimTypes.UserData));
                return _userId;
            }
        }

        public string Role
        {
            get
            {
                return this.GetValueFromClaims(ClaimTypes.Role);
            }
        }

        #region Private Funcltions
        private string GetValueFromClaims(string key)
        {
            if (HttpContext.Current == null || HttpContext.Current.User == null || HttpContext.Current.User.Identity == null)
            {
                return string.Empty;
            }

            var claims = (HttpContext.Current.User.Identity as ClaimsIdentity).Claims;
            var value = string.Empty;

            if (claims != null && claims.Count() > 0)
            {
                value = claims.FirstOrDefault(x => x.Type == key).Value;
            }

            return value;
        }
        #endregion

        //protected int Username = 0;
        //public BaseController()
        //{
        //    var loggedInUser = new AuthRepository().GetUserByID(Convert.ToInt32(User.Identity.GetUserId()));
        //    Username = loggedInUser == null ? 0 : loggedInUser.Id;
        //}

        private ApplicationRoleManager _AppRoleManager = null;
        private ApplicationUserManager _AppUserManager = null;

        protected ApplicationRoleManager AppRoleManager
        {
            get
            {
                return _AppRoleManager ?? Request.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
        }

        protected ApplicationUserManager AppUserManager
        {
            get
            {
                return _AppUserManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        protected IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        protected string GenerateUniqueUsername(string firstName, string lastName)
        {
            string username = firstName + "." + lastName;
            bool IsUnique = false;

            while (!IsUnique)
            {
                var user = _AppUserManager.Users.Where(x => x.UserName == username).FirstOrDefault();
                if (user == null)
                {
                    IsUnique = true;
                }
                else
                {
                    Random random = new Random();
                    username = firstName + "." + lastName + random.Next(1, 50);
                }
            }
            return username;
        }

        //protected ApplicationUser GetCurrentUser()
        //{
        //    // return new AuthRepository().GetUserByID(User.Identity.GetUserId());
        //    var user = AppUserManager.FindByName(User.Identity.Name);
        //    return user;
        //}

        //protected int GetCurrentUserId()
        //{
        //    var user = AppUserManager.FindByName(User.Identity.Name);
        //    return user.Id;
        //}

        //protected string GetCurrentUserName()
        //{
        //    return User.Identity.Name;
        //}


        protected string GetUserRole()
        {
            return AppUserManager.GetRoles(UserId).FirstOrDefault();
        }


    }
    */
}
