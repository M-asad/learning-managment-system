﻿using LMS.Common.Helper;
using LMS.Core.Attribute;
using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.Enums;
using LMS.Core.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace LMS.API.Controllers
{
    [RoutePrefix("api/User")]
    [AuthorizeRoles(UserRoles.Admin)]
    public class UserController : BaseController<IUserService, UserDTO, ApplicationUser>
    {
        public UserController(IUserService service)
                    : base(service)
        {
        }

        public override Task<UserDTO> Get(int id)
        {
            return base.Get(id);
        }

        public override Task<UserDTO> Post(UserDTO dtoObject)
        {
            return base.Post(dtoObject);
        }

        public override Task<UserDTO> Put(UserDTO dtoObject)
        {
            return base.Put(dtoObject);
        }

        public override Task Delete(int id)
        {
            return base.Delete(id);
        }

        [HttpPost]
        [Route("ChangePassword")]
        [Authorize]
        public async Task<ChangePasswordDTO> ChangePassword(ChangePasswordDTO dtoObject)
        {
            return await Service.ChangePassword(dtoObject);
        }

        /// <summary>
        /// Change password of any user. Accessable to SuperAdmin and Admin
        /// </summary>
        /// <param name="dtoObject"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ChangeUserPassword")]
        [AuthorizeRoles(UserRoles.Admin)]
        public async Task<ChangePasswordDTO> ChangeUserPassword(ChangePasswordDTO dtoObject)
        {
            return await Service.ChangeUserPassword(dtoObject);
        }
        
        #region Image
        [HttpPost]
        [Route("Image")]
        public async Task<ResourceDTO> UploadImage(ResourceDTO dtoObject)
        {
            return await this.Service.UploadImage(dtoObject);
        }

        [HttpPut]
        [Route("Image")]
        public async Task<ResourceDTO> UpdateImage(ResourceDTO dtoObject)
        {
            return await this.Service.UpdateImage(dtoObject);
        }

        [HttpGet]
        [Route("Image/{userId}")]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> GetImage(int userId)
        {
            return await this.Service.GetImage(userId);
        }
        #endregion
    }
}