﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using LMS.Core.Entity;
using LMS.Core.DTO;
using LMS.Core.Attribute;
using LMS.Core.Enums;
using LMS.Core.IService;
using System.Threading.Tasks;
using LMS.Common.Helper;
using LMS.API.Models;

namespace LMS.API.Controllers
{
    [RoutePrefix("api/Parent")]
    [AuthorizeRoles(UserRoles.Admin)]
    public class ParentController : BaseController<IParentService, ParentDTO, Parent>
    {
        public ParentController(IParentService service)
                    : base(service)
        {
        }

        /// <summary>
        /// Give all parents for dropdown
        /// </summary>
        /// <returns>SelectListItem</returns>
        [HttpGet]
        [Route("GetSelectList")]
        [AuthorizeRoles(UserRoles.Admin)]
        public async Task<IList<SelectListItem>> GetSelectList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var row in await this.Service.GetAllAsync())
            {
                list.Add(new SelectListItem()
                {
                    Id = row.Id,
                    Text = row.FullName
                });
            }
            return list;
        }

        /// <summary>
        /// Give listitems for autocomplete. SearchContains must be provided in request header
        /// </summary>
        /// <returns>SelectListItem</returns>
        [HttpGet]
        [Route("GetSelectListByPrefix")]
        [AuthorizeRoles(UserRoles.Admin)]
        public async Task<IList<SelectListItem>> GetSelectListByPrefix([FromUri]PrefixModel model)
        {
            var request = new JsonApiRequest();
            request.Filter[Filters.Contains].Add("prefix", model.prefix);
            List<SelectListItem> list = new List<SelectListItem>();
            var result = await this.Service.GetAllAsync(request);
            foreach (var row in result.Data)
            {
                list.Add(new SelectListItem()
                {
                    Id = row.Id,
                    Text = row.FullName
                });
            }
            return list;
        }

        /// <summary>
        /// Returns true if parent have active students 
        /// </summary>
        /// <param name="id">parent id</param>
        [HttpGet]
        [Route("HaveDependencies/{id}")]
        [AuthorizeRoles(UserRoles.Admin)]
        public IHttpActionResult HaveDependencies(int id)
        {
            return Ok(this.Service.HaveDependencies(id));
        }
    }
}
