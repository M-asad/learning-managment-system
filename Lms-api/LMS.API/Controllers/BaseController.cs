﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using LMS.Core.Attribute;
using LMS.Core.IService;
using LMS.Core.DTO;
using LMS.Core.Entity;
using System.Threading.Tasks;
using LMS.Core.Enums;
using LMS.Common.Helper;
using LMS.API.Models;

namespace LMS.API.Controllers
{
    [Authorize]
    [ValidationAttribute]
    public class BaseController : ApiController
    {
    }

    public abstract class BaseController<TService> : BaseController
      where TService : IBaseService
    {
        private TService service;

        public BaseController(TService service)
        {
            this.service = service;
        }

        protected TService Service
        {
            get
            {
                return this.service;
            }
        }

        protected JsonApiRequest GetJsonApiRequest(int pageNumber)
        {
            var request = new JsonApiRequest();
            request.Pagination = new JsonApiPagination()
            {
                PageNumber = pageNumber,
                PageSize = ConfigManager.RecordCount
            };
            return request;
        }

        protected JsonApiRequest GetJsonApiRequest(SearchModel model)
        {
            var request = new JsonApiRequest();
            request.Pagination = new JsonApiPagination()
            {
                PageNumber = model.Page,
                PageSize = ConfigManager.RecordCount
            };
            if (model.Prefix != null)
                request.Filter[Filters.Contains].Add("prefix", model.Prefix);
            return request;
        }
    }

    public abstract class BaseController<TService, TDTO, TEntity, TListDTO> : BaseController<TService>
        where TEntity : IAuditModel, new()
        where TDTO : BaseDTO<TEntity>, new()
        where TService : IBaseService<TDTO, TListDTO>
        where TListDTO : BaseDTO<TEntity>, new()
    {
        private TService service;

        public BaseController(TService service)
            : base(service)
        {
            this.service = service;
        }
        
        [HttpGet]
        [Route("")]
        public virtual Task<IResultSet<TListDTO>> Get([FromUri]SearchModel search)
        {
            var result = this.Service.GetAllAsync(this.GetJsonApiRequest(search));
            return result;
        }

        [HttpGet]
        [Route("{id}")]
        public virtual Task<TDTO> Get(int id)
        {
            return this.service.GetAsync(id);
        }

        [HttpPost]
        [Route("")]
        public virtual Task<TDTO> Post(TDTO dtoObject)
        {
            return this.service.CreateAsync(dtoObject);
        }

        [HttpPut]
        [Route("")]
        public virtual Task<TDTO> Put(TDTO dtoObject)
        {
            return this.service.UpdateAsync(dtoObject);
        }

        [HttpDelete]
        [Route("{id}")]
        public virtual Task Delete(int id)
        {
            return this.service.DeleteAsync(id);
        }
    }
    
    public abstract class BaseController<TService, TDTO, TEntity> : BaseController<TService, TDTO, TEntity, TDTO>
        where TEntity : IAuditModel, new()
        where TDTO : BaseDTO<TEntity>, new()
        where TService : IBaseService<TDTO, TDTO>
    {
        private TService service;

        public BaseController(TService service)
            : base(service)
        {
            this.service = service;
        }
    }
}