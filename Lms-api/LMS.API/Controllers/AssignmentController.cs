﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LMS.Core.Entity;
using LMS.Core.DTO;
using LMS.Core.Enums;
using LMS.Core.Attribute;
using LMS.Core.IService;
using System.Threading.Tasks;
using LMS.API.Models;

namespace LMS.API.Controllers
{
    [RoutePrefix("api/Assignment")]
    [Authorize]
    public class AssignmentController : BaseController<IAssignmentService, AssignmentDTO, Assignment, AssignmentListDTO>
    {
        public AssignmentController(IAssignmentService service)
            : base(service)
        {
        }

        [HttpGet]
        [Route("")]
        public Task<IResultSet<AssignmentListDTO>> Get([FromUri]SearchModel search)
        {
            var request = this.GetJsonApiRequest(search);
            return this.Service.GetAllAsync(request);
        }

        /// <summary>
        /// Gives assignments of a student. StudentId should be passed
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentAssignments")]
        public IResultSet<AssignmentListDTO> GetStudentAssignments([FromUri]AssignmentSearchModel search)
        {
            var request = this.GetJsonApiRequest(search);
            return this.Service.GetStudentAssignments(request, search.StudentId);
        }

        [HttpGet]
        [Route("{id}")]
        public async override Task<AssignmentDTO> Get(int id)
        {
            return await this.Service.GetAsync(id);
        }

        [HttpPost]
        [Route("")]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public async Task<AssignmentDTO> Post(AssignmentDTO asg)
        {
            return await this.Service.CreateAsync(asg);
        }

        /// <summary>
        /// Assignment marks submission by teacher. Only Assignment Id and details needs to be sent
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("SubmitResult")]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public async Task<AssignmentDTO> SubmitResult(AssignmentDTO asg)
        {
            return await this.Service.SubmitResult(asg);
        }

        [HttpPut]
        [Route("")]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public async Task<AssignmentDTO> Put(AssignmentDTO asg)
        {
            return await this.Service.UpdateAsync(asg);
        }

        [HttpDelete]
        [Route("{id}")]
        [AuthorizeRoles(UserRoles.Admin, UserRoles.Teacher)]
        public async override Task Delete(int id)
        {
            await this.Service.DeleteAsync(id);
        }
    }
}
