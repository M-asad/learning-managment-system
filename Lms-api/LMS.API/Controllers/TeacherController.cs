﻿using LMS.API.Models;
using LMS.Common.Helper;
using LMS.Core.Attribute;
using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.Enums;
using LMS.Core.IService;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace LMS.API.Controllers
{
    [RoutePrefix("api/Teacher")]
    [AuthorizeRoles(UserRoles.Admin)]
    public class TeacherController : BaseController<ITeacherService, TeacherDTO, Teacher>
    {
        public TeacherController(ITeacherService service)
                    : base(service)
        {
        }

        /// <summary>
        /// Give all teachers for dropdown
        /// </summary>
        /// <returns>SelectListItem</returns>
        [HttpGet]
        [Route("GetSelectList")]
        [AuthorizeRoles(UserRoles.Admin)]
        public async Task<IList<SelectListItem>> GetSelectList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var row in await this.Service.GetAllAsync())
            {
                list.Add(new SelectListItem()
                {
                    Id = row.Id,
                    Text = row.FullName
                });
            }
            return list;
        }

        /// <summary>
        /// Give listitems for autocomplete. SearchContains must be provided in request header
        /// </summary>
        /// <returns>SelectListItem</returns>
        [HttpGet]
        [Route("GetSelectListByPrefix")]
        [AuthorizeRoles(UserRoles.Admin)]
        public async Task<IList<SelectListItem>> GetSelectListByPrefix([FromUri]SearchModel search)
        {
            var request = new JsonApiRequest();
            request.Filter[Filters.Contains].Add("prefix", search.Prefix);
            List<SelectListItem> list = new List<SelectListItem>();
            var result = await this.Service.GetAllAsync(request);
            foreach (var row in result.Data)
            {
                list.Add(new SelectListItem()
                {
                    Id = row.Id,
                    Text = row.FullName
                });
            }
            return list;
        }

        [HttpGet]
        [Route("HaveDependencies/{id}")]
        [AuthorizeRoles(UserRoles.Admin)]
        public IHttpActionResult HaveDependencies(int id)
        {
            return Ok(this.Service.HaveDependencies(id));
        }
    }
}
