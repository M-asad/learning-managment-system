﻿using LMS.API.Infrastructure;
using LMS.API.Models;
using LMS.Core.Entity;
using LMS.Core.DTO;
using LMS.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LMS.Core.Attribute;
using LMS.Core.Enums;
using LMS.Core.Infrastructure;
using LMS.Core.IService;
using System.Threading.Tasks;
using LMS.Common.Helper;

namespace LMS.API.Controllers
{
    [RoutePrefix("api/Exam")]
    [Authorize]
    public class ExamController : BaseController
    {
        IExamService service;
        IRequestInfo requestInfo;

        public ExamController(IExamService service, IRequestInfo requestInfo)
        {
            this.service = service;
            this.requestInfo = requestInfo;
        }

        /// <summary>
        /// Gets exam list. note: use only one filterContain 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AuthorizeRoles(UserRoles.Admin)]
        [Route("")]
        public HttpResponseMessage Get([FromUri]SearchModel model)
        {
            int pageCount = 0;
            List<ExamListDTO> list = this.service.GetExamList(model.Prefix, ConfigManager.RecordCount, model.Page, out pageCount);
            ResultSet<ExamListDTO> result = new ResultSet<ExamListDTO>()
            {
                Data = list,
                PageCount = pageCount
            };
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpGet]
        [Authorize]
        [Route("{id}")]
        public HttpResponseMessage Get(int id)
        {
            ExamDetailDTO data = this.service.GetExamDetail(id);
            if (data == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No data found.");
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [HttpGet]
        [Authorize]
        [Route("GetExamMaster/{id}")]
        public HttpResponseMessage GetExamMaster(int id)
        {
            ExamListDTO data = this.service.GetExamMaster(id);
            if (data == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No data found.");
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [HttpGet]
        [Authorize]
        [Route("GetCurrentYearExams")]
        public HttpResponseMessage GetCurrentYearExams()
        {
            List<KeyValueDTO> list = this.service.GetCurrentYearExam();
            if (list == null || list.Count == 0)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No data found.");
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [HttpGet]
        [Authorize]
        [Route("GetExamCourses/{Id}")]
        public HttpResponseMessage GetExamCourses(int Id)
        {
            List<KeyValueDTO> list = this.service.GetExamCourses(Id);
            if (list == null || list.Count == 0)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No data found.");
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [HttpGet]
        [Authorize]
        [Route("GetCurrentYearClasses")]
        public HttpResponseMessage GetCurrentYearClasses()
        {
            List<KeyValueDTO> list = this.service.GetCurrentYearClasses();
            if (list == null || list.Count == 0)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No data found.");
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [HttpPost]
        [AuthorizeRoles(UserRoles.Admin)]
        [Route("")]
        public async Task<IHttpActionResult> Create(ExamDTO dto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            
            if (dto.ClassIds == null || dto.ClassIds.Length < 1)
            {
                ModelState.AddModelError("", "Classes are required");
                return BadRequest(ModelState);
            }
            dto = await this.service.CreateAsync(dto);
            return Ok(this.service.GetExamDetail(dto.Id));
        }

        [HttpPut]
        [AuthorizeRoles(UserRoles.Admin)]
        [Route("")]
        public async Task<IHttpActionResult> Update(ExamDTO dto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (dto.ClassIds == null || dto.ClassIds.Length < 1)
            {
                ModelState.AddModelError("", "Classes are required");
                return BadRequest(ModelState);
            }
            dto = await this.service.UpdateAsync(dto);
            return Ok(this.service.GetExamDetail(dto.Id));
        }

        [HttpPost]
        [Route("SaveDetail")]
        [AuthorizeRoles(UserRoles.Admin)]
        public IHttpActionResult SaveDetail(ExamDetailSaveDTO dto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(this.service.SaveDetail(dto));
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task Delete(int id)
        {
            await this.service.DeleteAsync(id);
        }
    }
}
