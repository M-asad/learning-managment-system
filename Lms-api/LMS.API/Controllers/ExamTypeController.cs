﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using LMS.Core.Entity;
using LMS.Core.DTO;
using LMS.Core.Attribute;
using LMS.Core.Enums;
using LMS.Core.IService;
using System.Threading.Tasks;
using LMS.Common.Helper;
using LMS.API.Models;

namespace LMS.API.Controllers
{
    [RoutePrefix("api/ExamType")]
    [AuthorizeRoles(UserRoles.Admin)]
    public class ExamTypeController : BaseController<IExamTypeService, ExamTypeDTO, ExamType>
    {
        public ExamTypeController(IExamTypeService service)
                    : base(service)
        {
        }

        /// <summary>
        /// Give all ExamTypes for dropdown
        /// </summary>
        /// <returns>SelectListItem</returns>
        [HttpGet]
        [Route("GetSelectList")]
        [AuthorizeRoles(UserRoles.Admin)]
        public async Task<IList<SelectListItem>> GetSelectList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var row in await this.Service.GetAllAsync())
            {
                list.Add(new SelectListItem()
                {
                    Id = row.Id,
                    Text = row.Type
                });
            }
            return list;
        }
    }
}
