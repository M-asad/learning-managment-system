﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.API
{
    public class ConfigManager
    {
        private static int? _recordCount = null;
        public static int RecordCount
        {
            get
            {
                if (_recordCount == null)
                {
                    _recordCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["RecordCount"]);
                }
                return _recordCount ?? 0;
            }
        }

        private static int? _autoCompleteRecordCount = null;
        public static int AutoCompleteRecordCount
        {
            get
            {
                if (_autoCompleteRecordCount == null)
                {
                    _autoCompleteRecordCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["AutoCompleteRecordCount"]);
                }
                return _autoCompleteRecordCount ?? 0;
            }
        }

        private static string _resetPasswordURL = string.Empty;
        public static string ResetPasswordURL
        {
            get
            {
                if (string.IsNullOrEmpty(_resetPasswordURL))
                {
                    _resetPasswordURL = System.Configuration.ConfigurationManager.AppSettings["ResetPasswordURL"];
                }
                return _resetPasswordURL;
            }
        }
    }
}