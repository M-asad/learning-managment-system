﻿using LMS.API.Infrastructure;
using LMS.Core.Constant;
using LMS.Core.DTO;
using LMS.Core.IService;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace LMS.API.Provider
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        private IUserService userService;

        public AuthorizationServerProvider(IUserService userService)
        {
            this.userService = userService;
        }

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            UserDTO user = await this.userService.ValidateUserAsync(context.UserName, context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "Invalid");
                return;
            }

            //context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            string userId = Convert.ToString(user.Id);
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim(General.ClaimsUserId, userId));
            identity.AddClaim(new Claim(General.ClaimsUserName, user.UserName));
            identity.AddClaim(new Claim(ClaimTypes.Role, user.Role));
            identity.AddClaim(new Claim(General.ClaimsRoleId, user.RoleId.ToString()));
            identity.AddClaim(new Claim(General.ClaimsRoleName, user.Role));

            // To send extra details with token
            var props = new AuthenticationProperties(new Dictionary<string, string>
            {
                { "role_name", user.Role }
            });
            var ticket = new AuthenticationTicket(identity, props);
            //var properties = new AuthenticationProperties();
            //var ticket = new AuthenticationTicket(identity, properties);
            context.Validated(ticket);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }
    }
}