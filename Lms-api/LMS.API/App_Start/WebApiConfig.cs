﻿using LMS.API.Infrastructure;
using LMS.Core;
using Newtonsoft.Json.Serialization;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using LMS.API.Provider;
using System.Web.Http.ExceptionHandling;

namespace LMS.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //config.MapHttpAttributeRoutes(new CentralizedPrefixProvider("api"));
            config.MapHttpAttributeRoutes();

            var container = LMS.Core.IoC.Container;
            container.RegisterInstance(typeof(IHttpControllerActivator), "ControllerActivator", new UnityHttpControllerActivator(container), new Unity.Lifetime.ExternallyControlledLifetimeManager());
            config.Services.Replace(typeof(IHttpControllerActivator), new UnityHttpControllerActivator(container));
            config.Services.Replace(typeof(IExceptionHandler), new GlobalErrorHandling());

            // This directly makes JSON the default response for a web browser (which sends Accept: text/html)
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new System.Net.Http.Headers.MediaTypeHeaderValue("text/html"));
            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            GlobalConfiguration.Configuration.DependencyResolver = config.DependencyResolver;

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "ActionApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //config.Routes.MapHttpRoute(
            //   "HelpPage_Default",
            //   "Help/{action}/{apiId}",
            //   new { areaName = "HelpPage", controller = "Help", action = "Index", apiId = UrlParameter.Optional });

            config.Routes.MapHttpRoute(
                name: "loginRoutes",
                routeTemplate: "Login",
                defaults: new { controller = "Account", action = "Angular" });

            config.Routes.MapHttpRoute(
               name: "Angular",
               //routeTemplate: "{*path}",
               routeTemplate: "",
               defaults: new { controller = "Account", action = "Angular" });
        }
    }
}
