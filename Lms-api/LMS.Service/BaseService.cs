﻿using LMS.Common.Helper;
using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.IRepository;
using LMS.Core.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Service
{
    public abstract class BaseService : IBaseService
    {
        public BaseService(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
        }

        public IUnitOfWork UnitOfWork { get; private set; }
    }

    public abstract class BaseService<TDTO, TListDTO> : BaseService, IBaseService<TDTO, TListDTO>
    {
        public BaseService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public abstract Task<TDTO> CreateAsync(TDTO dtoObject);

        public abstract Task<IList<TDTO>> CreateAsync(IList<TDTO> dtoObject);

        public abstract Task DeleteAsync(int id);

        public abstract Task<int> GetCount();

        public abstract Task<IList<TDTO>> GetAllAsync();
        
        public abstract Task<IResultSet<TListDTO>> GetAllAsync(JsonApiRequest request);

        public abstract Task<IList<TDTO>> GetAllAsync(IList<int> keys);

        public abstract Task<IResultSet<TDTO>> GetAllAsync(IList<int> keys, JsonApiRequest request);

        public abstract Task<TDTO> GetAsync(int id);

        public abstract Task<TDTO> UpdateAsync(TDTO dtoObject);

        public abstract Task<IList<TDTO>> UpdateAsync(IList<TDTO> dtoObject);
    }

    public abstract class BaseService<TRepository, TEntity, TDTO, TListDTO> : BaseService<TDTO, TListDTO>, IBaseService<TRepository, TEntity, TDTO, TListDTO>
        where TEntity : IAuditModel, new()
        where TDTO : BaseDTO<TEntity>, new()
        where TRepository : IBaseRepository<TEntity>
        where TListDTO : BaseDTO<TEntity>, new()
    {
        private TRepository repository;

        public BaseService(IUnitOfWork unitOfWork, TRepository repository)
            : base(unitOfWork)
        {
            this.repository = repository;
        }

        public TRepository Repository
        {
            get
            {
                return this.repository;
            }
        }

        public async override Task<TDTO> CreateAsync(TDTO dtoObject)
        {
            var result = await this.Create(dtoObject);
            await this.UnitOfWork.SaveAsync();

            dtoObject.ConvertFromEntity(result);
            return dtoObject;
        }

        public async override Task<IList<TDTO>> CreateAsync(IList<TDTO> dtoObjects)
        {
            this.UnitOfWork.DBContext.Configuration.AutoDetectChangesEnabled = false;
            List<TEntity> results = new List<TEntity>();
            int count = 0;
            foreach (TDTO dtoObject in dtoObjects)
            {
                results.Add(await this.Create(dtoObject));
                count++;

                if (count == 100)
                {
                    await this.UnitOfWork.SaveAsync();
                    count = 0;
                }
            }

            await this.UnitOfWork.SaveAsync();
            this.UnitOfWork.DBContext.Configuration.AutoDetectChangesEnabled = true;

            return BaseDTO<TEntity>.ConvertEntityListToDTOList<TDTO>(results);
        }

        public async override Task DeleteAsync(int id)
        {
            await this.Delete(id);
            await this.UnitOfWork.SaveAsync();
        }

        public async Task DeleteAsync(IList<int> ids)
        {
            this.UnitOfWork.DBContext.Configuration.AutoDetectChangesEnabled = false;
            foreach (int id in ids)
            {
                await this.Delete(id);
            }

            await this.UnitOfWork.SaveAsync();
            this.UnitOfWork.DBContext.Configuration.AutoDetectChangesEnabled = true;
        }

        public async override Task<int> GetCount()
        {
            return await this.repository.GetCount();
        }

        public async override Task<IList<TDTO>> GetAllAsync()
        {
            IEnumerable<TEntity> entity = await this.repository.GetAll();
            return BaseDTO<TEntity>.ConvertEntityListToDTOList<TDTO>(entity);
        }

        public async override Task<IResultSet<TListDTO>> GetAllAsync(JsonApiRequest request)
        {
            IResultSet<TEntity> entityResult = await this.repository.GetAll(request);
            IResultSet<TListDTO> result = new ResultSet<TListDTO>();
            result.Data = BaseDTO<TEntity>.ConvertEntityListToDTOList<TListDTO>(entityResult.Data);
            result.PageCount = entityResult.PageCount;
            return result;
        }

        public async override Task<IList<TDTO>> GetAllAsync(IList<int> keys)
        {
            IEnumerable<TEntity> entity = await this.repository.GetAll(keys);
            return BaseDTO<TEntity>.ConvertEntityListToDTOList<TDTO>(entity);
        }

        public async override Task<IResultSet<TDTO>> GetAllAsync(IList<int> keys, JsonApiRequest request)
        {
            IResultSet<TEntity> entityResult = await this.repository.GetAll(keys, request);
            IResultSet<TDTO> result = new ResultSet<TDTO>();
            result.Data = BaseDTO<TEntity>.ConvertEntityListToDTOList<TDTO>(entityResult.Data);
            result.PageCount = entityResult.PageCount;
            return result;
        }

        public async override Task<TDTO> GetAsync(int id)
        {
            TEntity entity = await this.repository.GetAsync(id);
            if (entity == null)
            {
                return null;
            }

            TDTO dto = new TDTO();
            dto.ConvertFromEntity(entity);
            return dto;
        }

        public async override Task<TDTO> UpdateAsync(TDTO dtoObject)
        {
            var result = await this.Update(dtoObject);
            try
            {
                await UnitOfWork.SaveAsync();
            }
            catch (Exception exception)
            {
                throw exception;
            }

            dtoObject.ConvertFromEntity(result);
            return dtoObject;
        }

        public async override Task<IList<TDTO>> UpdateAsync(IList<TDTO> dtoObjects)
        {
            List<TEntity> results = new List<TEntity>();
            this.UnitOfWork.DBContext.Configuration.AutoDetectChangesEnabled = false;
            int count = 0;
            foreach (TDTO dtoObject in dtoObjects)
            {
                results.Add(await this.Update(dtoObject));
                count++;

                if (count == 100)
                {
                    await this.UnitOfWork.SaveAsync();
                    count = 0;
                }
            }

            await this.UnitOfWork.SaveAsync();
            this.UnitOfWork.DBContext.Configuration.AutoDetectChangesEnabled = true;

            return BaseDTO<TEntity>.ConvertEntityListToDTOList<TDTO>(results);
        }

        public async Task<IList<TEntity>> UpdateAsync(IList<TEntity> entityObjects)
        {
            this.UnitOfWork.DBContext.Configuration.AutoDetectChangesEnabled = false;
            int count = 0;
            foreach (var entityObject in entityObjects)
            {
                await this.repository.Update(entityObject);
                count++;

                if (count == 100)
                {
                    await this.UnitOfWork.SaveAsync();
                    count = 0;
                }
            }

            await this.UnitOfWork.SaveAsync();
            this.UnitOfWork.DBContext.Configuration.AutoDetectChangesEnabled = true;

            return entityObjects;
        }

        protected async Task<TEntity> Create(TDTO dtoObject)
        {
            TEntity entity = dtoObject.ConvertToEntity();
            return await this.repository.Create(entity);
        }

        protected async Task<TEntity> Update(TDTO dtoObject)
        {
            var entity = await this.repository.GetDefaultAsync(dtoObject.Id);
            entity = dtoObject.ConvertToEntity(entity);
            return await this.repository.Update(entity);
        }

        protected async Task Delete(int id)
        {
            await this.repository.DeleteAsync(id);
        }
    }
    public abstract class BaseService<TRepository, TEntity, TDTO> : BaseService<TRepository, TEntity, TDTO, TDTO>, IBaseService<TRepository, TEntity, TDTO>
        where TEntity : IAuditModel, new()
        where TDTO : BaseDTO<TEntity>, new()
        where TRepository : IBaseRepository<TEntity>
    {

        public BaseService(IUnitOfWork unitOfWork, TRepository repository)
            : base(unitOfWork, repository)
        {
        }
    }
}