﻿using LMS.Core.IService;
using System;
using System.Threading.Tasks;
using LMS.Core.Entity;
using LMS.Core.IRepository;
using LMS.Core.DTO;
using System.Net.Http;
using LMS.Core.Infrastructure;
using System.Net;
using LMS.Core.Constant;
using System.Net.Http.Headers;
using LMS.Core.Enums;
using System.Collections.Generic;

namespace LMS.Service
{
    public class ParentService : BaseService<IParentRepository, Parent, ParentDTO>, IParentService
    {
        private IExceptionHelper exceptionHelper;
        private IRequestInfo requestInfo;
        private IUserService userService;

        public ParentService(
            IUnitOfWork unitOfWork,
            IExceptionHelper exceptionHelper,
            IRequestInfo requestInfo,
            IResourceRepository resourceRepository,
            IUserService userService)
            : base(unitOfWork, unitOfWork.ParentRepository)
        {
            this.exceptionHelper = exceptionHelper;
            this.requestInfo = requestInfo;
            this.userService = userService;

            var academicYear = this.UnitOfWork.AcademicYearRepository.GetCurrentYear();
            this.Repository.AcademicYearId = academicYear.Id;
        }

        public override async Task<ParentDTO> CreateAsync(ParentDTO dtoObject)
        {
            if (await Repository.FindByName(dtoObject.FullName) != null)
            {
                UnitOfWork.ExceptionHelper.ThrowAPIException(Message.AlreadyExists("Parent"));
            }
            else
            {
                Parent parent = dtoObject.ConvertToEntity();
                string username = await userService.GenerateUsername(dtoObject.FullName);
                string password = Core.Helper.UserHelper.GeneratePassword();
                
                using (var tran = this.UnitOfWork.BeginTransaction())
                {
                    ApplicationUser user = await UnitOfWork.UserRepository.Create(new ApplicationUser()
                    {
                        Email = dtoObject.Email,
                        UserName = username
                    }, password, UserRoles.Parent.ToString());

                    parent.UserId = user.Id;
                    try
                    {
                        parent = await Repository.Create(parent);
                        UnitOfWork.Save();
                        tran.Commit();
                        dtoObject.Id = parent.Id;
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        throw ex;
                    }
                }
                return dtoObject;
            }
            return null;
        }

        public override async Task<ParentDTO> UpdateAsync(ParentDTO dtoObject)
        {
            var entity = await this.Repository.GetDefaultAsync(dtoObject.Id);
            if (entity == null)
                UnitOfWork.ExceptionHelper.ThrowAPIException(Message.NotFound("Parent"));

            if (await Repository.FindByName(dtoObject.FullName, dtoObject.Id) != null)
            {
                UnitOfWork.ExceptionHelper.ThrowAPIException(Message.AlreadyExists("Parent"));
            }
            else
            {
                entity = dtoObject.ConvertToEntity(entity);
                var user = await this.UnitOfWork.UserRepository.GetDetailAsync(entity.UserId);
                using (var tran = this.UnitOfWork.BeginTransaction())
                {
                    try
                    {
                        await Repository.Update(entity);
                        if (user.Email != dtoObject.Email)
                        {
                            user.Email = dtoObject.Email;
                            await this.UnitOfWork.UserRepository.Update(user);
                        }
                        await UnitOfWork.SaveAsync();
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        throw ex;
                    }
                }
                return dtoObject;
            }
            return null;
        }

        public async override Task DeleteAsync(int id)
        {
            var Parent = await this.Repository.GetDefaultAsync(id);
            if(Parent == null)
                UnitOfWork.ExceptionHelper.ThrowAPIException(Message.NotFound("Parent"));

            await this.UnitOfWork.UserRepository.DeleteAsync(Parent.UserId);
            await base.DeleteAsync(id);
        }

        public bool HaveDependencies(int id)
        {
            return this.Repository.HaveDependencies(id);
        }        
        
        public bool HaveParentChildRelation(int parentUserId, int studentID)
        {
            return HaveParentChildRelation(parentUserId, studentID);
        }

        public async override Task<ParentDTO> GetAsync(int id)
        {
            Parent entity = await this.Repository.GetAsync(id);
            if (entity == null)
                return null;
            
            ParentDTO dto = new ParentDTO();
            dto.ConvertFromEntity(entity);
            this.UnitOfWork.StudentRepository.AcademicYearId = this.Repository.AcademicYearId;
            dto.Students = this.UnitOfWork.StudentRepository.GetStudentsByParentId(dto.Id);
            return dto;
        }
    }
}
