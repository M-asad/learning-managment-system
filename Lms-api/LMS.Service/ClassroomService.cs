﻿using LMS.Core.Constant;
using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;
using LMS.Core.IService;
using LMS.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using LMS.Core.DTO.CustomDTOs;

namespace LMS.Service
{
    public class ClassroomService : BaseService<IClassroomRepository, Classroom, ClassroomDTO>, IClassroomService
    {
        IExceptionHelper exceptionHelper;
        public ClassroomService(IUnitOfWork unitOfWork, IExceptionHelper exceptionHelper)
            : base(unitOfWork, unitOfWork.ClassroomRepository)
        {
            this.exceptionHelper = exceptionHelper;
        }

        public async override Task<ClassroomDTO> CreateAsync(ClassroomDTO dtoObject)
        {
            // Get active Academic year Id from db 
            var academicYear = this.UnitOfWork.AcademicYearRepository.GetCurrentYear();
            dtoObject.AcademicYearId = academicYear.Id;

            if (this.Repository.IsClassroomExists(academicYear.Id, dtoObject.ClassId, dtoObject.SectionId))
                exceptionHelper.ThrowAPIException(Message.AlreadyExists("Classroom"));

            // TODO: Are Student enrolled in other classes
            //var studentIds = dtoObject.ClassroomStudents.Select(x => x.StudentId);
            //if (this.UnitOfWork.ClassroomStudentRepository.AreStudentEnrolled(studentIds))
            //{
            //}

            var classObject = await this.UnitOfWork.ClassRepository.GetAsync(dtoObject.ClassId);
            var sectionObject = await this.UnitOfWork.SectionRepository.GetAsync(dtoObject.SectionId);
            
            using (var tran = this.UnitOfWork.BeginTransaction())
            {
                dtoObject.ClassroomName = classObject.Name + " - " + sectionObject.Title;
                var classroom = await this.Create(dtoObject);
                int rollNo = 1;
                foreach (ClassroomStudentDTO csDTO in dtoObject.ClassroomStudents)
                {
                    var studentEntity = csDTO.ConvertToEntity();
                    studentEntity.ClassroomId = classroom.Id;
                    studentEntity.RollNo = rollNo++;
                    studentEntity = await this.UnitOfWork.ClassroomStudentRepository.Create(studentEntity);
                }

                foreach (ClassroomCourseDTO ccDTO in dtoObject.ClassroomCourses)
                {
                    var classroomCourse = ccDTO.ConvertToEntity();
                    classroomCourse.ClassroomId = classroom.Id;
                    classroomCourse = await this.UnitOfWork.ClassroomCourseRepository.Create(classroomCourse);
                }

                await this.UnitOfWork.SaveAsync();
                tran.Commit();
                return dtoObject;
            }
        }

        public async override Task<ClassroomDTO> UpdateAsync(ClassroomDTO dtoObject)
        {
            var entity = await this.Repository.GetDefaultAsync(dtoObject.Id);
            if (entity == null)
                exceptionHelper.ThrowAPIException(Message.NotFound("Classroom"));

            if (dtoObject.ClassId != entity.ClassId || dtoObject.SectionId != entity.SectionId)
            {
                //if (this.Repository.IsClassroomExists(entity.AcademicYearId, dtoObject.ClassId, dtoObject.SectionId))
                //    exceptionHelper.ThrowAPIException(Message.AlreadyExists("Classroom"));
                exceptionHelper.ThrowAPIException("Cannot change Class and Section");
            }
            using (var tran = this.UnitOfWork.BeginTransaction())
            {
                entity.ClassTeacherId = dtoObject.ClassTeacherId;
                entity = await this.Repository.Update(entity);

                #region student enrollment
                var enrolledStudents = this.UnitOfWork.ClassroomStudentRepository.GetClassroomStudents(dtoObject.Id);
                var currentStudents = dtoObject.ClassroomStudents.Select(x => x.StudentId).ToArray();
                var existingStudents = enrolledStudents.Select(x => x.StudentId).ToArray();
                var newStudents = dtoObject.ClassroomStudents.Where(x => !existingStudents.Contains(x.StudentId)).Select(x => x.StudentId).ToArray();
                var removedStudents = existingStudents.Where(x => !currentStudents.Contains(x)).ToArray();
                List<int> rollNosToAssign = new List<int>(); 

                // deleting removed student
                foreach (ClassroomStudent cs in enrolledStudents.Where(x=> removedStudents.Contains(x.StudentId)).ToList())
                {
                    rollNosToAssign.Add(cs.RollNo); // adding rollno to assign to new students
                    cs.IsDeleted = true;
                    cs.LastModifiedBy = this.UnitOfWork.RequestInfo.UserName;
                    cs.LastModifiedOn = DateTime.Now;
                    cs.RollNo = 0;                    
                }

                // adding new student
                var maxRollNo = this.UnitOfWork.ClassroomStudentRepository.GetMaxRollNo(dtoObject.Id);
                int index = 0;
                while(rollNosToAssign.Count < newStudents.Length)
                {
                    rollNosToAssign.Add(++maxRollNo);
                }
                foreach (ClassroomStudentDTO csDTO in dtoObject.ClassroomStudents.Where(x => newStudents.Contains(x.StudentId)).ToList())
                {
                    var studentEntity = csDTO.ConvertToEntity();
                    studentEntity.ClassroomId = entity.Id;
                    studentEntity.RollNo = rollNosToAssign[index];
                    studentEntity = await this.UnitOfWork.ClassroomStudentRepository.Create(studentEntity);
                    index++;
                }

                #endregion student enrollment

                // TODO: Get old courses, delete removed ones and add new ones
                var existingRecords = this.UnitOfWork.ClassroomCourseRepository.Get(x => x.ClassroomId == dtoObject.Id);
                var toUpdate = dtoObject.ClassroomCourses.Where(x => x.Id > 0).Select(x => x.Id).ToArray();

                // deleting removed courses
                foreach (ClassroomCourse cc in existingRecords.Where(x => !toUpdate.Contains(x.Id)).ToList())
                {
                    cc.IsDeleted = true;
                    cc.LastModifiedBy = this.UnitOfWork.RequestInfo.UserName;
                    cc.LastModifiedOn = DateTime.Now;
                }

                // updating records
                foreach (ClassroomCourse cc in existingRecords.Where(x => toUpdate.Contains(x.Id)).ToList())
                {
                    var dto = dtoObject.ClassroomCourses.Where(x => x.Id == cc.Id).FirstOrDefault();
                    cc.CourseId = dto.CourseId;
                    cc.TeacherId = dto.TeacherId;
                    cc.LastModifiedBy = this.UnitOfWork.RequestInfo.UserName;
                    cc.LastModifiedOn = DateTime.Now;
                }

                // Add new course-teacher records
                foreach (ClassroomCourseDTO ccDTO in dtoObject.ClassroomCourses.Where(x => x.Id == 0).ToList())
                {
                    var classroomCourse = ccDTO.ConvertToEntity();
                    classroomCourse.ClassroomId = entity.Id;
                    classroomCourse = await this.UnitOfWork.ClassroomCourseRepository.Create(classroomCourse);
                }

                await this.UnitOfWork.SaveAsync();
                tran.Commit();
            }
            return dtoObject;
        }

        public List<StudentSimpleDTO> GetStudents(int classroomId)
        {
            return this.UnitOfWork.ClassroomStudentRepository.GetStudents(classroomId);
        }

        public ClassroomMasterDTO GetClassroomMaster(int classroomId)
        {
            return this.Repository.GetClassroomMaster(classroomId);
        }

        public List<ClassKeyValue> GetCurrentClassesddl()
        {
            return this.Repository.GetCurrentClassesddl();
        }

        public List<ClassroomListDTO> GetCurrentYearClasses()
        {
            var academicYear = this.UnitOfWork.AcademicYearRepository.GetCurrentYear();
            return this.Repository.GetClasses(academicYear.Id);
        }

        public List<ClassroomListDTO> GetClasses(int academicYearId)
        {
            return this.Repository.GetClasses(academicYearId);
        }

        public ClassroomDTO GetClassroomDetail(int classroomId)
        {
            var detail = this.Repository.GetClassroomDetail(classroomId);
            if (detail == null)
                exceptionHelper.ThrowAPIException("Classroom not found");
            detail.ClassroomStudents = this.UnitOfWork.ClassroomStudentRepository.GetEnrolledStudents(classroomId);
            detail.ClassroomCourses = this.UnitOfWork.ClassroomCourseRepository.GetCourseTeachers(classroomId);
            return detail;
        }

        public List<string> GetClassroomCourses(int classroomID)
        {
            return this.Repository.GetClassroomCourses(classroomID);
        }

        public bool IsClassroomExists(int academicYearId, int classID, int sectionID)
        {
            return this.Repository.IsClassroomExists(academicYearId, classID, sectionID);
        }

        public bool IsUserClassTeacher(int teacherUserId, int classroomID)
        {
            return this.Repository.IsUserClassTeacher(teacherUserId, classroomID);
        }

        public SectionDTO GetSection(int classroomId)
        {
            var dto = new SectionDTO();
            dto.ConvertFromEntity(this.Repository.GetSection(classroomId));
            return dto;
        }
    }
}
