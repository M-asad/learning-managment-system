﻿using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.IRepository;
using LMS.Core.IService;
using LMS.Repository;

namespace LMS.Service
{
    public class SectionService : BaseService<ISectionRepository, Section, SectionDTO>, ISectionService
    {
        public SectionService(IUnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.SectionRepository)
        {

        }
    }
}
