﻿using System;
using System.Threading.Tasks;
using LMS.Core.Entity;
using LMS.Core.IRepository;
using LMS.Core.DTO;
using LMS.Core.Infrastructure;
using LMS.Core.Constant;
using LMS.Core.Enums;
using LMS.Core.IService;
using System.Collections.Generic;
using LMS.Common.Helper;
using System.Linq;
using LMS.Core.DTO.CustomDTOs;

namespace LMS.Service
{
    public class StudentService : BaseService<IStudentRepository, Student, StudentDTO, StudentListDTO>, IStudentService
    {
        public StudentService(IUnitOfWork unitOfWork)
            : base(unitOfWork, unitOfWork.StudentRepository)
        {
            var academicYear = this.UnitOfWork.AcademicYearRepository.GetCurrentYear();
            this.Repository.AcademicYearId = academicYear.Id;
        }

        public async override Task<IResultSet<StudentListDTO>> GetAllAsync(JsonApiRequest request)
        {
            var prefix = request.Filter[Filters.Contains].FirstOrDefault(x => x.Key == "prefix").Value;
            int pageCount = 0;
            List<StudentListDTO> list = this.Repository.GetStudents(prefix, request.Pagination.PageSize, request.Pagination.PageNumber, out pageCount);
            IResultSet<StudentListDTO> result = new ResultSet<StudentListDTO>()
            {
                Data = list,
                PageCount = pageCount
            };
            return result;
        }

        public async override Task<StudentDTO> GetAsync(int id)
        {
            Student entity = await this.Repository.GetAsync(id);
            if (entity == null)
            {
                return null;
            }
            if (this.UnitOfWork.RequestInfo.Role.Equals(UserRoles.Parent.ToString())
                && entity.Parent != null && entity.Parent.UserId != this.UnitOfWork.RequestInfo.UserId)
            {
                this.UnitOfWork.ExceptionHelper.ThrowAPIException(Message.NotFound("Student"));
            }
            StudentDTO dto = new StudentDTO();
            dto.ConvertFromEntity(entity);
            var classroomDetail = this.UnitOfWork.ClassroomStudentRepository.GetStudentClassroom(id);
            if(classroomDetail != null)
            {
                dto.ClassroomId = classroomDetail.ClassroomId;
                dto.ClassroomName = classroomDetail.ClassroomName;
                dto.RollNo = classroomDetail.RollNo;
            }
            return dto;
        }

        public override async Task<StudentDTO> CreateAsync(StudentDTO dtoObject)
        {
            if (!UnitOfWork.ParentRepository.Exists(dtoObject.ParentId))
            {
                UnitOfWork.ExceptionHelper.ThrowAPIException(Message.NotFound("Parent"));
            }
            if (dtoObject.ClassroomId != 0 && !UnitOfWork.ClassroomRepository.Exists(dtoObject.ClassroomId))
            {
                UnitOfWork.ExceptionHelper.ThrowAPIException(Message.NotFound("Classroom"));
            }
            if (Repository.IsGRNoExists(dtoObject.GRNo))
            {
                UnitOfWork.ExceptionHelper.ThrowAPIException(Message.AlreadyExists("GR No."));
            }
            using (var tran = this.UnitOfWork.BeginTransaction())
            {
                Student student = dtoObject.ConvertToEntity();
                student = await Repository.Create(student);
                if (dtoObject.ClassroomId != 0)
                {
                    await this.UnitOfWork.ClassroomStudentRepository.Create(new ClassroomStudent()
                    {
                        ClassroomId = dtoObject.ClassroomId,
                        StudentId = student.Id,
                        RollNo = this.UnitOfWork.ClassroomStudentRepository.GetMaxRollNo(dtoObject.ClassroomId) + 1,
                    });
                }
                await this.UnitOfWork.SaveAsync();
                tran.Commit();
                dtoObject.Id = student.Id;
            }
            return await GetAsync(dtoObject.Id);
        }

        public override async Task<StudentDTO> UpdateAsync(StudentDTO dtoObject)
        {
            if (!UnitOfWork.ParentRepository.Exists(dtoObject.ParentId))
            {
                UnitOfWork.ExceptionHelper.ThrowAPIException(Message.NotFound("Parent"));
            }
            if (dtoObject.ClassroomId != 0 && !UnitOfWork.ClassroomRepository.Exists(dtoObject.ClassroomId))
            {
                UnitOfWork.ExceptionHelper.ThrowAPIException(Message.NotFound("Classroom"));
            }
            if (Repository.IsGRNoExists(dtoObject.GRNo, dtoObject.Id))
            {
                UnitOfWork.ExceptionHelper.ThrowAPIException(Message.AlreadyExists("GR No."));
            }

            using (var tran = this.UnitOfWork.BeginTransaction())
            {
                var entity = await this.Repository.GetAsync(dtoObject.Id);
                if(entity != null)
                {
                    Student student = dtoObject.ConvertToEntity(entity);
                    student = await Repository.Update(student);

                    //if (dtoObject.ClassroomId != entity.ClassroomStudents.FirstOrDefault().ClassroomId)
                    //{
                    //    await this.UnitOfWork.ClassroomStudentRepository.Create(new ClassroomStudent()
                    //    {
                    //        ClassroomId = dtoObject.ClassroomId,
                    //        StudentId = student.Id,
                    //        RollNo = this.UnitOfWork.ClassroomStudentRepository.GetMaxRollNo(dtoObject.ClassroomId) + 1,
                    //    });
                    //}
                    await this.UnitOfWork.SaveAsync();
                    tran.Commit();
                    dtoObject.Id = student.Id;
                }
            }
            return await GetAsync(dtoObject.Id);
        }

        public List<StudentSimpleDTO> GetUnEnrolledStudents(string prefix, int count)
        {
            return this.UnitOfWork.ClassroomStudentRepository.GetUnEnrolledStudents(prefix, count);
        }

        public List<StudentListDTO> GetStudentsByParentId(int parentId)
        {           
            return this.Repository.GetStudentsByParentId(parentId);
        }

        public List<StudentSimpleDTO> GetEnrolledStudents(string prefix, int count)
        {
            return this.Repository.GetEnrolledStudents(prefix, count);
        }
    }
}
