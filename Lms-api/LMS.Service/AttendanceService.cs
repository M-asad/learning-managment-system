﻿using LMS.Core.Constant;
using LMS.Core.DTO;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;
using LMS.Core.IService;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Service
{
    public class AttendanceService : IAttendanceService
    {
        IExceptionHelper exceptionHelper;
        IAttendanceRepository repository;
        IUnitOfWork unitOfWork;
        public AttendanceService(IAttendanceRepository repository, IExceptionHelper exceptionHelper, IUnitOfWork unitOfWork)
        {
            this.repository = repository;
            this.exceptionHelper = exceptionHelper;
            this.unitOfWork = unitOfWork;
        }

        public async Task<List<AttendanceCalendarDTO>> GetAttendanceCalendar(int classroomId, DateTime fromDate, DateTime toDate)
        {
            var classroom = await this.unitOfWork.ClassroomRepository.GetAsync(classroomId);
            if (classroom == null)
            {
                exceptionHelper.ThrowAPIException(Message.NotFound("Classroom"));
            }
            return this.repository.GetAttendanceCalendar(classroomId, fromDate, toDate);
        }

        public ClassAttendanceDTO GetAttendance(int classroomId, DateTime date, bool fillEmpty)
        {
            return this.repository.GetAttendance(classroomId, date, fillEmpty);
        }

        public AttendanceCalendarDTO GetStudentCalendar(StudentDTO student, int year, int month)
        {
            return this.repository.GetStudentCalendar(student, year, month);
        }

        public void SaveAttendance(SaveClassAttendanceDTO attendanceData)
        {
            this.repository.SaveAttendance(attendanceData);
        }

        public void UpdateAttendance(AttendanceUpdateDTO attendanceData)
        {
            this.repository.UpdateAttendance(attendanceData);
        }

        public void SaveStudentCalendar(AttendanceCalendarDTO attendanceData)
        {
            this.repository.SaveStudentCalendar(attendanceData);
        }
    }
}
