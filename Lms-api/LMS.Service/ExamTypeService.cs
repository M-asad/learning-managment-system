﻿using LMS.Core.IService;
using System;
using System.Threading.Tasks;
using LMS.Core.Entity;
using LMS.Core.IRepository;
using LMS.Core.DTO;
using System.Net.Http;
using LMS.Core.Infrastructure;
using System.Net;
using LMS.Core.Constant;
using System.Net.Http.Headers;
using LMS.Core.Enums;
using System.Collections.Generic;

namespace LMS.Service
{
    public class ExamTypeService : BaseService<IExamTypeRepository, ExamType, ExamTypeDTO>, IExamTypeService
    {
        private IRequestInfo requestInfo;

        public ExamTypeService(
            IUnitOfWork unitOfWork,
            IRequestInfo requestInfo)
            : base(unitOfWork, unitOfWork.ExamTypeRepository)
        {
            this.requestInfo = requestInfo;
        }     
    }
}
