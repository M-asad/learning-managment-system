﻿using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.IRepository;
using LMS.Core.IService;

namespace LMS.Service
{
    public class TimeTableService : BaseService<ITimeTableRepository, TimeTable, TimeTableDTO>, ITimeTableService
    {
        public TimeTableService(IUnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.TimeTableRepository)
        {

        }
    }
}
