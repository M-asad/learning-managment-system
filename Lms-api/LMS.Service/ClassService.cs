﻿using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.IRepository;
using LMS.Core.IService;
using LMS.Repository;

namespace LMS.Service
{
    public class ClassService : BaseService<IClassRepository, Class, ClassDTO>, IClassService
    {
        public ClassService(IUnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.ClassRepository)
        {

        }
    }
}
