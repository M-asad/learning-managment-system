﻿using LMS.Common.Helper;
using LMS.Core.Constant;
using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;
using LMS.Core.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Service
{
    public class AssignmentService : BaseService<IAssignmentRepository, Assignment, AssignmentDTO, AssignmentListDTO>, IAssignmentService
    {
        IExceptionHelper exceptionHelper;
        IAssignmentAttachmentRepository attachmentRepository;
        INotificationService notificationService;
        public AssignmentService(IUnitOfWork unitOfWork,
            IExceptionHelper exceptionHelper,
            IAssignmentAttachmentRepository attachmentRepository,
            INotificationService notificationService)
            : base(unitOfWork, unitOfWork.AssignmentRepository)
        {
            this.exceptionHelper = exceptionHelper;
            this.attachmentRepository = attachmentRepository;
            this.notificationService = notificationService;
        }

        public async override Task<AssignmentDTO> CreateAsync(AssignmentDTO dtoObject)
        {
            if (this.Repository.isTitleExists(dtoObject.Title, 0))
            {
                exceptionHelper.ThrowAPIException(Message.AlreadyExists("Title"));
            }
            var result = await this.Create(dtoObject);
            await this.UnitOfWork.SaveAsync();
            await notificationService.SendNotification(NotificationTypes.AssignmentCreation, result.Id);

            dtoObject.ConvertFromEntity(result);
            return dtoObject;
        }

        public async override Task<AssignmentDTO> UpdateAsync(AssignmentDTO dtoObject)
        {
            var result = await this.Update(dtoObject);

            dtoObject.ConvertFromEntity(result);
            return dtoObject;
        }

        protected async Task<Assignment> Update(AssignmentDTO dtoObject)
        {
            var entity = await this.Repository.GetAsync(dtoObject.Id);

            if (entity == null)
            {
                exceptionHelper.ThrowAPIException(Message.NotFound("Assignment"));
            }
            if (entity.Date <= DateTime.Now)
            {
                exceptionHelper.ThrowAPIException(Message.AssignmentDateReached);
            }
            if (this.Repository.isTitleExists(dtoObject.Title, dtoObject.Id))
            {
                exceptionHelper.ThrowAPIException(Message.AlreadyExists("Assignment"));
            }

            using (var tran = this.UnitOfWork.BeginTransaction())
            {
                entity.Title = dtoObject.Title;
                entity.Description = dtoObject.Description;
                entity.CourseId = dtoObject.CourseId;
                entity.TotalMarks = dtoObject.TotalMarks;
                entity.PassingMarks = dtoObject.PassingMarks;
                entity.Date = dtoObject.Date;
                entity = await this.Repository.Update(entity);

                var dbAttachments = entity.AssignmentAttachments.ToList();
                // Deleting Attachment that are removed
                foreach (var at in dbAttachments)
                {
                    if (!dtoObject.AttachmentURLs.Contains(at.FIleURL))
                    {
                        at.IsDeleted = true;
                        await attachmentRepository.Update(at);
                    }
                }
                // Add new attachments
                foreach (var url in dtoObject.AttachmentURLs.ToList())
                {
                    if (!dbAttachments.Select(x => x.FIleURL).Contains(url))
                    {
                        await attachmentRepository.Create(new AssignmentAttachment()
                        {
                            AssignmentID = entity.Id,
                            FIleURL = url
                        });
                    }
                }
                await this.UnitOfWork.SaveAsync();
                tran.Commit();
            }
            return await this.Repository.GetAsync(dtoObject.Id);
        }

        public async override Task<IResultSet<AssignmentListDTO>> GetAllAsync(JsonApiRequest request)
        {
            var prefix = request.Filter[Filters.Contains].FirstOrDefault(x => x.Key == "prefix").Value;
            int pageCount = 0;
            List<AssignmentListDTO> list = this.Repository.GetAssignments(new PaginationDTO()
            {
                Page = request.Pagination.PageNumber,
                Count = request.Pagination.PageSize,
                Prefix = prefix
            }, out pageCount);
            IResultSet<AssignmentListDTO> result = new ResultSet<AssignmentListDTO>()
            {
                Data = list,
                PageCount = pageCount
            };
            return result;
        }

        public bool isTitleExists(string title, int id = 0)
        {
            return this.Repository.isTitleExists(title, id);
        }

        public bool isAssignmentOwner(int assignmentId)
        {
            return this.Repository.isAssignmentOwner(assignmentId);
        }

        public IResultSet<AssignmentListDTO> GetStudentAssignments(JsonApiRequest request, int studentId)
        {
            var prefix = request.Filter[Filters.Contains].FirstOrDefault(x => x.Key == "prefix").Value;
            int pageCount = 0;
            List<AssignmentListDTO> list = this.Repository.GetStudentAssignments(new PaginationDTO()
            {
                Page = request.Pagination.PageNumber,
                Count = request.Pagination.PageSize,
                Prefix = prefix
            }, studentId, out pageCount);
            IResultSet<AssignmentListDTO> result = new ResultSet<AssignmentListDTO>()
            {
                Data = list,
                PageCount = pageCount
            };
            return result;
        }

        public async Task<AssignmentDTO> SubmitResult(AssignmentDTO dtoObject)
        {
            var entity = await this.Repository.GetAsync(dtoObject.Id);
            if (entity == null)
            {
                exceptionHelper.ThrowAPIException(Message.NotFound("Assignment"));
            }
            if (dtoObject.Details != null && dtoObject.Details.Max(x => x.ObtainedMarks) > entity.TotalMarks)
            {
                exceptionHelper.ThrowAPIException(Message.ObtainedMarksGreaterThanTotal);
            }
            foreach (var detail in entity.AssignmentDetails)
            {
                detail.ObtainedMarks = dtoObject.Details.Where(x => x.Id == detail.Id).FirstOrDefault().ObtainedMarks;
                if (detail.ObtainedMarks >= entity.PassingMarks)
                    detail.HasPassed = true;
                else
                    detail.HasPassed = false;
            }
            entity = await this.Repository.Update(entity);
            await this.UnitOfWork.SaveAsync();

            dtoObject.ConvertFromEntity(entity);
            return dtoObject;
        }
    }
}
