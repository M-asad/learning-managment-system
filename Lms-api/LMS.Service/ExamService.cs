﻿using LMS.Core.Constant;
using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;
using LMS.Core.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Service
{
    public class ExamService : BaseService<IExamRepository, Exam, ExamDTO>, IExamService
    {
        IExceptionHelper exceptionHelper;
        public ExamService(IUnitOfWork unitOfWork, IExceptionHelper exceptionHelper)
            : base(unitOfWork, unitOfWork.ExamRepository)
        {
            this.exceptionHelper = exceptionHelper;
        }

        public List<ExamListDTO> GetExamList(string prefix, int count, int page, out int pageCount)
        {
            return this.Repository.GetExamList(prefix, count, page, out pageCount);
        }
        public ExamDetailDTO GetExamDetail(int examId)
        {
            return this.Repository.GetExamDetail(examId);
        }
        public bool AreClassesExist(int Id, List<int> classes)
        {
            return this.Repository.AreClassesExist(Id, classes);
        }
        public bool ValidRange(int Id, DateTime startDate, DateTime endDate)
        {
            return this.Repository.ValidRange(Id, startDate, endDate);
        }
        public List<KeyValueDTO> GetCurrentYearExam()
        {
            return this.Repository.GetCurrentYearExam();
        }
        public ExamListDTO GetExamMaster(int examId)
        {
            return this.Repository.GetExamMaster(examId);
        }
        public List<KeyValueDTO> GetExamCourses(int examId)
        {
            return this.Repository.GetExamCourses(examId);
        }
        public List<KeyValueDTO> GetCurrentYearClasses()
        {
            return this.Repository.GetCurrentYearClasses();
        }
        public async Task<ExamDTO> CreateAsync(ExamDTO dtoObject)
        {
            if (this.Repository.IsExamExists(dtoObject.StartDate, dtoObject.EndDate, dtoObject.ClassIds))
            {
                exceptionHelper.ThrowAPIException("Another exam with conflicting dates is found for some classes.");
            }
            var academicYear = this.UnitOfWork.AcademicYearRepository.GetCurrentYear();
            dtoObject.AcademicYearId = academicYear.Id;

            var entity = dtoObject.ConvertToEntity();
            entity = await this.Repository.Create(entity);
            await this.UnitOfWork.SaveAsync();
            dtoObject.ConvertFromEntity(entity);
            return dtoObject;
        }
        public async Task<ExamDTO> UpdateAsync(ExamDTO dtoObject)
        {
            var entity = await this.Repository.GetAsync(dtoObject.Id);
            if (entity == null)
                exceptionHelper.ThrowAPIException(Message.NotFound("Exam"));

            var originalClasses = entity.ClassIds.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            var removedRecords = originalClasses.Where(x => !dtoObject.ClassIds.Contains(Convert.ToInt32(x))).ToList();
            if (removedRecords.Count > 0)
            {
                if (this.Repository.AreClassesExist(dtoObject.Id, removedRecords))
                {
                    exceptionHelper.ThrowAPIException("Classes which are assigned exams cannot be removed");
                }
            }
            if (entity.StartDate < dtoObject.StartDate || entity.EndDate > dtoObject.EndDate)
            {
                if (!this.Repository.ValidRange(dtoObject.Id, dtoObject.StartDate, dtoObject.EndDate))
                {
                    exceptionHelper.ThrowAPIException("Dates cannot be squeezed. Exams are already defined for it");
                }
            }
            entity.Description = dtoObject.Description;
            entity.StartDate = dtoObject.StartDate.Date;
            entity.EndDate = dtoObject.EndDate.Date;
            entity.ClassIds = String.Join(",", dtoObject.ClassIds);
            entity = await this.Repository.Update(entity);
            await UnitOfWork.SaveAsync();
            return dtoObject;
        }
        public int SaveDetail(ExamDetailSaveDTO dto)
        {
            return this.Repository.SaveDetail(dto);
        }

        public bool IsExamExists(DateTime startDate, DateTime endDate, int[] classes)
        {
            return this.Repository.IsExamExists(startDate, endDate, classes);
        }
    }
}
