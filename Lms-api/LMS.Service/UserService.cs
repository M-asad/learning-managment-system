﻿using LMS.Core.IService;
using System;
using System.Threading.Tasks;
using LMS.Core.Entity;
using LMS.Core.IRepository;
using LMS.Core.DTO;
using System.Net.Http;
using LMS.Core.Infrastructure;
using System.Net;
using LMS.Core.Constant;
using System.Net.Http.Headers;

namespace LMS.Service
{
    public class UserService : BaseService<IUserRepository, ApplicationUser, UserDTO>, IUserService
    {
        //private INotificationService notificationService;
        private IExceptionHelper exceptionHelper;
        private IRequestInfo requestInfo;
        private IResourceRepository resourceRepository;

        public UserService(
            IUnitOfWork unitOfWork,
            //INotificationService notificationService,
            IExceptionHelper exceptionHelper,
            IRequestInfo requestInfo,
            IResourceRepository resourceRepository)
            : base(unitOfWork, unitOfWork.UserRepository)
        {
            //this.notificationService = notificationService;
            this.exceptionHelper = exceptionHelper;
            this.requestInfo = requestInfo;
            this.resourceRepository = resourceRepository;
        }

        public async override Task<UserDTO> GetAsync(int id)
        {
            return await base.GetAsync(id);
        }

        public async Task<UserDTO> FindByUserNameAsync(string username)
        {
            var entity = await this.Repository.FindByUserNameAsync(username);
            if (entity == null)
            {
                return null;
            }

            var dto = new UserDTO();
            dto.ConvertFromEntity(entity);
            return dto;
        }

        public async Task<UserDTO> ValidateUserAsync(string username, string password)
        {
            try
            {
                ApplicationUserManager manager = new ApplicationUserManager(new ApplicationUserStore(this.requestInfo.Context));
                var entity = await manager.FindAsync(username, password);
                if (entity == null)
                {
                    return null;
                }
                UserDTO userDto = await this.GetAsync(entity.Id);
                return userDto;
            }
            catch (Exception ex)
            {
                // TODO: add logging 
                return null;
            }
        }

        //public async Task<string> ForgotPassword(string username)
        //{
        //    string token = Guid.NewGuid().ToString();

        //    ApplicationUser entity = await this.Repository.FindByEmailAsync(username);
        //    if (entity != null && !entity.IsDeleted)
        //    {
        //        entity.PasswordToken = token;
        //        await this.Repository.Update(entity);

        //        UserDTO dto = new UserDTO(entity);

        //        await this.notificationService.ForgotPasswordNotificationAsync(dto, token);

        //        return token;
        //    }

        //    return null;
        //}

        //public async Task<string> ForgotPassword(string token, ForgotPasswordDTO dtoObject)
        //{
        //    ApplicationUser user = await this.GetByPasswordToken(token);
        //    if (user != null && !user.IsDeleted)
        //    {
        //        await this.Repository.ChangePassword(user.Id, dtoObject.NewPassword);

        //        user.PasswordToken = string.Empty;
        //        await this.Repository.Update(user);

        //        UserDTO dto = new UserDTO(user);

        //        return token;
        //    }

        //    return null;
        //}

        //public async Task<bool> ValidateTokenAsync(string token)
        //{
        //    return await this.GetByPasswordToken(token) != null;
        //}

        public override async Task<UserDTO> CreateAsync(UserDTO dtoObject)
        {
            if (await this.FindByUserNameAsync(dtoObject.UserName) != null)
            {
                UnitOfWork.ExceptionHelper.ThrowAPIException(Message.AlreadyExists("User"));
            }
            else
            {
                ApplicationUser user = dtoObject.ConvertToEntity();
                user = await Repository.Create(user, dtoObject.Password, dtoObject.Role);
                dtoObject.Id = user.Id;
                return dtoObject;
            }

            return null;
        }

        public async Task<string> GetUserRole(int userId)
        {
            return await Repository.GetUserRole(userId);
        }

        public override async Task<UserDTO> UpdateAsync(UserDTO dtoObject)
        {
            var databaseEntity = await Repository.GetAsync(dtoObject.Id);

            if (dtoObject.UserName != databaseEntity.UserName)
            {
                if (await this.FindByUserNameAsync(dtoObject.UserName) != null)
                {
                    UnitOfWork.ExceptionHelper.ThrowAPIException(Message.AlreadyExists("User NIC"));
                }
            }

            databaseEntity.Roles.Clear();
            var entity = dtoObject.ConvertToEntity(databaseEntity);

            var savedEntity = await Repository.Update(entity);
            await UnitOfWork.SaveAsync();
            return dtoObject;
        }

        public async Task<ChangePasswordDTO> ChangePassword(ChangePasswordDTO dtoObject)
        {
            if (string.IsNullOrEmpty(dtoObject.UserName))
            {
                dtoObject.UserName = UnitOfWork.RequestInfo.UserName;
            }

            return await Repository.ChangePassword(dtoObject);
        }

        public async Task<ChangePasswordDTO> ChangeUserPassword(ChangePasswordDTO dtoObject)
        {
            if (string.IsNullOrEmpty(dtoObject.UserName) || string.IsNullOrEmpty(dtoObject.NewPassword))
            {
                this.exceptionHelper.ThrowAPIException(Message.UserPasswordRequiredFields);
            }

            var user = await this.FindByUserNameAsync(dtoObject.UserName);
            if (user == null)
            {
                this.exceptionHelper.ThrowAPIException(Message.UserInvalidUserName);
            }

            await Repository.ChangePassword(user.Id, dtoObject.NewPassword);
            return dtoObject;
        }

        public async Task<ResourceDTO> UploadImage(ResourceDTO dtoObject)
        {
            var entity = dtoObject.ConvertToEntity();
            entity = await this.resourceRepository.Create(entity);
            var user = await Repository.GetAsync(UnitOfWork.RequestInfo.UserId);
            user.PictureResourceId = entity.Id;
            user = await Repository.Update(user);
            await this.UnitOfWork.SaveAsync();
            dtoObject.ConvertFromEntity(entity);
            return dtoObject;
        }

        public async Task<ResourceDTO> UpdateImage(ResourceDTO dtoObject)
        {
            var entity = dtoObject.ConvertToEntity();
            entity = await this.resourceRepository.Update(entity);
            await this.UnitOfWork.SaveAsync();
            dtoObject.ConvertFromEntity(entity);
            return dtoObject;
        }

        public async Task<HttpResponseMessage> GetImage(int userId)
        {
            var user = await Repository.GetAsync(userId);
            if (user != null && user.PictureResourceId != null)
            {
                Resource resource = await this.resourceRepository.GetAsync(user.PictureResourceId.Value);
                if (resource != null && resource.BinaryData != null)
                {
                    var response = new HttpResponseMessage();
                    response.Content = new ByteArrayContent(resource.BinaryData);
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue(resource.ContentType);
                    response.Content.Headers.ContentLength = resource.BinaryData.Length;
                    response.Headers.CacheControl = new CacheControlHeaderValue()
                    {
                        Public = false,
                        MaxAge = new TimeSpan(1, 0, 0, 0)
                    };

                    return response;
                }
            }

            this.exceptionHelper.ThrowAPIException(HttpStatusCode.NotFound, Message.ResourceFileNotFound);
            return null;
        }

        public async override Task DeleteAsync(int id)
        {
            await base.DeleteAsync(id);
        }

        public async Task UpdateUserRole(int userId, int roleId)
        {
            await this.Repository.UpdateUserRole(userId, roleId);
        }

        public async Task<string> GenerateUsername(string name)
        {
            string username = name.Replace(' ', '.');
            int count = 0;
            while (true)
            {
                if (await this.Repository.FindByUserNameAsync(username) == null)
                {
                    return username;
                }
                username = name.Replace(' ', '.') + count++;
            }
        }

        #region Private Function
        //private async Task<ApplicationUser> GetByPasswordToken(string token)
        //{
        //    ApplicationUser user = await this.Repository.GetByPasswordToken(token);

        //    if (user == null)
        //    {
        //        UnitOfWork.ExceptionHelper.ThrowAPIException(Message.UserInvalidToken);
        //    }

        //    ApplicationUser entity = await this.Repository.GetAsync(user.Id);
        //    return entity;
        //}
        #endregion
    }
}
