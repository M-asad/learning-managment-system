﻿using LMS.Core.Constant;
using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;
using LMS.Core.IService;
using LMS.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Service
{
    public class ClassroomCourseService : BaseService<IClassroomCourseRepository, ClassroomCourse, ClassroomCourseDTO>, IClassroomCourseService
    {
        IExceptionHelper exceptionHelper;
        public ClassroomCourseService(IUnitOfWork unitOfWork, IExceptionHelper exceptionHelper)
            : base(unitOfWork, unitOfWork.ClassroomCourseRepository)
        {
            this.exceptionHelper = exceptionHelper;
        }
    }
}
