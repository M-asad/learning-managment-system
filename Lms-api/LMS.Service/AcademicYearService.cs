﻿using LMS.Core.Entity;
using LMS.Core.IRepository;
using LMS.Core.DTO;
using LMS.Core.Infrastructure;
using LMS.Core.Constant;
using LMS.Core.Enums;
using LMS.Core.IService;
using System;
using System.Threading.Tasks;

namespace LMS.Service
{
    public class AcademicYearService : BaseService<IAcademicYearRepository, AcademicYear, AcademicYearDTO>, IAcademicYearService
    {
        public AcademicYearService(
              IUnitOfWork unitOfWork,
              IUserService userService)
            : base(unitOfWork, unitOfWork.AcademicYearRepository)
        {
        }

        public AcademicYearDTO GetCurrentYear()
        {
            var entity = this.Repository.GetCurrentYear();
            AcademicYearDTO dto = new AcademicYearDTO();
            dto.ConvertFromEntity(entity);
            return dto;
        }
    }
}
