﻿using System;
using System.Threading.Tasks;
using LMS.Core.Entity;
using LMS.Core.IRepository;
using LMS.Core.DTO;
using LMS.Core.Infrastructure;
using LMS.Core.Constant;
using LMS.Core.Enums;
using LMS.Core.IService;
using LMS.Core.Helper;
using LMS.Core;
using System.Net.Mail;

namespace LMS.Service
{
    public class NotificationService : BaseService<INotificationRepository, Notification, NotificationDTO>, INotificationService
    {
        private IExceptionHelper exceptionHelper;
        private IRequestInfo requestInfo;
        private INotificationTypeRepository _notificationTypeRepository;
        private IAssignmentRepository _assignmentRepository;

        public NotificationService(
            IUnitOfWork unitOfWork,
            IExceptionHelper exceptionHelper,
            INotificationTypeRepository notificationTypeRepository,
            IAssignmentRepository assignmentRepository,
            IRequestInfo requestInfo)
            : base(unitOfWork, unitOfWork.NotificationRepository)
        {
            this.exceptionHelper = exceptionHelper;
            this.requestInfo = requestInfo;
            this._notificationTypeRepository = notificationTypeRepository;
            this._assignmentRepository = assignmentRepository;
        }

        public async Task<bool> SendNotification(string type, int key)
        {
            // Get template from DB by type
            var notificationType = _notificationTypeRepository.GetNotificationType(type);
            var template = EmailHelper.GetTemplate(type);
            // Apply switch
            switch (type)
            {
                case NotificationTypes.AssignmentCreation:
                    {
                        // Get recepients from key 
                        var list = _assignmentRepository.GetAssignmentCreationEmailData(key);
                        var subject = notificationType.Subject.Replace("{COURSE}", list[0].Course);
                        var body = notificationType.Template.Replace("{COURSE}", list[0].Course)
                                .Replace("{DATE}", list[0].DueDate.ToShortDateString())
                                .Replace("{ASSIGNMENTID}", key.ToString())
                                .Replace("{TITLE}", list[0].Title);
                        template = template.Replace("{{Content}}", body)
                            .Replace("{WEBSITEURL}", ConfigManager.WebsiteURL);
                        var client = EmailHelper.GetSmtpClient();
                        foreach (var item in list)
                        {
                            subject = subject.Replace("{STUDENT}", item.Student);
                            body = template.Replace("{PARENT}", item.Parent).Replace("{STUDENT}", item.Student);                            
                            var notificationEntry = new Notification()
                            {
                                NotificationTypeId = notificationType.Id,
                                Subject = subject,
                                Body = body,
                                IsRead = false,
                                RetryCount = 0,
                                UserId = item.ParentUserId
                            };
                            try
                            {
                                var mailMessage = new MailMessage(ConfigManager.FromAddress, item.Email);
                                mailMessage.Subject = subject;
                                mailMessage.Body = body;
                                mailMessage.IsBodyHtml = true;
                                client.Send(mailMessage);

                                notificationEntry.IsEmailSent = true;
                                await this.Repository.Create(notificationEntry);
                            }
                            catch (Exception ex)
                            {
                                notificationEntry.IsEmailSent = false;
                                notificationEntry.Exception = ex.Message;
                                await this.Repository.Create(notificationEntry);
                            }
                        }
                        await this.UnitOfWork.SaveAsync();
                        break;
                    }
            }
            return true;
        }
    }
}
