﻿using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.IRepository;
using LMS.Core.IService;
using LMS.Repository;

namespace LMS.Service
{
    public class GradeService : BaseService<IGradeRepository, Grade, GradeDTO>, IGradeService
    {
        public GradeService(IUnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.GradeRepository)
        {

        }
    }
}
