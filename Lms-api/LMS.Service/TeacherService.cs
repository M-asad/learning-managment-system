﻿using System;
using System.Threading.Tasks;
using LMS.Core.Entity;
using LMS.Core.IRepository;
using LMS.Core.DTO;
using LMS.Core.Infrastructure;
using LMS.Core.Constant;
using LMS.Core.Enums;
using LMS.Core.IService;

namespace LMS.Service
{
    public class TeacherService : BaseService<ITeacherRepository, Teacher, TeacherDTO>, ITeacherService
    {
        private IExceptionHelper exceptionHelper;
        private IRequestInfo requestInfo;
        private IUserService userService;

        public TeacherService(
            IUnitOfWork unitOfWork,
            IExceptionHelper exceptionHelper,
            IRequestInfo requestInfo,
            IResourceRepository resourceRepository,
            IUserService userService)
            : base(unitOfWork, unitOfWork.TeacherRepository)
        {
            this.exceptionHelper = exceptionHelper;
            this.requestInfo = requestInfo;
            this.userService = userService;
        }

        public override async Task<TeacherDTO> CreateAsync(TeacherDTO dtoObject)
        {
            if (await Repository.FindByName(dtoObject.FullName) != null)
            {
                UnitOfWork.ExceptionHelper.ThrowAPIException(Message.AlreadyExists("Teacher"));
            }
            else
            {
                Teacher teacher = dtoObject.ConvertToEntity();
                string username = await userService.GenerateUsername(dtoObject.FullName);
                string password = Core.Helper.UserHelper.GeneratePassword();
                
                using (var tran = this.UnitOfWork.BeginTransaction())
                {
                    ApplicationUser user = await UnitOfWork.UserRepository.Create(new ApplicationUser()
                    {
                        Email = dtoObject.Email,
                        UserName = username
                    }, password, UserRoles.Teacher.ToString());

                    teacher.UserId = user.Id;
                    try
                    {
                        teacher = await Repository.Create(teacher);
                        UnitOfWork.Save();
                        tran.Commit();
                        dtoObject.Id = teacher.Id;
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        throw ex;
                    }
                }
                return dtoObject;
            }
            return null;
        }

        public override async Task<TeacherDTO> UpdateAsync(TeacherDTO dtoObject)
        {
            var entity = await this.Repository.GetDefaultAsync(dtoObject.Id);
            if (entity == null)
                UnitOfWork.ExceptionHelper.ThrowAPIException(Message.NotFound("Teacher"));

            if (await Repository.FindByName(dtoObject.FullName, dtoObject.Id) != null)
            {
                UnitOfWork.ExceptionHelper.ThrowAPIException(Message.AlreadyExists("Teacher"));
            }
            else
            {
                entity = dtoObject.ConvertToEntity(entity);
                var user = await this.UnitOfWork.UserRepository.GetDetailAsync(entity.UserId);
                using (var tran = this.UnitOfWork.BeginTransaction())
                {
                    try
                    {
                        await Repository.Update(entity);
                        if (user.Email != dtoObject.Email)
                        {
                            user.Email = dtoObject.Email;
                            await this.UnitOfWork.UserRepository.Update(user);
                        }
                        await UnitOfWork.SaveAsync();
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        throw ex;
                    }
                }
                return dtoObject;
            }
            return null;
        }

        public async override Task DeleteAsync(int id)
        {
            var teacher = await this.Repository.GetDefaultAsync(id);
            if(teacher == null)
                UnitOfWork.ExceptionHelper.ThrowAPIException(Message.NotFound("Teacher"));

            await this.UnitOfWork.UserRepository.DeleteAsync(teacher.UserId);
            await base.DeleteAsync(id);
        }

        public bool HaveDependencies(int id)
        {
            return this.Repository.HaveDependencies(id);
        }
    }
}
