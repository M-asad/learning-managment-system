﻿using LMS.Core.Constant;
using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;
using LMS.Core.IService;
using LMS.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Service
{
    public class ClassroomStudentService : BaseService<IClassroomStudentRepository, ClassroomStudent, ClassroomStudentDTO>, IClassroomStudentService
    {
        IExceptionHelper exceptionHelper;
        public ClassroomStudentService(IUnitOfWork unitOfWork, IExceptionHelper exceptionHelper)
            : base(unitOfWork, unitOfWork.ClassroomStudentRepository)
        {
            this.exceptionHelper = exceptionHelper;
        }

        public async Task<ClassroomStudentDTO> EnrollStudent(EnrollStudentDTO dtoObject)
        {
            var classroomStudentDTO = dtoObject.ConvertToClassroomStudentDTO();
            classroomStudentDTO.RollNo = this.UnitOfWork.ClassroomStudentRepository.GetMaxRollNo(dtoObject.ClassroomId) + 1;
            var result = await this.Create(classroomStudentDTO);
            await this.UnitOfWork.SaveAsync();
            classroomStudentDTO.ConvertFromEntity(result);
            return classroomStudentDTO;
        }
    }
}
