﻿using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.IRepository;
using LMS.Core.IService;
using LMS.Repository;

namespace LMS.Service
{
    public class CourseService : BaseService<ICourseRepository, Course, CourseDTO>, ICourseService
    {
        public CourseService(IUnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.CourseRepository)
        {

        }
    }
}
