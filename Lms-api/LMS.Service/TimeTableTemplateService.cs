﻿using LMS.Core.Constant;
using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.IRepository;
using LMS.Core.IService;
using System;
using System.Threading.Tasks;

namespace LMS.Service
{
    public class TimeTableTemplateService : BaseService<ITimeTableTemplateRepository, TimeTableTemplate, TimeTableTemplateDTO>, ITimeTableTemplateService
    {
        ITimeTableTemplateDetailRepository templateDetailRepository;

        public TimeTableTemplateService(IUnitOfWork unitOfWork,
            ITimeTableTemplateDetailRepository templateDetailRepository)
            : base(unitOfWork, unitOfWork.TimeTableTemplateRepository)
        {
            this.templateDetailRepository = templateDetailRepository;
        }

        public override async Task<TimeTableTemplateDTO> CreateAsync(TimeTableTemplateDTO dtoObject)
        {
            if (Repository.IsTitleExists(dtoObject.Title))
            {
                UnitOfWork.ExceptionHelper.ThrowAPIException(Message.AlreadyExists("Timetable Template"));
            }
            else
            {
                using (var tran = this.UnitOfWork.BeginTransaction())
                {
                    try
                    {
                        TimeTableTemplate template = dtoObject.ConvertToEntity();
                        template = await Repository.Create(template);
                        foreach (var dto in dtoObject.Details)
                        {
                            TimeTableTemplateDetail entity = dto.ConvertToEntity();
                            entity.TimeTableTemplateId = template.Id;
                            entity = await templateDetailRepository.Create(entity);
                        }
                        UnitOfWork.Save();
                        tran.Commit();
                        dtoObject.Id = template.Id;
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        throw ex;
                    }
                }
                return dtoObject;
            }
            return null;
        }

        public async Task<TimeTableTemplateDetailDTO> UpdateDetailAsync(TimeTableTemplateDetailDTO dtoObject)
        {
            var entity = await this.templateDetailRepository.GetDefaultAsync(dtoObject.Id);
            entity = dtoObject.ConvertToEntity(entity);
            entity = await this.templateDetailRepository.Update(entity);
            try
            {
                await UnitOfWork.SaveAsync();
            }
            catch (Exception exception)
            {
                throw exception;
            }

            dtoObject.ConvertFromEntity(entity);
            return dtoObject;
        }
    }
}
