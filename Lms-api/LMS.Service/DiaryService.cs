﻿using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.IRepository;
using LMS.Core.IService;
using LMS.Repository;

namespace LMS.Service
{
    public class DiaryService : BaseService<IDiaryRepository, Diary, DiaryDTO>, IDiaryService
    {
        public DiaryService(IUnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.DiaryRepository)
        {

        }
    }
}
