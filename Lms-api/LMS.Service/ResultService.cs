﻿using LMS.Core.IService;
using System;
using System.Threading.Tasks;
using LMS.Core.Entity;
using LMS.Core.IRepository;
using LMS.Core.DTO;
using System.Net.Http;
using LMS.Core.Infrastructure;
using System.Net;
using LMS.Core.Constant;
using System.Net.Http.Headers;
using LMS.Core.Enums;
using System.Collections.Generic;
using LMS.Repository;

namespace LMS.Service
{
    public class ResultService : IResultService
    {
        private IExceptionHelper exceptionHelper;
        private IRequestInfo requestInfo;
        private IResultRepository repository;
        IUnitOfWork unitOfWork;

        public ResultService(
            IUnitOfWork unitOfWork,
            IExceptionHelper exceptionHelper,
            IRequestInfo requestInfo,
            IResultRepository repository)
        {
            this.exceptionHelper = exceptionHelper;
            this.requestInfo = requestInfo;
            this.unitOfWork = unitOfWork;
            this.repository = repository;
        }

        public ResultSaveDTO GetResultDetail(int examID)
        {
            return this.repository.GetResultDetail(examID);
        }

        public List<StudentResultCommulativeDTO> GetStudentCommulativeResultList(int examID, int classroomID)
        {
            return this.repository.GetStudentCommulativeResultList(examID, classroomID);
        }

        public StudentResultListDTO GetStudentResultList(int examID, int classroomID, int courseID)
        {
            return this.repository.GetStudentResultList(examID, classroomID, courseID);
        }

        public bool SaveResult(ResultSaveDTO data)
        {
            return this.repository.SaveResult(data);
        }

        public List<KeyValueDTO> GetClassroomForTeacher(int examId, int userId)
        {
            return this.repository.GetClassroomForTeacher(examId, userId);
        }

        public List<KeyValueDTO> GetExamClassroomList(int examId)
        {
            return this.repository.GetExamClassroomList(examId);
        }

        public List<ExamListDTO> GetResultList(string prefix, int count, int page, out int pageCount)
        {
            if (this.unitOfWork.RequestInfo.Role.Equals(UserRoles.Teacher.ToString()))
                return this.repository.GetTeacherResultList(prefix, count, page, this.requestInfo.UserId, out pageCount);
            else
                return this.repository.GetResultList(prefix, count, page, out pageCount);
        }

        public StudentResultMasterDTO GetStudentResult(int examID, int studentID)
        {
            return this.repository.GetStudentResult(examID, studentID);
        }

        public List<KeyValueDTO> GetExamStudents(int examID, int classroomID)
        {
            return this.repository.GetExamStudents(examID, classroomID);
        }

        public ResultClassroomsDTO GetExamClassrooms(int examId, int userid)
        {
            return this.repository.GetExamClassrooms(examId, userid);
        }

        public void SaveClassroomResult(int examID, int classroomID, List<StudentResultCommulativeDTO> list)
        {
            this.repository.SaveClassroomResult(examID, classroomID, list);
        }

        public void PublishClassroomResult(int examID, int classroomID)
        {
            this.repository.PublishClassroomResult(examID, classroomID);
        }

        public bool IsExamPublishable(int examID)
        {
            return this.repository.IsExamPublishable(examID);
        }

        public bool PublishResult(int examID)
        {
            return this.repository.PublishResult(examID);
        }
        public List<StudentResultMasterDTO> GetStudentPastResult(int studentID)
        {
            return this.repository.GetStudentPastResult(studentID);
        }
    }
}
