﻿using System.Threading.Tasks;

namespace LMS.Core.Helper
{
    public interface IValidationHelper
    {
        Task<string> ValidateLoginUser();
    }
}
