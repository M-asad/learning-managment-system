﻿
namespace LMS.Core.Helper
{
    public static class UserHelper
    {
        public static string GeneratePassword() {
            return System.Web.Security.Membership.GeneratePassword(8, 2);
        }
    }
}
