﻿using LMS.Core.Constant;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.Helper
{
    public class ValidationHelper : IValidationHelper
    {
        private IUnitOfWork unitOfWork;
        private IRequestInfo requestInfo;
        private IExceptionHelper exceptionHelper;

        public ValidationHelper(IUnitOfWork unitOfWork, IRequestInfo requestInfo, IExceptionHelper exceptionHelper)
        {
            this.unitOfWork = unitOfWork;
            this.requestInfo = requestInfo;
            this.exceptionHelper = exceptionHelper;
        }

        public async Task<string> ValidateLoginUser()
        {
            int userId = this.requestInfo.UserId;
            if (userId < 1)
            {
                return Message.LoginUserNotFound;
            }

            var loginUser = await this.unitOfWork.UserRepository.GetEntityOnly(this.requestInfo.UserId);

            if (loginUser == null)
            {
                return Message.LoginUserNotFound;
            }

            return Message.SuccessfullyCompleted;
        }
    }
}
