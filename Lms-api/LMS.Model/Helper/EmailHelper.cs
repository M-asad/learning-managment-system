﻿using LMS.Core.DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.Helper
{
    public class EmailHelper
    {
        public static bool SendEmail(MailMessage mailMessage)
        {
            try
            {
                var config = GetEmailConfiguration();
                SmtpClient SmtpClient = new SmtpClient();
                SmtpClient.Host = config.SmtpClient;
                SmtpClient.EnableSsl = config.EnableSSL;
                SmtpClient.Port = config.SMTPPort;
                SmtpClient.Credentials = new System.Net.NetworkCredential(config.FromAddress, config.Password);
                SmtpClient.Send(mailMessage);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        public static SmtpClient GetSmtpClient()
        {
            var config = GetEmailConfiguration();
            SmtpClient SmtpClient = new SmtpClient();
            SmtpClient.Host = config.SmtpClient;
            SmtpClient.EnableSsl = config.EnableSSL;
            SmtpClient.Port = config.SMTPPort;
            SmtpClient.Credentials = new System.Net.NetworkCredential(config.FromAddress, config.Password);
            return SmtpClient;
        }

        public static EmailConfigurationDTO GetEmailConfiguration()
        {
            return new DTO.EmailConfigurationDTO()
            {
                FromAddress = System.Configuration.ConfigurationManager.AppSettings.Get("FromAddress"),
                SmtpClient = System.Configuration.ConfigurationManager.AppSettings.Get("SmtpClient"),
                Password = System.Configuration.ConfigurationManager.AppSettings.Get("Password"),
                SMTPPort = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("SMTPPort")),
                EnableSSL = (System.Configuration.ConfigurationManager.AppSettings.Get("EnableSSL").ToUpper() == "YES") ? true : false
            };
        }

        public static string GetTemplate(string type)
        {
            // var templatePath = System.Configuration.ConfigurationManager.AppSettings.Get("TemplatePath");
            var templatePath = System.Web.Hosting.HostingEnvironment.MapPath("~/EmailTemplates/");
            string filePath = templatePath + type + ".html";
            string htmlString = string.Empty;

            if (File.Exists(filePath))
            {
                htmlString = File.ReadAllText(filePath);
            }
            else
            {
                htmlString = File.ReadAllText(templatePath + "MasterPage.html");
            }
            return htmlString;
        }
    }
}
