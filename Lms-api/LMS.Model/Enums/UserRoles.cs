﻿namespace LMS.Core.Enums
{
    public enum UserRoles
    {
        SuperAdmin = 1,
        Admin = 2,
        Parent = 3,
        Teacher = 4
    }
}
