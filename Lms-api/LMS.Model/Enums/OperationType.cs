﻿namespace LMS.Core.Enums
{
    public enum OperationType
    {
        Create = 1,
        Read = 2,
        Update = 3,
        Delete = 4,
        Authorization = 5
    }
}
