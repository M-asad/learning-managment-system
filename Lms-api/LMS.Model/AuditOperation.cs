﻿using LMS.Core.Enums;
using System;

namespace LMS.Core
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class AuditOperation : System.Attribute
    {
        public AuditOperation(OperationType operationType)
        {
            OperationType = operationType;
        }

        public OperationType OperationType { get; set; }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class IgnoreAuditOperation : System.Attribute
    {
        public IgnoreAuditOperation()
        {
        }
    }
}
