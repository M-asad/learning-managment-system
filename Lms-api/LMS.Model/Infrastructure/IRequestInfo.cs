﻿using LMS.Core.DBContext;
using LMS.Core.Enums;

namespace LMS.Core.Infrastructure
{
    public interface IRequestInfo
    {
        int UserId { get; }

        string UserName { get; }

        string Role { get; }
        int RoleId { get; }
        // UserRoles UserRole { get; }

        LMSContext Context { get; set; }
    }
}
