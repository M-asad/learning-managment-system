﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.Infrastructure
{
    public interface IEmailService : IIdentityMessageService
    {
    }
}
