﻿using LMS.Core.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.Infrastructure
{
    public class ApplicationUserManager : UserManager<ApplicationUser, int>
    {
        public ApplicationUserManager(ApplicationUserStore userStore) : base(userStore)
        {
        }

        public ApplicationUserManager(ApplicationUserStore userStore, IEmailService emailService) : base(userStore)
        {
            var provider = new DpapiDataProtectionProvider("tokenProvider");
            this.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser, int>(provider.Create("dataProtection"));
            this.EmailService = emailService;            
        }
    }
}
