﻿using LMS.Core.DBContext;
using LMS.Core.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace LMS.Core.Infrastructure
{
    public class ApplicationRoleStore : RoleStore<ApplicationRole, int, ApplicationUserRole>
    {
        public ApplicationRoleStore(LMSContext context) : base(context)
        {
        }

        public ApplicationRoleStore(IRequestInfo requestInfo) : base(requestInfo.Context)
        {
        }
    }
}
