﻿using LMS.Core.DBContext;
using LMS.Core.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace LMS.Core.Infrastructure
{
    public class ApplicationUserStore : UserStore<ApplicationUser, ApplicationRole, int, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        public ApplicationUserStore(IRequestInfo requestInfo) : base(requestInfo.Context)
        {
        }

        public ApplicationUserStore(LMSContext context) : base(context)
        {
        }
    }
}