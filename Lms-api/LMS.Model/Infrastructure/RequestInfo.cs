﻿using LMS.Core.Constant;
using LMS.Core.DBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Runtime.Remoting.Messaging;

namespace LMS.Core.Infrastructure
{
    public class RequestInfo : IRequestInfo
    {
        private const string ApplicationConfigContextKey = "ApplicationConfigContext";
        private Dictionary<int, LMSContext> threadContexts = new Dictionary<int, LMSContext>();
        private LMSContext context;

        public RequestInfo()
        {
        }

        public int UserId
        {
            get
            {
                return Convert.ToInt32(this.GetValueFromClaims(General.ClaimsUserId));
            }
        }

        public string UserName
        {
            get
            {
                return this.GetValueFromClaims(General.ClaimsUserName);
            }
        }

        public LMSContext Context
        {
            get
            {
                LMSContext context;

                if (HttpContext.Current == null)
                {
                    context = CallContext.LogicalGetData("context") as LMSContext;

                    if (context == null)
                    {
                        object lockObj = new object();

                        var threadId = Thread.CurrentThread.ManagedThreadId;
                        if (!this.threadContexts.ContainsKey(threadId))
                        {
                            lock (lockObj)
                            {
                                if (!this.threadContexts.ContainsKey(threadId))
                                {
                                    this.threadContexts.Add(threadId, new LMSContext());
                                }
                            }
                        }

                        context = this.threadContexts[threadId];
                    }
                }
                else if (HttpContext.Current.Items.Contains(ApplicationConfigContextKey))
                {
                    context = (LMSContext)HttpContext.Current.Items[ApplicationConfigContextKey];
                }
                else
                {
                    context = new LMSContext();
                    HttpContext.Current.Items[ApplicationConfigContextKey] = context;
                }

                return context;
            }

            set
            {
                if (HttpContext.Current == null)
                {
                    var threadId = Thread.CurrentThread.ManagedThreadId;
                    this.threadContexts[threadId] = value;
                }
                else
                {
                    HttpContext.Current.Items[ApplicationConfigContextKey] = value;
                }
            }
        }

        public string Role
        {
            get
            {
                return this.GetValueFromClaims(General.ClaimsRoleName);
            }
        }

        public int RoleId
        {
            get
            {
                return Convert.ToInt32(this.GetValueFromClaims(General.ClaimsRoleId));
            }
        }

        //public UserRoles UserRole
        //{
        //    get
        //    {
        //        return Roles.GetRoleObject(Roles.GetRoleId(this.Role));
        //    }
        //}

        #region Private Funcltions
        private string GetValueFromClaims(string key)
        {
            if (HttpContext.Current == null || HttpContext.Current.User == null || HttpContext.Current.User.Identity == null)
            {
                return string.Empty;
            }

            var claims = (HttpContext.Current.User.Identity as ClaimsIdentity).Claims;
            var value = string.Empty;
            if (claims != null && claims.Count() > 0)
            {
                value = claims.FirstOrDefault(x => x.Type == key).Value;
            }

            return value;
        }
        #endregion
    }
}
