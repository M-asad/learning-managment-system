﻿using LMS.Core.Entity;
using Microsoft.AspNet.Identity;

namespace LMS.Core.Infrastructure
{
    public class ApplicationRoleManager : RoleManager<ApplicationRole, int>
    {
        public ApplicationRoleManager(ApplicationRoleStore roleStore) : base(roleStore)
        {
        }
    }   
}
