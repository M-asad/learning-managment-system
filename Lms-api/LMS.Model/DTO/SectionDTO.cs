﻿using LMS.Core.Entity;

namespace LMS.Core.DTO
{
    public class SectionDTO : BaseDTO<Section>
    {
        public string Title { get; set; }

        public override Section ConvertToEntity(Section entity)
        {
            entity = base.ConvertToEntity(entity);
            entity.Title = this.Title;
            return entity;
        }

        public override void ConvertFromEntity(Section entity)
        {
            base.ConvertFromEntity(entity);
            this.Title = entity.Title;
        }
    }
}
