﻿using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class DiaryDTO : BaseDTO<Diary>
    {
        public DiaryDTO()
        {
        }

        public DiaryDTO(Diary entity)
            : base(entity)
        {
        }

        public int ClassroomId { get; set; }

        public DateTime Date { get; set; }

        [Required]
        [StringLength(2000)]
        public string Detail { get; set; }

        public int CourseId { get; set; }

        public string Course { get; set; }

        public int TeacherId { get; set; }

        public string Teacher { get; set; }

        public override Diary ConvertToEntity(Diary entity)
        {
            entity = base.ConvertToEntity(entity);
            entity.ClassroomId = this.ClassroomId;
            entity.Date = Date;
            entity.Detail = Detail;
            entity.CourseId = CourseId;
            entity.TeacherId = TeacherId;
            return entity;
        }

        public override void ConvertFromEntity(Diary entity)
        {
            base.ConvertFromEntity(entity);
            this.Date = entity.Date;
            this.Detail = entity.Detail;
            this.CourseId = entity.CourseId;
            this.Course = entity.Course.Title;
            this.TeacherId = entity.TeacherId;
            this.Teacher = entity.Teacher.FullName;
        }
    }
}
