﻿using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class StudentListDTO : BaseDTO<Student>
    {
        public string GRNo { get; set; }

        public int? RollNo { get; set; }

        public string FullName { get; set; }
        
        public string Picture { get; set; }

        public string ClassroomName { get; set; }
    }
}
