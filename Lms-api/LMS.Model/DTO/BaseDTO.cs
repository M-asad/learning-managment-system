﻿using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LMS.Core.DTO
{
    public abstract class BaseDTO<TEntity> : IBase
        where TEntity : IAuditModel, new()
    {
        public BaseDTO()
        {
        }

        public BaseDTO(TEntity entity)
        {
            this.ConvertFromEntity(entity);
        }

        public int Id { get; set; }

        public static List<TDTO> ConvertEntityListToDTOList<TDTO>(IEnumerable<TEntity> entityList) where TDTO : BaseDTO<TEntity>, new()
        {
            var result = new List<TDTO>();

            if (entityList != null)
            {
                foreach (var entity in entityList)
                {
                    var dto = new TDTO();
                    dto.ConvertFromEntity(entity);
                    result.Add(dto);
                }
            }

            return result;
        }

        public static IList<TEntity> ConvertDTOListToEntity(IEnumerable<BaseDTO<TEntity>> dtoList, IEnumerable<TEntity> entityList)
        {
            var result = new List<TEntity>();
            if (dtoList != null)
            {
                foreach (var dto in dtoList)
                {
                    var entityFromDb = entityList.SingleOrDefault(x => x.Id.Equals(dto.Id));

                    if (entityFromDb != null)
                    {
                        result.Add(dto.ConvertToEntity(entityFromDb));
                    }
                    else
                    {
                        result.Add(dto.ConvertToEntity());
                    }
                }
            }

            foreach (var entity in entityList.Where(x => !dtoList.Any(y => y.Id.Equals(x.Id))))
            {
                entity.IsDeleted = true;
                result.Add(entity);
            }

            return result;
        }

        public static IList<TEntity> ConvertDTOListToEntity(IEnumerable<BaseDTO<TEntity>> dtoList)
        {
            var result = new List<TEntity>();

            if (dtoList != null)
            {
                foreach (var dto in dtoList)
                {
                    result.Add(dto.ConvertToEntity());
                }
            }

            return result;
        }

        public TEntity ConvertToEntity()
        {
            TEntity entity = new TEntity();
            return this.ConvertToEntity(entity);
        }

        public virtual TEntity ConvertToEntity(TEntity entity)
        {
            entity.Id = this.Id.Equals(0) ? entity.Id : this.Id;
            return entity;
        }

        public virtual void ConvertFromEntity(TEntity entity)
        {
            this.Id = entity.Id;
        }

        protected DateTime? GetNullableDate(DateTime? date)
        {
            return date == null? date : Convert.ToDateTime(date);
        }
    }
}
