﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class ExamDetailDTO
    {
        public int Id { get; set; }
        public int ExamId { get; set; }
        public string ExamType { get; set; }
        public string Description { get; set; }
        public string AcademicYear { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public List<ClassDTO> Classes { get; set; }
        public List<ExamTimeTableDTO> TimeTable { get; set; }
    }

    public class ExamTimeTableDTO
    {
        public string Date { get; set; }
        public List<ExamTimeTableDetailDTO> Details { get; set; }
    }

    public class ExamTimeTableDetailDTO
    {
        public int ExamDetailID { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public int CourseId { get; set; }
        public string Course { get; set; }
        public int? TheoryMarks { get; set; }
        public int? LabMarks { get; set; }
        public int? OtherMarks { get; set; }
        public int? Duration { get; set; }
    }

    public class ExamDetailSaveDTO
    {
        public int ExamId { get; set; }
        public int ExamDetailID { get; set; }
        public int ClassId { get; set; }
        public DateTime Date { get; set; }
        public int CourseId { get; set; }
        public int Duration { get; set; }
        public int TheoryMarks { get; set; }
        public int? LabMarks { get; set; }
        public int? OtherMarks { get; set; }
    }
}
