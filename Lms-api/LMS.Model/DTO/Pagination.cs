﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class PaginationDTO
    {
        public string Prefix { get; set; }
        public int Count { get; set; }
        public int Page { get; set; }
    }
}
