﻿using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    /*
    public class ClassroomDetailDTO
    {
        public ClassroomDetailDTO()
        {
            CourseTeachers = new List<CourseTeacherDTO>();
            ClassroomStudents = new List<ClassroomStudentDetailDTO>();
        }
        
        public int ClassroomId { get; set; }

        public string ClassroomName { get; set; }

        public string AcademicYear { get; set; }

        public int ClassTeacherId { get; set; }

        public string ClassTeacher { get; set; }
        
        public int ClassId { get; set; }

        public int SectionId { get; set; }

        public List<ClassroomStudentDetailDTO> ClassroomStudents { get; set; }

        public List<CourseTeacherDTO> CourseTeachers { get; set; }
    }
    */
    public class ClassroomMasterDTO
    {
        public int ClassroomId { get; set; }

        public string ClassroomName { get; set; }

        public string AcademicYear { get; set; }

        public int ClassTeacherId { get; set; }

        public string ClassTeacher { get; set; }
    }
}
