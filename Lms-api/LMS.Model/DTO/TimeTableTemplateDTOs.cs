﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMS.Core.Entity;

namespace LMS.Core.DTO
{
    public class TimeTableTemplateDTO : BaseDTO<TimeTableTemplate>
    {
        public string Title { get; set; }

        public List<TimeTableTemplateDetailDTO> Details { get; set; }

        public override TimeTableTemplate ConvertToEntity(TimeTableTemplate entity)
        {
            entity = base.ConvertToEntity(entity);
            entity.Id = this.Id;
            entity.Title = this.Title;

            return entity;
            //List<TimeTableTemplateDetail> details = new List<TimeTableTemplateDetail>();
            //foreach (var row in Details)
            //{
            //    details.Add(new TimeTableTemplateDetail()
            //    {
            //        TimeTableTemplateId = row.TimeTableTemplateId,
            //        Id = row.Id,
            //        TimeTablePeriodId = row.TimeTablePeriodId,
            //        StartTime = row.StartTime,
            //        EndTime = row.EndTime,
            //        Day = row.Day,
            //        CreatedBy = UserId.ToString(),
            //        CreatedOn = DateTime.Now
            //    });
            //}
            //return new TimeTableTemplate()
            //{
            //    Id = Id,
            //    Title = Title,
            //    TimeTableTemplateDetails = details
            //};
        }

        public override void ConvertFromEntity(TimeTableTemplate entity)
        {
            base.ConvertFromEntity(entity);
            this.Id = entity.Id;
            this.Title = entity.Title;
            this.Details = TimeTableTemplateDetailDTO.ConvertEntityListToDTOList<TimeTableTemplateDetailDTO>(entity.TimeTableTemplateDetails);
        }
    }
}
