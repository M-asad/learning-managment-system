﻿using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class AssignmentListDTO : BaseDTO<Assignment>
    {
        public string Title { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime EndDate { get; set; }

        public string Course { get; set; }

        public string Classroom { get; set; }
    }
}
