﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class EnrollStudentDTO
    {
        public EnrollStudentDTO() { }
        public int ClassroomId { get; set; }

        public int StudentId { get; set; }

        public ClassroomStudentDTO ConvertToClassroomStudentDTO()
        {
            ClassroomStudentDTO result = new ClassroomStudentDTO();
            result.ClassroomId = this.ClassroomId;
            result.StudentId = this.StudentId;
            return result;
        }
    }
}
