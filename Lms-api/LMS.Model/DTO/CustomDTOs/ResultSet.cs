﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class ResultSet<T> : IResultSet<T>
    {
        public IEnumerable<T> Data { get; set; }
        public int PageCount { get; set; }
    }

    public interface IResultSet<T> 
    {
        IEnumerable<T> Data { get; set; }
        int PageCount { get; set; }
    }
}
