﻿using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class ParentSimpleDTO : BaseDTO<Parent>
    {
        public ParentSimpleDTO()
        {
        }
        public ParentSimpleDTO(Parent entity)
            : base(entity)
        {
        }

        public string FullName { get; set; }

        public override Parent ConvertToEntity(Parent entity)
        {
            entity = base.ConvertToEntity(entity);
            entity.FullName = this.FullName;

            return entity;
        }

        public override void ConvertFromEntity(Parent entity)
        {
            base.ConvertFromEntity(entity);
            FullName = entity.FullName;
        }
    }
}
