﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class ClassroomStudentDetailDTO
    {
        public int StudentID { get; set; }
        public string GRNo { get; set; }
        public string StudentName { get; set; }
        public int ParentId { get; set; }
        public string ParentName { get; set; }
        public int ClassroomId { get; set; }
        public string ClassroomName { get; set; }
        public int RollNo { get; set; }
    }
}
