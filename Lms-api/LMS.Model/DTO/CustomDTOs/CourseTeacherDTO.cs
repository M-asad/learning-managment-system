﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class CourseTeacherDTO
    {
        public int CourseId { get; set; }

        public string CourseName { get; set; }

        public int TeacherId { get; set; }

        public string TeacherName { get; set; }
    }
}
