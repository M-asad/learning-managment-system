﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO.CustomDTOs
{
    public class StudentSimpleDTO
    {
        public int Id { get; set; }

        public string GRNo { get; set; }

        public string FullName { get; set; }

        public int ParentId { get; set; }

        public string Parent { get; set; }
    }
}
