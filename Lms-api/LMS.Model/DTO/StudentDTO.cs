﻿using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class StudentDTO : BaseDTO<Student>
    {
        public StudentDTO()
        {
        }

        public StudentDTO(Student entity)
            : base(entity)
        {
        }

        [Required]
        public string GRNo { get; set; }

        [Required]
        public string FullName { get; set; }

        [Required]
        public DateTime JoinDate { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }

        public string BirthPlace { get; set; }

        [Required]
        public string Gender { get; set; }

        public string BloodGroup { get; set; }

        public string Nationality { get; set; }

        public string Religion { get; set; }

        public DateTime? LeavingDate { get; set; }

        public ParentSimpleDTO Parent { get; set; }

        [Required]
        public int ParentId { get; set; }

        public string Picture { get; set; }

        public int ClassroomId { get; set; }

        public int RollNo { get; set; }

        public string ClassroomName { get; set; }

        public override Student ConvertToEntity(Student entity)
        {
            entity = base.ConvertToEntity(entity);
            entity.GRNo = this.GRNo;
            entity.FullName = this.FullName;
            entity.Gender = this.Gender;
            entity.BirthDate = this.BirthDate;
            entity.BirthPlace = this.BirthPlace;
            entity.JoinDate = this.JoinDate;
            entity.LeavingDate = this.LeavingDate;
            entity.Nationality = this.Nationality;
            entity.Religion = this.Religion;
            entity.ParentId = this.ParentId;
            entity.Picture = this.Picture;
            entity.BloodGroup = this.BloodGroup;
            return entity;
        }

        public override void ConvertFromEntity(Student entity)
        {
            base.ConvertFromEntity(entity);

            this.GRNo = entity.GRNo;
            this.FullName = entity.FullName;
            this.Gender = entity.Gender;
            this.BirthDate = entity.BirthDate;
            this.BirthPlace = entity.BirthPlace;
            this.JoinDate = entity.JoinDate;
            this.LeavingDate = entity.LeavingDate;
            this.Nationality = entity.Nationality;
            this.Religion = entity.Religion;
            this.Picture = entity.Picture;
            this.BloodGroup = entity.BloodGroup;

            var classroomStudent = entity.ClassroomStudents.FirstOrDefault();
            if (classroomStudent != null)
            {
                this.ClassroomId = classroomStudent.Id;
                this.RollNo = classroomStudent.RollNo;
            }
            
            if (entity.Parent != null)
            {
                ParentSimpleDTO parentDTO = new ParentSimpleDTO();
                parentDTO.ConvertFromEntity(entity.Parent);
                this.Parent = parentDTO;
            }
            this.ParentId = entity.ParentId;
        }

    }
}
