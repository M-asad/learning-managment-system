﻿using LMS.Core.Entity;

namespace LMS.Core.DTO
{
    public class AssignmentDetailDTO : BaseDTO<AssignmentDetail>
    {
        public int StudentId { get; set; }

        public double? ObtainedMarks { get; set; }

        public bool? HasPassed { get; set; }

        public AssignmentDetailDTO() { }
        public AssignmentDetailDTO(AssignmentDetail entity)
            : base(entity)
        {
        }

        public override AssignmentDetail ConvertToEntity(AssignmentDetail entity)
        {
            entity = base.ConvertToEntity(entity);
            entity.StudentId = this.StudentId;
            entity.ObtainedMarks = this.ObtainedMarks;
            entity.HasPassed = this.HasPassed;
            return entity;
        }

        public override void ConvertFromEntity(AssignmentDetail entity)
        {
            base.ConvertFromEntity(entity);
            this.StudentId = entity.StudentId;
            this.ObtainedMarks = entity.ObtainedMarks;
            this.HasPassed = entity.HasPassed;
        }
    }
}
