﻿using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{

    public class ClassroomDTO : BaseDTO<Classroom>
    {
        public ClassroomDTO()
        {
            ClassroomCourses = new List<ClassroomCourseDTO>();
            ClassroomStudents = new List<ClassroomStudentDTO>();
        }

        public ClassroomDTO(Classroom entity)
            : base(entity)
        {
        }

        public string ClassroomName { get; set; }

        public int ClassId { get; set; }

        public int SectionId { get; set; }

        public int AcademicYearId { get; set; }

        public string AcademicYear { get; set; }

        public int ClassTeacherId { get; set; }

        public string ClassTeacher { get; set; }

        public IList<ClassroomCourseDTO> ClassroomCourses { get; set; }

        public IList<ClassroomStudentDTO> ClassroomStudents { get; set; }

        public override Classroom ConvertToEntity(Classroom entity)
        {
            entity = base.ConvertToEntity(entity);
            entity.ClassId = this.ClassId;
            entity.SectionId = this.SectionId;
            entity.AcademicYearId = this.AcademicYearId;
            entity.ClassTeacherId = this.ClassTeacherId;
            entity.ClassroomName = this.ClassroomName;

            return entity;
        }

        public override void ConvertFromEntity(Classroom entity)
        {
            base.ConvertFromEntity(entity);

            this.ClassId = entity.ClassId;
            this.SectionId = entity.SectionId;
            this.AcademicYearId = entity.AcademicYearId;
            this.ClassTeacherId = entity.ClassTeacherId;
            this.ClassroomName = entity.ClassroomName;
            //this.ClassroomCourses = ClassroomCourseDTO.ConvertEntityListToDTOList<ClassroomCourseDTO>(entity.ClassroomCourses);
            //this.ClassroomStudents = ClassroomStudentDTO.ConvertEntityListToDTOList<ClassroomStudentDTO>(entity.ClassroomStudents);
        }
    }
}
