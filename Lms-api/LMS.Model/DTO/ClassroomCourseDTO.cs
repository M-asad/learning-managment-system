﻿using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class ClassroomCourseDTO : BaseDTO<ClassroomCourse>
    {
        public int ClassroomId { get; set; }

        public int CourseId { get; set; }

        public string CourseName { get; set; }

        public int TeacherId { get; set; }

        public string TeacherName { get; set; }

        //public CourseDTO Course { get; set; }

        //public TeacherDTO Teacher { get; set; }

        public override ClassroomCourse ConvertToEntity(ClassroomCourse entity)
        {
            entity = base.ConvertToEntity(entity);
            entity.ClassroomId = this.ClassroomId;
            entity.CourseId = this.CourseId;
            entity.TeacherId = this.TeacherId;
            //if (this.Course != null) entity.Course = this.Course.ConvertToEntity();
            //if (this.Teacher != null) entity.Teacher = this.Teacher.ConvertToEntity();

            return entity;
        }

        public override void ConvertFromEntity(ClassroomCourse entity)
        {
            base.ConvertFromEntity(entity);

            this.ClassroomId = entity.ClassroomId;
            this.CourseId = entity.CourseId;
            this.TeacherId = entity.TeacherId;
            //if (this.Course != null) this.Course.ConvertFromEntity(entity.Course);
            //if (this.Teacher != null) this.Teacher.ConvertFromEntity(entity.Teacher);
        }
    }
}
