﻿using LMS.Core.Entity;
using System.Collections.Generic;

namespace LMS.Core.DTO
{
    public class NotificationDTO : BaseDTO<Notification>
    {
        public NotificationDTO()
        {
        }

        public NotificationDTO(Notification entity)
            : base(entity)
        {
        }

        public int NotificationTypeId { get; set; }

        public int UserId { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public bool? IsRead { get; set; }

        public bool IsEmailSent { get; set; }

        public string Exception { get; set; }

        public int RetryCount { get; set; }

        public override Notification ConvertToEntity(Notification entity)
        {
            entity = base.ConvertToEntity(entity);
            entity.UserId = this.UserId;
            entity.NotificationTypeId = this.NotificationTypeId;
            entity.Subject = this.Subject;
            entity.Body = this.Body;
            entity.IsRead = this.IsRead;
            entity.IsEmailSent = this.IsEmailSent;
            entity.Exception = this.Exception;
            entity.RetryCount = this.RetryCount;
            return entity;
        }

        public override void ConvertFromEntity(Notification entity)
        {
            base.ConvertFromEntity(entity);
            this.NotificationTypeId = entity.NotificationTypeId;
            this.UserId = entity.UserId;
            this.Subject = entity.Subject;
            this.Body = entity.Body;
            this.IsRead = entity.IsRead;
            this.IsEmailSent = entity.IsEmailSent;
            this.Exception = entity.Exception;
            this.RetryCount = entity.RetryCount;
        }
    }
}
