﻿using LMS.Core.Entity;

namespace LMS.Core.DTO
{
    public class GradeDTO : BaseDTO<Grade>
    {
        public string Title { get; set; }

        public int StartPercentage { get; set; }

        public int EndPercentage { get; set; }

        public override Grade ConvertToEntity(Grade entity)
        {
            entity = base.ConvertToEntity(entity);
            entity.Title = this.Title;
            entity.StartPercentage = this.StartPercentage;
            entity.EndPercentage = this.EndPercentage;
            return entity;
        }

        public override void ConvertFromEntity(Grade entity)
        {
            base.ConvertFromEntity(entity);
            this.Title = entity.Title;
            this.StartPercentage = entity.StartPercentage;
            this.EndPercentage = entity.EndPercentage;
        }
    }
}
