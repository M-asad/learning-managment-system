﻿using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class ParentDTO : BaseDTO<Parent>
    {
        public ParentDTO()
        {
        }
        public ParentDTO(Parent entity)
            : base(entity)
        {
        }

        [Required]
        public string FullName { get; set; }

        [Required]
        public string Address { get; set; }

        public string Phone { get; set; }

        [Required]
        public string Cell { get; set; }

        public string Email { get; set; }

        [Required]
        public string CNIC { get; set; }

        [Required]
        public string Gender { get; set; }

        public bool? EmailOptIn { get; set; }

        public IList<StudentListDTO> Students { get; set; }

        public override Parent ConvertToEntity(Parent entity)
        {
            entity = base.ConvertToEntity(entity);
            entity.FullName = this.FullName;
            entity.CNIC = this.CNIC;
            entity.Address = this.Address;
            entity.Phone = this.Phone;
            entity.Email = this.Email;
            entity.Cell = this.Cell;
            entity.Gender = this.Gender;
            entity.EmailOptIn = this.EmailOptIn;

            return entity;
        }

        public override void ConvertFromEntity(Parent entity)
        {
            base.ConvertFromEntity(entity);
            FullName = entity.FullName;
            CNIC = entity.CNIC;
            Address = entity.Address;
            Phone = entity.Phone;
            Email = entity.Email;
            Cell = entity.Cell;
            Gender = entity.Gender;
            EmailOptIn = entity.EmailOptIn;
        }
    }
}
