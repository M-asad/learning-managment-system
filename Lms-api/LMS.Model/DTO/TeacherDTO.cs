﻿using LMS.Core.Entity;
using System;
using System.ComponentModel.DataAnnotations;

namespace LMS.Core.DTO
{
    public class TeacherDTO : BaseDTO<Teacher>
    {
        public TeacherDTO()
        {
        }
        public TeacherDTO(Teacher entity)
            : base(entity)
        {
        }

        [Required]
        public string FullName { get; set; }

        [Required]
        public DateTime? JoinDate { get; set; }

        public DateTime? ResignationDate { get; set; }

        [Required]
        public DateTime? BirthDate { get; set; }

        public string Qualification { get; set; }

        [Required]
        public string Gender { get; set; }

        public string BloodGroup { get; set; }

        public string Nationality { get; set; }

        public string Religion { get; set; }

        public string Picture { get; set; }

        [Required]
        public string CNIC { get; set; }

        [Required]
        public string Address { get; set; }
        
        public string Phone { get; set; }

        public string Email { get; set; }

        [Required]
        public string Cell { get; set; }

        public override Teacher ConvertToEntity(Teacher entity)
        {
            entity = base.ConvertToEntity(entity);

            //entity.Id = this.Id == 0 ? entity.Id : this.Id;

            entity.FullName = this.FullName;
            entity.JoinDate = this.JoinDate;
            entity.ResignationDate = this.ResignationDate;
            entity.BirthDate = this.BirthDate;
            entity.Qualification = this.Qualification;
            entity.Gender = this.Gender;
            entity.BloodGroup = this.BloodGroup;
            entity.Nationality = this.Nationality;
            entity.Religion = this.Religion;
            entity.Picture = this.Picture;
            entity.CNIC = this.CNIC;
            entity.Address = this.Address;
            entity.Phone = this.Phone;
            entity.Email = this.Email;
            entity.Cell = this.Cell;

            return entity;
        }

        public override void ConvertFromEntity(Teacher entity)
        {
            base.ConvertFromEntity(entity);
            //Id = entity.Id;
            FullName = entity.FullName;
            JoinDate = entity.JoinDate;
            ResignationDate = entity.ResignationDate;
            BirthDate = entity.BirthDate;
            Qualification = entity.Qualification;
            Gender = entity.Gender;
            BloodGroup = entity.BloodGroup;
            Nationality = entity.Nationality;
            Religion = entity.Religion;
            Picture = entity.Picture;
            CNIC = entity.CNIC;
            Address = entity.Address;
            Phone = entity.Phone;
            Email = entity.Email;
            Cell = entity.Cell;
        }
    }    
}
