﻿using LMS.Core.Entity;
using LMS.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LMS.Core.DTO
{
    public class AssignmentDTO : BaseDTO<Assignment>
    {
        public AssignmentDTO() { }
        public AssignmentDTO(Assignment entity)
            : base(entity)
        {
        }

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime Date { get; set; }

        public int CourseId { get; set; }

        public int ClassroomId { get; set; }

        public double TotalMarks { get; set; }

        public double PassingMarks { get; set; }

        public List<AssignmentDetailDTO> Details { get; set; }
        public List<string> AttachmentURLs { get; set; }

        public override Assignment ConvertToEntity(Assignment entity)
        {
            entity = base.ConvertToEntity(entity);
            List<AssignmentAttachment> attachments = new List<AssignmentAttachment>();
            if (this.AttachmentURLs != null)
            {
                this.AttachmentURLs.ForEach(u => attachments.Add(new AssignmentAttachment() { FIleURL = u }));
            }
            entity.Title = this.Title;
            entity.Description = this.Description;
            entity.CourseId = this.CourseId;
            entity.ClassroomId = this.ClassroomId;
            entity.Date = this.Date;
            entity.TotalMarks = this.TotalMarks;
            entity.PassingMarks = this.PassingMarks;
            entity.AssignmentAttachments = attachments;
            return entity;
        }

        public override void ConvertFromEntity(Assignment entity)
        {
            base.ConvertFromEntity(entity);
            this.Title = entity.Title;
            this.Description = entity.Description;
            this.CourseId = entity.CourseId;
            this.ClassroomId = entity.ClassroomId;
            this.Date = entity.Date;
            this.TotalMarks = entity.TotalMarks;
            this.PassingMarks = entity.PassingMarks;
            this.Details = AssignmentDetailDTO.ConvertEntityListToDTOList<AssignmentDetailDTO>(entity.AssignmentDetails);
            this.AttachmentURLs = entity.AssignmentAttachments.Select(x => x.FIleURL).ToList();
        }
    }
}