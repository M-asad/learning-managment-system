﻿using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class ClassroomStudentDTO : BaseDTO<ClassroomStudent>
    {
        public ClassroomStudentDTO()
        {
        }
        public ClassroomStudentDTO(ClassroomStudent entity)
            : base(entity)
        {
        }
        public int ClassroomId { get; set; }

        public int StudentId { get; set; }

        public int RollNo { get; set; }

        public bool? HasPromoted { get; set; }

        public string GRNo { get; set; }

        public string StudentName { get; set; }

        public int ParentId { get; set; }

        public string ParentName { get; set; }

        public override ClassroomStudent ConvertToEntity(ClassroomStudent entity)
        {
            entity = base.ConvertToEntity(entity);
            entity.ClassroomId = this.ClassroomId;
            entity.StudentId = this.StudentId;
            entity.RollNo = this.RollNo;
            entity.HasPromoted = this.HasPromoted;
            
            return entity;
        }

        public override void ConvertFromEntity(ClassroomStudent entity)
        {
            base.ConvertFromEntity(entity);

            this.ClassroomId = entity.ClassroomId;
            this.StudentId = entity.StudentId;
            this.RollNo = entity.RollNo;
            this.HasPromoted = entity.HasPromoted;
            if (entity.Student != null)
            {
                this.StudentId = entity.Student.Id;
                this.StudentName = entity.Student.FullName;
            }
        }

        public void ConvertFromDTO(EnrollStudentDTO dto)
        {
            this.ClassroomId = dto.ClassroomId;
            this.StudentId = dto.StudentId;
        }
    }
}
