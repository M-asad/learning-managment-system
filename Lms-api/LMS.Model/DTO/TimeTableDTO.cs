﻿using System;
using LMS.Core.Entity;

namespace LMS.Core.DTO
{
    public class TimeTableDTO : BaseDTO<TimeTable>
    {
        public int TimeTableTemplateDetailId { get; set; }

        public int ClassroomId { get; set; }

        public int CourseId { get; set; }

        public override TimeTable ConvertToEntity(TimeTable entity)
        {
            entity = base.ConvertToEntity(entity);
            entity.TimeTableTemplateDetailId = this.TimeTableTemplateDetailId;
            entity.ClassroomId = this.ClassroomId;
            entity.CourseId = this.CourseId;

            return entity;
        }

        public override void ConvertFromEntity(TimeTable entity)
        {
            base.ConvertFromEntity(entity);
            this.TimeTableTemplateDetailId = entity.TimeTableTemplateDetailId;
            this.ClassroomId = entity.ClassroomId;
            this.CourseId = entity.CourseId;
        }
    }
}
