﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class EmailConfigurationDTO
    {
        public string FromAddress { get; set; }

        public string SmtpClient { get; set; }

        public string Password { get; set; }

        public int SMTPPort { get; set; }

        public bool EnableSSL { get; set; }
    }
}
