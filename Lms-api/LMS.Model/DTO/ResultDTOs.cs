﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class StudentResultDTO
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public string GRNo { get; set; }
        public int? TheoryMarks { get; set; }
        public int? LabMarks { get; set; }
        public int? OtherMarks { get; set; }
    }

    public class StudentResultListDTO
    {
        public int? TheoryMarks { get; set; }
        public int? LabMarks { get; set; }
        public int? OtherMarks { get; set; }
        public List<StudentResultDTO> ResultList { get; set; }
    }

    public class StudentResultCommulativeDTO
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public string GRNo { get; set; }
        public string Total { get; set; }
        public string Obtained { get; set; }
        public string Grade { get; set; }
        public string Percentage { get; set; }
        public string Remarks { get; set; }
    }

    public class SaveClassroomResult
    {
        public int examId { get; set; }
        public int ClassroomId { get; set; }
        public List<StudentResultCommulativeDTO> results { get; set; }
    }

    public class PublishClassroomResults
    {
        public int examId { get; set; }
        public int ClassroomId { get; set; }
    }
}
