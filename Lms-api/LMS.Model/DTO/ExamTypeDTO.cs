﻿using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LMS.Core.DTO
{
    public class ExamTypeDTO : BaseDTO<ExamType>
    {
        public string Type { get; set; }

        public override ExamType ConvertToEntity(ExamType entity)
        {
            entity = base.ConvertToEntity(entity);
            entity.Type = this.Type;

            return entity;
        }

        public override void ConvertFromEntity(ExamType entity)
        {
            base.ConvertFromEntity(entity);
            this.Type = entity.Type;
        }
    }
}
