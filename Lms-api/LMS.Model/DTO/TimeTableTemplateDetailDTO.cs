﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMS.Core.Entity;

namespace LMS.Core.DTO
{
    public class TimeTableTemplateDetailDTO : BaseDTO<TimeTableTemplateDetail>
    {
        public int TimeTableTemplateId { get; set; }

        public int TimeTablePeriodId { get; set; }

        public TimeSpan StartTime { get; set; }

        public TimeSpan EndTime { get; set; }

        public int Day { get; set; }

        public override void ConvertFromEntity(TimeTableTemplateDetail entity)
        {
            base.ConvertFromEntity(entity);
            this.TimeTableTemplateId = entity.TimeTableTemplateId;
            this.TimeTablePeriodId = entity.TimeTablePeriodId;
            this.StartTime = entity.StartTime;
            this.EndTime = entity.EndTime;
            this.Day = entity.Day;
        }

        public override TimeTableTemplateDetail ConvertToEntity(TimeTableTemplateDetail entity)
        {
            entity = base.ConvertToEntity(entity);
            entity.TimeTableTemplateId = this.TimeTableTemplateId;
            entity.TimeTablePeriodId = TimeTablePeriodId;
            entity.StartTime = this.StartTime;
            entity.EndTime = this.EndTime;
            entity.Day = this.Day;

            return entity;
        }
    }
}
