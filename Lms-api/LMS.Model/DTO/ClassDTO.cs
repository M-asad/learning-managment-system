﻿using LMS.Core.Entity;

namespace LMS.Core.DTO
{
    public class ClassDTO : BaseDTO<Class>
    {
        public string Name { get; set; }

        public override Class ConvertToEntity(Class entity)
        {
            entity = base.ConvertToEntity(entity);
            entity.Name = this.Name;
            return entity;
        }

        public override void ConvertFromEntity(Class entity)
        {
            base.ConvertFromEntity(entity);
            this.Name = entity.Name;
        }
    }
}
