﻿using LMS.Core.Constant;
using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class UserDTO : BaseDTO<ApplicationUser>
    {
        public UserDTO()
        {
        }

        public UserDTO(ApplicationUser entity)
            : base(entity)
        {
        }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string Role { get; set; }

        [Required]
        public int RoleId { get; set; }

        public int? PictureResouceId { get; set; }

        public override ApplicationUser ConvertToEntity(ApplicationUser entity)
        {
            entity = base.ConvertToEntity(entity);

            entity.Id = this.Id == 0 ? entity.Id : this.Id;

            if (!string.IsNullOrEmpty(Roles.GetRole(this.RoleId)))
            {
                entity.Roles.Add(new ApplicationUserRole { RoleId = this.RoleId, UserId = entity.Id });
            }
            entity.Email = this.Email;
            entity.UserName = this.UserName;
            entity.EmailConfirmed = true;
            entity.PictureResourceId = this.PictureResouceId;

            return entity;
        }

        public override void ConvertFromEntity(ApplicationUser entity)
        {
            base.ConvertFromEntity(entity);

            this.Id = entity.Id;
            this.UserName = entity.UserName;
            this.Email = entity.Email;
            this.PictureResouceId = entity.PictureResourceId;

            if (entity.Roles != null && entity.Roles.Count > 0)
            {
                if (entity.Roles.Count > 0)
                {
                    this.RoleId = entity.Roles.FirstOrDefault().RoleId;
                    this.Role = Roles.GetRole(this.RoleId);
                }
            }
        }

        public void ConvertFromEntity(ApplicationUser entity, Action<UserDTO, ApplicationUser> action)
        {
            this.ConvertFromEntity(entity);
            action(this, entity);
        }

        public void ConvertFromEntity(ApplicationUser entity, List<Action<UserDTO, ApplicationUser>> actions)
        {
            foreach (var action in actions)
            {
                this.ConvertFromEntity(entity, action);
            }
        }
    }
}
