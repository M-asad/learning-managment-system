﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class ResultSaveDTO
    {
        public int ExamId { get; set; }
        public int ClassroomId { get; set; }
        public int CourseId { get; set; }
        public List<StudentResultDTO> StudentResults { get; set; }
    }

}
