﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    #region Calendar DTOs
    public class AttendanceCalendarDTO
    {
        public string GrNo { get; set; }

        public int StudentId { get; set; }

        public string StudentName { get; set; }

        public List<CalendarDetailDTO> Attendance { get; set; }
    }

    public class CalendarDetailDTO
    {
        public int AttendanceID { get; set; }

        public DateTime Date { get; set; }

        public string AttendanceType { get; set; }
    }
    #endregion

    public class ClassAttendanceDTO
    {
        public int ClassroomId { get; set; }

        public string ClassroomTitle { get; set; }

        public DateTime AttendanceDate { get; set; }

        public List<StudentAttendanceDTO> studentAttendance { get; set; }
    }

    public class StudentAttendanceDTO
    {
        public string GrNo { get; set; }

        public int StudentId { get; set; }

        public string StudentName { get; set; }

        public int AttendanceID { get; set; }

        public string AttendanceType { get; set; }
    }


    public class AttendanceUpdateDTO
    {
        public int AttendanceID { get; set; }

        public string AttendanceType { get; set; }
    }

    #region Save Attendance

    public class SaveClassAttendanceDTO
    {
        public int ClassroomId { get; set; }

        public DateTime AttendanceDate { get; set; }

        public List<SaveStudentAttendanceDTO> studentAttendance { get; set; }
    }

    public class SaveStudentAttendanceDTO
    {
        public int StudentId { get; set; }

        public string AttendanceType { get; set; }
    }
    #endregion
}
