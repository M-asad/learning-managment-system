﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class ExamListDTO
    {
        public int Id { get; set; }
        public string ExamType { get; set; }
        public string Description { get; set; }
        public string AcademicYear { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}
