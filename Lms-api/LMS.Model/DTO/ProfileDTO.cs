﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class ProfileDTO
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string Picture { get; set; }
        public string Role { get; set; }
    }
}
