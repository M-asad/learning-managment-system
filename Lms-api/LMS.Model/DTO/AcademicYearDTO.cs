﻿using LMS.Core.Entity;
using System;

namespace LMS.Core.DTO
{
    public class AcademicYearDTO : BaseDTO<AcademicYear>
    {
        public AcademicYearDTO() { }

        public AcademicYearDTO(AcademicYear entity) : base(entity)
        {
        }

        public string Title { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool IsCurrent { get; set; }


        public override AcademicYear ConvertToEntity(AcademicYear entity)
        {
            entity = base.ConvertToEntity(entity);
            entity.Title = this.Title;
            entity.StartDate = this.StartDate;
            entity.EndDate = this.EndDate;
            entity.IsCurrent = this.IsCurrent;

            return entity;
        }

        public override void ConvertFromEntity(AcademicYear entity)
        {
            base.ConvertFromEntity(entity);
            this.Title = entity.Title;
            this.StartDate = entity.StartDate;
            this.EndDate = entity.EndDate;
            this.IsCurrent = entity.IsCurrent;
        }
    }
}
