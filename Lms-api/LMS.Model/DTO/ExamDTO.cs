﻿using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LMS.Core.DTO
{
    public class ExamDTO : BaseDTO<Exam>
    {
        public int ExamTypeID { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int AcademicYearId { get; set; }
        public int[] ClassIds { get; set; }

        public override Exam ConvertToEntity(Exam entity)
        {
            entity = base.ConvertToEntity(entity);
            entity.ExamTypeID = this.ExamTypeID;
            entity.Description = this.Description;
            entity.StartDate= this.StartDate;
            entity.EndDate = this.EndDate;
            entity.AcademicYearId = this.AcademicYearId;
            entity.ClassIds = String.Join(",", this.ClassIds);

            return entity;
        }

        public override void ConvertFromEntity(Exam entity)
        {
            base.ConvertFromEntity(entity);

            this.ExamTypeID = entity.ExamTypeID;
            this.Description = entity.Description;
            this.StartDate = entity.StartDate;
            this.EndDate = entity.EndDate;
            this.AcademicYearId = entity.AcademicYearId;
            this.ClassIds = entity.ClassIds.Split(',').Select(x => Convert.ToInt32(x)).ToArray();
        }
    }
}
