﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{

    public class ResultClassroomsDTO
    {
        public int ExamId { get; set; }
        public string ExamName { get; set; }
        public List<ResultClassroomDetailDTO> Details { get; set; }
    }

    public class ResultClassroomDetailDTO
    {
        public int ClassroomId { get; set; }
        public string ClassName { get; set; }
        public int TotalStudents { get; set; }
        public int PassedStudents { get; set; }
        public bool IsPublished { get; set; }
        public int ClassTeacherId { get; set; }
        public bool IsClassTeacher { get; set; }
    }


}
