﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class ClassroomListDTO
    {
        public int ClassId { get; set; }

        public string Class { get; set; }

        public int CoursesAssigned { get; set; }

        public int StudentEnrolled { get; set; }

        public int TeachersAssigned { get; set; }

        public int Sections { get; set; }

        public List<ClassroomSectionDTO> ClassList { get; set; }
    }

    public class ClassroomSectionDTO
    {
        public int ClassroomId { get; set; }

        public string Section { get; set; }
    }
}
