﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class AssignmentEmailDTO
    {
        public string Student { get; set; }
        public string Parent { get; set; }
        public int ParentUserId { get; set; }
        public bool EmailOptIn { get; set; }
        public string Email { get; set; }
        public string Title { get; set; }
        public DateTime DueDate { get; set; }
        public string Course { get; set; }
    }
}
