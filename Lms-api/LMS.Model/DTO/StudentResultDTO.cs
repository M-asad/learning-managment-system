﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class StudentResultMasterDTO
    {
        public string ExamType { get; set; }
        public string ExamDescription { get; set; }
        public string AcademicYear { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StudentName { get; set; }
        public string TotalMarks { get; set; }
        public string TotalObtained { get; set; }
        public string Percentage { get; set; }
        public string Grade { get; set; }
        public string Remarks { get; set; }

        public List<StudentResultDetailViewDTO> DetailList { get; set; }
    }

    public class StudentResultDetailViewDTO
    {
        public string Course { get; set; }
        public int? TheoryTotal { get; set; }
        public decimal TheoryObtained { get; set; }
        public int? LabTotal { get; set; }
        public decimal LabObtained { get; set; }
        public int? OtherTotal { get; set; }
        public decimal OtherObtained { get; set; }
        public int? CourseTotal { get; set; }
        public decimal CourseObtained { get; set; }
    }

}
