﻿using LMS.Core.Entity;
using System;

namespace LMS.Core.DTO
{
    public class ResourceDTO : IBase
    {
        public int Id { get; set; }

        public string Value { get; set; }

        public string FileName { get; set; }

        public string ContentType { get; set; }

        public Resource ConvertToEntity()
        {
            var entity = new Resource();
            entity.Id = this.Id == 0 || this.Id.Equals(default(int)) ? entity.Id : this.Id;
            entity.BinaryData = Convert.FromBase64String(this.Value);
            entity.FileName = this.FileName;
            entity.ContentType = this.ContentType;
            return entity;
        }

        public void ConvertFromEntity(Resource entity)
        {
            this.Id = entity.Id;
            this.Value = Convert.ToBase64String(entity.BinaryData);
            this.FileName = entity.FileName;
            this.ContentType = entity.ContentType;
        }
    }
}
