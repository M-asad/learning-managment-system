﻿using LMS.Core.Entity;

namespace LMS.Core.DTO
{
    public class CourseDTO : BaseDTO<Course>
    {
        public CourseDTO()
        {
        }

        public CourseDTO(Course entity)
            : base(entity)
        {
        }

        public string Title { get; set; }

        public override Course ConvertToEntity(Course entity)
        {
            entity = base.ConvertToEntity(entity);
            entity.Title = this.Title;
            return entity;
        }

        public override void ConvertFromEntity(Course entity)
        {
            base.ConvertFromEntity(entity);
            this.Title = entity.Title;
        }
    }
}
