﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.DTO
{
    public class KeyValueDTO
    {
        public int Id { get; set; }
        public string Text { get; set; }
    }

    public class ParentKeyValue
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

    public class ClassKeyValue
    {
        public int ClassroomId { get; set; }

        public string ClassroomName { get; set; }
    }

    public class StudentPrefixKeyValue
    {
        public int StudentId { get; set; }

        public string GRNo { get; set; }

        public string Name { get; set; }

        public string ParentName { get; set; }
    }

    public class TeahcherKeyValue
    {
        public int TeacherId { get; set; }

        public string Name { get; set; }
    }
}
