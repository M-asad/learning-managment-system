using LMS.Core.IService;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMS.Core.DBContext;
using LMS.Core.Infrastructure;
using LMS.Core.Entity;
using LMS.Core.DTO;
using LMS.Core.Constant;
using LMS.Core.Enums;

namespace LMS.Core.Migrations
{
    public sealed class Configuration : DbMigrationsConfiguration<LMSContext>
    {
        private IUserService userService;

        public Configuration()
        {
            this.AutomaticMigrationsEnabled = true;
            this.AutomaticMigrationDataLossAllowed = false;
        }

        public IUserService _userService
        {
            get
            {
                return this.userService;
            }

            set
            {
                this.userService = value;
            }
        }

        protected override void Seed(LMSContext context)
        {
            #region SetupData
            this.SetupSeed(context);
            this.CreateRoles(context);
            #endregion

            #region Email Template
            this.SetupEmailTemplate(context);
            #endregion

            #region Initial User          

            string emailAddress = "smohsin.pk@gmail.com";
            ApplicationUserManager manager = new ApplicationUserManager(new ApplicationUserStore(context));
            ApplicationUser applicationUser = manager.FindByEmailAsync(emailAddress).Result;
            if (applicationUser == null)
            {
                this.CreateSuperAdmin(manager, context, emailAddress, "SyedMohsin", "mochin!0am");
            }

            emailAddress = "m.asad.fareed@gmail.com";
            applicationUser = manager.FindByEmailAsync(emailAddress).Result;
            if (applicationUser == null)
            {
                this.CreateSuperAdmin(manager, context, emailAddress, "AsadFareed", "Achad!0am");
            }

            emailAddress = "osamaurrehman@gmail.com";
            applicationUser = manager.FindByEmailAsync(emailAddress).Result;
            if (applicationUser == null)
            {
                this.CreateSuperAdmin(manager, context, emailAddress, "OsamaUrRehman", "Ochama!0am");
            }
            #endregion
        }

        private void CreateSuperAdmin(ApplicationUserManager manager, LMSContext context, string email, string username, string password)
        {
            ApplicationUser applicationUser = new ApplicationUser();

            var userDTO = new UserDTO
            {
                Password = password,
                UserName = username,
                Email = email,
                RoleId = Roles.GetRoleId(UserRoles.SuperAdmin)
            };

            applicationUser = userDTO.ConvertToEntity();
            applicationUser.CreatedBy = "Configuration";
            applicationUser.CreatedOn = DateTime.UtcNow;
            applicationUser.LastModifiedBy = "Configuration";
            applicationUser.LastModifiedOn = DateTime.UtcNow;

            manager.CreateAsync(applicationUser, password).Wait();
            applicationUser = manager.FindByEmailAsync(applicationUser.Email).Result;
            manager.AddToRoleAsync(applicationUser.Id, UserRoles.SuperAdmin.ToString()).Wait();

            context.SaveChanges();
        }

        private void CreateRole(ApplicationRoleManager roleManager, UserRoles role)
        {
            roleManager.CreateAsync(new ApplicationRole { Id = Roles.GetRoleId(role), Name = role.ToString() }).Wait();
        }

        #region SetupSeed

        private void SetupSeed(LMSContext context)
        {
            this.SetupClasses(context);
            this.SetupSections(context);
            this.SetupGrades(context);
            this.SetupAcademicYear(context);
            this.SetupTimeTablePeriod(context);
            this.SetupCourses(context);
            this.SetupAttendanceType(context);
            // this.SetupLookupValues(context);
        }

        #endregion

        #region privateMethods

        private void SetupClasses(LMSContext context)
        {
            bool exists = context.Database
                  .SqlQuery<int?>(@"
                        IF EXISTS (SELECT TOP 1 * FROM [dbo].[Class]) 
                           SELECT 1 AS res ELSE SELECT 0 AS res;")
                  .SingleOrDefault() == 0;
            if (exists)
            {
                context.Database.ExecuteSqlCommand(@"

                INSERT INTO [dbo].[Class] ([Id], [Name], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (1, N'Nursery', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Class] ([Id], [Name], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (2, N'Prep 1', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Class] ([Id], [Name], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (3, N'Prep 2', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Class] ([Id], [Name], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (4, N'Class 1', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Class] ([Id], [Name], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (5, N'Class 2', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Class] ([Id], [Name], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (6, N'Class 3', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Class] ([Id], [Name], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (7, N'Class 4', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Class] ([Id], [Name], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (8, N'Class 5', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Class] ([Id], [Name], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (9, N'Class 6', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Class] ([Id], [Name], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (10, N'Class 7', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Class] ([Id], [Name], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (11, N'Class 8', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Class] ([Id], [Name], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (12, N'Class 9', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Class] ([Id], [Name], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (13, N'Class 10', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                ");
            }
        }

        private void SetupSections(LMSContext context)
        {
            bool exists = context.Database
                  .SqlQuery<int?>(@"
                        IF EXISTS (SELECT TOP 1 * FROM [dbo].[Section]) 
                           SELECT 1 AS res ELSE SELECT 0 AS res;")
                  .SingleOrDefault() == 0;
            if (exists)
            {
                context.Database.ExecuteSqlCommand(@"

                INSERT INTO [dbo].[Section] ([Id], [Title], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (1, N'A', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Section] ([Id], [Title], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (2, N'B', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Section] ([Id], [Title], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (3, N'C', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Section] ([Id], [Title], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (4, N'D', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Section] ([Id], [Title], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (5, N'E', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Section] ([Id], [Title], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (6, N'F', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Section] ([Id], [Title], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (7, N'G', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Section] ([Id], [Title], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (8, N'H', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                ");
            }
        }

        private void SetupGrades(LMSContext context)
        {
            bool exists = context.Database
                  .SqlQuery<int?>(@"
                        IF EXISTS (SELECT TOP 1 * FROM [dbo].[Grade]) 
                           SELECT 1 AS res ELSE SELECT 0 AS res;")
                  .SingleOrDefault() == 0;
            if (exists)
            {
                context.Database.ExecuteSqlCommand(@"

                INSERT INTO [dbo].[Grade] ([Id], [Title], [StartPercentage], [EndPercentage], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (1, N'A+', 80, 100, N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Grade] ([Id], [Title], [StartPercentage], [EndPercentage], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (2, N'A', 70, 80, N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Grade] ([Id], [Title], [StartPercentage], [EndPercentage], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (3, N'B', 60, 70, N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Grade] ([Id], [Title], [StartPercentage], [EndPercentage], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (4, N'C', 50, 60, N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Grade] ([Id], [Title], [StartPercentage], [EndPercentage], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (5, N'D', 40, 50, N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Grade] ([Id], [Title], [StartPercentage], [EndPercentage], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (6, N'E', 33, 40, N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                INSERT INTO [dbo].[Grade] ([Id], [Title], [StartPercentage], [EndPercentage], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (7, N'F', 0, 33, N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                ");
            }
        }

        private void SetupAcademicYear(LMSContext context)
        {
            bool exists = context.Database
                  .SqlQuery<int?>(@"
                        IF EXISTS (SELECT TOP 1 * FROM [dbo].[AcademicYear]) 
                           SELECT 1 AS res ELSE SELECT 0 AS res;")
                  .SingleOrDefault() == 0;
            if (exists)
            {
                context.Database.ExecuteSqlCommand(@"

                SET IDENTITY_INSERT [dbo].[AcademicYear] ON 
                
                INSERT INTO [dbo].[AcademicYear] ([Id], [Title], [StartDate], [EndDate], [IsCurrent], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (1, N'Academic Year 2018 - 19', '2018-04-01', '2018-03-30', 1, N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                SET IDENTITY_INSERT [dbo].[AcademicYear] OFF 

                ");
            }
        }

        private void SetupTimeTablePeriod(LMSContext context)
        {
            bool exists = context.Database
                  .SqlQuery<int?>(@"
                        IF EXISTS (SELECT TOP 1 * FROM [dbo].[TimeTablePeriod]) 
                           SELECT 1 AS res ELSE SELECT 0 AS res;")
                  .SingleOrDefault() == 0;
            if (exists)
            {
                context.Database.ExecuteSqlCommand(@"
                
                INSERT INTO [dbo].[TimeTablePeriod] ([Id], [Name], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (1, N'Period 1', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)
                
                INSERT INTO [dbo].[TimeTablePeriod] ([Id], [Name], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (2, N'Period 2', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)
                
                INSERT INTO [dbo].[TimeTablePeriod] ([Id], [Name], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (3, N'Period 3', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)
                
                INSERT INTO [dbo].[TimeTablePeriod] ([Id], [Name], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (4, N'Period 4', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)
                
                INSERT INTO [dbo].[TimeTablePeriod] ([Id], [Name], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (5, N'Period 5', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)
                
                INSERT INTO [dbo].[TimeTablePeriod] ([Id], [Name], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (6, N'Period 6', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)
                
                INSERT INTO [dbo].[TimeTablePeriod] ([Id], [Name], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (7, N'Period 7', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)
                
                INSERT INTO [dbo].[TimeTablePeriod] ([Id], [Name], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (8, N'Period 8', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)

                ");
            }
        }

        private void SetupCourses(LMSContext context)
        {
            bool exists = context.Database
                  .SqlQuery<int?>(@"
                        IF EXISTS (SELECT TOP 1 * FROM [dbo].[Course]) 
                           SELECT 1 AS res ELSE SELECT 0 AS res;")
                  .SingleOrDefault() == 0;
            if (exists)
            {
                context.Database.ExecuteSqlCommand(@"
                
                SET IDENTITY_INSERT [dbo].[Course] ON 

                INSERT INTO [dbo].[Course] ([Id], [Title], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (1, N'Urdu', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)
                
                INSERT INTO [dbo].[Course] ([Id], [Title], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (2, N'English', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)
                
                INSERT INTO [dbo].[Course] ([Id], [Title], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (3, N'Maths', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)
                
                INSERT INTO [dbo].[Course] ([Id], [Title], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (4, N'Science', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)
                
                SET IDENTITY_INSERT [dbo].[Course] OFF 
                ");
            }
        }

        private void SetupEmailTemplate(LMSContext context)
        {
            ////////Forget Password

            
            context.Database.ExecuteSqlCommand(@"
            -- Assignment Creation                
            IF NOT EXISTS(SELECT 1 FROM NotificationType WHERE [Type] = 'AssignmentCreation') 
            BEGIN 
	            INSERT INTO NotificationType (Type, Subject, Template, CreatedBy, CreatedOn, LastModifiedBy, LastModifiedOn, IsDeleted)
	            VALUES ('AssignmentCreation', '{STUDENT} have a {COURSE} assignment', '<p>Hi {PARENT},</p><p>Your child {STUDENT} have an assignment of {COURSE} with a due date {DATE}. Please Submit before due date. <br/><a href=''{WEBSITEURL}/Assignment/{ASSIGNMENTID}''>{TITLE}</a></p>', 'Config', GETDATE(), 'Config', GETDATE(), 1)
            END
            ");
        }

        private void CreateRoles(LMSContext context)
        {
            var roleManager = new ApplicationRoleManager(new ApplicationRoleStore(context));
            if (roleManager.Roles.Count() == 0)
            {
                this.CreateRole(roleManager, UserRoles.SuperAdmin);
                this.CreateRole(roleManager, UserRoles.Admin);
                this.CreateRole(roleManager, UserRoles.Parent);
                this.CreateRole(roleManager, UserRoles.Teacher);
            }
        }

        private void SetupAttendanceType(LMSContext context)
        {
            bool exists = context.Database
                  .SqlQuery<int?>(@"
                        IF EXISTS (SELECT TOP 1 * FROM [dbo].[AttendanceType]) 
                           SELECT 1 AS res ELSE SELECT 0 AS res;")
                  .SingleOrDefault() == 0;
            if (exists)
            {
                context.Database.ExecuteSqlCommand(@"
                
                INSERT INTO [dbo].[AttendanceType] ([Id], [Type], [Description], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (1, N'P', N'Present', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)
                
                INSERT INTO [dbo].[AttendanceType] ([Id], [Type], [Description], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn], [IsDeleted]) VALUES (2, N'A', N'Absent', N'Configuration', GETDATE(), N'Configuration', GETDATE(), 0)
                
                ");
            }
        }

        private void SetupLookupValues(LMSContext context)
        {
            context.Database.ExecuteSqlCommand(@"
            IF NOT EXISTS(SELECT 1 FROM Lookup WHERE Key = '') 
            BEGIN 
	            INSERT INTO Lookup(Key, Value, IsActive) VALUES('', '', 1)
            END 
                
            ");
        }

        #endregion
    }
}