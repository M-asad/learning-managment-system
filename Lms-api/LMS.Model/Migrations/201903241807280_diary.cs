namespace LMS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class diary : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Diary", "Detail", c => c.String(nullable: false, maxLength: 2000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Diary", "Detail", c => c.String(nullable: false, maxLength: 1000));
        }
    }
}
