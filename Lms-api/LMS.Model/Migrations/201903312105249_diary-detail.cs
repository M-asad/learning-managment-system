namespace LMS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class diarydetail : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DiaryDetail",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DiaryId = c.Int(nullable: false),
                        StudentId = c.Int(nullable: false),
                        CreatedBy = c.String(nullable: false, maxLength: 20),
                        CreatedOn = c.DateTime(nullable: false),
                        LastModifiedBy = c.String(maxLength: 20),
                        LastModifiedOn = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Diary", t => t.DiaryId, cascadeDelete: true)
                .ForeignKey("dbo.Student", t => t.StudentId, cascadeDelete: false)
                .Index(t => t.DiaryId)
                .Index(t => t.StudentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DiaryDetail", "StudentId", "dbo.Student");
            DropForeignKey("dbo.DiaryDetail", "DiaryId", "dbo.Diary");
            DropIndex("dbo.DiaryDetail", new[] { "StudentId" });
            DropIndex("dbo.DiaryDetail", new[] { "DiaryId" });
            DropTable("dbo.DiaryDetail");
        }
    }
}
