﻿using LMS.Core.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Core.IService
{
    public interface IAttendanceService
    {
        Task<List<AttendanceCalendarDTO>> GetAttendanceCalendar(int classroomId, DateTime fromDate, DateTime toDate);
        ClassAttendanceDTO GetAttendance(int ClassroomId, DateTime Date, bool FillEmpty);
        AttendanceCalendarDTO GetStudentCalendar(StudentDTO student, int Year, int Month);
        void SaveAttendance(SaveClassAttendanceDTO attendanceData);
        void UpdateAttendance(AttendanceUpdateDTO attendanceData);
        void SaveStudentCalendar(AttendanceCalendarDTO attendanceData);
    }
}
