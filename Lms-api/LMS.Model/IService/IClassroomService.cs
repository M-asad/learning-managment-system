﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMS.Core.DTO;
using LMS.Core.DTO.CustomDTOs;

namespace LMS.Core.IService
{
    public interface IClassroomService : IBaseService<ClassroomDTO>
    {
        List<StudentSimpleDTO> GetStudents(int classroomId);
        ClassroomMasterDTO GetClassroomMaster(int classroomId);
        ClassroomDTO GetClassroomDetail(int classroomId);
        List<ClassKeyValue> GetCurrentClassesddl();
        List<ClassroomListDTO> GetCurrentYearClasses();
        List<string> GetClassroomCourses(int classroomID);
        List<ClassroomListDTO> GetClasses(int academicYearId);
        bool IsClassroomExists(int academicYearId, int classID, int sectionID);
        bool IsUserClassTeacher(int teacherUserId, int classroomID);
        SectionDTO GetSection(int classroomId);
    }
}
