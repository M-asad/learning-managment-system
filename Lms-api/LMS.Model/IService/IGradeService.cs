﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMS.Core.DTO;

namespace LMS.Core.IService
{
    public interface IGradeService : IBaseService<GradeDTO>
    {
    }
}
