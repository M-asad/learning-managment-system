﻿using System.Threading.Tasks;
using LMS.Core.DTO;

namespace LMS.Core.IService
{
    public interface IClassroomStudentService : IBaseService<ClassroomStudentDTO>
    {
        Task<ClassroomStudentDTO> EnrollStudent(EnrollStudentDTO dtoObject);
    }
}
