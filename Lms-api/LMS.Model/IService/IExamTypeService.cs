﻿using LMS.Core.Entity;
using LMS.Core.IRepository;
using LMS.Core.DTO;
using System.Threading.Tasks;
using System.Net.Http;

namespace LMS.Core.IService
{
    public interface IExamTypeService : IBaseService<IExamTypeRepository, ExamType, ExamTypeDTO>
    {
    }
}
