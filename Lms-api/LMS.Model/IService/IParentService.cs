﻿using LMS.Core.Entity;
using LMS.Core.IRepository;
using LMS.Core.DTO;
using System.Threading.Tasks;
using System.Net.Http;

namespace LMS.Core.IService
{
    public interface IParentService : IBaseService<IParentRepository, Parent, ParentDTO>
    {
        bool HaveDependencies(int id);

        bool HaveParentChildRelation(int parentUserId, int studentID);
    }
}
