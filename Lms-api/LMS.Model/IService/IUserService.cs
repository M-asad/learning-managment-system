﻿using LMS.Core.Entity;
using LMS.Core.IRepository;
using LMS.Core.DTO;
using System.Threading.Tasks;
using System.Net.Http;

namespace LMS.Core.IService
{
    public interface IUserService : IBaseService<IUserRepository, ApplicationUser, UserDTO>
    {
        Task<UserDTO> ValidateUserAsync(string userName, string password);

        Task<UserDTO> FindByUserNameAsync(string userName);

        //Task<string> ForgotPassword(string userName);

        //Task<string> ForgotPassword(string token, ForgotPasswordDTO dtoObject);

        // Task<bool> ValidateTokenAsync(string token);

        Task<string> GetUserRole(int userId);

        Task<ResourceDTO> UploadImage(ResourceDTO dtoObject);

        Task<ChangePasswordDTO> ChangePassword(ChangePasswordDTO dtoObject);

        Task<ChangePasswordDTO> ChangeUserPassword(ChangePasswordDTO dtoObject);

        Task<ResourceDTO> UpdateImage(ResourceDTO dtoObject);

        Task<HttpResponseMessage> GetImage(int userId);

        Task UpdateUserRole(int userId, int roleId);

        Task<string> GenerateUsername(string name);
    }
}
