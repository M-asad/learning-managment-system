﻿using LMS.Core.Entity;
using LMS.Core.IRepository;
using LMS.Core.DTO;
using System.Threading.Tasks;
using System.Net.Http;
using System.Collections.Generic;

namespace LMS.Core.IService
{
    public interface IResultService
    {
        ResultSaveDTO GetResultDetail(int examID);
        List<StudentResultCommulativeDTO> GetStudentCommulativeResultList(int examID, int classroomID);

        StudentResultListDTO GetStudentResultList(int examID, int classroomID, int courseID);

        bool SaveResult(ResultSaveDTO data);

        List<KeyValueDTO> GetClassroomForTeacher(int examId, int userId);

        List<KeyValueDTO> GetExamClassroomList(int examId);

        List<ExamListDTO> GetResultList(string prefix, int count, int page, out int pageCount);

        StudentResultMasterDTO GetStudentResult(int examID, int studentID);

        List<KeyValueDTO> GetExamStudents(int examID, int classroomID);

        ResultClassroomsDTO GetExamClassrooms(int examId, int userid);

        void SaveClassroomResult(int examID, int classroomID, List<StudentResultCommulativeDTO> list);

        void PublishClassroomResult(int examID, int classroomID);

        bool IsExamPublishable(int examID);

        bool PublishResult(int examID);
        List<StudentResultMasterDTO> GetStudentPastResult(int studentID);
    }
}
