﻿using LMS.Common.Helper;
using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.IRepository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Core.IService
{
    public interface IAssignmentService : IBaseService<IAssignmentRepository, Assignment, AssignmentDTO, AssignmentListDTO>
    {
        bool isTitleExists(string title, int id);
        bool isAssignmentOwner(int assignmentId);
        IResultSet<AssignmentListDTO> GetStudentAssignments(JsonApiRequest request, int studentId);
        Task<AssignmentDTO> SubmitResult(AssignmentDTO dtoObject);
    }
}
