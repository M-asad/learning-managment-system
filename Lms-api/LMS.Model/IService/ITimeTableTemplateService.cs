﻿using System;
using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.IRepository;
using System.Threading.Tasks;

namespace LMS.Core.IService
{
    public interface ITimeTableTemplateService : IBaseService<ITimeTableTemplateRepository, TimeTableTemplate, TimeTableTemplateDTO>
    {
        Task<TimeTableTemplateDetailDTO> UpdateDetailAsync(TimeTableTemplateDetailDTO dtoObject);
    }
}
