﻿using LMS.Core.Entity;
using LMS.Core.IRepository;
using LMS.Core.DTO;
using System.Threading.Tasks;
using System.Collections.Generic;
using LMS.Core.DTO.CustomDTOs;

namespace LMS.Core.IService
{
    public interface IStudentService : IBaseService<IStudentRepository, Student, StudentDTO, StudentListDTO>
    {
        // Task<IList<StudentDTO>> GetAllAsync(string prefix, int count);
        List<StudentSimpleDTO> GetUnEnrolledStudents(string prefix, int count);
        List<StudentListDTO> GetStudentsByParentId(int parentId);
        // List<StudentListDTO> GetStudents(string prefix, int count, int page, out int pageCount);
        List<StudentSimpleDTO> GetEnrolledStudents(string prefix, int count);
    }
}
