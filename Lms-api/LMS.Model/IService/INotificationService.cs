﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMS.Core.DTO;

namespace LMS.Core.IService
{
    public interface INotificationService : IBaseService<NotificationDTO>
    {
        Task<bool> SendNotification(string type, int key);
    }
}
