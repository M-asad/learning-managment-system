﻿using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.IRepository;

namespace LMS.Core.IService
{
    public interface ITimeTableService : IBaseService<ITimeTableRepository, TimeTable, TimeTableDTO>
    {
    }
}
