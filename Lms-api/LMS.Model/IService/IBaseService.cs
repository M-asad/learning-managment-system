﻿using LMS.Common.Helper;
using LMS.Core.DTO;
using LMS.Core.Enums;
using LMS.Core.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.IService
{
    public interface IBaseService
    {
        IUnitOfWork UnitOfWork { get; }
    }

    public interface IBaseService<TDTO, TListDTO> : IBaseService
    {
        Task<TDTO> GetAsync(int id);

        Task<int> GetCount();
        Task<IList<TDTO>> GetAllAsync();

        Task<IResultSet<TListDTO>> GetAllAsync(JsonApiRequest request);

        Task<IList<TDTO>> GetAllAsync(IList<int> keys);

        Task<IResultSet<TDTO>> GetAllAsync(IList<int> keys, JsonApiRequest request);

        [AuditOperation(OperationType.Create)]
        Task<TDTO> CreateAsync(TDTO dtoObject);

        [AuditOperation(OperationType.Create)]
        Task<IList<TDTO>> CreateAsync(IList<TDTO> dtoObject);

        [AuditOperation(OperationType.Update)]
        Task<TDTO> UpdateAsync(TDTO dtoObject);

        [AuditOperation(OperationType.Update)]
        Task<IList<TDTO>> UpdateAsync(IList<TDTO> dtoObject);

        [AuditOperation(OperationType.Delete)]
        Task DeleteAsync(int id);
    }

    public interface IBaseService<TDTO> : IBaseService<TDTO, TDTO>
    {
    }

    public interface IBaseService<TRepository, TEntity, TDTO, TListDTO> : IBaseService<TDTO, TListDTO>
    {
        TRepository Repository { get; }

        [AuditOperation(OperationType.Create)]
        Task<IList<TDTO>> CreateAsync(IList<TDTO> dtoObjects);

        [AuditOperation(OperationType.Update)]
        Task DeleteAsync(IList<int> ids);

        [AuditOperation(OperationType.Delete)]
        Task<IList<TDTO>> UpdateAsync(IList<TDTO> dtoObjects);

        [AuditOperation(OperationType.Update)]
        Task<IList<TEntity>> UpdateAsync(IList<TEntity> entityObjects);
    }

    public interface IBaseService<TRepository, TEntity, TDTO> : IBaseService<TRepository, TEntity, TDTO, TDTO> {

    }
}