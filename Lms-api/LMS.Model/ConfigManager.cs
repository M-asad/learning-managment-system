﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core
{
    public class ConfigManager
    {
        private static string _fromAddress;
        public static string FromAddress
        {
            get
            {
                if (string.IsNullOrEmpty(_fromAddress))
                 _fromAddress = System.Configuration.ConfigurationManager.AppSettings.Get("FromAddress");
                return _fromAddress;
            }
        }

        private static string _websiteURL;
        public static string WebsiteURL
        {
            get
            {
                if (string.IsNullOrEmpty(_websiteURL))
                    _websiteURL = System.Configuration.ConfigurationManager.AppSettings.Get("WebsiteURL");
                return _websiteURL;
            }
        }
    }
}
