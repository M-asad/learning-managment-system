namespace LMS.Core.DBContext
{
    using System.Data.Entity;
    using LMS.Core.Entity;
    using Microsoft.AspNet.Identity.EntityFramework;

    public partial class LMSContext : IdentityDbContext<ApplicationUser, ApplicationRole, int, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        public LMSContext()
            : base("name=LMSContext")
        {
            //Database.SetInitializer<LMSContext>(new CreateDatabaseIfNotExists<LMSContext>());       
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public virtual DbSet<AcademicYear> AcademicYears { get; set; }
        public virtual DbSet<Assignment> Assignments { get; set; }
        public virtual DbSet<AssignmentAttachment> AssignmentAttachments { get; set; }
        public virtual DbSet<AssignmentDetail> AssignmentDetails { get; set; }
        public virtual DbSet<Attendance> Attendances { get; set; }
        public virtual DbSet<AttendanceType> AttendanceTypes { get; set; }
        public virtual DbSet<Class> Classes { get; set; }
        public virtual DbSet<Classroom> Classrooms { get; set; }
        public virtual DbSet<ClassroomCourse> ClassroomCourses { get; set; }
        public virtual DbSet<ClassroomStudent> ClassroomStudents { get; set; }
        public virtual DbSet<Course> Courses { get; set; }
        public virtual DbSet<Exam> Exams { get; set; }
        public virtual DbSet<ExamClassroomMapping> ExamClassroomMappings { get; set; }
        public virtual DbSet<ExamDetailSetup> ExamDetailSetups { get; set; }
        public virtual DbSet<Result> Results { get; set; }
        public virtual DbSet<ResultDetail> ResultDetails { get; set; }
        public virtual DbSet<ExamType> ExamTypes { get; set; }
        public virtual DbSet<Grade> Grades { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<NotificationType> NotificationTypes { get; set; }
        public virtual DbSet<Parent> Parents { get; set; }
        public virtual DbSet<Section> Sections { get; set; }
        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<Teacher> Teachers { get; set; }
        public virtual DbSet<TimeTable> TimeTables { get; set; }
        public virtual DbSet<TimeTablePeriod> TimeTablePeriods { get; set; }
        public virtual DbSet<TimeTableTemplate> TimeTableTemplates { get; set; }
        public virtual DbSet<TimeTableTemplateDetail> TimeTableTemplateDetails { get; set; }
        public virtual DbSet<Diary> Diaries { get; set; }
        public virtual DbSet<DiaryDetail> DiaryDetails { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<System.Data.Entity.ModelConfiguration.Conventions.PluralizingTableNameConvention>();
            
            modelBuilder.Entity<Exam>()
                .HasMany(e => e.ExamDetailSetups)
                .WithRequired(e => e.Exam)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Exam>()
                .HasMany(e => e.Results)
                .WithRequired(e => e.Exam)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ExamClassroomMapping>()
                .HasMany(e => e.ResultDetails)
                .WithRequired(e => e.ExamClassroomMapping)
                .HasForeignKey(e => e.ExamClassroomMappingId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ExamDetailSetup>()
                .HasMany(e => e.ExamClassroomMappings)
                .WithRequired(e => e.ExamDetailSetup)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Result>()
                .Property(e => e.TotalMarks)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Result>()
                .Property(e => e.ObtainedTotal)
                .HasPrecision(10, 2);
            
            modelBuilder.Entity<Result>()
                .HasMany(e => e.ResultDetails)
                .WithRequired(e => e.Result)
                .HasForeignKey(e => e.ResultID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DiaryDetail>()
                .HasRequired(c => c.Student)
                .WithMany()
                .WillCascadeOnDelete(false);
        }
    }
}
