﻿using System;

namespace LMS.Core.Constant
{
    public static class General
    {
        #region claims
        public const string ClaimsUserName = "UserName";
        public const string ClaimsUserId = "UserId";
        public const string ClaimsRoleId = "RoleId";
        public const string ClaimsRoleName = "Role";
        public const string ClaimsRole = "Role";
        #endregion
    }
}
