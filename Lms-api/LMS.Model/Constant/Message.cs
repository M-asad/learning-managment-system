﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.Constant
{
    public class Message
    {
        #region DateTime
        public const string DateInvalidDate = "Invalid date format";

        public const string DateInvalidDateTime = "Invalid dateTime format";

        #endregion

        #region User
        public const string UserNewPasswordAndOldPasswordMatch = "New password and old password must not be same";

        public const string UserPasswordNotMatch = "New password and re-type password are not match";

        public const string UserInvalidToken = "Invalid token";

        public const string UserInvalidPassword = "Invalid password";

        public const string UserPasswordChangeSuccessfully = "Password change successfully";

        public const string UserInvalidUser = "Invalid user";

        public const string UserInvalidUserNameOrPassword = "The username or password is incorrect";

        public const string UserEmailNotConfirm = "User did not confirm email.";

        public const string UserInvalidUserName = "Invalid username";

        public const string UserInvalidRole = "Invalid role.";

        public const string UserInvalidEmail = "Invalid Email address.";

        public const string UserFirstAndLastName = "Only alphabetic characters are allowed in firstname and lastname fields";

        public const string UserInactive = "User is inactive.";

        public const string UserIncorrectOldPassword = "Incorrect old password";

        public const string UsernameExists = "User with the same username alrady exists";

        public const string LoginUserNotFound = "Login user not found";

        #region password
        public const string UserPasswordRequiredFields = "Please rovide username and new password";

        #endregion

        public const string Failure = "Failed";
        public const string SuccessfullyCompleted = "Success";
        // public const string AlreadyExists = "{0} already exists";
        // public const string NotFound = "{0} not found";
        #endregion

        #region file upload

        public const string UploadFileSizeLimitMessage = "You cannot upload file size more than 10MB";

        #endregion

        #region Resource

        public const string ResourceFileNotFound = "File not found";

        #endregion

        #region methods

        public static string NotFound(string content)
        {
            return string.Format("{0} Not found", content);
        }

        public static string AlreadyExists(string content)
        {
            return string.Format("{0} already exists", content);
        }
        #endregion

        #region Assignment
        public const string AssignmentDateReached = "Assignment cannot be updated once the date is reached.";
        public const string ObtainedMarksGreaterThanTotal = "Obtained marks cannot be more than total marks";
        #endregion

    }
}
