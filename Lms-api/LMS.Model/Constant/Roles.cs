﻿using LMS.Core.Enums;
using System.Collections.Generic;
using System.Linq;

namespace LMS.Core.Constant
{
    public static class Roles
    {
        private static Dictionary<UserRoles, int> userRoles = new Dictionary<UserRoles, int>();

        static Roles()
        {
            userRoles.Add(Enums.UserRoles.SuperAdmin, (int)Enums.UserRoles.SuperAdmin);
            userRoles.Add(Enums.UserRoles.Admin, (int)Enums.UserRoles.Admin);
            userRoles.Add(Enums.UserRoles.Teacher, (int)Enums.UserRoles.Teacher);
            userRoles.Add(Enums.UserRoles.Parent, (int)Enums.UserRoles.Parent);
        }

        public static int GetRoleId(UserRoles userRole)
        {
            return userRoles[userRole];
        }

        public static int GetRoleId(string userRole)
        {
            UserRoles role;
            System.Enum.TryParse(userRole, out role);
            return userRoles[role];
        }

        public static string GetRole(int roleId)
        {
            return userRoles.FirstOrDefault(x => x.Value == roleId).Key.ToString();
        }

        public static string GetRole(UserRoles role)
        {
            return userRoles.FirstOrDefault(x => x.Key == role).Key.ToString();
        }

        public static UserRoles GetRoleObject(int roleId)
        {
            return userRoles.FirstOrDefault(x => x.Value == roleId).Key;
        }

        public static bool IsRoleExists(int roleId)
        {
            return userRoles.Any(x => x.Value == roleId);
        }

        public static bool IsExists(int roleId)
        {
            return userRoles.ContainsValue(roleId);
        }
    }
}
