﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.Constant
{
    public class NotificationTypes
    {
        public const string AssignmentCreation = "AssignmentCreation";
        public const string AssignmentMarksSubmission = "AssignmentMarksSubmission";        
    }
}
