namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class Parent : EntityBase
    {
        public Parent()
        {
            Students = new List<Student>();
        }

        public int UserId { get; set; }

        [Required]
        [StringLength(100)]
        public string FullName { get; set; }

        [StringLength(200)]
        public string Address { get; set; }

        [StringLength(15)]
        public string Phone { get; set; }

        [StringLength(15)]
        public string Cell { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(20)]
        public string CNIC { get; set; }

        [Required]
        [StringLength(1)]
        public string Gender { get; set; }

        public bool? EmailOptIn { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser ApplicationUser { get; set; }

        public virtual IList<Student> Students { get; set; }
    }
}
