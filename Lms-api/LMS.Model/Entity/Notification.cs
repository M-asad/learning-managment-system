namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Notification : EntityBase
    {
        public int NotificationTypeId { get; set; }
        
        public int UserId { get; set; }

        [Required]
        [StringLength(500)]
        public string Subject { get; set; }

        [Required]
        [StringLength(4000)]
        public string Body { get; set; }

        public bool? IsRead { get; set; }

        [Required]
        public bool IsEmailSent { get; set; }

        public string Exception { get; set; }

        public int RetryCount { get; set; }

        [ForeignKey("NotificationTypeId")]
        public NotificationType NotificationType { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser ApplicationUser { get; set; }
    }
}
