namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class ClassroomCourse : EntityBase
    {
        public int ClassroomId { get; set; }

        public int CourseId { get; set; }

        public int TeacherId { get; set; }

        [ForeignKey("ClassroomId")]
        public Classroom Classroom { get; set; }

        [ForeignKey("CourseId")]
        public Course Course { get; set; }

        [ForeignKey("TeacherId")]
        public Teacher Teacher { get; set; }
    }
}
