namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class AcademicYear : EntityBase
    {
        
        public AcademicYear()
        {
            Classrooms = new List<Classroom>();
            Exams = new List<Exam>();
        }
        
        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool IsCurrent { get; set; }
                
        public virtual IList<Classroom> Classrooms { get; set; }
                
        public virtual IList<Exam> Exams { get; set; }
    }
}
