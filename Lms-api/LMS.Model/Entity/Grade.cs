namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class Grade : EntityBaseNonIDentity
    {
        public Grade()
        {
            Results = new List<Result>();
        }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        public int StartPercentage { get; set; }

        public int EndPercentage { get; set; }
        
        public virtual IList<Result> Results { get; set; }
    }
}
