namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class Attendance : EntityBase
    {
        public DateTime Date { get; set; }

        public int ClassroomId { get; set; }

        public int StudentId { get; set; }

        public int AttendanceTypeID { get; set; }

        [ForeignKey("AttendanceTypeID")]
        public AttendanceType AttendanceType { get; set; }

        [ForeignKey("ClassroomId")]
        public Classroom Classroom { get; set; }
    }
}
