namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class ClassroomStudent : EntityBase
    {
        public int ClassroomId { get; set; }

        public int StudentId { get; set; }

        public int RollNo { get; set; }

        public bool? HasPromoted { get; set; }

        [ForeignKey("ClassroomId")]
        public Classroom Classroom { get; set; }

        [ForeignKey("StudentId")]
        public Student Student { get; set; }
    }
}
