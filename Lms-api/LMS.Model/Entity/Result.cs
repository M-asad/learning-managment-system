namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class Result : EntityBase
    {
        public Result()
        {
            ResultDetails = new List<ResultDetail>();
        }

        public int ExamId { get; set; }

        public int StudentId { get; set; }

        public int GradeId { get; set; }

        public decimal? TotalMarks { get; set; }

        public decimal? ObtainedTotal { get; set; }

        [StringLength(500)]
        public string Remarks { get; set; }

        [ForeignKey("ExamId")]
        public Exam Exam { get; set; }

        [ForeignKey("GradeId")]
        public Grade Grade { get; set; }

        [ForeignKey("StudentId")]
        public Student Student { get; set; }

        public virtual IList<ResultDetail> ResultDetails { get; set; }
    }
}
