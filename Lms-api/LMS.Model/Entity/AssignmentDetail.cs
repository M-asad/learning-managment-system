namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class AssignmentDetail : EntityBase
    {
        public int AssignmentID { get; set; }

        public int StudentId { get; set; }

        public double? ObtainedMarks { get; set; }

        public bool? HasPassed { get; set; }

        [ForeignKey("AssignmentID")]
        public Assignment Assignment { get; set; }

        [ForeignKey("StudentId")]
        public Student Student { get; set; }
    }
}
