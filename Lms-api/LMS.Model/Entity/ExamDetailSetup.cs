namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class ExamDetailSetup : EntityBase
    {
        public ExamDetailSetup()
        {
            ExamClassroomMappings = new List<ExamClassroomMapping>();
        }

        public int ExamId { get; set; }

        public int? TheoryTotal { get; set; }

        public int? LabTotal { get; set; }

        public int? OtherTotal { get; set; }

        public DateTime Date { get; set; }

        public int? Duration { get; set; }

        public virtual Exam Exam { get; set; }
        
        public virtual IList<ExamClassroomMapping> ExamClassroomMappings { get; set; }
    }
}
