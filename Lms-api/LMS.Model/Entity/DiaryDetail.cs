﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.Entity
{
    public class DiaryDetail : EntityBase
    {
        public int DiaryId { get; set; }

        public int StudentId { get; set; }
        
        [ForeignKey("DiaryId")]
        public Diary Diary { get; set; }

        [ForeignKey("StudentId")]
        public Student Student { get; set; }
    }
}
