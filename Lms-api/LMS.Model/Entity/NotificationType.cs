namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class NotificationType : EntityBase
    {
        public NotificationType()
        {
            Notifications = new List<Notification>();
        }

        [Required]
        [StringLength(30)]
        public string Type { get; set; }

        [Required]
        [StringLength(500)]
        public string Subject { get; set; }

        [Required]
        [StringLength(4000)]
        public string Template { get; set; }

        public virtual IList<Notification> Notifications { get; set; }
    }
}
