namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class ExamType : EntityBase
    {
        public ExamType()
        {
            Exams = new List<Exam>();
        }

        [Required]
        [StringLength(100)]
        public string Type { get; set; }

        
        public virtual IList<Exam> Exams { get; set; }
    }
}
