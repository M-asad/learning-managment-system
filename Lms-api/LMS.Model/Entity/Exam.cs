namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Exam : EntityBase
    {        
        public Exam()
        {
            ExamDetailSetups = new List<ExamDetailSetup>();
            Results = new List<Result>();
        }

        public int ExamTypeID { get; set; }

        public int AcademicYearId { get; set; }

        [Required]
        [StringLength(200)]
        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        [StringLength(200)]
        public string ClassIds { get; set; }

        public bool? IsPublished { get; set; }

        [ForeignKey("AcademicYearId")]
        public AcademicYear AcademicYear { get; set; }

        [ForeignKey("ExamTypeID")]
        public ExamType ExamType { get; set; }

        public virtual IList<ExamDetailSetup> ExamDetailSetups { get; set; }

        public virtual IList<Result> Results { get; set; }
    }
}
