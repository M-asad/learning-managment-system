namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class TimeTable : EntityBase
    {
        public int TimeTableTemplateDetailId { get; set; }

        public int ClassroomId { get; set; }

        public int CourseId { get; set; }

        [ForeignKey("ClassroomId")]
        public virtual Classroom Classroom { get; set; }

        [ForeignKey("CourseId")]
        public virtual Course Course { get; set; }

        [ForeignKey("TimeTableTemplateDetailId")]
        public virtual TimeTableTemplateDetail TimeTableTemplateDetail { get; set; }
    }
}
