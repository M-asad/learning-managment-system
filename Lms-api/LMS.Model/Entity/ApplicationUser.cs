﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.Entity
{
    public class ApplicationUser : IdentityUser<int, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>, IAuditModel
    {
        public ApplicationUser()
        {
            Parents = new List<Parent>();
            Teachers = new List<Teacher>();
        }

        public int? PictureResourceId { get; set; }

        [StringLength(20)]
        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        [StringLength(20)]
        public string LastModifiedBy { get; set; }

        public DateTime LastModifiedOn { get; set; }

        public bool IsDeleted { get; set; }

        public virtual IList<Parent> Parents { get; set; }

        public virtual IList<Teacher> Teachers { get; set; }
    }
}
