namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class ExamClassroomMapping : EntityBase
    {
        public ExamClassroomMapping()
        {
            ResultDetails = new List<ResultDetail>();
        }

        public int ExamDetailSetupId { get; set; }

        public int ClassroomId { get; set; }

        public int CourseId { get; set; }

        public int TeacherId { get; set; }

        public bool? IsPublished { get; set; }

        [ForeignKey("ClassroomId")]
        public Classroom Classroom { get; set; }

        [ForeignKey("CourseId")]
        public Course Course { get; set; }

        [ForeignKey("ExamDetailSetupId")]
        public ExamDetailSetup ExamDetailSetup { get; set; }

        [ForeignKey("TeacherId")]
        public Teacher Teacher { get; set; }

        public virtual IList<ResultDetail> ResultDetails { get; set; }
    }
}
