namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class Student : EntityBase
    {
        public Student()
        {
            AssignmentDetails = new List<AssignmentDetail>();
            ClassroomStudents = new List<ClassroomStudent>();
            Results = new List<Result>();
        }

        [Required]
        [StringLength(10)]
        public string GRNo { get; set; }

        [Required]
        [StringLength(100)]
        public string FullName { get; set; }

        public DateTime JoinDate { get; set; }

        public DateTime BirthDate { get; set; }

        [StringLength(50)]
        public string BirthPlace { get; set; }

        [StringLength(1)]
        public string Gender { get; set; }

        [StringLength(3)]
        public string BloodGroup { get; set; }

        [StringLength(50)]
        public string Nationality { get; set; }

        [StringLength(50)]
        public string Religion { get; set; }

        public DateTime? LeavingDate { get; set; }

        public int ParentId { get; set; }

        [StringLength(1000)]
        public string Picture { get; set; }

        public virtual IList<AssignmentDetail> AssignmentDetails { get; set; }
        
        public virtual IList<ClassroomStudent> ClassroomStudents { get; set; }

        public virtual IList<Result> Results { get; set; }

        public virtual IList<DiaryDetail> DiaryDetails { get; set; }

        [ForeignKey("ParentId")]
        public Parent Parent { get; set; }
    }
}
