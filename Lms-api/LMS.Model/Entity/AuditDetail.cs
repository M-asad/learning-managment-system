﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.Entity
{
    public class AuditDetail : IBase
    {
        [Key]
        public int Id { get; set; }

        public string AuditData { get; set; }

        public string AuditParameters { get; set; }

        public int AuditID { get; set; }

        [ForeignKey("AuditID")]
        public Audit Audit { get; set; }
    }
}
