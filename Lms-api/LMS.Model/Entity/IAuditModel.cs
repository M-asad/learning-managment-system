﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.Entity
{
    public interface IAuditModel : IBase
    {
        string CreatedBy { get; set; }

        DateTime CreatedOn { get; set; }

        string LastModifiedBy { get; set; }

        DateTime LastModifiedOn { get; set; }

        bool IsDeleted { get; set; }
    }
}
