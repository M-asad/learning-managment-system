namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class Diary : EntityBase
    {
        public Diary()
        {
            DiaryDetails = new List<DiaryDetail>();
        }

        public DateTime Date { get; set; }

        [Required]
        [StringLength(2000)]
        public string Detail { get; set; }

        public int ClassroomId { get; set; }

        public int CourseId { get; set; }

        public int TeacherId { get; set; }

        [ForeignKey("ClassroomId")]
        public Classroom Classroom { get; set; }

        [ForeignKey("CourseId")]
        public Course Course { get; set; }

        [ForeignKey("TeacherId")]
        public Teacher Teacher { get; set; }

        public virtual IList<DiaryDetail> DiaryDetails { get; set; }
    }
}
