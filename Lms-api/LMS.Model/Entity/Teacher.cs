namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class Teacher : EntityBase
    {
        public Teacher()
        {
            Classrooms = new List<Classroom>();
            ClassroomCourses = new List<ClassroomCourse>();
        }

        public int UserId { get; set; }

        [Required]
        [StringLength(100)]
        public string FullName { get; set; }
        
        public DateTime? JoinDate { get; set; }

        public DateTime? ResignationDate { get; set; }

        public DateTime? BirthDate { get; set; }

        [StringLength(100)]
        public string Qualification { get; set; }

        [Required]
        [StringLength(1)]
        public string Gender { get; set; }

        [StringLength(3)]
        public string BloodGroup { get; set; }

        [StringLength(15)]
        public string Nationality { get; set; }

        [StringLength(20)]
        public string Religion { get; set; }

        [StringLength(200)]
        public string Picture { get; set; }

        [StringLength(15)]
        public string CNIC { get; set; }

        [StringLength(200)]
        public string Address { get; set; }

        [StringLength(15)]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(15)]
        public string Cell { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser ApplicationUser { get; set; }

        public virtual IList<Classroom> Classrooms { get; set; }
        
        public virtual IList<ClassroomCourse> ClassroomCourses { get; set; }
    }
}
