namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class Section : EntityBaseNonIDentity
    {
        public Section()
        {
            Classrooms = new List<Classroom>();
        }

        [Required]
        [StringLength(15)]
        public string Title { get; set; }
        
        public virtual IList<Classroom> Classrooms { get; set; }
    }
}
