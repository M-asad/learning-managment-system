namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class ResultDetail : EntityBase
    {
        public int ResultID { get; set; }

        public int? TheoryObtained { get; set; }

        public int? LabObtained { get; set; }

        public int? OtherObtained { get; set; }

        public int ExamClassroomMappingId { get; set; }

        [ForeignKey("ExamClassroomMappingId")]
        public ExamClassroomMapping ExamClassroomMapping { get; set; }

        [ForeignKey("ResultID")]
        public Result Result { get; set; }
    }
}
