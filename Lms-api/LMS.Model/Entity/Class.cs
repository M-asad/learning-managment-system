namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Class : EntityBaseNonIDentity
    {
        public Class()
        {
            Classrooms = new List<Classroom>();
        }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public virtual IList<Classroom> Classrooms { get; set; }
    }
}
