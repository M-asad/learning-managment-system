namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class TimeTableTemplateDetail : EntityBase
    {
        public TimeTableTemplateDetail()
        {
            TimeTables = new List<TimeTable>();
        }

        public int TimeTableTemplateId { get; set; }

        public int TimeTablePeriodId { get; set; }

        public TimeSpan StartTime { get; set; }

        public TimeSpan EndTime { get; set; }

        public int Day { get; set; }

        [ForeignKey("TimeTablePeriodId")]
        public virtual TimeTablePeriod TimeTablePeriod { get; set; }

        [ForeignKey("TimeTableTemplateId")]
        public virtual TimeTableTemplate TimeTableTemplate { get; set; }

        public virtual IList<TimeTable> TimeTables { get; set; }
    }
}
