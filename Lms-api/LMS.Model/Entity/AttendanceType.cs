namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class AttendanceType : EntityBaseNonIDentity
    {
        public AttendanceType()
        {
            Attendances = new List<Attendance>();
        }

        [Required]
        [StringLength(1)]
        public string Type { get; set; }

        [StringLength(50)]
        public string Description { get; set; }
        
        public virtual IList<Attendance> Attendances { get; set; }
    }
}
