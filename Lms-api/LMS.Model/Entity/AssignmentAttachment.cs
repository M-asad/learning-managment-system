namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class AssignmentAttachment : EntityBase
    {
        public int AssignmentID { get; set; }

        [StringLength(2000)]
        public string FIleURL { get; set; }

        [ForeignKey("AssignmentID")]
        public Assignment Assignment { get; set; }
    }
}
