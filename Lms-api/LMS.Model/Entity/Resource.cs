﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.Entity
{
    public class Resource : IBase
    {
        public int Id { get; set; }

        public byte[] BinaryData { get; set; }

        public string FileName { get; set; }

        public string ContentType { get; set; }
    }
}
