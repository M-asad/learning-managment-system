namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class Classroom : EntityBase
    {
        public Classroom()
        {
            Assignments = new List<Assignment>();
            Attendances = new List<Attendance>();
            TimeTables = new List<TimeTable>();
            ClassroomCourses = new List<ClassroomCourse>();
            ClassroomStudents = new List<ClassroomStudent>();
            ExamClassroomMappings = new List<ExamClassroomMapping>();
        }

        public int ClassId { get; set; }

        public int SectionId { get; set; }

        public string ClassroomName { get; set; }

        public int AcademicYearId { get; set; }

        public int ClassTeacherId { get; set; }

        [ForeignKey("ClassId")]
        public Class Class { get; set; }

        [ForeignKey("SectionId")]
        public Section Section { get; set; }

        [ForeignKey("AcademicYearId")]
        public AcademicYear AcademicYear { get; set; }

        public virtual IList<Assignment> Assignments { get; set; }

        public virtual IList<Attendance> Attendances { get; set; }

        public virtual IList<TimeTable> TimeTables { get; set; }

        public virtual IList<ClassroomCourse> ClassroomCourses { get; set; }

        public virtual IList<ClassroomStudent> ClassroomStudents { get; set; }
        
        public virtual IList<ExamClassroomMapping> ExamClassroomMappings { get; set; }
    }
}
