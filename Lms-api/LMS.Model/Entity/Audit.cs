﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.Entity
{
    public class Audit : IBase
    {
        public Audit()
        {
            this.AuditDetails = new List<AuditDetail>();
        }

        [Key]
        public int Id { get; set; }

        [MaxLength(100)]
        public string TableName { get; set; }

        [MaxLength(50)]
        public string ReferenceId { get; set; }

        public DateTime OperationTime { get; set; }

        public int OperationType { get; set; }

        [MaxLength(200)]
        public string Message { get; set; }

        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }

        public virtual IList<AuditDetail> AuditDetails { get; set; }
    }
}