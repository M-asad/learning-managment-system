namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class TimeTableTemplate : EntityBase
    {
        public TimeTableTemplate()
        {
            TimeTableTemplateDetails = new List<TimeTableTemplateDetail>();
        }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        public virtual IList<TimeTableTemplateDetail> TimeTableTemplateDetails { get; set; }
    }
}
