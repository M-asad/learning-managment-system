﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;

namespace LMS.Core.Entity
{
    public class ApplicationUserRole : IdentityUserRole<int>
    {
    }

    public class ApplicationUserClaim : IdentityUserClaim<int>
    {
        [Key]
        public override int Id { get; set; }
    }

    public class ApplicationUserLogin : IdentityUserLogin<int>
    {
        [Key]
        public int Id { get; set; }
    }
}
