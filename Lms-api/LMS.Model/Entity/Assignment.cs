namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class Assignment : EntityBase
    {
        public Assignment()
        {
            AssignmentAttachments = new List<AssignmentAttachment>();
            AssignmentDetails = new List<AssignmentDetail>();
        }

        [Required]
        [StringLength(200)]
        public string Title { get; set; }

        [Required]
        [StringLength(1000)]
        public string Description { get; set; }

        public DateTime Date { get; set; }

        public int CourseId { get; set; }

        public int ClassroomId { get; set; }

        public double TotalMarks { get; set; }

        public double PassingMarks { get; set; }
        
        public virtual IList<AssignmentAttachment> AssignmentAttachments { get; set; }

        [ForeignKey("ClassroomId")]
        public Classroom Classroom { get; set; }

        [ForeignKey("CourseId")]
        public Course Course { get; set; }
        
        public virtual IList<AssignmentDetail> AssignmentDetails { get; set; }
    }
}
