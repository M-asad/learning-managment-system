namespace LMS.Core.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class Course : EntityBase
    {
        public Course()
        {
            Assignments = new List<Assignment>();
            ClassroomCourses = new List<ClassroomCourse>();
            ExamClassroomMappings = new List<ExamClassroomMapping>();
        }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        
        public virtual IList<Assignment> Assignments { get; set; }
        
        public virtual IList<ClassroomCourse> ClassroomCourses { get; set; }

        public virtual IList<ExamClassroomMapping> ExamClassroomMappings { get; set; }
    }
}
