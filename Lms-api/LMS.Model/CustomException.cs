﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core
{
    public class CustomException : Exception
    {
        public string _message { get; set; }
        public string _statusCode { get; set; }

        public override string Message {
            get
            {
                return this._message;
            }
        }
        public string StatusCode
        {
            get
            {
                return this._statusCode;
            }
        }

        public CustomException(string message, string statusCode)
        {
            this._message = message;
            this._statusCode = statusCode;
        }
    }
}
