﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMS.Core.Entity;
using LMS.Core.DTO;

namespace LMS.Core.IRepository
{
    public interface IClassroomCourseRepository : IBaseRepository<ClassroomCourse>
    {
        List<ClassroomCourseDTO> GetCourseTeachers(int ClassroomId);
    }
}
