﻿using LMS.Core.DTO;
using LMS.Core.DTO.CustomDTOs;
using LMS.Core.Entity;
using System.Collections.Generic;

namespace LMS.Core.IRepository
{
    public interface IStudentRepository : IBaseRepository<Student>
    {
        bool IsGRNoExists(string GRno, int Id = 0);
        List<StudentListDTO> GetStudentsByParentId(int parentId);
        List<StudentListDTO> GetStudents(string prefix, int count, int page, out int pageCount);
        List<StudentSimpleDTO> GetEnrolledStudents(string prefix, int count);
    }
}
