﻿using LMS.Core.DBContext;
using LMS.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.IRepository
{
    public interface IUnitOfWork
    {
        #region Entities
        LMSContext DBContext { get; }

        IRequestInfo RequestInfo { get; }
        IUserRepository UserRepository { get; }
        IExceptionHelper ExceptionHelper { get; }
        ITeacherRepository TeacherRepository { get; }
        IParentRepository ParentRepository { get; }
        IAcademicYearRepository AcademicYearRepository { get; }
        IStudentRepository StudentRepository { get; }
        IClassRepository ClassRepository { get; }
        ISectionRepository SectionRepository { get; }
        ICourseRepository CourseRepository { get; }
        IGradeRepository GradeRepository { get; }
        IClassroomRepository ClassroomRepository { get; }
        IClassroomStudentRepository ClassroomStudentRepository { get; }
        IClassroomCourseRepository ClassroomCourseRepository { get; }
        IExamRepository ExamRepository { get; }
        IExamTypeRepository ExamTypeRepository { get; }
        IResultRepository ResultRepository { get; }
        ITimeTableRepository TimeTableRepository { get; }
        ITimeTableTemplateRepository TimeTableTemplateRepository { get; }
        ITimeTableTemplateDetailRepository TimeTableTemplateDetailRepository { get; }
        IAssignmentRepository AssignmentRepository { get; }
        IAssignmentDetailRepository AssignmentDetailRepository { get; }
        IAssignmentAttachmentRepository AssignmentAttachmentRepository { get; }
        INotificationRepository NotificationRepository { get; }
        IDiaryRepository DiaryRepository { get; }
        #endregion

        Task<int> SaveAsync();

        int Save();

        DbContextTransaction BeginTransaction();
    }
}