﻿using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.IRepository
{
    public interface IParentRepository : IBaseRepository<Parent>
    {
        Task<Parent> FindByName(string name);
        Task<Parent> FindByName(string name, int id);
        bool HaveDependencies(int id);
        bool Exists(int Id);
        bool HaveParentChildRelation(int parentUserId, int studentID);
    }
}
