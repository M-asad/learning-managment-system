﻿using LMS.Core.Entity;
using System.Threading.Tasks;

namespace LMS.Core.IRepository
{
    public interface IAcademicYearRepository : IBaseRepository<AcademicYear>
    {
        AcademicYear GetCurrentYear();
    }
}
