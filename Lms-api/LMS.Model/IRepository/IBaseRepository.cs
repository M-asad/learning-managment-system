﻿using LMS.Common.Helper;
using LMS.Core.DTO;
using LMS.Core.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Core.IRepository
{
    public interface IBaseRepository<TEntity>
    {
        int AcademicYearId { get; set; }

        [AuditOperation(OperationType.Read)]
        Task<TEntity> GetDefaultAsync(int id);

        [AuditOperation(OperationType.Read)]
        Task<IEnumerable<TEntity>> GetDefaultAsync(IList<int> ids);

        [AuditOperation(OperationType.Read)]
        Task<TEntity> GetAsync(int id);

        [AuditOperation(OperationType.Read)]
        Task<IEnumerable<TEntity>> GetAsync(IList<int> ids);

        [AuditOperation(OperationType.Read)]
        Task<TEntity> GetEntityOnly(int id);

        [AuditOperation(OperationType.Read)]
        Task<int> GetCount();

        [AuditOperation(OperationType.Read)]
        IEnumerable<TEntity> Get(System.Linq.Expressions.Expression<Func<TEntity, bool>> predicate);

        [AuditOperation(OperationType.Read)]
        Task<IEnumerable<TEntity>> GetAllDefault();

        [AuditOperation(OperationType.Read)]
        Task<IResultSet<TEntity>> GetAllDefault(JsonApiRequest request);

        [AuditOperation(OperationType.Read)]
        Task<IEnumerable<TEntity>> GetAll();

        [AuditOperation(OperationType.Read)]
        Task<IResultSet<TEntity>> GetAll(JsonApiRequest request);

        [AuditOperation(OperationType.Read)]
        Task<IEnumerable<TEntity>> GetAll(IList<int> keys);

        [AuditOperation(OperationType.Read)]
        Task<IResultSet<TEntity>> GetAll(IList<int> keys, JsonApiRequest request);

        [AuditOperation(OperationType.Create)]
        Task<TEntity> Create(TEntity entity);

        [AuditOperation(OperationType.Update)]
        Task<TEntity> Update(TEntity entity);

        [AuditOperation(OperationType.Delete)]
        Task DeleteAsync(int id);

        [AuditOperation(OperationType.Read)]
        bool Exists(int Id);
    }
}
