﻿using LMS.Core.DTO;
using LMS.Core.Entity;
using System.Collections.Generic;

namespace LMS.Core.IRepository
{
    public interface IAssignmentRepository : IBaseRepository<Assignment>
    {
        List<AssignmentListDTO> GetAssignments(PaginationDTO pagination, out int pageCount);
        bool isTitleExists(string title, int id);
        bool isAssignmentOwner(int assignmentId);
        List<AssignmentListDTO> GetStudentAssignments(PaginationDTO pagination, int studentId, out int pageCount);
        List<AssignmentEmailDTO> GetAssignmentCreationEmailData(int Id);
    }
}
