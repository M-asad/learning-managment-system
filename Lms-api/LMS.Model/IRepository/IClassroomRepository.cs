﻿using System.Collections.Generic;
using LMS.Core.Entity;
using LMS.Core.DTO;

namespace LMS.Core.IRepository
{
    public interface IClassroomRepository : IBaseRepository<Classroom>
    {
        bool IsClassroomExists(int academicYearId, int classID, int sectionID);
        ClassroomMasterDTO GetClassroomMaster(int ClassroomId);
        ClassroomDTO GetClassroomDetail(int ClassroomId);
        List<ClassKeyValue> GetCurrentClassesddl();
        List<string> GetClassroomCourses(int classroomID);
        List<ClassroomListDTO> GetClasses(int academicYearId);
        bool IsUserClassTeacher(int teacherUserId, int classroomID);
        Section GetSection(int classroomId);
    }
}
