﻿using LMS.Core.DTO;
using LMS.Core.Entity;
using System;
using System.Collections.Generic;

namespace LMS.Core.IRepository
{
    public interface IExamRepository : IBaseRepository<Exam>
    {
        List<ExamListDTO> GetExamList(string prefix, int count, int page, out int pageCount);
        ExamDetailDTO GetExamDetail(int examId);
        bool AreClassesExist(int Id, List<int> classes);
        bool ValidRange(int Id, DateTime startDate, DateTime endDate);
        List<KeyValueDTO> GetCurrentYearExam();
        ExamListDTO GetExamMaster(int examId);
        List<KeyValueDTO> GetExamCourses(int examId);
        List<KeyValueDTO> GetCurrentYearClasses();
        int SaveDetail(ExamDetailSaveDTO dto);
        bool IsExamExists(DateTime startDate, DateTime endDate, int[] classes);
    }
}
