﻿using LMS.Core.Entity;

namespace LMS.Core.IRepository
{
    public interface ITimeTableTemplateDetailRepository : IBaseRepository<TimeTableTemplateDetail>
    {
    }
}
