﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.IRepository
{
    public interface IAuditableRepository<TEntity> : IBaseRepository<TEntity>
    {
        Task HardDeleteAsync(int id);
    }
}
