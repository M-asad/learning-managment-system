﻿using LMS.Core.DTO;
using LMS.Core.Entity;
using System.Collections.Generic;

namespace LMS.Core.IRepository
{
    public interface IAssignmentDetailRepository : IBaseRepository<AssignmentDetail>
    {
    }
}
