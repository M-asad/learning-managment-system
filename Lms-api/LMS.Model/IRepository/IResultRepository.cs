﻿using LMS.Core.DTO;
using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.IRepository
{
    public interface IResultRepository
    {
        ResultSaveDTO GetResultDetail(int examId);
        List<StudentResultCommulativeDTO> GetStudentCommulativeResultList(int examId, int classroomID);

        StudentResultListDTO GetStudentResultList(int examId, int classroomID, int courseID);

        bool SaveResult(ResultSaveDTO data);

        List<KeyValueDTO> GetClassroomForTeacher(int examId, int userId);

        List<KeyValueDTO> GetExamClassroomList(int examId);

        List<ExamListDTO> GetResultList(string prefix, int count, int page, out int pageCount);

        List<ExamListDTO> GetTeacherResultList(string prefix, int count, int page, int teacherUserId, out int pageCount);

        StudentResultMasterDTO GetStudentResult(int examId, int studentID);

        List<KeyValueDTO> GetExamStudents(int examId, int classroomID);

        ResultClassroomsDTO GetExamClassrooms(int examId, int userid);

        void SaveClassroomResult(int examId, int classroomID, List<StudentResultCommulativeDTO> list);

        void PublishClassroomResult(int examId, int classroomID);

        bool IsExamPublishable(int examId);

        bool PublishResult(int examId);
        List<StudentResultMasterDTO> GetStudentPastResult(int studentID);
    }
}
