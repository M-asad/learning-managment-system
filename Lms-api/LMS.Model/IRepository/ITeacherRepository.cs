﻿using LMS.Core.Entity;
using System.Threading.Tasks;

namespace LMS.Core.IRepository
{
    public interface ITeacherRepository : IBaseRepository<Teacher>
    {
        Task<Teacher> FindByName(string name);
        Task<Teacher> FindByName(string name, int id);
        bool HaveDependencies(int id);
    }
}
