﻿using LMS.Core.DTO;
using LMS.Core.Entity;
using System;
using System.Collections.Generic;

namespace LMS.Core.IRepository
{
    public interface IAttendanceRepository
    {
        List<AttendanceCalendarDTO> GetAttendanceCalendar(int ClassroomId, DateTime FromDate, DateTime ToDate);
        ClassAttendanceDTO GetAttendance(int ClassroomId, DateTime Date, bool FillEmpty);
        AttendanceCalendarDTO GetStudentCalendar(StudentDTO student, int Year, int Month);
        void SaveAttendance(SaveClassAttendanceDTO attendanceData);
        void UpdateAttendance(AttendanceUpdateDTO attendanceData);
        void SaveStudentCalendar(AttendanceCalendarDTO attendanceData);
    }
}
