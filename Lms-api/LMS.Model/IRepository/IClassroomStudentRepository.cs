﻿using System.Collections.Generic;
using LMS.Core.Entity;
using LMS.Core.DTO;
using LMS.Core.DTO.CustomDTOs;

namespace LMS.Core.IRepository
{
    public interface IClassroomStudentRepository : IBaseRepository<ClassroomStudent>
    {
        List<StudentSimpleDTO> GetStudents(int classroomId);
        List<StudentSimpleDTO> GetUnEnrolledStudents(string prefix, int count);
        List<ClassroomStudentDTO> GetEnrolledStudents(int ClassroomId);
        int GetMaxRollNo(int classroomID);
        ClassroomStudentDetailDTO GetStudentClassroom(int studentId);
        List<ClassroomStudent> GetClassroomStudents(int ClassroomId);
    }
}
