﻿using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.IRepository
{
    public interface ITimeTableTemplateRepository : IBaseRepository<TimeTableTemplate>
    {
        bool IsTitleExists(string name);
    }
}
