﻿using LMS.Core.Entity;

namespace LMS.Core.IRepository
{
    public interface ITimeTableRepository : IBaseRepository<TimeTable>
    {
    }
}
