﻿using LMS.Core.DTO;
using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.IRepository
{
    public interface IUserRepository : IBaseRepository<ApplicationUser>
    {
        Task<ApplicationUser> Create(ApplicationUser entity, string password, string role);
        Task<ApplicationUser> FindAsync(string userName, string password);
        Task<ApplicationUser> FindByUserNameAsync(string userName);
        Task<ApplicationUser> FindByEmailAsync(string email);
        Task<string> GetUserRole(int userId);
        Task ChangePassword(int userId, string newPassword);
        Task<ChangePasswordDTO> ChangePassword(ChangePasswordDTO dtoObject);
        Task<Resource> UploadImage(Resource entity);
        Task UpdateUserRole(int userId, int roleId);
        Task<ApplicationUser> GetDetailAsync(int id);
    }
}
