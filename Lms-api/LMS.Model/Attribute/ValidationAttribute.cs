﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using LMS.Common.Helper;
using System.Net.Http;
using System.Net;

namespace LMS.Core.Attribute
{
    public sealed class ValidationAttribute : ActionFilterAttribute
    {
        //public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        //{
        //    foreach (var item in actionContext.ActionArguments)
        //    {
        //        item.Value.IsValid(false);
        //    }

        //    base.OnActionExecuting(actionContext);
        //}

        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var modelState = actionContext.ModelState;

            if (!modelState.IsValid)
            {
                var errorList = (from item in modelState.Values
                                 from error in item.Errors
                                 select error.ErrorMessage).ToList();
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, errorList);
            }
        }
    }
}
