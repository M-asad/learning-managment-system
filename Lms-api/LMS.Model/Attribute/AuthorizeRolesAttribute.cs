﻿using System.Web.Http;
using LMS.Core.Enums;

namespace LMS.Core.Attribute
{
    public class AuthorizeRolesAttribute : AuthorizeAttribute
    {
        public AuthorizeRolesAttribute(params UserRoles[] roles)
            : base()
        {
            if (roles.Length > 0)
                this.Roles = UserRoles.SuperAdmin + "," + string.Join(",", roles);
            else
                this.Roles = string.Join(",", roles);
        }
    }
}
