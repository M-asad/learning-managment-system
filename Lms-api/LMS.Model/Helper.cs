﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Core.Helpers
{
    public static class Helper
    {
        public static string ToStringOrDefault(this DateTime? dt)
        {
            return dt == null ? "" : ((DateTime)dt).ToString("MM/dd/yyyy");
        }

        public static string ToStringOrDefault(this DateTime? dt, string format)
        {
            return dt == null ? "" : ((DateTime)dt).ToString(format);
        }
    }
    //public static class RoleNames
    //{
    //    public const string SuperAdmin = "SuperAdmin";
    //    public const string Admin = "Admin";
    //    public const string Parent = "Parent";
    //    public const string Teacher = "Teacher";
    //}
}
