﻿using System;
using System.Threading.Tasks;
using LMS.Core.Entity;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;
using System.Data.Entity;
using System.Linq;

namespace LMS.Repository
{
    public class AcademicYearRepository : AuditableRepository<AcademicYear>, IAcademicYearRepository
    {
        public AcademicYearRepository(IRequestInfo requestInfo)
            : base(requestInfo)
        {
        }

        public AcademicYear GetCurrentYear()
        {
            return this.DefaultSingleQuery.FirstOrDefault(x => x.IsCurrent);
        }
    }
}
