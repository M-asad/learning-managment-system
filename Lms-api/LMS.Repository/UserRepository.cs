﻿using LMS.Core.Constant;
using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using LMS.Common.Helper;

namespace LMS.Repository
{
    public class UserRepository : AuditableRepository<ApplicationUser>, IUserRepository
    {
        private ApplicationUserManager userManager;

        private IExceptionHelper exceptionHelper;

        public UserRepository(IRequestInfo requestInfo, ApplicationUserManager userManager, IExceptionHelper exceptionHelper)
            : base(requestInfo)
        {
            this.userManager = new ApplicationUserManager(new ApplicationUserStore(requestInfo));
            // Configure validation logic for usernames
            this.userManager.UserValidator = new UserValidator<ApplicationUser, int>(this.userManager)
            {
                AllowOnlyAlphanumericUserNames = true
            };
            // Configure validation logic for passwords
            this.userManager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };

            //var dataProtectionProvider = options.DataProtectionProvider;
            //if (dataProtectionProvider != null)
            //{
            //    appUserManager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser, int>(dataProtectionProvider.Create("ASP.NET Identity"))
            //    {
            //        //Code for email confirmation and reset password life time
            //        TokenLifespan = TimeSpan.FromHours(6)
            //    };
            //}
            this.exceptionHelper = exceptionHelper;
        }

        protected override IQueryable<ApplicationUser> DefaultListQuery
        {
            get
            {
                return base.DefaultListQuery;
            }
        }

        public async override Task<IEnumerable<ApplicationUser>> GetAll()
        {
            return await this.DefaultListQuery.Include(x => x.Roles).ToListAsync();
        }

        public async override Task<IResultSet<ApplicationUser>> GetAll(JsonApiRequest request)
        {
            ResultSet<ApplicationUser> result = new ResultSet<ApplicationUser>();
            int pageCount = 0;
            IQueryable<ApplicationUser> queryable = this.DefaultListQuery.Include(x => x.Roles).GenerateQuery(request, out pageCount);

            if (request.Filter[Filters.Equal].ContainsKey("roleId"))
            {
                string roleId = request.Filter[Filters.Equal]["roleId"];
                queryable = queryable.Where(x => x.Roles.Any(y => y.RoleId == Convert.ToInt32(roleId)));
            }

            result.Data = await queryable.ToListAsync();
            result.PageCount = pageCount;
            return result;
        }

        public async override Task DeleteAsync(int id)
        {
            var entity = await GetAsync(id);
            if (entity != null)
            {
                entity.IsDeleted = true;
                entity.UserName += "_deleted";

                await this.Update(entity);
            }
        }

        public async override Task<IEnumerable<ApplicationUser>> GetAll(IList<int> ids)
        {
            return await this.DefaultListQuery.Include(x => x.Roles).Where(x => ids.Contains(x.Id)).ToListAsync();
        }

        public async override Task<IResultSet<ApplicationUser>> GetAll(IList<int> keys, JsonApiRequest request)
        {
            ResultSet<ApplicationUser> result = new ResultSet<ApplicationUser>();
            int pageCount = 0;
            IQueryable<ApplicationUser> queryable = this.DefaultListQuery.Include(x => x.Roles).GenerateQuery(request, out pageCount).Where(x => keys.Contains(x.Id));

            if (request.Filter[Filters.Equal].ContainsKey("roleId"))
            {
                string roleId = request.Filter[Filters.Equal]["roleId"];
                queryable = queryable.Where(x => x.Roles.Any(y => y.RoleId == Convert.ToInt32(roleId)));
            }

            result.Data = await queryable.ToListAsync();
            result.PageCount = pageCount;
            return result;
        }

        public async Task<ApplicationUser> Create(ApplicationUser entity, string password, string role)
        {
            try
            {
                entity.CreatedOn = DateTime.Now;
                entity.CreatedBy = RequestInfo.UserId.ToString();
                entity.LastModifiedOn = DateTime.Now;
                entity.LastModifiedBy = RequestInfo.UserId.ToString();
                IdentityResult result = await this.userManager.CreateAsync(entity, password);
                if (result.Succeeded)
                {
                    this.userManager.AddToRole(entity.Id, role);
                }
                else
                {
                    this.exceptionHelper.ThrowAPIException(result.Errors.ToList());
                }
            }
            catch (DbEntityValidationException databaseException)
            {
                var errors = new List<string>();
                var validationErrors = databaseException.EntityValidationErrors.Select(x => x.ValidationErrors);

                foreach (var error in validationErrors)
                {
                    errors.AddRange(error.Select(x => x.ErrorMessage));
                }

                this.exceptionHelper.ThrowAPIException(errors);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return entity;
        }

        public async Task<ApplicationUser> FindAsync(string userName, string password)
        {
            return await this.userManager.FindAsync(userName, password);
        }

        public async Task<ApplicationUser> FindByUserNameAsync(string userName)
        {
            try
            {
                var result = await this.userManager.FindByNameAsync(userName);
                return result;
            }
            catch
            {
                return null;
            }
        }

        public async Task<ApplicationUser> FindByEmailAsync(string email)
        {
            try
            {
                var result = await this.userManager.FindByEmailAsync(email);
                return result;
            }
            catch
            {
                return null;
            }
        }

        public async Task<string> GetUserRole(int userId)
        {
            var roles = await this.userManager.GetRolesAsync(userId);
            return roles.FirstOrDefault();
        }

        //public async Task<ApplicationUser> GetByPasswordToken(string token)
        //{
        //    return await this.DefaultSingleQuery.SingleOrDefaultAsync(x => x.PasswordToken == token);
        //}

        public async Task ChangePassword(int userId, string newPassword)
        {
            var result1 = await this.userManager.RemovePasswordAsync(userId);
            var result2 = await this.userManager.AddPasswordAsync(userId, newPassword);
            if (result2.Succeeded)
            {
            }
        }

        public async Task<ChangePasswordDTO> ChangePassword(ChangePasswordDTO dtoObject)
        {
            var user = await this.FindAsync(dtoObject.UserName, dtoObject.OldPassword);

            if (user == null)
            {
                this.exceptionHelper.ThrowAPIException(Message.UserInvalidPassword);
            }

            var result = await this.userManager.ChangePasswordAsync(user.Id, dtoObject.OldPassword, dtoObject.NewPassword);

            if (!result.Succeeded)
            {
                this.exceptionHelper.ThrowAPIException(result.Errors.ToList());
            }

            return dtoObject;
        }

        public async Task<Resource> UploadImage(Resource entity)
        {
            DBContext.Entry(entity).State = EntityState.Added;
            return entity;
        }

        public async Task UpdateUserRole(int userId, int roleId)
        {
            await this.userManager.RemoveFromRolesAsync(userId, this.userManager.GetRoles(userId).ToArray());
            await this.userManager.AddToRoleAsync(userId, Roles.GetRole(roleId));
        }

        public Task<ApplicationUser> GetDetailAsync(int id)
        {
            return this.DefaultListQuery.FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}