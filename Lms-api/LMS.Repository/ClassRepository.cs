﻿using LMS.Core.DBContext;
using LMS.Core.Entity;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;

namespace LMS.Repository
{
    public class ClassRepository : AuditableRepository<Class>, IClassRepository
    {
        public ClassRepository(IRequestInfo requestInfo)
            : base(requestInfo)
        {
        }
    }
}
