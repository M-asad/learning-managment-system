﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMS.Core.Entity;
using LMS.Core.DBContext;

namespace LMS.Repository
{
    public class TempRepo
    {
        public void ExecuteScript()
        {
            try
            {
                using (var ctx = new LMSContext())
                {
                    var result = ctx.Database
                        .SqlQuery<int>(@"IF (OBJECT_ID('FK_Exam_AcademicYear', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE dbo.EXAM DROP CONSTRAINT FK_Exam_AcademicYear	
	EXEC sp_rename 'Exam.AcademicYeadID', 'Id', 'COLUMN';
	ALTER TABLE dbo.EXAM ADD CONSTRAINT FK_Exam_AcademicYear 
	FOREIGN KEY (Id) REFERENCES AcademicYear(Id);
END
GO").FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
