﻿using System;
using System.Collections.Generic;
using System.Linq;
using LMS.Core.DBContext;
using LMS.Core.Entity;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;

namespace LMS.Repository
{
    public class SectionRepository : AuditableRepository<Section>, ISectionRepository
    {
        public SectionRepository(IRequestInfo requestInfo)
            : base(requestInfo)
        {
        }
    }
}
