﻿using LMS.Core.Entity;
using LMS.Core.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using LMS.Core.DBContext;
using LMS.Core.Infrastructure;
using System.Data.Entity;
using LMS.Core.IRepository;

namespace LMS.Repository
{
    public class ClassroomCourseRepository : AuditableRepository<ClassroomCourse>, IClassroomCourseRepository
    {
        public ClassroomCourseRepository(IRequestInfo requestInfo)
            : base(requestInfo)
        {
        }

        protected override IQueryable<ClassroomCourse> DefaultSingleQuery
        {
            get
            {
                return base.DefaultSingleQuery.Include(x => x.Teacher).Include(x => x.Course);
            }
        }

        public List<ClassroomCourseDTO> GetCourseTeachers(int ClassroomId)
        {
            var ctx = this.RequestInfo.Context;
            var list = (from cc in ctx.ClassroomCourses
                        join c in ctx.Courses on cc.CourseId equals c.Id
                        join t in ctx.Teachers on cc.TeacherId equals t.Id
                        where !c.IsDeleted && !cc.IsDeleted && !t.IsDeleted
                            && cc.ClassroomId == ClassroomId
                        select new ClassroomCourseDTO()
                        {
                            Id = cc.Id,
                            ClassroomId= cc.ClassroomId,
                            CourseId = cc.CourseId,
                            CourseName = c.Title,
                            TeacherId = cc.TeacherId,
                            TeacherName = t.FullName
                        }).ToList();
            return list;
        }
    }
}
