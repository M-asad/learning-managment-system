﻿using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using LMS.Core.DBContext;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;

namespace LMS.Repository
{
    public class GradeRepository : AuditableRepository<Grade>, IGradeRepository
    {
        public GradeRepository(IRequestInfo requestInfo)
            : base(requestInfo)
        {
        }
    }
}
