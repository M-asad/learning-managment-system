﻿using LMS.Core.Entity;
using LMS.Core.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using LMS.Core.DBContext;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;

namespace LMS.Repository
{
    public class ExamRepository : AuditableRepository<Exam>, IExamRepository
    {
        private IRequestInfo requestInfo;
        public ExamRepository(IRequestInfo requestInfo)
            : base(requestInfo)
        {
            this.requestInfo = requestInfo;
        }

        public List<ExamListDTO> GetExamList(string prefix, int count, int page, out int pageCount)
        {
            var ctx = this.requestInfo.Context;
            var query = (from e in ctx.Exams
                         join et in ctx.ExamTypes on e.ExamTypeID equals et.Id
                         join ac in ctx.AcademicYears on e.AcademicYearId equals ac.Id
                         where !e.IsDeleted && (!string.IsNullOrEmpty(prefix) ? e.Description.Contains(prefix) : true)
                         orderby e.StartDate descending
                         select new
                         {
                             e.Id,
                             et.Type,
                             e.Description,
                             ac.Title,
                             e.StartDate,
                             e.EndDate
                         });

            pageCount = (query.Count() / count) + 1;
            return query.Skip((page - 1) * count).Take(count).ToList()
                .Select(x => new ExamListDTO
                {
                    Id = x.Id,
                    ExamType = x.Type,
                    Description = x.Description,
                    AcademicYear = x.Title,
                    StartDate = x.StartDate.ToString("MM/dd/yyyy"),
                    EndDate = x.EndDate.ToString("MM/dd/yyyy")
                }).ToList();
        }

        public ExamDetailDTO GetExamDetail(int examId)
        {
            var ctx = this.requestInfo.Context;
            var exam = (from e in ctx.Exams
                        join et in ctx.ExamTypes on e.ExamTypeID equals et.Id
                        join ac in ctx.AcademicYears on e.AcademicYearId equals ac.Id
                        where !e.IsDeleted
                        && e.Id == examId
                        orderby e.Id descending
                        select new
                        {
                            ExamId = e.Id,
                            Id = et.Id,
                            ExamType = et.Type,
                            Description = e.Description,
                            AcademicYear = ac.Title,
                            StartDate = e.StartDate,
                            EndDate = e.EndDate,
                            ClassIds = e.ClassIds
                        }).FirstOrDefault();

            if (exam == null)
                return null;
            int[] classIDs = exam.ClassIds.Split(',').Select(x => Convert.ToInt32(x)).ToArray();
            var classes = ctx.Classes.Where(x => classIDs.Contains(x.Id)).Select(x => new ClassDTO
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            ExamDetailDTO detail = new Core.DTO.ExamDetailDTO()
            {
                ExamId = exam.ExamId,
                Id = exam.Id,
                ExamType = exam.ExamType,
                Description = exam.Description,
                AcademicYear = exam.AcademicYear,
                StartDate = exam.StartDate.ToString("MM/dd/yyyy"),
                EndDate = exam.EndDate.ToString("MM/dd/yyyy"),
                Classes = classes
            };
            // Get detail data here
            var list = (from ed in ctx.ExamDetailSetups
                        join mp in ctx.ExamClassroomMappings on ed.Id equals mp.ExamDetailSetupId
                        join cr in ctx.Classrooms on mp.ClassroomId equals cr.Id
                        join c in ctx.Courses on mp.CourseId equals c.Id
                        join cl in ctx.Classes on cr.ClassId equals cl.Id
                        where !ed.IsDeleted
                        && ed.Id == examId
                        select new
                        {
                            ed.Id,
                            ed.ExamId,
                            cr.ClassId,
                            mp.CourseId,
                            ed.Duration,
                            ed.Date,
                            ed.TheoryTotal,
                            ed.LabTotal,
                            ed.OtherTotal,
                            CourseName = c.Title
                        }).ToList();


            List<ExamTimeTableDTO> timetables = new List<ExamTimeTableDTO>();

            for (DateTime date = exam.StartDate.Date; date < exam.EndDate.Date; date = date.AddDays(1))
            {
                if (date.Date.DayOfWeek != DayOfWeek.Saturday && date.Date.DayOfWeek != DayOfWeek.Sunday)
                {
                    ExamTimeTableDTO timetable = new ExamTimeTableDTO()
                    {
                        Date = date.ToString("MM/dd/yyyy"),
                        Details = new List<ExamTimeTableDetailDTO>()
                    };
                    foreach (var classid in classIDs)
                    {
                        var dbRecord = list.Where(x => x.Id == examId && x.Date == date && x.ClassId == classid).FirstOrDefault();
                        if (dbRecord != null)
                        {
                            timetable.Details.Add(new ExamTimeTableDetailDTO()
                            {
                                ExamDetailID = dbRecord.Id,
                                ClassId = dbRecord.ClassId,
                                ClassName = classes.Where(x => x.Id == dbRecord.ClassId).Select(x => x.Name).FirstOrDefault(),
                                CourseId = dbRecord.CourseId,
                                Course = dbRecord.CourseName,
                                Duration = dbRecord.Duration,
                                TheoryMarks = dbRecord.TheoryTotal,
                                LabMarks = dbRecord.LabTotal,
                                OtherMarks = dbRecord.OtherTotal
                            });
                        }
                        else
                        {
                            timetable.Details.Add(new ExamTimeTableDetailDTO()
                            {
                                ExamDetailID = 0,
                                ClassId = classid,
                                ClassName = classes.Where(x => x.Id == classid).Select(x => x.Name).FirstOrDefault(),
                                CourseId = 0,
                                Course = "",
                                Duration = null,
                                TheoryMarks = null,
                                LabMarks = null,
                                OtherMarks = null
                            });
                        }
                    }
                    timetables.Add(timetable);
                }
            }
            detail.TimeTable = timetables;
            return detail;
        }

        //public bool Update(Exam exam)
        //{
        //    try
        //    {
        //        using (var ctx = new LMSContext())
        //        {
        //            var original = Get(x => x.Id == exam.Id);
        //            if (original == null)
        //                throw new KeyNotFoundException("Exam not found");
        //            // throw new KeyNotFoundException("CE:Exam");

        //            var currentClasses = exam.ClassIds.Split(',').ToList();
        //            var originalClasses = original.ClassIds.Split(',').ToList();
        //            var removedRecords = originalClasses.Where(x => !currentClasses.Contains(x)).ToList();
        //            if (removedRecords.Count > 0)
        //            {
        //                var count = (ctx.ExamDetails.Where(x => x.Id == exam.Id && removedRecords.Contains(Convert.ToString(x.ClassId)))).Count();
        //                if (count > 0)
        //                {
        //                    // records already updated

        //                }
        //            }
        //            newClasses.Where(x => !original.ClassIds.Split(',').ToList().Contains(x));


        //            original.StartDate = exam.StartDate;
        //            original.Description = exam.Description;
        //            original.ClassIds = String.Join(",", exam.ClassIds);
        //            original.LastModifiedBy = requestInfo.UserId.ToString();
        //            original.LastModifiedOn = DateTime.Now;
        //            return true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public bool AreClassesExist(int Id, List<int> classes)
        {
            var ctx = this.requestInfo.Context;
            var count = (from ed in ctx.ExamDetailSetups
                         join mp in ctx.ExamClassroomMappings on ed.Id equals mp.ExamDetailSetupId
                         join cr in ctx.Classrooms on mp.ClassroomId equals cr.Id
                         where !ed.IsDeleted &&
                                !mp.IsDeleted &&
                                ed.Id == Id &&
                                classes.Contains(cr.ClassId)
                                && !cr.IsDeleted
                         select 1).Count();
            // var count = ctx.ExamDetailSetups.Where(x => x.Id == Id && classes.Contains(x.ClassId)).Count();
            return count > 0 ? true : false;
        }

        public bool ValidRange(int Id, DateTime startDate, DateTime endDate)
        {
            var ctx = this.requestInfo.Context;
            var count = ctx.ExamDetailSetups.Where(x => x.Id == Id && (x.Date < startDate || x.Date > endDate)).Count();
            return count > 0 ? false : true;
        }

        //public bool SaveExam(ExamMasterDTO data, int academicYearId)
        //{
        //    var ctx = this.requestInfo.Context;
        //    var entity = new Exam()
        //    {
        //        Id = data.Id,
        //        Description = data.Description,
        //        StartDate = data.StartDate,
        //        EndDate = data.EndDate,
        //        AcademicYearId = academicYearId,
        //        CreatedBy = requestInfo.UserId.ToString(),
        //        CreatedOn = DateTime.Now,
        //        LastModifiedBy = requestInfo.UserId.ToString(),
        //        LastModifiedOn = DateTime.Now,
        //        IsDeleted = false,
        //        ClassIds = String.Join(",", data.ClassIds)
        //    };
        //    ctx.Exams.Add(entity);
        //    ctx.SaveChanges();
        //    var Id = data.Id;

        //    var classrooms = (from cr in ctx.Classrooms
        //                      where !cr.IsDeleted && cr.Id == Id && data.ClassIds.Contains(cr.ClassId)
        //                      select cr).ToList();

        //    List<ExamClassroomMapping> mappings = new List<ExamClassroomMapping>();
        //    foreach (var cr in classrooms)
        //    {
        //        mappings.Add(new ExamClassroomMapping()
        //        {
        //            ClassroomId = cr.Id,
        //            TeacherId = cr.ClassTeacherId,
        //            IsDeleted = false
        //        });
        //    }
        //    ctx.ExamClassroomMappings.AddRange(mappings);
        //    return true;
        //}

        public List<KeyValueDTO> GetCurrentYearExam()
        {
            var ctx = this.requestInfo.Context;
            var list = (from e in ctx.Exams
                        join ac in ctx.AcademicYears on e.AcademicYearId equals ac.Id
                        where !e.IsDeleted && ac.IsCurrent
                        select new KeyValueDTO
                        {
                            Id = e.Id,
                            Text = e.Description
                        }).ToList();
            return list;
        }

        public ExamListDTO GetExamMaster(int examId)
        {
            var ctx = this.requestInfo.Context;
            var data = (from e in ctx.Exams
                        join et in ctx.ExamTypes on e.ExamTypeID equals et.Id
                        join ac in ctx.AcademicYears on e.AcademicYearId equals ac.Id
                        where !e.IsDeleted && e.Id == examId
                        orderby e.StartDate descending
                        select new
                        {
                            Id = e.Id,
                            ExamType = et.Type,
                            Description = e.Description,
                            AcademicYear = ac.Title,
                            StartDate = e.StartDate,
                            EndDate = e.EndDate
                        }).FirstOrDefault();
            if (data != null)
            {
                ExamListDTO dto = new ExamListDTO()
                {
                    Id = data.Id,
                    Description = data.Description,
                    ExamType = data.ExamType,
                    AcademicYear = data.AcademicYear,
                    StartDate = data.StartDate.ToString("MM/dd/yyyy"),
                    EndDate = data.EndDate.ToString("MM/dd/yyyy")
                };
                return dto;
            }
            return null;
        }

        public List<KeyValueDTO> GetExamCourses(int examId)
        {
            var ctx = this.requestInfo.Context;
            var date = DateTime.Now.Date;
            var query = (from ac in ctx.AcademicYears.Where(x => !x.IsDeleted && x.IsCurrent)
                         join e in ctx.Exams.Where(x => !x.IsDeleted) on ac.Id equals e.AcademicYearId
                         join ed in ctx.ExamDetailSetups.Where(x => !x.IsDeleted) on e.Id equals ed.ExamId
                         join edm in ctx.ExamClassroomMappings.Where(x => !x.IsDeleted) on ed.Id equals edm.ExamDetailSetupId
                         join c in ctx.Courses.Where(x => !x.IsDeleted) on edm.CourseId equals c.Id
                         where e.Id == examId && ed.Date <= date
                         select new KeyValueDTO
                         {
                             Id = c.Id,
                             Text = c.Title,
                         }).Distinct();

            return query.ToList();
        }

        public List<KeyValueDTO> GetCurrentYearClasses()
        {
            var ctx = this.requestInfo.Context;
            var list = (from ac in ctx.AcademicYears.Where(x => !x.IsDeleted && x.IsCurrent)
                        join cr in ctx.Classrooms.Where(x => !x.IsDeleted) on ac.Id equals cr.AcademicYearId
                        join c in ctx.Classes.Where(x => !x.IsDeleted) on cr.ClassId equals c.Id
                        select new KeyValueDTO
                        {
                            Id = c.Id,
                            Text = c.Name
                        }).Distinct().ToList();

            return list;
        }

        public int SaveDetail(ExamDetailSaveDTO dto)
        {
            var ctx = this.RequestInfo.Context;
            var detail = (from ed in ctx.ExamDetailSetups
                          where !ed.IsDeleted && ed.Id == dto.ExamDetailID
                          select ed).FirstOrDefault();
            if (detail != null)
            {
                detail.Duration = dto.Duration;
                detail.TheoryTotal = dto.TheoryMarks;
                detail.LabTotal = dto.LabMarks;
                detail.OtherTotal = dto.OtherMarks;
                detail.LastModifiedBy = RequestInfo.UserId.ToString();
                detail.LastModifiedOn = DateTime.Now;
                ctx.SaveChanges();

                // Todo: course can also be changed

                return detail.Id;
            }
            else
            {
                using (var trans = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        var exam = (from e in ctx.Exams
                                    where !e.IsDeleted && e.Id == dto.ExamId
                                    select e).FirstOrDefault();
                        //int[] classIDs = exam.ClassIds.Split(',').Select(x => Convert.ToInt32(x)).ToArray();

                        var classrooms = (from cr in ctx.Classrooms
                                          join cc in ctx.ClassroomCourses on cr.Id equals cc.ClassroomId
                                          where !cr.IsDeleted &&
                                                cr.Id == exam.Id &&
                                                cr.ClassId == dto.ClassId &&
                                                cc.CourseId == dto.CourseId &&
                                                !cc.IsDeleted
                                          select new
                                          {
                                              cr.Id,
                                              cc.TeacherId
                                          }).ToList();

                        //// Get classrooms here and add in mapping table
                        //var classrooms = (from ac in ctx.AcademicYears.Where(x => !x.IsDeleted && x.IsCurrent)
                        //                  join e in ctx.Exams on ac.Id equals e.Id
                        //                  join c in ctx.Classrooms on new { ed.ClassId, ac.Id } equals new { c.ClassId, c.Id }
                        //                  join cc in ctx.ClassroomCourses on c.ClassroomId equals cc.ClassroomId
                        //                  where e.IsDeleted && ed.IsDeleted && !c.IsDeleted
                        //                  select new
                        //                  {
                        //                      c.ClassroomId,
                        //                      cc.TeacherId
                        //                  }).ToList();

                        ExamDetailSetup entity = new Core.Entity.ExamDetailSetup()
                        {
                            ExamId = dto.ExamId,
                            Date = dto.Date,
                            Duration = dto.Duration,
                            TheoryTotal = dto.TheoryMarks,
                            LabTotal = dto.LabMarks,
                            OtherTotal = dto.OtherMarks,
                            CreatedBy = base.RequestInfo.UserId.ToString(),
                            CreatedOn = DateTime.Now,
                            LastModifiedBy = base.RequestInfo.UserId.ToString(),
                            LastModifiedOn = DateTime.Now,
                            IsDeleted = false
                        };
                        ctx.ExamDetailSetups.Add(entity);
                        ctx.SaveChanges();
                        var ExamDetailSetupId = entity.Id;

                        List<ExamClassroomMapping> mappings = new List<ExamClassroomMapping>();
                        foreach (var cr in classrooms)
                        {
                            mappings.Add(new ExamClassroomMapping()
                            {
                                ClassroomId = cr.Id,
                                CourseId = dto.CourseId,
                                ExamDetailSetupId = ExamDetailSetupId,
                                TeacherId = cr.TeacherId,
                                CreatedBy = base.RequestInfo.UserId.ToString(),
                                CreatedOn = DateTime.Now,
                                LastModifiedBy = base.RequestInfo.UserId.ToString(),
                                LastModifiedOn = DateTime.Now,
                                IsDeleted = false
                            });
                        }
                        ctx.ExamClassroomMappings.AddRange(mappings);
                        ctx.SaveChanges();

                        trans.Commit();
                        return entity.Id;
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        return 0;
                    }
                }
            }
        }

        public bool IsExamExists(DateTime startDate, DateTime endDate, int[] classes)
        {
            var ctx = this.requestInfo.Context;
            var ClassIds = (from e in ctx.Exams
                            where (e.StartDate >= startDate && e.StartDate <= endDate)
                            || (e.EndDate >= startDate && e.EndDate <= endDate)
                            && !e.IsDeleted
                            select e.ClassIds).FirstOrDefault();
            if (ClassIds != null)
            {
                List<string> array = ClassIds.Split(',').ToList();
                foreach (var classId in classes)
                {
                    if (array.Contains(Convert.ToString(classId)))
                        return true;
                }
            }
            return false;
        }
    }
}
