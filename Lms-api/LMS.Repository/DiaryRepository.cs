﻿using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using LMS.Core.DBContext;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;

namespace LMS.Repository
{
    public class DiaryRepository : AuditableRepository<Diary>, IDiaryRepository
    {
        public DiaryRepository(IRequestInfo requestInfo)
            : base(requestInfo)
        {
        }
    }
}
