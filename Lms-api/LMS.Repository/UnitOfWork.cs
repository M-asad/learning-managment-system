﻿using LMS.Core.DBContext;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        #region privates
        private readonly IRequestInfo requestInfo;
        private readonly IUserRepository userRepository;
        private readonly IExceptionHelper exceptionHelper;
        private readonly ITeacherRepository teacherRepository;
        private readonly IParentRepository parentRepository;
        private readonly IAcademicYearRepository academicYearRepository;
        private readonly IStudentRepository studentRepository;
        private readonly IClassRepository classRepository;
        private readonly ISectionRepository sectionRepository;
        private readonly ICourseRepository courseRepository;
        private readonly IGradeRepository gradeRepository;
        private readonly IClassroomRepository classroomRepository;
        private readonly IClassroomStudentRepository classroomStudentRepository;
        private readonly IClassroomCourseRepository classroomCourseRepository;
        private readonly IExamRepository examRepository;
        private readonly IExamTypeRepository examTypeRepository;
        private readonly IResultRepository resultRepository;
        private readonly ITimeTableRepository timeTableRepository;
        private readonly ITimeTableTemplateRepository timeTableTemplateRepository;
        private readonly ITimeTableTemplateDetailRepository timeTableTemplateDetailRepository;
        private readonly IAssignmentRepository assignmentRepository;
        private readonly IAssignmentAttachmentRepository assignmentAttachmentRepository;
        private readonly INotificationRepository notificationRepository;
        private readonly IAssignmentDetailRepository assignmentDetailRepository;
        private readonly IDiaryRepository diaryRepository;
        #endregion

        public UnitOfWork(
            IRequestInfo requestInfo,
            IUserRepository userRepository,
            IExceptionHelper exceptionHelper,
            ITeacherRepository teacherRepository,
            IParentRepository parentRepository,
            IAcademicYearRepository academicYearRepository,
            IStudentRepository studentRepository,
            IClassRepository classRepository,
            ISectionRepository sectionRepository,
            ICourseRepository courseRepository,
            IGradeRepository gradeRepository,
            IClassroomRepository classroomRepository,
            IClassroomStudentRepository classroomStudentRepository,
            IClassroomCourseRepository classroomCourseRepository,
            IExamRepository examRepository,
            IExamTypeRepository examTypeRepository,
            IResultRepository resultRepository,
            ITimeTableRepository timeTableRepository,
            ITimeTableTemplateRepository timeTableTemplateRepository,
            ITimeTableTemplateDetailRepository timeTableTemplateDetailRepository,
            IAssignmentRepository assignmentRepository,
            IAssignmentDetailRepository assignmentDetailRepository,
            IAssignmentAttachmentRepository assignmentAttachmentRepository,
            INotificationRepository notificationRepository,
            IDiaryRepository diaryRepository)
        {
            this.requestInfo = requestInfo;
            this.userRepository = userRepository;
            this.exceptionHelper = exceptionHelper;
            this.teacherRepository = teacherRepository;
            this.parentRepository = parentRepository;
            this.academicYearRepository = academicYearRepository;
            this.studentRepository = studentRepository;
            this.classRepository = classRepository;
            this.sectionRepository = sectionRepository;
            this.courseRepository = courseRepository;
            this.gradeRepository = gradeRepository;
            this.classroomRepository = classroomRepository;
            this.classroomStudentRepository = classroomStudentRepository;
            this.classroomCourseRepository = classroomCourseRepository;
            this.examRepository = examRepository;
            this.examTypeRepository = examTypeRepository;
            this.resultRepository = resultRepository;
            this.timeTableRepository = timeTableRepository;
            this.timeTableTemplateRepository = timeTableTemplateRepository;
            this.timeTableTemplateDetailRepository = timeTableTemplateDetailRepository;
            this.assignmentRepository = assignmentRepository;
            this.assignmentAttachmentRepository = assignmentAttachmentRepository;
            this.notificationRepository = notificationRepository;
            this.assignmentDetailRepository = assignmentDetailRepository;
            this.diaryRepository = diaryRepository;
        }

        #region public properties
        public LMSContext DBContext
        {
            get
            {
                return this.requestInfo.Context;
            }
        }

        public IRequestInfo RequestInfo
        {
            get
            {
                return this.requestInfo;
            }
        }

        public IUserRepository UserRepository
        {
            get
            {
                return this.userRepository;
            }
        }

        public IExceptionHelper ExceptionHelper
        {
            get
            {
                return this.exceptionHelper;
            }
        }

        public ITeacherRepository TeacherRepository
        {
            get
            {
                return this.teacherRepository;
            }
        }

        public IParentRepository ParentRepository
        {
            get
            {
                return this.parentRepository;
            }
        }

        public IAcademicYearRepository AcademicYearRepository
        {
            get
            {
                return this.academicYearRepository;
            }
        }

        public IStudentRepository StudentRepository
        {
            get
            {
                return this.studentRepository;
            }
        }

        public IClassRepository ClassRepository
        {
            get
            {
                return this.classRepository;
            }
        }

        public ISectionRepository SectionRepository
        {
            get
            {
                return this.sectionRepository;
            }
        }

        public ICourseRepository CourseRepository
        {
            get
            {
                return this.courseRepository;
            }
        }

        public IGradeRepository GradeRepository
        {
            get
            {
                return this.gradeRepository;
            }
        }

        public IClassroomRepository ClassroomRepository
        {
            get
            {
                return this.classroomRepository;
            }
        }

        public IClassroomStudentRepository ClassroomStudentRepository
        {
            get
            {
                return this.classroomStudentRepository;
            }
        }

        public IClassroomCourseRepository ClassroomCourseRepository
        {
            get
            {
                return this.classroomCourseRepository;
            }
        }

        public IExamRepository ExamRepository
        {
            get
            {
                return this.examRepository;
            }
        }

        public IExamTypeRepository ExamTypeRepository
        {
            get
            {
                return this.examTypeRepository;
            }
        }

        public IResultRepository ResultRepository
        {
            get
            {
                return this.resultRepository;
            }
        }

        public ITimeTableRepository TimeTableRepository
        {
            get
            {
                return this.timeTableRepository;
            }
        }

        public ITimeTableTemplateRepository TimeTableTemplateRepository
        {
            get
            {
                return this.timeTableTemplateRepository;
            }
        }

        public ITimeTableTemplateDetailRepository TimeTableTemplateDetailRepository
        {
            get
            {
                return this.timeTableTemplateDetailRepository;
            }
        }
        
        public IAssignmentRepository AssignmentRepository
        {
            get
            {
                return this.assignmentRepository;
            }
        }

        public IAssignmentDetailRepository AssignmentDetailRepository
        {
            get
            {
                return this.assignmentDetailRepository;
            }
        }

        public IAssignmentAttachmentRepository AssignmentAttachmentRepository
        {
            get
            {
                return this.assignmentAttachmentRepository;
            }
        }

        public INotificationRepository NotificationRepository
        {
            get
            {
                return this.notificationRepository;
            }
        }

        public IDiaryRepository DiaryRepository
        {
            get
            {
                return this.diaryRepository;
            }
        }

        #endregion

        public async Task<int> SaveAsync()
        {
            try
            {
                LogChanges();
                return await this.DBContext.SaveChangesAsync();
            }
            catch (DbEntityValidationException e)
            {
                this.exceptionHelper.ThrowAPIException(e.EntityValidationErrors.First().ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return 0;
        }

        public int Save()
        {
            try
            {
                return this.DBContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public System.Data.Entity.DbContextTransaction BeginTransaction()
        {
            return this.DBContext.Database.BeginTransaction();
        }

        private void LogChanges()
        {
            // Added entries
            List<System.Data.Entity.Infrastructure.DbEntityEntry> addedChanges = this.DBContext.ChangeTracker.Entries().Where(x => x.State == System.Data.Entity.EntityState.Added).ToList();
            foreach (var change in addedChanges)
            {
                var entityName = change.Entity.GetType().Name;

                foreach (var prop in change.CurrentValues.PropertyNames)
                {
                    var currentValue = Convert.ToString(change.CurrentValues[prop]);
                    // prop
                    // currentValue
                    // entityName 
                }
            }

            //// modified entries
            //List<System.Data.Entity.Infrastructure.DbEntityEntry> modifiedChanges = this.DBContext.ChangeTracker.Entries().Where(x => x.State == System.Data.Entity.EntityState.Modified).ToList();
            //foreach (var change in modifiedChanges)
            //{
            //    var entityName = change.Entity.GetType().Name;
            //    // var primaryKey = GetPrimaryKeyValue(change);
            //    var objectStateEntry = ((System.Data.Entity.Infrastructure.IObjectContextAdapter)this).ObjectContext.ObjectStateManager.GetObjectStateEntry(change.Entity);
            //    var primaryKey = objectStateEntry.EntityKey.EntityKeyValues[0].Value;

            //    foreach (var prop in change.OriginalValues.PropertyNames)
            //    {
            //        var originalValue = change.OriginalValues[prop].ToString();
            //        var currentValue = change.CurrentValues[prop].ToString();
            //        if (originalValue != currentValue) //Only create a log if the value changes
            //        {
            //            //Create the Change Log
            //        }
            //    }
            //}
        }
    }
}
