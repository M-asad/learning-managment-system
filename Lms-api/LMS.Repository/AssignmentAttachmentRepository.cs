﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMS.Core.Entity;
using LMS.Core.DTO;
using LMS.Core.Infrastructure;
using LMS.Core.Enums;
using System.Threading.Tasks;
using LMS.Core.IRepository;
using System.Data.Entity;

namespace LMS.Repository
{
    public class AssignmentAttachmentRepository : AuditableRepository<AssignmentAttachment>, IAssignmentAttachmentRepository
    {
        public AssignmentAttachmentRepository(IRequestInfo requestInfo)
            : base(requestInfo)
        {
        }        
    }
}
