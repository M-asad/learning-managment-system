﻿using LMS.Common.Helper;
using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Repository
{
    public class NotificationRepository : AuditableRepository<Notification>, INotificationRepository
    {
        private IExceptionHelper exceptionHelper;
        public NotificationRepository(IRequestInfo requestInfo, IExceptionHelper exceptionHelper)
            : base(requestInfo)
        {
            this.exceptionHelper = exceptionHelper;
        }
    }
}
