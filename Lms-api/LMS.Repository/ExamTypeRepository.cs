﻿using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using LMS.Core.DBContext;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;

namespace LMS.Repository
{
    public class ExamTypeRepository : AuditableRepository<ExamType>, IExamTypeRepository
    {
        public ExamTypeRepository(IRequestInfo requestInfo)
            : base(requestInfo)
        {
        }
    }
}
