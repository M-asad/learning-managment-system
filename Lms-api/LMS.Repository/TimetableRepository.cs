﻿using LMS.Core.Entity;
using LMS.Core.DTO;
using LMS.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using LMS.Core.DBContext;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;

namespace LMS.Repository
{
    public class TimeTableRepository : AuditableRepository<TimeTable>, ITimeTableRepository
    {
        public TimeTableRepository(IRequestInfo requestInfo)
            : base(requestInfo)
        {
        }
    }
}
