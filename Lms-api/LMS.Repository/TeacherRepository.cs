﻿using LMS.Common.Helper;
using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Repository
{
    public class TeacherRepository : AuditableRepository<Teacher>, ITeacherRepository
    {
        private IExceptionHelper exceptionHelper;
        public TeacherRepository(IRequestInfo requestInfo, IExceptionHelper exceptionHelper)
            : base(requestInfo)
        {
            this.exceptionHelper = exceptionHelper;
        }

        public override async Task<IResultSet<Teacher>> GetAll(JsonApiRequest request)
        {
            var prefixFilter = request.Filter[Filters.Contains].Where(x => x.Key == "prefix").FirstOrDefault();
            return await base.GetList(request, x => x.FullName.Contains(prefixFilter.Value) || x.Phone.Contains(prefixFilter.Value) || x.Email.Contains(prefixFilter.Value));
        }

        public bool HaveDependencies(int id)
        {
            try
            {
                var ctx = this.RequestInfo.Context;

                // Check if teacher is assigned any course this academic year
                var IsCourseAssigned = (from ac in ctx.AcademicYears
                                        join cr in ctx.Classrooms on ac.Id equals cr.AcademicYearId
                                        join cs in ctx.ClassroomCourses on cr.Id equals cs.ClassroomId
                                        where !ac.IsDeleted && ac.IsCurrent && !cr.IsDeleted && !cs.IsDeleted &&
                                                cs.TeacherId == id
                                        select cs).Any();

                // Check if the teacher is Class teacher of any course
                var IsClassTeacher = (from ac in ctx.AcademicYears
                                      join cr in ctx.Classrooms on ac.Id equals cr.AcademicYearId
                                      where !ac.IsDeleted && ac.IsCurrent && !cr.IsDeleted &&
                                          cr.ClassTeacherId == id
                                      select cr).Any();

                // Improvement - both queries can be handled in single query

                return IsCourseAssigned || IsClassTeacher;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Teacher> FindByName(string name)
        {
            return await this.DefaultSingleQuery.Where(x => x.FullName == name).FirstOrDefaultAsync();
        }

        public async Task<Teacher> FindByName(string name, int id)
        {
            return await this.DefaultSingleQuery.Where(x => x.Id != id && x.FullName == name).FirstOrDefaultAsync();
        }
    }
}
