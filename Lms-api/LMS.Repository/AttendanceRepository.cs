﻿using LMS.Core.Entity;
using LMS.Core.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using LMS.Core.DBContext;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;

namespace LMS.Repository
{
    public class AttendanceRepository : IAttendanceRepository
    {
        private IRequestInfo requestInfo;
        public AttendanceRepository(IRequestInfo requestInfo)
        {
            this.requestInfo = requestInfo;
        }

        /// <summary>
        /// Give weeks attendance of all students of a classroom
        /// </summary>
        public List<AttendanceCalendarDTO> GetAttendanceCalendar(int ClassroomId, DateTime FromDate, DateTime ToDate)
        {
            var ctx = this.requestInfo.Context;
            // Create date list
            var dates = new List<DateTime>();
            for (var dt = FromDate; dt <= ToDate; dt = dt.AddDays(1))
            {
                dates.Add(dt);
            }

            // This will give all enrolled student of a classroom
            var query = (from cs in ctx.ClassroomStudents
                         join st in ctx.Students on cs.StudentId equals st.Id
                         join a in ctx.Attendances on st.Id equals a.StudentId
                         into joinTable
                         from att in joinTable.DefaultIfEmpty()
                         join t in ctx.AttendanceTypes on att.AttendanceTypeID equals t.Id
                         into typeJoin
                         from type in typeJoin.DefaultIfEmpty()
                         where cs.ClassroomId == ClassroomId
                         select new
                         {
                             StudentId = st.Id,
                             st.GRNo,
                             StudentName = st.FullName,
                             att,
                             type
                         }).ToList();

            List<AttendanceCalendarDTO> list = new List<AttendanceCalendarDTO>();

            // Looping all students w/ and w/o attendance data
            foreach (var student in query.Select(x => new { x.GRNo, x.StudentId, x.StudentName }).Distinct())
            {
                List<CalendarDetailDTO> attendance = new List<CalendarDetailDTO>();
                // Traversing all dates for every student and adding attendance or dummy object
                foreach (var date in dates)
                {
                    // Get available attendance data for this date plus student
                    var studentAttendance = query.Where(x => x.StudentId == student.StudentId && x.att != null && x.att.Date == date).Select(x => new { x.att, x.type }).FirstOrDefault();
                    if (studentAttendance != null)
                    {
                        attendance.Add(new CalendarDetailDTO()
                        {
                            AttendanceID = studentAttendance.att.Id,
                            Date = date,
                            AttendanceType = studentAttendance.type.Type
                        });
                    }
                    else
                    {
                        // No attendance
                        attendance.Add(new CalendarDetailDTO()
                        {
                            AttendanceID = 0,
                            Date = date,
                            AttendanceType = "NA"
                        });
                    }
                }
                list.Add(new AttendanceCalendarDTO()
                {
                    GrNo = student.GRNo,
                    StudentId = student.StudentId,
                    StudentName = student.StudentName,
                    Attendance = attendance
                });
            }
            return list;
        }

        /// <summary>
        /// Shows all attendance of all enrolled student of a classroom for update page
        /// </summary>
        public ClassAttendanceDTO GetAttendance(int ClassroomId, DateTime Date, bool FillEmpty)
        {
            var ctx = this.requestInfo.Context;
            // This will give all enrolled student of a classroom
            var query = (from cs in ctx.ClassroomStudents
                         join st in ctx.Students on cs.StudentId equals st.Id
                         join a in ctx.Attendances on new { StudentId = st.Id, Date } equals new { a.StudentId, a.Date }
                         into joinTable
                         from att in joinTable.DefaultIfEmpty()
                         join t in ctx.AttendanceTypes on att.AttendanceTypeID equals t.Id
                         into typeJoin
                         from type in typeJoin.DefaultIfEmpty()
                         where cs.ClassroomId == ClassroomId
                         select new
                         {
                             StudentId = st.Id,
                             st.GRNo,
                             StudentName = st.FullName,
                             att,
                             type
                         }).ToList();

            List<StudentAttendanceDTO> list = new List<StudentAttendanceDTO>();

            // Looping all students 
            foreach (var student in query.Select(x => new { x.GRNo, x.StudentId, x.StudentName }).Distinct())
            {
                // Get available attendance data for this student
                var studentAttendance = query.Where(x => x.StudentId == student.StudentId && x.att != null && x.att.Date == Date).FirstOrDefault();
                if (studentAttendance != null)
                {
                    list.Add(new StudentAttendanceDTO()
                    {
                        GrNo = student.GRNo,
                        StudentId = studentAttendance.StudentId,
                        StudentName = studentAttendance.StudentName,
                        AttendanceID = studentAttendance.att.Id,
                        AttendanceType = studentAttendance.type.Type
                    });
                }
                else if (FillEmpty)
                {
                    // No attendance
                    list.Add(new StudentAttendanceDTO()
                    {
                        GrNo = student.GRNo,
                        StudentId = student.StudentId,
                        StudentName = student.StudentName,
                        AttendanceID = 0,
                        AttendanceType = "NA"
                    });
                }
            }
            // TODO: Get classroom name only
            var classroom = new ClassroomRepository(this.requestInfo).GetClassroomDetail(ClassroomId);
            ClassAttendanceDTO master = new ClassAttendanceDTO()
            {
                ClassroomId = ClassroomId,
                ClassroomTitle = classroom.ClassroomName,
                AttendanceDate = Date,
                studentAttendance = list
            };
            return master;
        }

        /// <summary>
        /// Give months attendance of a student
        /// </summary>
        public AttendanceCalendarDTO GetStudentCalendar(StudentDTO student, int Year, int Month)
        {
            var ctx = this.requestInfo.Context;
            DateTime FromDate = new DateTime(Year, Month, 1);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            int StudentId = student.Id;

            // Create date list
            var dates = new List<DateTime>();
            for (var dt = FromDate; dt <= ToDate; dt = dt.AddDays(1))
            {
                dates.Add(dt);
            }

            var query = (from cs in ctx.ClassroomStudents
                         join a in ctx.Attendances on cs.StudentId equals a.StudentId
                         into joinTable
                         from att in joinTable.DefaultIfEmpty()
                         join t in ctx.AttendanceTypes on att.AttendanceTypeID equals t.Id
                         into typeJoin
                         from type in typeJoin.DefaultIfEmpty()
                         where cs.StudentId == StudentId
                         select new
                         {
                             att,
                             type
                         }).ToList();

            AttendanceCalendarDTO result = new AttendanceCalendarDTO()
            {
                GrNo = student.GRNo,
                StudentId = student.Id,
                StudentName = student.FullName
            };

            List<CalendarDetailDTO> list = new List<CalendarDetailDTO>();
            // Traversing all dates and adding attendance or dummy object
            foreach (var date in dates)
            {
                // Get available attendance data for this date plus student
                var studentAttendance = query.Where(x => x.att != null && x.att.Date == date).Select(x => new { x.att, x.type }).FirstOrDefault();
                if (studentAttendance != null)
                {
                    list.Add(new CalendarDetailDTO()
                    {
                        AttendanceID = studentAttendance.att.Id,
                        Date = date,
                        AttendanceType = studentAttendance.type.Type
                    });
                }
                else
                {
                    // No attendance
                    list.Add(new CalendarDetailDTO()
                    {
                        AttendanceID = 0,
                        Date = date,
                        AttendanceType = "NA"
                    });
                }
                result.Attendance = list;
            }

            return result;
        }

        /// <summary>
        /// Save / Update attendance of a classroom 
        /// </summary>
        public void SaveAttendance(SaveClassAttendanceDTO attendanceData)
        {
            var ctx = this.requestInfo.Context;
            // Get attendance types
            var types = ctx.AttendanceTypes.Where(x => !x.IsDeleted).ToList();

            // Get current attendance for date
            ClassAttendanceDTO currentData = GetAttendance(attendanceData.ClassroomId, attendanceData.AttendanceDate, false);

            // Loop all student 
            foreach (var studentAtt in attendanceData.studentAttendance)
            {
                int typeId = types.Where(x => x.Type == studentAtt.AttendanceType).Select(x => x.Id).FirstOrDefault();

                if (typeId != 0)
                {
                    // Save if not exists else Update 
                    // if (studentAtt.AttendanceID == 0)
                    if (currentData.studentAttendance.Where(x => x.StudentId == studentAtt.StudentId).Any())
                    {
                        Attendance attendance = ctx.Attendances.Where(x => x.StudentId == studentAtt.StudentId && x.Date == attendanceData.AttendanceDate && !x.IsDeleted).FirstOrDefault();
                        //if (studentAtt.AttendanceID == 0)
                        //    attendance = ctx.Attendances.Where(x => x.StudentId == studentAtt.StudentId && x.Date == attendanceData.AttendanceDate && !x.IsDeleted).FirstOrDefault();
                        //else
                        //    attendance = ctx.Attendances.Where(x => x.AttendanceID == studentAtt.AttendanceID && !x.IsDeleted).FirstOrDefault();
                        attendance.AttendanceTypeID = typeId;
                        attendance.LastModifiedBy = requestInfo.UserId.ToString();
                        attendance.LastModifiedOn = DateTime.Now;
                    }
                    else
                    {
                        ctx.Attendances.Add(new Attendance()
                        {
                            StudentId = studentAtt.StudentId,
                            AttendanceTypeID = typeId,
                            ClassroomId = attendanceData.ClassroomId,
                            Date = attendanceData.AttendanceDate,
                            CreatedBy = requestInfo.UserId.ToString(),
                            CreatedOn = DateTime.Now,
                            LastModifiedBy = requestInfo.UserId.ToString(),
                            LastModifiedOn = DateTime.Now,
                            IsDeleted = false
                        });
                    }
                    ctx.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Update single occurance of attendance
        /// </summary>
        public void UpdateAttendance(AttendanceUpdateDTO attendanceData)
        {
            var ctx = this.requestInfo.Context;
            var record = ctx.Attendances.Where(x => x.Id == attendanceData.AttendanceID).FirstOrDefault();

            if (record != null)
            {
                var type = ctx.AttendanceTypes.Where(x => x.Type == attendanceData.AttendanceType).FirstOrDefault();
                record.AttendanceTypeID = type.Id;
                record.LastModifiedBy = requestInfo.UserId.ToString();
                record.LastModifiedOn = DateTime.Now;
                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// Save / Update attendance of a classroom 
        /// </summary>
        public void SaveStudentCalendar(AttendanceCalendarDTO attendanceData)
        {
            var ctx = this.requestInfo.Context;
            // Get all attendance types
            var types = ctx.AttendanceTypes.Where(x => !x.IsDeleted).ToList();
            using (var trans = ctx.Database.BeginTransaction())
            {
                try
                {
                    bool isChange = false;
                    // Loop all dates of a month 
                    foreach (var studentAtt in attendanceData.Attendance)
                    {
                        if (studentAtt.AttendanceType != "NA")
                        {
                            int typeId = types.Where(x => x.Type == studentAtt.AttendanceType).Select(x => x.Id).FirstOrDefault();
                            if (studentAtt.AttendanceID == 0)
                            {
                                // Get current classroom of student 
                                var classroomID = (from s in ctx.Students.Where(x => !x.IsDeleted && x.Id == attendanceData.StudentId) 
                                                   join cs in ctx.ClassroomStudents.Where(x => !x.IsDeleted) on s.Id equals cs.StudentId
                                                   join c in ctx.Classrooms.Where(x => !x.IsDeleted) on cs.ClassroomId equals c.Id
                                                   join ac in ctx.AcademicYears.Where(x => x.IsCurrent && !x.IsDeleted) on c.AcademicYearId equals ac.Id
                                                   select cs.ClassroomId).FirstOrDefault();

                                var entity = new Attendance()
                                {
                                    StudentId = attendanceData.StudentId,
                                    AttendanceTypeID = typeId,
                                    ClassroomId = classroomID,
                                    Date = studentAtt.Date,
                                    CreatedBy = requestInfo.UserId.ToString(),
                                    CreatedOn = DateTime.Now,
                                    LastModifiedBy = requestInfo.UserId.ToString(),
                                    LastModifiedOn = DateTime.Now,
                                    IsDeleted = false
                                };
                                ctx.Attendances.Add(entity);
                            }
                            else
                            {
                                // Update 
                                Attendance attendance = ctx.Attendances.Where(x => x.Id == studentAtt.AttendanceID && !x.IsDeleted).FirstOrDefault();
                                attendance.AttendanceTypeID = typeId;
                                attendance.LastModifiedBy = requestInfo.UserId.ToString();
                                attendance.LastModifiedOn = DateTime.Now;
                            }
                            isChange = true;
                            ctx.SaveChanges();
                        }
                    }
                    if (isChange)
                        trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                }
            }
        }
    }
}
