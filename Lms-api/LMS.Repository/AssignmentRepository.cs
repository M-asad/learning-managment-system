﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMS.Core.Entity;
using LMS.Core.DTO;
using LMS.Core.Infrastructure;
using LMS.Core.Enums;
using System.Threading.Tasks;
using LMS.Core.IRepository;
using System.Data.Entity;

namespace LMS.Repository
{
    public class AssignmentRepository : AuditableRepository<Assignment>, IAssignmentRepository
    {
        public AssignmentRepository(IRequestInfo requestInfo)
            : base(requestInfo)
        {
        }

        protected override IQueryable<Assignment> DefaultSingleQuery
        {
            get
            {
                return base.DefaultSingleQuery.Include(x => x.AssignmentDetails);
            }
        }

        public async override Task<Assignment> GetAsync(int id)
        {
            var ctx = this.RequestInfo.Context;
            Assignment entity = await this.DefaultSingleQuery.SingleOrDefaultAsync(x => x.Id.Equals(id));
            entity.AssignmentAttachments = ctx.AssignmentAttachments.Where(x => x.AssignmentID == id && !x.IsDeleted).ToList();

            return entity;
        }

        public async override Task<Assignment> Create(Assignment assignment)
        {
            var ctx = this.RequestInfo.Context;
            assignment.CreatedBy = RequestInfo.UserId.ToString();
            assignment.CreatedOn = DateTime.Now;
            assignment.LastModifiedBy = RequestInfo.UserId.ToString();
            assignment.LastModifiedOn = DateTime.Now;
            assignment.IsDeleted = false;

            // Get all students of classroom to assign exams
            var studentIDs = (from cs in ctx.ClassroomStudents
                              join st in ctx.Students on cs.StudentId equals st.Id
                              where cs.ClassroomId == assignment.ClassroomId && !cs.IsDeleted && !st.IsDeleted
                              select st.Id).ToList();

            foreach (var id in studentIDs)
            {
                assignment.AssignmentDetails.Add(new AssignmentDetail()
                {
                    StudentId = id,
                    ObtainedMarks = null,
                    CreatedBy = RequestInfo.UserId.ToString(),
                    CreatedOn = DateTime.Now,
                    LastModifiedBy = RequestInfo.UserId.ToString(),
                    LastModifiedOn = DateTime.Now,
                    IsDeleted = false
                });
            }

            // Attachment
            foreach (var at in assignment.AssignmentAttachments)
            {
                at.CreatedBy = RequestInfo.UserId.ToString();
                at.CreatedOn = DateTime.Now;
                at.LastModifiedBy = RequestInfo.UserId.ToString();
                at.LastModifiedOn = DateTime.Now;
                at.IsDeleted = false;
            }

            ctx.Assignments.Add(assignment);
            ctx.SaveChanges();
            return assignment;

        }

        public List<AssignmentListDTO> GetAssignments(PaginationDTO pagination, out int pageCount)
        {
            var ctx = this.RequestInfo.Context;
            int prefixInt = 0;
            int.TryParse(pagination.Prefix, out prefixInt);
            IQueryable<AssignmentListDTO> query;

            switch (this.RequestInfo.RoleId)
            {
                case (int)UserRoles.Teacher:
                    {
                        query = (from asg in ctx.Assignments
                                 join cr in ctx.Classrooms on asg.ClassroomId equals cr.Id
                                 join c in ctx.Courses on asg.CourseId equals c.Id
                                 join cl in ctx.Classes on cr.ClassId equals cl.Id
                                 join s in ctx.Sections on cr.SectionId equals s.Id
                                 where !asg.IsDeleted &&
                                        asg.CreatedBy == RequestInfo.UserId.ToString() &&
                                        !string.IsNullOrEmpty(pagination.Prefix) ? (
                                            asg.Title.Contains(pagination.Prefix)
                                        ) : true
                                        && !cr.IsDeleted
                                        && !c.IsDeleted
                                        && !s.IsDeleted
                                        && !cl.IsDeleted
                                 select new AssignmentListDTO()
                                 {
                                     Id = asg.Id,
                                     Title = asg.Title,
                                     EndDate = asg.Date,
                                     CreatedOn = asg.CreatedOn,
                                     Course = c.Title,
                                     Classroom = cl.Name + " - " + s.Title
                                 });
                        break;
                    }
                case (int)UserRoles.Parent:
                    {
                        var Id = ctx.Parents.Where(x => x.UserId == RequestInfo.UserId && !x.IsDeleted).FirstOrDefault().Id;
                        query = (from st in ctx.Students.Where(x => !x.IsDeleted && x.ParentId == Id)
                                 join ad in ctx.AssignmentDetails.Where(x => !x.IsDeleted) on st.Id equals ad.StudentId
                                 join asg in ctx.Assignments.Where(x => !x.IsDeleted) on ad.AssignmentID equals asg.Id
                                 join cr in ctx.Classrooms.Where(x => !x.IsDeleted) on asg.ClassroomId equals cr.Id
                                 join c in ctx.Courses.Where(x => !x.IsDeleted) on asg.CourseId equals c.Id
                                 join cl in ctx.Classes.Where(x => !x.IsDeleted) on cr.ClassId equals cl.Id
                                 join s in ctx.Sections.Where(x => !x.IsDeleted) on cr.SectionId equals s.Id
                                 where !string.IsNullOrEmpty(pagination.Prefix) ? (
                                    (asg.Title).Contains(pagination.Prefix)
                                 ) : true
                                 select new AssignmentListDTO()
                                 {
                                     Id = asg.Id,
                                     Title = asg.Title,
                                     EndDate = asg.Date,
                                     CreatedOn = asg.CreatedOn,
                                     Course = c.Title,
                                     Classroom = cl.Name + " - " + s.Title
                                 });
                        break;
                    }
                default:
                    {
                        query = (from asg in ctx.Assignments
                                 join cr in ctx.Classrooms on asg.ClassroomId equals cr.Id
                                 join c in ctx.Courses on asg.CourseId equals c.Id
                                 join cl in ctx.Classes on cr.ClassId equals cl.Id
                                 join s in ctx.Sections on cr.SectionId equals s.Id
                                 where !asg.IsDeleted
                                 && !string.IsNullOrEmpty(pagination.Prefix) ? (
                                 (asg.Title).Contains(pagination.Prefix)
                                 ) : true
                                 && !cr.IsDeleted
                                 && !c.IsDeleted
                                 && !s.IsDeleted
                                 && !cl.IsDeleted
                                 select new AssignmentListDTO()
                                 {
                                     Id = asg.Id,
                                     Title = asg.Title,
                                     EndDate = asg.Date,
                                     CreatedOn = asg.CreatedOn,
                                     Course = c.Title,
                                     Classroom = cl.Name + " - " + s.Title
                                 });
                        break;
                    }
            }
            query = query.OrderByDescending(x => x.EndDate);

            pageCount = (query.Count() / pagination.Count) + 1;
            return query.Skip((pagination.Page - 1) * pagination.Count).Take(pagination.Count).ToList();

        }

        public bool isTitleExists(string title, int id)
        {
            var ctx = this.RequestInfo.Context;
            return ctx.Assignments.Where(x => x.IsDeleted && (id == 0 || x.Id != id) && x.Title == title).Any();
        }

        public bool isAssignmentOwner(int assignmentId)
        {
            var ctx = this.RequestInfo.Context;
            return ctx.Assignments.Where(x => x.IsDeleted && x.Id == assignmentId && x.CreatedBy == this.RequestInfo.UserId.ToString()).Any();
        }

        public List<AssignmentListDTO> GetStudentAssignments(PaginationDTO pagination, int studentId, out int pageCount)
        {
            var ctx = this.RequestInfo.Context;
            int prefixInt = 0;
            int.TryParse(pagination.Prefix, out prefixInt);
            var query = (from st in ctx.Students.Where(x => !x.IsDeleted && x.Id == studentId)
                         join ad in ctx.AssignmentDetails.Where(x => !x.IsDeleted) on st.Id equals ad.StudentId
                         join asg in ctx.Assignments.Where(x => !x.IsDeleted) on ad.AssignmentID equals asg.Id
                         join cr in ctx.Classrooms.Where(x => !x.IsDeleted) on asg.ClassroomId equals cr.Id
                         join c in ctx.Courses.Where(x => !x.IsDeleted) on asg.CourseId equals c.Id
                         join cl in ctx.Classes.Where(x => !x.IsDeleted) on cr.ClassId equals cl.Id
                         join s in ctx.Sections.Where(x => !x.IsDeleted) on cr.SectionId equals s.Id
                         where !string.IsNullOrEmpty(pagination.Prefix) ? (
                            (asg.Title).Contains(pagination.Prefix)
                         ) : true
                         select new AssignmentListDTO()
                         {
                             Id = asg.Id,
                             Title = asg.Title,
                             EndDate = asg.Date,
                             CreatedOn = asg.CreatedOn,
                             Course = c.Title,
                             Classroom = cl.Name + " - " + s.Title
                         });
            query = query.OrderByDescending(x => x.EndDate);

            pageCount = (query.Count() / pagination.Count) + 1;
            return query.Skip((pagination.Page - 1) * pagination.Count).Take(pagination.Count).ToList();
        }

        public List<AssignmentEmailDTO> GetAssignmentCreationEmailData(int Id)
        {
            var ctx = this.RequestInfo.Context;
            var list = (from asg in ctx.Assignments
                         where asg.Id == Id
                         from det in ctx.AssignmentDetails 
                         where det.AssignmentID == asg.Id && det.IsDeleted == false
                         join st in ctx.Students on det.StudentId equals st.Id
                         join p in ctx.Parents on st.ParentId equals p.Id
                         join c in ctx.Courses on asg.CourseId equals c.Id
                         select new AssignmentEmailDTO()
                         {
                             Student  = st.FullName,
                             Parent = p.FullName,
                             ParentUserId = p.UserId,
                             EmailOptIn = p.EmailOptIn ?? false,
                             Email = p.Email,
                             Title = asg.Title,
                             DueDate = asg.Date,
                             Course = c.Title
                         }).ToList();
            return list;
        }
    }
}
