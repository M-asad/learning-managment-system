﻿using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using LMS.Core.IRepository;
using LMS.Core.DBContext;
using LMS.Core.Infrastructure;

namespace LMS.Repository
{
    public abstract class AuditableRepository<TEntity> : BaseRepository<TEntity>, IAuditableRepository<TEntity>
       where TEntity : class, IAuditModel
    {
        public AuditableRepository(IRequestInfo requestInfo)
            : base(requestInfo)
        {
        }

        protected override IQueryable<TEntity> DefaultListQuery
        {
            get
            {
                return base.DefaultListQuery.Where(x => !x.IsDeleted);
            }
        }

        protected override IQueryable<TEntity> DefaultSingleQuery
        {
            get
            {
                return base.DefaultSingleQuery.Where(x => !x.IsDeleted);
            }
        }

        public override async Task<TEntity> Create(TEntity entity)
        {
            entity.CreatedBy = RequestInfo.UserId.ToString();
            entity.CreatedOn = DateTime.UtcNow;
            entity.LastModifiedBy = RequestInfo.UserId.ToString();
            entity.LastModifiedOn = DateTime.UtcNow;
            entity.IsDeleted = false;

            return await base.Create(entity);
        }

        public override async Task<TEntity> Update(TEntity entity)
        {
            entity.LastModifiedOn = DateTime.UtcNow;
            entity.LastModifiedBy = RequestInfo.UserId.ToString();

            return await base.Update(entity);
        }

        public override async Task DeleteAsync(int id)
        {
            var entity = await GetAsync(id);
            if (entity == null)
                throw new System.Exception("Record not found.");

            entity.LastModifiedOn = DateTime.UtcNow;
            entity.LastModifiedBy = RequestInfo.UserId.ToString();
            entity.IsDeleted = true;

            await base.Update(entity);
        }

        public async Task HardDeleteAsync(int id)
        {
            await base.DeleteAsync(id);
        }

        public virtual void UpdateChildrenWithOutLog<TChildEntity>(TChildEntity childEntity) where TChildEntity : class, IBase
        {
            if (childEntity.Id > 0)
            {
                DBContext.Entry(childEntity).State = EntityState.Modified;
            }
            else
            {
                DBContext.Entry(childEntity).State = EntityState.Added;
            }
        }

        protected void UpdateChildrenWithoutLog<TChildEntity>(ICollection<TChildEntity> childEntities) where TChildEntity : class, IBase
        {
            foreach (var entity in childEntities)
            {
                this.UpdateChildrenWithOutLog(entity);
            }
        }        
    }    
}
