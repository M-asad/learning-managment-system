﻿using LMS.Core.Entity;
using LMS.Core.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using LMS.Core.DBContext;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;
using LMS.Core.DTO.CustomDTOs;

namespace LMS.Repository
{
    public class ClassroomStudentRepository : AuditableRepository<ClassroomStudent>, IClassroomStudentRepository
    {
        public ClassroomStudentRepository(IRequestInfo requestInfo)
            : base(requestInfo)
        {
        }

        public List<StudentSimpleDTO> GetStudents(int classroomId)
        {
            try
            {
                //var students = this.DefaultSingleQuery.Where(x => x.ClassroomId == classroomId)
                //            .Join(DBContext.Students.Where(x => !x.IsDeleted),
                //                    cs => cs.StudentId,
                //                    student => student.Id,
                //                    (cs, student) => student).ToList();
                //return students;

                var ctx = this.RequestInfo.Context;
                var students = (from cs in ctx.ClassroomStudents
                                join st in ctx.Students on cs.StudentId equals st.Id
                                join p in ctx.Parents on st.ParentId equals p.Id
                                where !cs.IsDeleted && !st.IsDeleted
                                && cs.ClassroomId == classroomId
                                select new StudentSimpleDTO
                                {
                                    Id = cs.StudentId,
                                    FullName = st.FullName,
                                    GRNo = st.GRNo,
                                    ParentId = p.Id,
                                    Parent = p.FullName
                                }).ToList();
                return students;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get unenrolled students of this academic year 
        /// </summary>
        /// <returns></returns>
        public List<StudentSimpleDTO> GetUnEnrolledStudents(string prefix, int count)
        {
            try
            {
                var ctx = this.RequestInfo.Context;
                var students = (from st in ctx.Students.Where(x => !x.IsDeleted)
                                join p in ctx.Parents.Where(x => !x.IsDeleted) on st.ParentId equals p.Id
                                join csl in ctx.ClassroomStudents.Where(x => !x.IsDeleted) on st.Id equals csl.StudentId into incs
                                from cs in incs.DefaultIfEmpty()
                                join crl in ctx.Classrooms.Where(x => !x.IsDeleted && x.AcademicYearId == this.AcademicYearId) on cs.ClassroomId equals crl.Id into incr
                                from cr in incr.DefaultIfEmpty()
                                where cs == null
                                && (!string.IsNullOrEmpty(prefix) ? (
                                    (st.FullName).Contains(prefix)
                                    || st.GRNo.Contains(prefix)
                                ) : true)
                                select new StudentSimpleDTO()
                                {
                                    Id = st.Id,
                                    FullName = st.FullName,
                                    GRNo = st.GRNo,
                                    Parent = p.FullName,
                                    ParentId = st.ParentId
                                }).Take(count).ToList();
                return students;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ClassroomStudentDTO> GetEnrolledStudents(int ClassroomId)
        {
            var ctx = this.RequestInfo.Context;
            var list = (from cs in ctx.ClassroomStudents
                        join st in ctx.Students on cs.StudentId equals st.Id
                        join p in ctx.Parents on st.ParentId equals p.Id
                        where !cs.IsDeleted && !st.IsDeleted
                            && cs.ClassroomId == ClassroomId
                        select new ClassroomStudentDTO()
                        {
                            Id = cs.Id,
                            ClassroomId = cs.ClassroomId,
                            StudentId = cs.StudentId,
                            StudentName = st.FullName,
                            GRNo = st.GRNo,
                            ParentId = p.Id,
                            ParentName = p.FullName,
                            RollNo = cs.RollNo
                        }).ToList();

            return list;
        }

        public int GetMaxRollNo(int classroomID)
        {
            var ctx = this.RequestInfo.Context;
            var rollno = ctx.ClassroomStudents.Where(x => !x.IsDeleted && x.ClassroomId == classroomID).Select(x => x.RollNo).DefaultIfEmpty(0).Max();
            return rollno;
        }

        public ClassroomStudentDetailDTO GetStudentClassroom(int studentId)
        {
            var ctx = this.RequestInfo.Context;
            return (from cs in ctx.ClassroomStudents
                    join cr in ctx.Classrooms on cs.ClassroomId equals cr.Id
                    where !cs.IsDeleted && cs.StudentId == studentId
                    select new ClassroomStudentDetailDTO()
                    {
                        StudentID = cs.StudentId,
                        RollNo = cs.RollNo,
                        ClassroomId = cr.Id,
                        ClassroomName = cr.ClassroomName
                    }).FirstOrDefault();
        }


        public List<ClassroomStudent> GetClassroomStudents(int ClassroomId)
        {
            var ctx = this.RequestInfo.Context;
            var list = (from cs in ctx.ClassroomStudents
                        where !cs.IsDeleted && cs.ClassroomId == ClassroomId
                        select cs).ToList();

            return list;
        }
    }
}
