﻿using LMS.Core.Entity;
using LMS.Core.DTO;
using LMS.Core.DBContext;
using LMS.Core.Infrastructure;
using System.Linq;
using System.Data.Entity;
using LMS.Core.IRepository;

namespace LMS.Repository
{
    public class TimeTableTemplateRepository : AuditableRepository<TimeTableTemplate>, ITimeTableTemplateRepository
    {
        public TimeTableTemplateRepository(IRequestInfo requestInfo)
            : base(requestInfo)
        {
        }

        protected override IQueryable<TimeTableTemplate> DefaultSingleQuery
        {
            get
            {
                return base.DefaultSingleQuery.Include(x => x.TimeTableTemplateDetails);
            }
        }

        //public TimeTableTemplateDTO GetTimeTableTemplate(int TemplateID)
        //{
        //    try
        //    {
        //        using (var ctx = new LMSContext())
        //        {
        //            TimeTableTemplateDTO template = new TimeTableTemplateDTO();
        //            template.ConvertFromEntity(ctx.TimeTableTemplates.Where(x => x.IsDeleted && x.TemplateID == TemplateID).FirstOrDefault());

        //            //var query = from ts in ctx.TimeTableTemplates
        //            //            join td in ctx.TimeTableTemplateDetails on ts.TemplateID equals td.TemplateID
        //            //            select new
        //            //            {
        //            //                ts.TemplateID,
        //            //                ts.Title,
        //            //                td.TemplateDetailID,
        //            //                td.PeriodID,
        //            //                td.StartTime,
        //            //                td.EndTime,
        //            //                td.Day
        //            //            };

        //            return template;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public override void Add(TimeTableTemplate entity)
        //{
        //    try
        //    {
        //        using (var ctx = new LMSContext())
        //        {
        //            entity.CreatedBy = LoggedInUsername;
        //            entity.CreatedOn = DateTime.Now;
        //            entity.IsDeleted = true;
        //            base.Add(entity);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public override void Update(TimeTableTemplate entity)
        //{
        //    try
        //    {
        //        using (var ctx = new LMSContext())
        //        {
        //            entity.LastModifiedBy = LoggedInUsername;
        //            entity.LastModifiedOn = DateTime.Now;
        //            base.Add(entity);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public bool IsTitleExists(string title)
        {
            var ctx = this.RequestInfo.Context;
            return this.DefaultSingleQuery.Where(x => x.Title == title).Any();
        }
    }
}
