﻿using LMS.Core.Entity;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;

namespace LMS.Repository
{
    public class ResourceRepository : BaseRepository<Resource>, IResourceRepository
    {
        public ResourceRepository(IRequestInfo requestInfo) : base(requestInfo)
        {
        }
    }
}
