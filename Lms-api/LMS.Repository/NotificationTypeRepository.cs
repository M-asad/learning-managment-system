﻿using LMS.Common.Helper;
using LMS.Core.DTO;
using LMS.Core.Entity;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Repository
{
    public class NotificationTypeRepository : AuditableRepository<NotificationType>, INotificationTypeRepository
    {
        private IExceptionHelper exceptionHelper;
        public NotificationTypeRepository(IRequestInfo requestInfo, IExceptionHelper exceptionHelper)
            : base(requestInfo)
        {
            this.exceptionHelper = exceptionHelper;
        }

        public NotificationType GetNotificationType(string type)
        {
            var ctx = this.RequestInfo.Context;
            return ctx.NotificationTypes.Where(x => x.Type == type).FirstOrDefault();
        }
    }
}
