﻿using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using LMS.Core.DBContext;
using LMS.Core.DTO;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;
using System.Threading.Tasks;
using System.Data.Entity;
using LMS.Common.Helper;
using System.Linq.Expressions;

namespace LMS.Repository
{
    public class ParentRepository : AuditableRepository<Parent>, IParentRepository
    {
        private IExceptionHelper exceptionHelper;
        public ParentRepository(IRequestInfo requestInfo, IExceptionHelper exceptionHelper)
            : base(requestInfo)
        {
            this.exceptionHelper = exceptionHelper;
        }

        public override async Task<IResultSet<Parent>> GetAll(JsonApiRequest request)
        {
            var prefixFilter = request.Filter[Filters.Contains].Where(x => x.Key == "prefix").FirstOrDefault();
            return await base.GetList(request, x => x.FullName.Contains(prefixFilter.Value) || x.Phone.Contains(prefixFilter.Value) || x.Email.Contains(prefixFilter.Value));
        }

        public bool HaveDependencies(int id)
        {
            try
            {
                var ctx = this.RequestInfo.Context;

                // Check if this parent have active students 
                return (from ac in ctx.AcademicYears
                        join cr in ctx.Classrooms on ac.Id equals cr.AcademicYearId
                        join cs in ctx.ClassroomStudents on cr.Id equals cs.ClassroomId
                        join st in ctx.Students on cs.StudentId equals st.Id
                        where !ac.IsDeleted && ac.IsCurrent && !cr.IsDeleted && !cs.IsDeleted && !st.IsDeleted
                                && st.ParentId == id
                        select cs).Count() > 0 ? true : false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Parent> FindByName(string name)
        {
            return await this.DefaultSingleQuery.Where(x => x.FullName == name).FirstOrDefaultAsync();
        }

        public async Task<Parent> FindByName(string name, int id)
        {
            return await this.DefaultSingleQuery.Where(x => x.Id != id && x.FullName == name).FirstOrDefaultAsync();
        }

        public bool Exists(int Id)
        {
            return this.DefaultSingleQuery.Where(x => x.Id == Id).Any();
        }

        public bool HaveParentChildRelation(int parentUserId, int studentID)
        {
            var ctx = this.RequestInfo.Context;
            var isParent = (from p in ctx.Parents.Where(x => x.IsDeleted)
                            join st in ctx.Students.Where(x => x.IsDeleted) on p.Id equals st.Id
                            where p.UserId == parentUserId && st.Id == studentID
                            select st).Any();

            return isParent;
        }
    }
}
