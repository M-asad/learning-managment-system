﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMS.Core.Entity;
using LMS.Core.DTO;
using System.Data.SqlClient;
using LMS.Core.DBContext;
using LMS.Core.Infrastructure;
using System.Data.Entity;
using LMS.Core.IRepository;
using System.Threading.Tasks;
using LMS.Core.Enums;
using LMS.Core.Constant;
using LMS.Core.DTO.CustomDTOs;

namespace LMS.Repository
{
    public class StudentRepository : AuditableRepository<Student>, IStudentRepository
    {
        private IExceptionHelper exceptionHelper;
        public StudentRepository(IRequestInfo requestInfo, IExceptionHelper exceptionHelper)
            : base(requestInfo)
        {
            this.exceptionHelper = exceptionHelper;
        }

        protected override IQueryable<Student> DefaultListQuery
        {
            get
            {
                return base.DefaultListQuery.Where(x => !x.IsDeleted).Include(x => x.ClassroomStudents);
            }
        }
        protected override IQueryable<Student> DefaultSingleQuery
        {
            get
            {
                return base.DefaultSingleQuery.Where(x => !x.IsDeleted).Include(x => x.ClassroomStudents).Include(x => x.Parent);
            }
        }

        public bool IsGRNoExists(string GRno, int Id = 0)
        {
            return this.DefaultSingleQuery.Where(x => x.GRNo == GRno && (Id > 0 ? x.Id != Id : true)).Any();
        }
        
        public List<StudentListDTO> GetStudents(string prefix, int count, int page, out int pageCount)
        {
            var ctx = this.RequestInfo.Context;
            int prefixInt = 0;
            int.TryParse(prefix, out prefixInt);
            var query = (from st in ctx.Students
                         join csInner in ctx.ClassroomStudents.Where(x => !x.IsDeleted) on st.Id equals csInner.StudentId into inner
                         from cs in inner.DefaultIfEmpty()

                         join crInner in ctx.Classrooms.Where(x => !x.IsDeleted && x.AcademicYearId == this.AcademicYearId) on cs.ClassroomId equals crInner.Id into crJoin
                         from cr in crJoin.DefaultIfEmpty()

                         where !st.IsDeleted
                         && (!string.IsNullOrEmpty(prefix) ? (
                         (st.FullName).Contains(prefix)
                             || cr.ClassroomName.Contains(prefix)
                             || (prefixInt > 0 ? cs.RollNo == prefixInt : 1 == 2)
                             || st.GRNo.Contains(prefix)
                         ) : true)
                         select new StudentListDTO()
                         {
                             Id = st.Id,
                             GRNo = st.GRNo,
                             FullName = st.FullName,
                             RollNo = cs.RollNo,
                             Picture = st.Picture,
                             ClassroomName = cr.ClassroomName
                         }).OrderBy(x => x.GRNo);

            pageCount = (query.Count() / count) + 1;
            return query.Skip((page - 1) * count).Take(count).ToList();
        }
        
        //public StudentDTO GetStudent(int id)
        //{
        //    using (var ctx = new LMSContext())
        //    {
        //        var studentIDParameter = new SqlParameter("@StudentId", id);

        //        var result = ctx.Database
        //            .SqlQuery<StudentDTO>("Exec usp_GetStudent @StudentId", studentIDParameter)
        //            .FirstOrDefault();
        //        return result;
        //    }
        //}

        //public List<StudentListDTO> GetStudentsByParent(int id)
        //{
        //    using (var ctx = new LMSContext())
        //    {
        //        var IDParameter = new SqlParameter("@ID", id);

        //        var result = ctx.Database
        //            .SqlQuery<StudentListDTO>("Exec usp_GetStudentsByParent @ID", IDParameter)
        //            .ToList();
        //        return result;
        //    }
        //}

        //public List<StudentPrefixKeyValue> GetStudentsDDL(string prefix, int count)
        //{
        //    using (var ctx = new LMSContext())
        //    {
        //        var students = (from st in ctx.Students
        //                        join p in ctx.Parents on st.Id equals p.Id
        //                        where (!string.IsNullOrEmpty(prefix) ? ((st.FullName).Contains(prefix) ||
        //                                 (st.GRNo).Contains(prefix)) : true) &&
        //                                 st.IsDeleted
        //                        select new StudentPrefixKeyValue
        //                        {
        //                            StudentId = st.Id,
        //                            GRNo = st.GRNo,
        //                            Name = st.GRNo + " - " + st.FullName,
        //                            ParentName = p.FullName
        //                        }).Take(count).ToList();
        //        return students;
        //    }
        //}

        //public List<StudentPrefixKeyValue> GetUnenrolledStudentsByPrefix(string prefix, int count)
        //{
        //    using (var ctx = new LMSContext())
        //    {
        //        var enrolledStudent = (from cr in ctx.Classrooms.Where(x => !x.IsDeleted)
        //                               join cs in ctx.ClassroomStudents.Where(x => !x.IsDeleted) on cr.Id equals cs.ClassroomId
        //                               join ac in ctx.AcademicYears.Where(x => !x.IsDeleted) on cr.Id equals ac.Id
        //                               where ac.IsCurrent
        //                               select cs.StudentId).ToList();

        //        var students = (from st in ctx.Students
        //                        join p in ctx.Parents on st.Id equals p.Id
        //                        where !enrolledStudent.Contains(st.Id) &&
        //                                 (!string.IsNullOrEmpty(prefix) ? ((st.FullName).Contains(prefix) ||
        //                                 (st.GRNo).Contains(prefix)) : true) &&
        //                                 st.IsDeleted
        //                        select new StudentPrefixKeyValue
        //                        {
        //                            StudentId = st.Id,
        //                            GRNo = st.GRNo,
        //                            Name = st.GRNo + " - " + st.FullName,
        //                            ParentName = p.FullName
        //                        }).Take(count).ToList();
        //        return students;
        //    }
        //}

        public List<StudentListDTO> GetStudentsByParentId(int parentId)
        {
            var ctx = this.RequestInfo.Context;
            var query = (from st in ctx.Students
                         join csInner in ctx.ClassroomStudents.Where(x => !x.IsDeleted) on st.Id equals csInner.StudentId into inner
                         from cs in inner.DefaultIfEmpty()

                         join crInner in ctx.Classrooms.Where(x => !x.IsDeleted && x.AcademicYearId == this.AcademicYearId) on cs.ClassroomId equals crInner.Id into crJoin
                         from cr in crJoin.DefaultIfEmpty()

                         where !st.IsDeleted
                                && st.LeavingDate == null
                                && st.ParentId == parentId
                         select new StudentListDTO()
                         {
                             Id = st.Id,
                             GRNo = st.GRNo,
                             FullName = st.FullName,
                             RollNo = cs.RollNo,
                             Picture = st.Picture,
                             ClassroomName = cr.ClassroomName
                         }).OrderBy(x => x.GRNo);

            return query.ToList();
        }

        public List<StudentSimpleDTO> GetEnrolledStudents(string prefix, int count)
        {
            int prefixInt = 0;
            int.TryParse(prefix, out prefixInt);
            var ctx = this.RequestInfo.Context;
            var list = (from cs in ctx.ClassroomStudents
                        join st in ctx.Students on cs.StudentId equals st.Id
                        join p in ctx.Parents on st.ParentId equals p.Id
                        join cr in ctx.Classrooms on cs.ClassroomId equals cr.Id 
                        where !cs.IsDeleted && !st.IsDeleted
                            && (!string.IsNullOrEmpty(prefix) ? (
                                (st.FullName).Contains(prefix)
                                || st.GRNo.Contains(prefix)
                         ) : true)
                        select new StudentSimpleDTO()
                        {
                            Id = cs.StudentId,
                            FullName = st.FullName,
                            GRNo = st.GRNo,
                            ParentId = p.Id,
                            Parent = p.FullName
                        }).Distinct().Take(count).ToList();

            return list;
        }
    }
}
