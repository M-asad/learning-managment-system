﻿using LMS.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using LMS.Core.DBContext;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;
using System.Data.Entity;
using LMS.Core.DTO;

namespace LMS.Repository
{
    public class ClassroomRepository : AuditableRepository<Classroom>, IClassroomRepository
    {
        public ClassroomRepository(IRequestInfo requestInfo)
            : base(requestInfo)
        {
        }

        protected override IQueryable<Classroom> DefaultSingleQuery
        {
            get
            {
                return base.DefaultSingleQuery.Include(x => x.ClassroomCourses).Include(x => x.ClassroomStudents);
            }
        }

        public bool IsClassroomExists(int academicYearId, int classID, int sectionID)
        {
            return this.DefaultSingleQuery.Where(x => x.AcademicYearId == academicYearId && x.ClassId == classID && x.SectionId == sectionID).Any();
        }

        public List<ClassKeyValue> GetCurrentClassesddl()
        {
            var ctx = this.RequestInfo.Context;
            var list = (from cr in ctx.Classrooms
                        join c in ctx.Classes on cr.ClassId equals c.Id
                        join s in ctx.Sections on cr.SectionId equals s.Id
                        join ac in ctx.AcademicYears on cr.AcademicYearId equals ac.Id
                        where !cr.IsDeleted && !c.IsDeleted && !s.IsDeleted && !ac.IsDeleted && ac.IsCurrent
                        select new ClassKeyValue
                        {
                            ClassroomId = cr.Id,
                            ClassroomName = c.Name + " - " + s.Title
                        }).ToList();
            return list;
        }
                
        public List<ClassroomListDTO> GetClasses(int academicYearId)
        {
            var ctx = this.RequestInfo.Context;
            var list = (from cr in ctx.Classrooms
                        join c in ctx.Classes on cr.ClassId equals c.Id
                        join s in ctx.Sections on cr.SectionId equals s.Id
                        join inner in ctx.ClassroomCourses.Where(x => !x.IsDeleted) on cr.Id equals inner.ClassroomId
                        into outer
                        from cc in outer.DefaultIfEmpty()
                        join a in ctx.ClassroomStudents.Where(x => !x.IsDeleted) on cr.Id equals a.ClassroomId
                        into joinTable
                        from cs in joinTable.DefaultIfEmpty()
                        where !cr.IsDeleted && !c.IsDeleted && !s.IsDeleted && cr.AcademicYearId == academicYearId
                        select new
                        {
                            ClassId = c.Id,
                            ClassName = c.Name,
                            SectionId = s.Id,
                            Section = s.Title,
                            ClassroomId = cr.Id,
                            cr.AcademicYearId,
                            CourseId = (cc == null ? 0 : cc.CourseId),
                            TeacherId = (cc == null ? 0 : cc.TeacherId),
                            StudentId = (cs == null ? 0 : cs.StudentId)
                        }).ToList();

            List<ClassroomListDTO> returnList = new List<ClassroomListDTO>();

            var ClassIds = list.Select(x => x.ClassId).Distinct().ToList();
            foreach (var classId in ClassIds)
            {
                var first = list.Where(x => x.ClassId == classId).FirstOrDefault();

                ClassroomListDTO clDTO = new ClassroomListDTO()
                {
                    ClassId = classId,
                    Class = first.ClassName,
                    StudentEnrolled = list.Where(x => x.ClassId == classId).Select(s => s.StudentId).Distinct().Count(),
                    CoursesAssigned = list.Where(x => x.ClassId == classId).GroupBy(c => c.CourseId).Select(c => c.Key).Distinct().Count(),
                    TeachersAssigned = list.Where(x => x.ClassId == classId).GroupBy(c => c.TeacherId).Select(c => c.Key).Distinct().Count(),
                    Sections = list.Where(x => x.ClassId == classId).GroupBy(s => s.SectionId).Select(c => c.Key).Distinct().Count(),
                };
                clDTO.ClassList = new List<ClassroomSectionDTO>();
                foreach (var row in list.Where(x => x.ClassId == classId).Select(s => new { s.ClassroomId, s.Section }).Distinct().GroupBy(x => new { x.ClassroomId, x.Section }).ToList())
                {
                    clDTO.ClassList.Add(new ClassroomSectionDTO()
                    {
                        ClassroomId = row.Key.ClassroomId,
                        Section = row.Key.Section
                    });
                }
                returnList.Add(clDTO);
            }
            return returnList;
        }

        public ClassroomDTO GetClassroomDetail(int ClassroomId)
        {
            var ctx = this.RequestInfo.Context;
            var detail = (from cr in ctx.Classrooms
                          join c in ctx.Classes on cr.ClassId equals c.Id
                          join s in ctx.Sections on cr.SectionId equals s.Id
                          join ac in ctx.AcademicYears on cr.AcademicYearId equals ac.Id
                          join ct in ctx.Teachers on cr.ClassTeacherId equals ct.Id
                          where !cr.IsDeleted && !c.IsDeleted && !s.IsDeleted && !ac.IsDeleted && !ct.IsDeleted
                              && cr.Id == ClassroomId
                          select new ClassroomDTO
                          {
                              Id = cr.Id,
                              AcademicYear = ac.Title,
                              ClassroomName = c.Name + " - " + s.Title,
                              ClassTeacherId = ct.Id,
                              ClassTeacher = ct.FullName,
                              ClassId = cr.ClassId,
                              SectionId = cr.SectionId
                          }).FirstOrDefault();

            return detail;
        }

        public ClassroomMasterDTO GetClassroomMaster(int classroomId)
        {
            var ctx = this.RequestInfo.Context;
            var detail = (from cr in ctx.Classrooms
                          join c in ctx.Classes on cr.ClassId equals c.Id
                          join s in ctx.Sections on cr.SectionId equals s.Id
                          join ac in ctx.AcademicYears on cr.AcademicYearId equals ac.Id
                          join ct in ctx.Teachers on cr.ClassTeacherId equals ct.Id
                          where !cr.IsDeleted && !c.IsDeleted && !s.IsDeleted && !ac.IsDeleted && !ct.IsDeleted
                              && cr.Id == classroomId
                          select new ClassroomMasterDTO
                          {
                              ClassroomId = cr.Id,
                              AcademicYear = ac.Title,
                              ClassroomName = c.Name + " - " + s.Title,
                              ClassTeacherId = ct.Id,
                              ClassTeacher = ct.FullName
                          }).FirstOrDefault();

            return detail;
        }

        public bool IsUserClassTeacher(int teacherUserId, int classroomID)
        {
            var ctx = this.RequestInfo.Context;
            bool result = (from t in ctx.Teachers
                           join cr in ctx.Classrooms on t.Id equals cr.ClassTeacherId
                           where !t.IsDeleted && !cr.IsDeleted && cr.Id == classroomID && t.UserId == teacherUserId
                           select 1).Any();
            return result;
        }

        public List<string> GetClassroomCourses(int classroomID)
        {
            var ctx = this.RequestInfo.Context;
            var courses = (from cc in ctx.ClassroomCourses
                           join c in ctx.Courses on cc.CourseId equals c.Id
                           where !cc.IsDeleted && !c.IsDeleted
                               && cc.ClassroomId == classroomID
                           select c.Title).ToList();

            return courses;
        }

        public int[] GetCurrentYearClassroomIds()
        {
            var ctx = this.RequestInfo.Context;
            var ids = (from cr in ctx.Classrooms
                       join ac in ctx.AcademicYears on cr.AcademicYearId equals ac.Id
                       where ac.IsCurrent && !cr.IsDeleted
                       select cr.Id).ToArray();
            return ids;
        }

        public Section GetSection(int classroomId)
        {
            var ctx = this.RequestInfo.Context;
            var sectionId = ctx.Classrooms.Where(x => x.Id == classroomId && !x.IsDeleted).Select(x => x.SectionId).FirstOrDefault();
            sectionId++;
            return ctx.Sections.Where(x => x.Id == sectionId && !x.IsDeleted).FirstOrDefault();
        }
    }
}
