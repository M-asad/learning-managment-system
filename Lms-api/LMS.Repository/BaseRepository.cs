﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Data;
using LMS.Core.IRepository;
using System.Threading.Tasks;
using LMS.Core.Entity;
using LMS.Core.DBContext;
using LMS.Core.Infrastructure;
using LMS.Core.DTO;
using LMS.Common.Helper;
using System.Linq.Expressions;
using System;

namespace LMS.Repository
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity>
        where TEntity : class, IBase
    {
        public BaseRepository(IRequestInfo requestInfo)
        {
            this.RequestInfo = requestInfo;
        }

        public int AcademicYearId { get; set; }
        protected IRequestInfo RequestInfo { get; private set; }

        protected LMSContext DBContext
        {
            get { return this.RequestInfo.Context; }
        }

        protected IDbSet<TEntity> DbSet
        {
            get
            {
                return this.DBContext.Set<TEntity>();
            }
        }

        protected virtual IQueryable<TEntity> DefaultListQuery
        {
            get
            {
                return this.DefaultQuery.OrderBy(x => x.Id);
            }
        }

        protected virtual IQueryable<TEntity> DefaultSingleQuery
        {
            get
            {
                return this.DefaultQuery;
            }
        }

        private IQueryable<TEntity> DefaultQuery
        {
            get
            {
                return this.DbSet.AsQueryable();
            }
        }

        private Expression<Func<TEntity, bool>> Predicate = x => true;

        public async Task<TEntity> GetDefaultAsync(int id)
        {
            return await this.DefaultSingleQuery.SingleOrDefaultAsync(x => x.Id.Equals(id));
        }

        public async Task<IEnumerable<TEntity>> GetDefaultAsync(IList<int> ids)
        {
            return await this.DefaultListQuery.Where(x => ids.Contains(x.Id)).ToListAsync();
        }

        public virtual async Task<TEntity> GetAsync(int id)
        {
            return await this.DefaultSingleQuery.SingleOrDefaultAsync(x => x.Id.Equals(id));
        }

        public virtual async Task<IEnumerable<TEntity>> GetAsync(IList<int> ids)
        {
            return await this.DefaultListQuery.Where(x => ids.Contains(x.Id)).ToListAsync();
        }

        public virtual async Task<int> GetCount()
        {
            return await this.DefaultListQuery.CountAsync();
        }

        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> predicate)
        {
            return this.DefaultListQuery.Where(predicate).ToList();
        }

        public async Task<IEnumerable<TEntity>> GetAllDefault()
        {
            return await this.DefaultListQuery.ToListAsync();
        }

        public async Task<IResultSet<TEntity>> GetAllDefault(JsonApiRequest request)
        {
            IResultSet<TEntity> result = new ResultSet<TEntity>();
            int pageCount = 0;
            result.Data = await this.GetAllQueryable(request, Predicate, out pageCount).ToListAsync();
            result.PageCount = pageCount;
            return result;
        }

        public virtual async Task<IEnumerable<TEntity>> GetAll()
        {
            return await this.DefaultListQuery.ToListAsync();
        }

        public virtual async Task<IResultSet<TEntity>> GetAll(JsonApiRequest request)
        {
            return await this.GetList(request, Predicate);
        }

        public async Task<IResultSet<TEntity>> GetList(JsonApiRequest request, Expression<Func<TEntity, bool>> expression)
        {
            IResultSet<TEntity> result = new ResultSet<TEntity>();
            int pageCount = 0;
            result.Data = await this.GetAllQueryable(request, expression, out pageCount).ToListAsync();
            result.PageCount = pageCount;
            return result;
        }

        public virtual async Task<IEnumerable<TEntity>> GetAll(IList<int> keys)
        {
            return await this.DefaultListQuery.Where(x => keys.Contains(x.Id)).ToListAsync();
        }

        public virtual async Task<IResultSet<TEntity>> GetAll(IList<int> keys, JsonApiRequest request)
        {
            IResultSet<TEntity> result = new ResultSet<TEntity>();
            int pageCount = 0;
            result.Data = await this.GetAllQueryable(request, Predicate, out pageCount).Where(x => keys.Contains(x.Id)).ToListAsync();
            result.PageCount = pageCount;
            return result;
        }

        public virtual async Task<TEntity> Create(TEntity entity)
        {
            this.DBContext.Entry(entity).State = EntityState.Added;
            return entity;
        }

        public virtual async Task<TEntity> Update(TEntity entity)
        {
            var localEntity = this.GetExisting(entity);
            if (localEntity == null)
            {
                this.DBContext.Entry(entity).State = EntityState.Modified;
            }
            else
            {
                this.DBContext.Entry(localEntity).State = EntityState.Modified;
            }

            return entity;
        }

        public virtual async Task DeleteAsync(int id)
        {
            var entity = await this.GetAsync(id);
            if (entity == null)
                throw new System.Exception("Record not found.");
            this.DBContext.Entry(entity).State = EntityState.Deleted;
        }

        public virtual async Task<TEntity> GetEntityOnly(int id)
        {
            return await this.DBContext.Set<TEntity>().AsQueryable().FirstOrDefaultAsync(x => x.Id.Equals(id));
        }

        public TEntity GetExisting(TEntity entity)
        {
            return this.DBContext.Set<TEntity>().Local.FirstOrDefault(x => x.Id.Equals(entity.Id));
        }

        protected virtual IQueryable<TEntity> GetAllQueryable(JsonApiRequest request, Expression<Func<TEntity, bool>> expression,  out int count)
        {
            var prefixFilter = request.Filter[Filters.Contains].Where(x => x.Key == "prefix").FirstOrDefault();
            if(prefixFilter.Value != null)
                return this.DefaultListQuery.GenerateQuery(request, out count).Where(expression);
            else
                return this.DefaultListQuery.GenerateQuery(request, out count);
        }

        protected void DeleteRange<TEntityList>(TEntityList entityList) where TEntityList : IQueryable
        {
            foreach (var each in entityList)
            {
                this.DBContext.Entry(each).State = EntityState.Deleted;
            }
        }

        public bool Exists(int Id)
        {
            return this.DefaultSingleQuery.Where(x => x.Id == Id).Any();
        }
    }
}
