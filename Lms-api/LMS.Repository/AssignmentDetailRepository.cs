﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMS.Core.Entity;
using LMS.Core.DTO;
using LMS.Core.Infrastructure;
using LMS.Core.Enums;
using System.Threading.Tasks;
using LMS.Core.IRepository;
using System.Data.Entity;

namespace LMS.Repository
{
    public class AssignmentDetailRepository : AuditableRepository<AssignmentDetail>, IAssignmentDetailRepository
    {
        public AssignmentDetailRepository(IRequestInfo requestInfo)
            : base(requestInfo)
        {
        }
    }
}
