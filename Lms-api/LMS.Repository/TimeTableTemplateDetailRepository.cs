﻿using LMS.Core.Entity;
using LMS.Core.DBContext;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;

namespace LMS.Repository
{
    public class TimeTableTemplateDetailRepository : AuditableRepository<TimeTableTemplateDetail>, ITimeTableTemplateDetailRepository
    {
        public TimeTableTemplateDetailRepository(IRequestInfo requestInfo)
            : base(requestInfo)
        {
        }
    }
}
