﻿using LMS.Core.Entity;
using LMS.Core.DTO;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using LMS.Core.DBContext;
using LMS.Core.Infrastructure;
using LMS.Core.IRepository;

namespace LMS.Repository
{
    public class ResultRepository : IResultRepository
    {
        IRequestInfo requestInfo;
        public ResultRepository(IRequestInfo requestInfo)
        {
            this.requestInfo = requestInfo;
        }

        public ResultSaveDTO GetResultDetail(int examId)
        {
            var ctx = this.requestInfo.Context;
            var list = (from e in ctx.Exams
                        join ed in ctx.ExamDetailSetups on e.Id equals ed.ExamId
                        join edm in ctx.ExamClassroomMappings on ed.Id equals edm.ExamDetailSetupId
                        join c in ctx.Courses on edm.CourseId equals c.Id
                        join cr in ctx.Classrooms on edm.ClassroomId equals cr.Id
                        join cs in ctx.ClassroomStudents on cr.Id equals cs.ClassroomId
                        join st in ctx.Students on cs.StudentId equals st.Id
                        join inner in ctx.ResultDetails on edm.Id equals inner.ExamClassroomMappingId
                        into outer
                        from rd in outer.DefaultIfEmpty()
                        where !e.IsDeleted && e.Id == examId
                        orderby e.StartDate descending
                        select new
                        {
                            ExamId = e.Id,
                            CourseId = edm.CourseId,
                            ClassroomId = edm.ClassroomId,
                            StudentId = st.Id,
                            StudentName = st.FullName,
                            GRNo = st.GRNo,
                            TheoryMarks = rd.TheoryObtained ?? 0,
                            LabMarks = rd.LabObtained ?? 0,
                            OtherMarks = rd.OtherObtained ?? 0

                        }).ToList();

            if (list != null && list.Count > 0)
            {
                var first = list.FirstOrDefault();
                ResultSaveDTO data = new ResultSaveDTO()
                {
                    ExamId = first.ExamId,
                    ClassroomId = first.ClassroomId,
                    CourseId = first.CourseId
                };
                List<StudentResultDTO> results = new List<StudentResultDTO>();
                foreach (var row in list)
                {
                    results.Add(new StudentResultDTO()
                    {
                        StudentId = row.StudentId,
                        StudentName = row.StudentName,
                        GRNo = row.GRNo,
                        TheoryMarks = row.TheoryMarks,
                        LabMarks = row.LabMarks,
                        OtherMarks = row.OtherMarks
                    });
                }
                data.StudentResults = results;
                return data;
            }
            return null;
        }

        public List<StudentResultCommulativeDTO> GetStudentCommulativeResultList(int examId, int classroomID)
        {
            var ctx = this.requestInfo.Context;
            var list = (from e in ctx.Exams
                        join ed in ctx.ExamDetailSetups on e.Id equals ed.ExamId
                        join edm in ctx.ExamClassroomMappings on ed.Id equals edm.ExamDetailSetupId
                        join cs in ctx.ClassroomStudents on edm.ClassroomId equals cs.ClassroomId
                        // Join with classroom and student to get all student 
                        join st in ctx.Students on cs.StudentId equals st.Id
                        // Then join exam result and detail to get existing results 
                        join innerER in ctx.Results on new { ExamId = e.Id, StudentId = st.Id } equals new { innerER.ExamId, innerER.StudentId }
                        into er2
                        from er in er2.DefaultIfEmpty()
                        join inner in ctx.ResultDetails on new { rid = er.Id, mid = edm.Id } equals new { rid = inner.Id, mid = inner.ExamClassroomMappingId }
                        into lj
                        from rd in lj.DefaultIfEmpty()

                        where !e.IsDeleted && e.Id == examId && edm.ClassroomId == classroomID
                        orderby e.StartDate descending
                        select new
                        {
                            StudentId = st.Id,
                            StudentName = st.FullName,
                            GRNo = st.GRNo,
                            Total = er.TotalMarks,
                            Obtained = er.ObtainedTotal,
                            Percentage = ((er.ObtainedTotal ?? (decimal)0) / (er.TotalMarks ?? (decimal)1)) * 100,
                            Remarks = er.Remarks
                        }).Distinct().ToList();

            // Calculate grades
            var grades = ctx.Grades.Where(x => !x.IsDeleted).ToList();
            var results = new List<StudentResultCommulativeDTO>();
            foreach (var record in list)
            {
                var Grade = ctx.Grades.Where(x => !x.IsDeleted && record.Percentage >= x.StartPercentage && record.Percentage < x.EndPercentage).FirstOrDefault();
                results.Add(new StudentResultCommulativeDTO()
                {
                    StudentId = record.StudentId,
                    StudentName = record.StudentName,
                    GRNo = record.GRNo,
                    Total = Convert.ToString(record.Total),
                    Obtained = Convert.ToString(record.Obtained),
                    Percentage = Convert.ToString(Math.Round(record.Percentage, 2)),
                    Grade = Grade.Title,
                    Remarks = record.Remarks
                });
            }
            return results;
        }

        public StudentResultListDTO GetStudentResultList(int examId, int classroomID, int courseID)
        {
            var ctx = this.requestInfo.Context;
            var list = (from e in ctx.Exams.Where(x => !x.IsDeleted)
                        join ed in ctx.ExamDetailSetups.Where(x => !x.IsDeleted) on e.Id equals ed.ExamId
                        join edm in ctx.ExamClassroomMappings.Where(x => !x.IsDeleted) on ed.Id equals edm.ExamDetailSetupId
                        join cs in ctx.ClassroomStudents.Where(x => !x.IsDeleted) on edm.ClassroomId equals cs.ClassroomId
                        // Join with classroom and student to get all student 
                        join st in ctx.Students.Where(x => !x.IsDeleted) on cs.StudentId equals st.Id
                        // Then join exam result and detail to get existing results 
                        join innerER in ctx.Results.Where(x => !x.IsDeleted) on new { ExamId = e.Id, StudentId = st.Id } equals new { innerER.ExamId, innerER.StudentId }
                        into er2
                        from er in er2.DefaultIfEmpty()
                        join inner in ctx.ResultDetails on new { rid = er.Id, mid = edm.Id } equals new { rid = inner.ResultID, mid = inner.ExamClassroomMappingId }
                        into lj
                        from rd in lj.DefaultIfEmpty()
                        where e.Id == examId && edm.ClassroomId == classroomID && (edm == null || edm.CourseId == courseID)
                        orderby e.StartDate descending
                        select new StudentResultDTO
                        {
                            StudentId = st.Id,
                            StudentName = st.FullName,
                            GRNo = st.GRNo,
                            TheoryMarks = rd.TheoryObtained,
                            LabMarks = rd.LabObtained,
                            OtherMarks = rd.OtherObtained
                        }).Distinct().ToList();

            var master = (from e in ctx.Exams
                          join ed in ctx.ExamDetailSetups on e.Id equals ed.ExamId
                          join edm in ctx.ExamClassroomMappings on ed.Id equals edm.ExamDetailSetupId
                          where !e.IsDeleted && e.Id == examId && edm.ClassroomId == classroomID && edm.CourseId == courseID
                          orderby e.StartDate descending
                          select new StudentResultListDTO
                          {
                              TheoryMarks = ed.TheoryTotal,
                              LabMarks = ed.LabTotal,
                              OtherMarks = ed.OtherTotal
                          }).FirstOrDefault();
            if (master != null)
                master.ResultList = list;
            return master;
        }

        public bool SaveResult(ResultSaveDTO data)
        {
            var ctx = this.requestInfo.Context;
            // Getting Previous exam results of all students, Exam Setup and Mapping table to get totals 
            // To Avoid n hits for students

            // Get previous results 
            var studentIDs = data.StudentResults.Select(x => x.StudentId).ToList();
            // Get results data of only those employees which is sent 
            var previousResults = (from r in ctx.Results
                                   join rd in ctx.ResultDetails on r.Id equals rd.ResultID
                                   where studentIDs.Contains(r.StudentId) &&
                                        r.ExamId == data.ExamId &&
                                        !r.IsDeleted &&
                                        !rd.IsDeleted
                                   select new
                                   {
                                       Commulative = r,
                                       Detail = rd
                                   }).ToList();

            // Get setup and mapping data to get total and course
            var setupDetailAll = (from ed in ctx.ExamDetailSetups
                                  join mp in ctx.ExamClassroomMappings on ed.Id equals mp.ExamDetailSetupId
                                  where ed.ExamId == data.ExamId &&
                                        mp.ClassroomId == data.ClassroomId &&
                                         !mp.IsDeleted && !ed.IsDeleted
                                  select new
                                  {
                                      Setup = ed,
                                      Mapping = mp
                                  }).ToList();

            var setupDetail = setupDetailAll.Where(x => x.Mapping.CourseId == data.CourseId).FirstOrDefault();

            // Get exam result to update entries
            var Results = (from er in ctx.Results
                           where !er.IsDeleted &&
                                er.ExamId == data.ExamId
                           select er).ToList();

            foreach (var row in data.StudentResults)
            {
                if (row.TheoryMarks != null || row.LabMarks != null || row.OtherMarks != null)
                {
                    // If result exists, Add details and save commulative in Results
                    var studentAllResult = previousResults.Where(x => x.Commulative.StudentId == row.StudentId).ToList();
                    if (studentAllResult != null && studentAllResult.Count > 0)
                    {
                        decimal prevSumObtained = studentAllResult.Sum(x => x.Detail.TheoryObtained + x.Detail.LabObtained + x.Detail.OtherObtained) ?? 0;
                        //var commulativeObtTheory = studentAllResult.Sum(x => x.Detail.TheoryObtained);
                        //var commulativeObtLab = studentAllResult.Sum(x => x.Detail.LabObtained);
                        //var commulativeObtOther = studentAllResult.Sum(x => x.Detail.OtherObtained);
                        var originalResult = Results.Where(x => x.StudentId == row.StudentId).FirstOrDefault();

                        decimal prevTotal = setupDetailAll.Where(x => x.Mapping.ClassroomId == data.ClassroomId)
                            .Sum(x => x.Setup.TheoryTotal + x.Setup.LabTotal + x.Setup.OtherTotal ?? 0);
                        decimal prevCourseTotal = setupDetail.Setup.TheoryTotal + setupDetail.Setup.LabTotal + setupDetail.Setup.OtherTotal ?? 0;
                        decimal courseObtained = (row.TheoryMarks ?? 0) + (row.LabMarks ?? 0) + (row.OtherMarks ?? 0);

                        // Check if the result exists for given course or not 
                        var studentResult = studentAllResult.Where(x => x.Detail.ExamClassroomMappingId == setupDetail.Mapping.Id).FirstOrDefault();
                        if (studentResult != null)
                        {
                            // Update Results with Commulative data  
                            decimal prevCourseObtained = studentResult.Detail.TheoryObtained + studentResult.Detail.LabObtained + studentResult.Detail.OtherObtained ?? 0;
                            decimal sumObtained = prevSumObtained - prevCourseObtained + courseObtained; // Subtracting this course marks from total and addding changed obtained
                            decimal percentage = (sumObtained / prevTotal) * 100;
                            int GradeId = ctx.Grades.Where(x => !x.IsDeleted && percentage >= x.StartPercentage && percentage < x.EndPercentage).FirstOrDefault().Id;

                            originalResult.LastModifiedBy = this.requestInfo.UserId.ToString();
                            originalResult.LastModifiedOn = DateTime.Now;
                            originalResult.GradeId = GradeId;
                            originalResult.TotalMarks = prevTotal;
                            originalResult.ObtainedTotal = sumObtained;

                            var record = ctx.ResultDetails.Where(x => x.ExamClassroomMappingId == setupDetail.Mapping.Id).FirstOrDefault();
                            record.TheoryObtained = row.TheoryMarks;
                            record.LabObtained = row.LabMarks;
                            record.OtherObtained = row.OtherMarks;
                            record.LastModifiedBy = this.requestInfo.UserId.ToString();
                            record.LastModifiedOn = DateTime.Now;

                            ctx.SaveChanges();
                        }
                        else
                        {
                            // Update Results with Commulative data 
                            var sumObtained = prevSumObtained - prevCourseTotal + courseObtained;
                            decimal percentage = (sumObtained / prevTotal) * 100;
                            int GradeId = ctx.Grades.Where(x => !x.IsDeleted && percentage >= x.StartPercentage && percentage < x.EndPercentage).FirstOrDefault().Id;

                            originalResult.LastModifiedBy = this.requestInfo.UserId.ToString();
                            originalResult.LastModifiedOn = DateTime.Now;
                            originalResult.GradeId = GradeId;
                            originalResult.TotalMarks = prevTotal;
                            originalResult.ObtainedTotal = sumObtained;

                            // Add entries in ResultDetail
                            ResultDetail detail = new ResultDetail()
                            {
                                ResultID = originalResult.Id,
                                ExamClassroomMappingId = setupDetail.Mapping.Id,
                                TheoryObtained = row.TheoryMarks,
                                LabObtained = row.LabMarks,
                                OtherObtained = row.OtherMarks,
                                CreatedBy = this.requestInfo.UserId.ToString(),
                                CreatedOn = DateTime.Now,
                                IsDeleted = false
                            };
                            ctx.ResultDetails.Add(detail);
                            ctx.SaveChanges();
                        }
                    }
                    else
                    {
                        // You are here because there is no data of student in given exam
                        // Addition of data in Result and ResultDetail
                        decimal totalMarks = (setupDetail.Setup.TheoryTotal ?? 0) + (setupDetail.Setup.LabTotal ?? 0) + (setupDetail.Setup.OtherTotal ?? 0);
                        decimal totalObtained = (row.TheoryMarks ?? 0) + (row.LabMarks ?? 0) + (row.OtherMarks ?? 0);
                        decimal percentage = (totalObtained / totalMarks) * 100;
                        int GradeId = ctx.Grades.Where(x => !x.IsDeleted && percentage >= x.StartPercentage && percentage < x.EndPercentage).FirstOrDefault().Id;

                        using (var trans = ctx.Database.BeginTransaction())
                        {
                            try
                            {
                                var entity = new Result()
                                {
                                    ExamId = data.ExamId,
                                    StudentId = row.StudentId,
                                    TotalMarks = totalMarks,
                                    ObtainedTotal = totalObtained,
                                    GradeId = GradeId,
                                    CreatedBy = this.requestInfo.UserId.ToString(),
                                    CreatedOn = DateTime.Now,
                                    LastModifiedBy = this.requestInfo.UserId.ToString(),
                                    LastModifiedOn = DateTime.Now,
                                    IsDeleted = false,
                                };
                                ctx.Results.Add(entity);
                                ctx.SaveChanges();
                                var ResultID = entity.Id;

                                ResultDetail detail = new ResultDetail()
                                {
                                    ResultID = ResultID,
                                    ExamClassroomMappingId = setupDetail.Mapping.Id,
                                    TheoryObtained = row.TheoryMarks,
                                    LabObtained = row.LabMarks,
                                    OtherObtained = row.OtherMarks,
                                    CreatedBy = this.requestInfo.UserId.ToString(),
                                    CreatedOn = DateTime.Now,
                                    LastModifiedBy = this.requestInfo.UserId.ToString(),
                                    LastModifiedOn = DateTime.Now,
                                    IsDeleted = false
                                };
                                ctx.ResultDetails.Add(detail);
                                ctx.SaveChanges();
                                trans.Commit();
                            }
                            catch (Exception ex)
                            {
                                trans.Rollback();
                                throw ex;
                            }
                        }
                    }
                }
            }
            return true;
        }

        //public ExamDetailDTO GetResultsByParentId(int parentID)
        //{
        //    try
        //    {
        //        using (var ctx = new LMSContext())
        //        {
        //            var exam = (from e in ctx.Exams
        //                        join ed in ctx.ExamDetails on e.Id equals ed.ExamId
        //                        join r in ctx.Results on e.Id equals r.ExamId
        //                        join rd in ctx.ResultDetails on r.ResultID equals rd.ResultID
        //                        join ac in ctx.AcademicYears on e.AcademicYearId equals ac.AcademicYearId
        //                        where e.IsDeleted && e.Id == examId
        //                        orderby e.StartDate descending
        //                        select new
        //                        {
        //                            examId = e.Id,
        //                            Description = e.Description,
        //                            AcademicYear = ac.Title,
        //                            StartDate = e.StartDate,
        //                            EndDate = e.EndDate,
        //                            ClassIds = e.ClassIds,
        //                            ed,
        //                            r,
        //                            rd
        //                        }).ToList();

        //            if (exam == null)
        //                return null;

        //            return exam;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public List<KeyValueDTO> GetClassroomForTeacher(int examId, int userId)
        {
            var ctx = this.requestInfo.Context;
            var teacher = ctx.Teachers.Where(x => x.UserId == userId).FirstOrDefault();

            var query = (from ac in ctx.AcademicYears.Where(x => !x.IsDeleted && x.IsCurrent)
                         join e in ctx.Exams.Where(x => !x.IsDeleted) on ac.Id equals e.AcademicYearId
                         join ed in ctx.ExamDetailSetups.Where(x => !x.IsDeleted) on e.Id equals ed.ExamId
                         join edm in ctx.ExamClassroomMappings.Where(x => !x.IsDeleted) on ed.Id equals edm.ExamDetailSetupId
                         join cr in ctx.Classrooms.Where(x => !x.IsDeleted) on edm.ClassroomId equals cr.Id
                         join c in ctx.Classes.Where(x => !x.IsDeleted) on cr.ClassId equals c.Id
                         join s in ctx.Sections.Where(x => !x.IsDeleted) on cr.SectionId equals s.Id
                         where e.Id == examId &&
                                edm.TeacherId == teacher.Id
                         select new KeyValueDTO
                         {
                             Id = cr.Id,
                             Text = c.Name + " " + s.Title,
                         }).Distinct();

            return query.ToList();
        }

        // for dropdown
        public List<KeyValueDTO> GetExamClassroomList(int examId = 1)
        {
            var ctx = this.requestInfo.Context;
            var query = (from ac in ctx.AcademicYears.Where(x => !x.IsDeleted && x.IsCurrent)
                         join e in ctx.Exams.Where(x => !x.IsDeleted) on ac.Id equals e.AcademicYearId
                         join ed in ctx.ExamDetailSetups.Where(x => !x.IsDeleted) on e.Id equals ed.ExamId
                         join edm in ctx.ExamClassroomMappings.Where(x => !x.IsDeleted) on ed.Id equals edm.ExamDetailSetupId
                         join cr in ctx.Classrooms.Where(x => !x.IsDeleted) on edm.ClassroomId equals cr.Id
                         join c in ctx.Classes.Where(x => !x.IsDeleted) on cr.ClassId equals c.Id
                         join s in ctx.Sections.Where(x => !x.IsDeleted) on cr.SectionId equals s.Id
                         where e.Id == examId
                         select new KeyValueDTO
                         {
                             Id = cr.Id,
                             Text = c.Name + " " + s.Title,
                         }).Distinct();

            return query.ToList();
        }

        public List<ExamListDTO> GetResultList(string prefix, int count, int page, out int pageCount)
        {
            var ctx = this.requestInfo.Context;
            pageCount = 0;
            var date = DateTime.Now.Date;
            var query = (from ac in ctx.AcademicYears.Where(x => !x.IsDeleted && x.IsCurrent)
                         join e in ctx.Exams.Where(x => !x.IsDeleted) on ac.Id equals e.AcademicYearId
                         join et in ctx.ExamTypes.Where(x => !x.IsDeleted) on e.ExamTypeID equals et.Id
                         join ed in ctx.ExamDetailSetups.Where(x => !x.IsDeleted) on e.Id equals ed.ExamId
                         join edm in ctx.ExamClassroomMappings.Where(x => !x.IsDeleted) on ed.Id equals edm.ExamDetailSetupId
                         join er in ctx.Results.Where(x => !x.IsDeleted) on e.Id equals er.ExamId
                         where ed.Date <= date && !string.IsNullOrEmpty(prefix) ? (e.Description.Contains(prefix)) : true
                         //orderby e.StartDate descending
                         select new
                         {
                             ExamId = e.Id,
                             et.Type,
                             e.Description,
                             ac.Title,
                             e.StartDate,
                             e.EndDate
                         }).Distinct();
            int totalRecords = query.Count();
            if (totalRecords > 0)
            {
                pageCount = (totalRecords / count) + 1;
                return query.OrderByDescending(x => x.StartDate).Skip((page - 1) * count).Take(count).ToList()
                    .Select(x => new ExamListDTO
                    {
                        Id = x.ExamId,
                        ExamType = x.Type,
                        Description = x.Description,
                        AcademicYear = x.Title,
                        StartDate = x.StartDate.ToString("MM/dd/yyyy"),
                        EndDate = x.EndDate.ToString("MM/dd/yyyy")
                    }).ToList();
            }
            return new List<ExamListDTO>();
        }

        public List<ExamListDTO> GetTeacherResultList(string prefix, int count, int page, int teacherUserId, out int pageCount)
        {
            var ctx = this.requestInfo.Context;
            pageCount = 0;
            var date = DateTime.Now.Date;
            var teacherId = (from u in ctx.Users.Where(x => !x.IsDeleted)
                             join t in ctx.Teachers.Where(x => !x.IsDeleted) on u.Id equals t.UserId
                             where u.Id == teacherUserId
                             select t.Id).FirstOrDefault();

            var query = (from ac in ctx.AcademicYears.Where(x => !x.IsDeleted && x.IsCurrent)
                         join e in ctx.Exams.Where(x => !x.IsDeleted) on ac.Id equals e.AcademicYearId
                         join et in ctx.ExamTypes.Where(x => !x.IsDeleted) on e.ExamTypeID equals et.Id
                         join ed in ctx.ExamDetailSetups.Where(x => !x.IsDeleted) on e.Id equals ed.ExamId
                         join edm in ctx.ExamClassroomMappings.Where(x => !x.IsDeleted) on ed.Id equals edm.ExamDetailSetupId
                         join er in ctx.Results.Where(x => !x.IsDeleted) on e.Id equals er.ExamId
                         join erd in ctx.ResultDetails.Where(x => !x.IsDeleted) on er.Id equals erd.ResultID
                         join cc in ctx.ClassroomCourses.Where(x => !x.IsDeleted) on edm.TeacherId equals cc.TeacherId
                         where ed.Date <= date && !string.IsNullOrEmpty(prefix) ? (e.Description.Contains(prefix)) : true &&
                         cc.TeacherId == teacherId
                         orderby e.StartDate descending
                         select new
                         {
                             ExamId = e.Id,
                             et.Type,
                             e.Description,
                             ac.Title,
                             e.StartDate,
                             e.EndDate
                         }).Distinct();
            int totalRecords = query.Count();
            if (totalRecords > 0)
            {
                pageCount = (totalRecords / count) + 1;
                return query.OrderByDescending(x => x.StartDate).Skip((page - 1) * count).Take(count).ToList()
                    .Select(x => new ExamListDTO
                    {
                        Id = x.ExamId,
                        ExamType = x.Type,
                        Description = x.Description,
                        AcademicYear = x.Title,
                        StartDate = x.StartDate.ToString("MM/dd/yyyy"),
                        EndDate = x.EndDate.ToString("MM/dd/yyyy")
                    }).ToList();
            }
            return new List<ExamListDTO>();
        }

        // Result of single student
        public StudentResultMasterDTO GetStudentResult(int examId, int studentID)
        {
            var ctx = this.requestInfo.Context;
            //var data = (from e in ctx.Exams
            //            join et in ctx.ExamTypes on e.ExamTypeID equals et.ExamTypeID
            //            join ac in ctx.AcademicYears on e.AcademicYearId equals ac.AcademicYearId
            //            where e.IsDeleted && e.Id == examId
            //            orderby e.StartDate descending
            //            select new
            //            {
            //                ExamType = et.Type,
            //                ExamDescription = e.Description,
            //                StartDate = e.StartDate,
            //                EndDate = e.EndDate,
            //                AcademicYear = ac.Title,
            //            }).FirstOrDefault();
            //StudentResultMasterDTO master = new DTO.StudentResultMasterDTO()
            //{
            //    ExamType = data.ExamType,
            //    ExamDescription = data.ExamDescription,
            //    StartDate = data.StartDate.ToString("MM/dd/yyyy"),
            //    EndDate = data.EndDate.ToString("MM/dd/yyyy"),
            //    AcademicYear = data.AcademicYear
            //};

            var list = (from st in ctx.Students.Where(x => !x.IsDeleted)
                        join cs in ctx.ClassroomStudents.Where(x => !x.IsDeleted) on st.Id equals cs.StudentId
                        join edm in ctx.ExamClassroomMappings.Where(x => !x.IsDeleted) on cs.ClassroomId equals edm.ClassroomId
                        join ed in ctx.ExamDetailSetups.Where(x => !x.IsDeleted) on edm.ExamDetailSetupId equals ed.Id
                        join e in ctx.Exams.Where(x => !x.IsDeleted) on ed.ExamId equals e.Id
                        join er in ctx.Results.Where(x => !x.IsDeleted) on new { ExamId = e.Id, sId = st.Id } equals new { ExamId = er.ExamId, sId = er.StudentId }
                        join erd in ctx.ResultDetails.Where(x => !x.IsDeleted) on er.Id equals erd.ResultID
                        join c in ctx.Courses.Where(x => !x.IsDeleted) on edm.CourseId equals c.Id
                        join g in ctx.Grades.Where(x => !x.IsDeleted) on er.GradeId equals g.Id
                        join et in ctx.ExamTypes.Where(x => !x.IsDeleted) on e.ExamTypeID equals et.Id
                        join ac in ctx.AcademicYears.Where(x => !x.IsDeleted) on e.AcademicYearId equals ac.Id
                        join cr in ctx.Classrooms.Where(x => !x.IsDeleted) on edm.ClassroomId equals cr.Id
                        join cl in ctx.Classes.Where(x => !x.IsDeleted) on cr.ClassId equals cl.Id
                        join sc in ctx.Sections.Where(x => !x.IsDeleted) on cr.SectionId equals sc.Id

                        where e.Id == examId && st.Id == studentID
                        select new
                        {
                            ExamType = et.Type,
                            ExamDescription = e.Description,
                            StartDate = e.StartDate,
                            EndDate = e.EndDate,
                            AcademicYear = ac.Title,
                            Classroom = cl.Name + " - " + sc.Title,
                            StudentId = st.Id,
                            StudentName = st.FullName,
                            GRNo = st.GRNo,
                            Total = er.TotalMarks ?? 0,
                            Obtained = er.ObtainedTotal ?? 0,
                            Grade = g.Title,
                            Remarks = er.Remarks,
                            Course = c.Title,
                            TheoryTotal = ed.TheoryTotal,
                            LabTotal = ed.LabTotal,
                            OtherTotal = ed.OtherTotal,
                            TheoryObtained = erd.TheoryObtained,
                            LabObtained = erd.LabObtained,
                            OtherObtained = erd.OtherObtained,
                        }).ToList();

            //var list = (from e in ctx.Exams.Where(x => !x.IsDeleted)
            //            join et in ctx.ExamTypes.Where(x => !x.IsDeleted) on e.ExamTypeID equals et.ExamTypeID
            //            join ac in ctx.AcademicYears.Where(x => !x.IsDeleted) on e.AcademicYearId equals ac.AcademicYearId
            //            join ed in ctx.ExamDetailSetups.Where(x => !x.IsDeleted) on e.Id equals ed.ExamId
            //            join edm in ctx.ExamClassroomMappings.Where(x => !x.IsDeleted) on ed.Id equals edm.ExamDetailSetupId
            //            join cr in ctx.Classrooms.Where(x => !x.IsDeleted) on edm.ClassroomId equals cr.ClassroomId
            //            join cl in ctx.Classes.Where(x => !x.IsDeleted) on cr.ClassId equals cl.ClassId
            //            join sc in ctx.Sections.Where(x => !x.IsDeleted) on cr.SectionId equals sc.SectionId
            //            join er in ctx.Results.Where(x => !x.IsDeleted) on e.Id equals er.ExamId
            //            join erd in ctx.ResultDetails.Where(x => !x.IsDeleted) on er.ResultID equals erd.ResultID
            //            join st in ctx.Students.Where(x => !x.IsDeleted) on er.StudentId equals st.Id
            //            join rd in ctx.ResultDetails.Where(x => !x.IsDeleted) on edm.MappingID equals rd.ExamClassroomMappingId
            //            join c in ctx.Courses.Where(x => !x.IsDeleted) on edm.CourseId equals c.Id
            //            join g in ctx.Grades.Where(x => !x.IsDeleted) on er.GradeId equals g.GradeId
            //            where e.Id == examId && st.StudentId == studentID
            //            orderby e.StartDate descending
            //            select new
            //            {
            //                ExamType = et.Type,
            //                ExamDescription = e.Description,
            //                StartDate = e.StartDate,
            //                EndDate = e.EndDate,
            //                AcademicYear = ac.Title,
            //                Classroom = cl.Name + " - " + sc.Title,
            //                StudentId = st.StudentId,
            //                StudentName = st.FullName,
            //                GRNo = st.GRNo,
            //                Total = er.TotalMarks ?? 0,
            //                Obtained = er.ObtainedTotal ?? 0,
            //                Grade = g.Title,
            //                Remarks = er.Remarks,
            //                Course = c.Title,
            //                TheoryTotal = ed.TheoryTotal,
            //                LabTotal = ed.LabTotal,
            //                OtherTotal = ed.OtherTotal,
            //                TheoryObtained = rd.TheoryObtained,
            //                LabObtained = rd.LabObtained,
            //                OtherObtained = rd.OtherObtained,
            //            }).ToList();

            if (list != null && list.Count > 0)
            {
                var first = list.FirstOrDefault();
                StudentResultMasterDTO master = new StudentResultMasterDTO()
                {
                    ExamType = first.ExamType,
                    ExamDescription = first.ExamDescription,
                    StartDate = first.StartDate.ToString("MM/dd/yyyy"),
                    EndDate = first.EndDate.ToString("MM/dd/yyyy"),
                    AcademicYear = first.AcademicYear,
                    StudentName = first.StudentName,
                    TotalMarks = ((first.TheoryTotal ?? 0) + (first.LabTotal ?? 0) + (first.OtherTotal ?? 0)).ToString("##.##"),
                    TotalObtained = ((first.TheoryObtained ?? 0) + (first.LabObtained ?? 0) + (first.OtherObtained ?? 0)).ToString("##.##"),
                    Grade = first.Grade,
                    Remarks = first.Remarks,
                    Percentage = ((first.Obtained / first.Total) * (decimal)100).ToString("##.##")
                };

                List<StudentResultDetailViewDTO> details = new List<StudentResultDetailViewDTO>();
                foreach (var row in list)
                {
                    details.Add(new StudentResultDetailViewDTO()
                    {
                        Course = row.Course,
                        TheoryTotal = row.TheoryTotal,
                        TheoryObtained = row.TheoryObtained ?? 0,
                        LabTotal = row.LabTotal,
                        LabObtained = row.LabObtained ?? 0,
                        OtherTotal = row.OtherTotal,
                        OtherObtained = row.OtherObtained ?? 0,
                        CourseTotal = row.TheoryTotal + row.LabTotal + row.OtherTotal,
                        CourseObtained = (row.TheoryObtained ?? 0) + (row.LabObtained ?? 0) + (row.OtherObtained ?? 0)
                    });
                }
                master.DetailList = details;
                return master;
            }
            return null;
        }

        public List<KeyValueDTO> GetExamStudents(int examId, int classroomID)
        {
            var ctx = this.requestInfo.Context;
            var list = (from er in ctx.Results.Where(x => !x.IsDeleted)
                        join erd in ctx.ResultDetails.Where(x => !x.IsDeleted) on er.Id equals erd.ResultID
                        join edm in ctx.ExamClassroomMappings.Where(x => !x.IsDeleted) on erd.ExamClassroomMappingId equals edm.Id
                        join st in ctx.Students.Where(x => !x.IsDeleted) on er.StudentId equals st.Id
                        where er.ExamId == examId && edm.ClassroomId == classroomID
                        orderby new { st.FullName } descending
                        select new KeyValueDTO
                        {
                            Id = st.Id,
                            Text = st.GRNo + " - " + st.FullName,
                        }).ToList();
            return list;
        }

        public ResultClassroomsDTO GetExamClassrooms(int examId, int userid)
        {
            var ctx = this.requestInfo.Context;
            int teacherID = ctx.Teachers.Where(x => x.UserId == userid).Select(x => x.Id).FirstOrDefault();
            var exam = ctx.Exams.Where(x => x.Id == examId && !x.IsDeleted).FirstOrDefault();
            ResultClassroomsDTO master = new ResultClassroomsDTO()
            {
                ExamId = exam.Id,
                ExamName = exam.Description
            };
            var IDParameter = new SqlParameter("@examId", examId);

            var result = ctx.Database
                .SqlQuery<ResultClassroomDetailDTO>("Exec usp_GetExamClassrooms @examId", IDParameter)
                .ToList();

            // Set classteacher flag
            foreach (var row in result)
            {
                if (row.ClassTeacherId == teacherID)
                    row.IsClassTeacher = true;
            }
            master.Details = result;
            return master;
        }

        public void SaveClassroomResult(int examId, int classroomID, List<StudentResultCommulativeDTO> list)
        {
            var ctx = this.requestInfo.Context;
            var results = ctx.Results.Where(x => x.ExamId == examId).ToList();
            foreach (var row in list)
            {
                var studentResult = results.Where(x => x.StudentId == row.StudentId).FirstOrDefault();
                if (studentResult != null)
                {
                    studentResult.Remarks = row.Remarks;
                    studentResult.LastModifiedBy = this.requestInfo.UserId.ToString();
                    studentResult.LastModifiedOn = DateTime.Now;
                }
            }
            ctx.SaveChanges();
        }

        public void PublishClassroomResult(int examId, int classroomID)
        {
            var ctx = this.requestInfo.Context;
            var query = (from e in ctx.Exams
                         join ed in ctx.ExamDetailSetups on e.Id equals ed.ExamId
                         where e.Id == examId
                         select ed.Id);
            var results = (from edm in ctx.ExamClassroomMappings.Where(x => !x.IsDeleted)
                           where query.ToList().Contains(edm.ExamDetailSetupId)
                                 && edm.ClassroomId == classroomID
                           select edm).ToList();

            foreach (var row in results)
            {
                row.IsPublished = true;
                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// Check all exam classroom results are published and exam is ready to be published
        /// </summary>
        /// <param name="examID"></param>
        /// <returns></returns>
        public bool IsExamPublishable(int examId)
        {
            var ctx = this.requestInfo.Context;
            var unPublishedClassrooms = (from e in ctx.Exams.Where(x => !x.IsDeleted)
                                         join ed in ctx.ExamDetailSetups.Where(x => !x.IsDeleted) on e.Id equals ed.ExamId
                                         join edm in ctx.ExamClassroomMappings.Where(x => !x.IsDeleted) on ed.Id equals edm.ExamDetailSetupId
                                         where e.Id == examId && edm.IsPublished == false
                                         select 1).Any();
            return !unPublishedClassrooms;
        }

        public bool PublishResult(int examId)
        {
            var ctx = this.requestInfo.Context;
            var exam = (from e in ctx.Exams
                        where e.Id == examId
                        select e).FirstOrDefault();

            if (exam != null)
            {
                exam.IsPublished = true;
                ctx.SaveChanges();
                return true;
            }
            return false;
        }

        public List<StudentResultMasterDTO> GetStudentPastResult(int studentID)
        {
            var ctx = this.requestInfo.Context;
            var list = (from st in ctx.Students.Where(x => !x.IsDeleted)
                        join cs in ctx.ClassroomStudents.Where(x => !x.IsDeleted) on st.Id equals cs.StudentId
                        join edm in ctx.ExamClassroomMappings.Where(x => !x.IsDeleted) on cs.ClassroomId equals edm.ClassroomId
                        join ed in ctx.ExamDetailSetups.Where(x => !x.IsDeleted) on edm.ExamDetailSetupId equals ed.Id
                        join e in ctx.Exams.Where(x => !x.IsDeleted) on ed.ExamId equals e.Id
                        join er in ctx.Results.Where(x => !x.IsDeleted) on new { ExamId = e.Id, sId = st.Id } equals new { ExamId = er.ExamId, sId = er.StudentId }
                        join erd in ctx.ResultDetails.Where(x => !x.IsDeleted) on er.Id equals erd.ResultID
                        join g in ctx.Grades.Where(x => !x.IsDeleted) on er.GradeId equals g.Id
                        join et in ctx.ExamTypes.Where(x => !x.IsDeleted) on e.ExamTypeID equals et.Id
                        join ac in ctx.AcademicYears.Where(x => !x.IsDeleted) on e.AcademicYearId equals ac.Id
                        join cr in ctx.Classrooms.Where(x => !x.IsDeleted) on edm.ClassroomId equals cr.Id
                        join cl in ctx.Classes.Where(x => !x.IsDeleted) on cr.ClassId equals cl.Id
                        join sc in ctx.Sections.Where(x => !x.IsDeleted) on cr.SectionId equals sc.Id

                        where st.Id == studentID
                        select new
                        {
                            ExamType = et.Type,
                            ExamDescription = e.Description,
                            StartDate = e.StartDate,
                            EndDate = e.EndDate,
                            AcademicYear = ac.Title,
                            Classroom = cl.Name + " - " + sc.Title,
                            StudentId = st.Id,
                            StudentName = st.FullName,
                            GRNo = st.GRNo,
                            Total = er.TotalMarks ?? 0,
                            Obtained = er.ObtainedTotal ?? 0,
                            Grade = g.Title,
                            Remarks = er.Remarks,
                            TheoryTotal = ed.TheoryTotal,
                            LabTotal = ed.LabTotal,
                            OtherTotal = ed.OtherTotal,
                            TheoryObtained = erd.TheoryObtained,
                            LabObtained = erd.LabObtained,
                            OtherObtained = erd.OtherObtained,
                        }).ToList();

            if (list != null && list.Count > 0)
            {
                List<StudentResultMasterDTO> objList = new List<StudentResultMasterDTO>();
                foreach (var row in list)
                {
                    objList.Add(new StudentResultMasterDTO()
                    {
                        ExamType = row.ExamType,
                        ExamDescription = row.ExamDescription,
                        StartDate = row.StartDate.ToString("MM/dd/yyyy"),
                        EndDate = row.EndDate.ToString("MM/dd/yyyy"),
                        AcademicYear = row.AcademicYear,
                        StudentName = row.StudentName,
                        TotalMarks = ((row.TheoryTotal ?? 0) + (row.LabTotal ?? 0) + (row.OtherTotal ?? 0)).ToString("##.##"),
                        TotalObtained = ((row.TheoryObtained ?? 0) + (row.LabObtained ?? 0) + (row.OtherObtained ?? 0)).ToString("##.##"),
                        Grade = row.Grade,
                        Remarks = row.Remarks,
                        Percentage = ((row.Obtained / row.Total) * (decimal)100).ToString("##.##")
                    });
                }
                return objList;
            }
            return null;
        }
    }
}
